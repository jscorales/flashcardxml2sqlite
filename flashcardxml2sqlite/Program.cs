﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;
using flashcardxml2sqlite.Models;
using System.IO;

namespace flashcardxml2sqlite
{
    public class Program
    {
        private const string XmlFile = @"..\..\MCAT_FLASHCARDS_ALL.xml";
        private static readonly StringBuilder SqlStringBuilder = new StringBuilder();

        private static void Main(string[] args)
        {
            ProcessXml();

            Console.Read();
        }

        private static void ProcessXml()
        {
            XDocument xdoc = XDocument.Load(XmlFile);
            var cards = from card in xdoc.Descendants("card")
                        select new
                        {
                            Id = card.Attribute("id").Value,
                            Topic = card.Element("topic").Value,
                            SubTopic = card.Element("subtopic") != null ? card.Element("subtopic").Value : string.Empty,
                            Question = card.Element("question").FirstNode.ToString(),
                            Answer = card.Element("answer").FirstNode.ToString()
                        };

            foreach (var card in cards)
            {
                var category = CategoryManager.Create(GenerateSlug(card.Topic), card.Topic);
                CardManager.Create(card.Id, category, card.SubTopic, card.Question, card.Answer);
            }

            CreateSqlDump();
        }

        private static void CreateSqlDump()
        {
            //clean categories table
            SqlStringBuilder.Append("DELETE FROM categories;\n");
            //clean flashcards table
            SqlStringBuilder.Append("DELETE FROM flashcards;\n");
            //update sqlite sequence table
            SqlStringBuilder.Append("UPDATE sqlite_sequence SET seq = 0 WHERE name = 'categories';\n");

            //Categories
            var categories = CategoryManager.Categories;
            foreach (var category in categories)
            {
                SqlStringBuilder.AppendFormat("INSERT INTO categories VALUES({0},'{1}','{2}','{3}','{4}');\n", category.Id, category.Uid, category.Title, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff"), DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff"));
            }

            //Cards
            var cards = CardManager.Cards;
            foreach (var card in cards)
            {
                var questionText = CleanupText(card.Question);
                var answerText = CleanupText(card.Answer);

                //Additional, Id, Uid, Category.Id, Question, Answer, SubTopicTitle, CreatedAt, UpdatedAt, Available, Status
                SqlStringBuilder.AppendFormat("INSERT INTO flashcards VALUES({0}, {1},'{2}',{3},NULL,'{4}','{5}','{6}','{7}','{8}',{9},'{10}');\n", card.Additional, card.Id, card.Uid, card.Category.Id, questionText, answerText, card.SubTopicTitle, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff"), DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.ffffff"), card.Available, card.Status);
            }

            Console.WriteLine("Sql Dump: {0}", SqlStringBuilder.ToString());

            var outputDirectory = @"..\..\output";
            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            var outputFile = Path.Combine(outputDirectory, "flashcards_dump.sql");
            if (File.Exists(outputFile))
                File.Delete(outputFile);

            File.AppendAllText(outputFile, SqlStringBuilder.ToString());
        }

        private static string CleanupText(string text)
        {
            var tempText = text;

            if (text.Contains("'"))
                tempText = text.Replace("'", "&#39;");

            return tempText;
        }

        public static string GenerateSlug(string phrase)
        {
            string str = phrase.ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        } 
    }
}
