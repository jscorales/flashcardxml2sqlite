INSERT INTO categories VALUES(1,'gmat-overview','GMAT Overview','2014-06-16 12:25:50.496962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(2,'analytical-writing-assessment','Analytical Writing Assessment','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(3,'integrated-reasoning','Integrated Reasoning','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(4,'verbal-overview','Verbal Overview','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(5,'critical-reasoning','Critical Reasoning','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(6,'reading-comprehension','Reading Comprehension','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(7,'sentence-correction','Sentence Correction','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(8,'quantitative-overview','Quantitative Overview','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(9,'quantitative-strategy','Quantitative Strategy','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(10,'problem-solving','Problem Solving','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(11,'data-sufficiency','Data Sufficiency','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(12,'algebra','Algebra','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(13,'arithmetic','Arithmetic','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(14,'number-properties','Number Properties','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(15,'proportions','Proportions','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(16,'statistics','Statistics','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(17,'math-formulas','Math Formulas','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO categories VALUES(18,'geometry','Geometry','2014-06-16 12:25:50.503962','2014-06-16 12:25:50.503962');
INSERT INTO flashcards VALUES(0, 1,'000',1,NULL,'<div class="prodid-question">
  <p>What are the four Core Competencies that the GMAT rewards?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Critical Thinking</li>
    <li>Pattern Recognition</li>
    <li>Paraphrasing</li>
    <li>Attention to the Right Detail</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 2,'001',1,NULL,'<div class="prodid-question">
  <p>What does Critical Thinking mean for the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>One of the hallmarks of successful test takers is their skill in asking the right
					questions. GMAT critical thinkers first consider what they’re asked to do, then
					study the given information and ask the right questions, especially “How can I
					get the answer in the most direct way?” and “How can I use the question format
					to my advantage?”</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 3,'002',1,NULL,'<div class="prodid-question">
  <p>How does Pattern Recognition pertain to the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>Because the GMAT is a standardized test, the testmaker must use the same kinds of
					questions with the same traps and pitfalls, subject to the same strategic
					solutions. Recognizing these patterns will increase your accuracy and
					efficiency.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 4,'003',1,NULL,'<div class="prodid-question">
  <p>How do you use Paraphrasing on the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>The GMAT rewards test takers who can reduce difficult, abstract, or polysyllabic
					prose to simple terms. In the Quantitative section, this also means translating
					English into math or simplifying complicated equations.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 5,'004',1,NULL,'<div class="prodid-question">
  <p>Why is Attention to the Right Detail important on the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>Attention to the Right Detail will help you discern the essential details from
					those that can slow you down or confuse you.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 6,'005',1,NULL,'<div class="prodid-question">
  <p>How is the GMAT scored?</p>
</div>','<div class="prodid-answer">
  <p>You will receive five scores on the GMAT:</p>
  <ul>
    <li>Analytical Writing Assessment (0–6 in half-point increments)</li>
    <li>Integrated Reasoning (1–8 in whole-point increments)</li>
    <li>Quantitative (0–60 in one-point increments; scores almost always fall
						between 7 and 50)</li>
    <li>Verbal (0–60 in one-point increments; scores almost always fall between 9
						and 44)</li>
    <li>Total Score (200–800 in 10-point increments; this score is generally
						considered the most important for business school admissions)</li>
  </ul>
  <p>For each score you will also receive a percentile rank, indicating the percentage
					of test takers who scored below you.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 7,'006',2,NULL,'<div class="prodid-question">
  <p>What does the Analytical Writing Assessment (AWA) on the GMAT ask of you?</p>
</div>','<div class="prodid-answer">
  <p>The AWA asks you to assess an argument and present its flaws and what would be
					required to strengthen the argument.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 8,'007',2,NULL,'<div class="prodid-question">
  <p>How do business schools use the Analytical Writing Assessment (AWA)?</p>
</div>','<div class="prodid-answer">
  <p>It’s another way for them to assess your readiness for business school. While the
					AWA score doesn’t carry as much weight as the overall 200–800 score does, a
					score below 4 is generally seen as a detriment to an application, while a 6 can
					help make an application more competitive. Schools can also see the text of your
					essay and have been known to compare the writing style against other writing
					samples in a student’s application to make sure that student didn’t get too much
					outside help with the application.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 9,'008',2,NULL,'<div class="prodid-question">
  <p>How is the Analytical Writing Assessment (AWA) connected to other parts of the
					test?</p>
</div>','<div class="prodid-answer">
  <p>The AWA requires you to identify conclusion, evidence, and assumptions, just as
					Critical Reasoning questions do. It also requires you to find flaws in the
					argument and to strengthen it. Your writing should draw on your knowledge of
					keywords from Reading Comprehension and of grammar and style from Sentence
					Correction.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 10,'009',2,NULL,'<div class="prodid-question">
  <p>What is the Kaplan Method for the Analytical Writing Assessment (AWA)?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Take apart the argument. (2 minutes)</li>
    <li>Select the points you will make. (5 minutes)</li>
    <li>Organize, using Kaplan’s essay template. (1 minute)</li>
    <li>Type your essay. (20 minutes)</li>
    <li>Proofread your work. (2 minutes)</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 11,'010',2,NULL,'<div class="prodid-question">
  <p>True or False: There is spellcheck on the Analytical Writing Assessment
					(AWA).</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. There is no spellcheck or grammar check. </p>
  <p>There is, however, a cut/paste function in case you want to move something
					around.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 12,'011',2,NULL,'<div class="prodid-question">
  <p>What are the grading criteria for the Analytical Writing Assessment (AWA)?</p>
</div>','<div class="prodid-answer">
  <p>The graders are looking for how well you</p>
  <ul>
    <li>identify key elements of the argument and examine them;</li>
    <li>arrange your analysis of the argument presented;</li>
    <li>give appropriate examples and reasons for support; and</li>
    <li>master the components of written English.</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 13,'012',2,NULL,'<div class="prodid-question">
  <p>What is the meaning of each score on the AWA scoring scale?</p>
</div>','<div class="prodid-answer">
  <p>Scores are given in half-point increments along the following scale:</p>
  <ul>
    <li>6 – Excellent</li>
    <li>5 – Good</li>
    <li>4 – Satisfactory (Note: A score of 4 or better is considered
						“adequate.”)</li>
    <li>3 – Inadequate</li>
    <li>2 – Substantially flawed</li>
    <li>1 – Seriously deficient</li>
    <li>0 – No score (irrelevant or unintelligible response)</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 14,'013',2,NULL,'<div class="prodid-question">
  <p>What is important to know about the AWA General Instructions?</p>
</div>','<div class="prodid-answer">
  <p>The General Instructions are always the same, so you can learn them before Test
					Day. Then you do not need to spend any of your 30 minutes reading them during
					the test. The only thing that changes is the specific argument you are given as
					a prompt.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 15,'014',2,NULL,'<div class="prodid-question">
  <p>Who grades the Analytical Writing Assessment (AWA)?</p>
</div>','<div class="prodid-answer">
  <p>Each essay is graded by a human grader and a software program. If these two
					graders agree on your score, that is the score you receive. If they differ by 1
					point, you get the average of these two scores. If they differ by 2 or more
					points, a third reader is brought in to grade the essay.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 16,'015',2,NULL,'<div class="prodid-question">
  <p>What is true about every AWA essay prompt?</p>
</div>','<div class="prodid-answer">
  <p>There will be a conclusion supported by evidence, there will be assumptions to be
					found, and those assumptions will be flawed or inadequately supported by the
					evidence given. Finding these components, in this order, should be the beginning
					of your brainstorming.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 17,'016',2,NULL,'<div class="prodid-question">
  <p>Should you give your opinion on the position taken in the argument presented
					in the essay prompt?</p>
</div>','<div class="prodid-answer">
  <p>No. Your opinion about the topic is irrelevant, and you are not expected to know
					anything about the topic. The only opinion you should express is that the
					argument, as presented, is flawed.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 18,'017',2,NULL,'<div class="prodid-question">
  <p>What is the Kaplan template for the AWA essay?</p>
</div>','<div class="prodid-answer">
  <p>¶1: Show that you understand the argument by putting it in your own words
					(summarizing the conclusion and evidence) and explaining that it’s flawed.</p>
  <p>¶2: Point out one flawed assumption in the author’s reasoning; explain why
					it’s questionable.</p>
  <p>¶3: Identify another source of the author’s faulty reasoning; explain why.
					(If you’ve found a third flaw and have enough time, you can write another
					paragraph about it, but this is not necessary.)</p>
  <p>¶4: Describe evidence that would, if it were provided, strengthen the
					argument.</p>
  <p>¶5: Conclude that without such evidence, you’re not persuaded. (If you’re
					running out of time, this can be just a sentence or two added to ¶4.)</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 19,'018',2,NULL,'<div class="prodid-question">
  <p>Why is it important that you allow at least 2 minutes for proofreading?</p>
</div>','<div class="prodid-answer">
  <p>Beyond the fact that you are graded on writing style—so you want to fix any
					spelling or grammatical errors you find—the proofreading step can also act as a
					time buffer. If you’re aiming to be finished with your essay in 30 minutes but
					you go 45 seconds over, you won’t finish your conclusion. If you aim to be done
					in 28 minutes with 2 minutes to proofread but you go 45 seconds over, you’ll
					still be able to complete your essay.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 20,'019',2,NULL,'<div class="prodid-question">
  <p>Why is it important to brainstorm and outline before writing?</p>
</div>','<div class="prodid-answer">
  <p> You simply don’t have enough time <i>not</i> to! The graders care most about
					structure and organization, and there’s not a lot of time to edit what you’ve
					written, so you want to make sure you put everything in the right place the
					first time. Even if you’re not the type of person who outlined essays in
					college, you should practice doing so now. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 21,'020',2,NULL,'<div class="prodid-question">
  <p>What are some writing tips for the Analytical Writing Assessment (AWA)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>Be as concise as possible.</li>
    <li>Do not use “I,” “me,” or “my.”</li>
    <li>Write in the active voice.</li>
    <li>Use the same grammar rules that you study for Sentence Correction to make
						sure your essay is well written.</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 22,'021',3,NULL,'<div class="prodid-question">
  <p>How many questions are in the Integrated Reasoning section?</p>
</div>','<div class="prodid-answer">
  <p>There are 12 questions in the Integrated Reasoning section. Many of these
					questions contain multiple parts.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 23,'022',3,NULL,'<div class="prodid-question">
  <p>What is the length of the Integrated Reasoning section?</p>
  <p>When does it appear on the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>Test takers have 30 minutes to complete the Integrated Reasoning section.</p>
  <p>Integrated Reasoning is the second section of the GMAT. It comes after the
					30-minute Analytical Writing Assessment and before the 75-minute Quantitative
					and Verbal sections.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 24,'023',3,NULL,'<div class="prodid-question">
  <p>How is the Integrated Reasoning section scored?</p>
</div>','<div class="prodid-answer">
  <p>The Integrated Reasoning section is scored on a 1–8 scale, with integer score
					increments. The Integrated Reasoning score has no impact on the overall 200–800
					score.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 25,'024',3,NULL,'<div class="prodid-question">
  <p>What are the four question types in the Integrated Reasoning section?</p>
</div>','<div class="prodid-answer">
  <p>The four question types in the Integrated Reasoning section are the
					following:</p>
  <ul>
    <li>Graphics Interpretation</li>
    <li>Multi-Source Reasoning</li>
    <li>Table Analysis</li>
    <li>Two-Part Analysis</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 26,'025',3,NULL,'<div class="prodid-question">
  <p>What are Graphics Interpretation questions?</p>
</div>','<div class="prodid-answer">
  <p>Graphics Interpretation questions test your ability to interpret and analyze data
					presented visually in graphs and graphical images.</p>
  <p>Each question consists of two fill-in-the-blank statements, to be completed based
					on the information from either one graph or multiple graphs and a short
					paragraph of text.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 27,'026',3,NULL,'<div class="prodid-question">
  <p>What are Multi-Source Reasoning questions?</p>
</div>','<div class="prodid-answer">
  <p>Multi-Source Reasoning questions test your ability to synthesize information from
					multiple sources to answer questions.</p>
  <p>The given information is presented on two or three tabbed pages, shown on the
					left side of the screen. The data on the tabbed pages can be in the form of
					text, charts, or tables and may be presented as a combination of all three.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 28,'027',3,NULL,'<div class="prodid-question">
  <p>What are Table Analysis questions?</p>
</div>','<div class="prodid-answer">
  <p>Table Analysis questions measure your ability to interpret and analyze
					information presented in a sortable table, similar to a spreadsheet.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 29,'028',3,NULL,'<div class="prodid-question">
  <p>What are Two-Part Analysis questions?</p>
</div>','<div class="prodid-answer">
  <p>Two-Part Analysis questions ask you to solve a problem for which the solution has
					two related parts. The information given can take the form of either a
					Quantitative-type word problem or a Verbal-type short paragraph.</p>
  <p>Both parts of the question must be answered correctly for you to receive credit
					for the question.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 30,'029',3,NULL,'<div class="prodid-question">
  <p>What constitutes a question on the Integrated Reasoning section?</p>
  <p>What constitutes a correct answer?</p>
</div>','<div class="prodid-answer">
  <p>A question consists of all the material on one screen at any given time.
					Questions may have multiple parts.</p>
  <p> For a question to be marked as correct, <i>all</i> parts of the question must be
					answered correctly. There is no partial credit. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 31,'030',3,NULL,'<div class="prodid-question">
  <p>What is the “multiple–dichotomous choice” question format?</p>
  <p>Which Integrated Reasoning question types may feature this question format?</p>
</div>','<div class="prodid-answer">
  <p>Multiple–dichotomous choice questions contain three parts, each of which must be
					answered by one of two answer choices (most commonly Yes/No or True/False).</p>
  <p>The multiple–dichotomous choice question format is found in Table Analysis and
					Multi-Source Reasoning questions.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 32,'031',3,NULL,'<div class="prodid-question">
  <p>What is the “drop-down menu” question format?</p>
  <p>Which Integrated Reasoning question type features this question format?</p>
</div>','<div class="prodid-answer">
  <p>Drop-down menu questions have incomplete sentences with blanks; clicking on an
					arrow next to the blank activates a drop-down menu. The menu will contain
					multiple answer choices that can be used to complete the sentence. Test takers
					click on the correct answer to enter it into the sentence.</p>
  <p>The drop-down menu question format is found in Graphics Interpretation
					questions.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 33,'032',3,NULL,'<div class="prodid-question">
  <p>What is the “multiple-choice” question format?</p>
  <p>Which Integrated Reasoning question type may feature this question format?</p>
</div>','<div class="prodid-answer">
  <p>Each multiple-choice question has five answer choices, one of which is
					correct.</p>
  <p>The multiple-choice question format is found in Multi-Source Reasoning questions.
					(The Quantitative and Verbal sections of the GMAT also contain traditional
					multiple-choice questions.)</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 34,'033',3,NULL,'<div class="prodid-question">
  <p>How are Two-Part Analysis questions formatted?</p>
</div>','<div class="prodid-answer">
  <p>Two-Part Analysis items pose two questions, the solutions to which are related in
					some way (e.g., two sequential steps in a process or values for two variables in
					a system of equations).</p>
  <p>The answer choices are presented in table format; any answer choice may be
					selected as the solution to either question. You must choose one answer choice
					for each of the two questions. Only one combination of answer choices is correct
					for both questions.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 35,'034',3,NULL,'<div class="prodid-question">
  <p>What functions are available on the onscreen calculator for the Integrated
					Reasoning section?</p>
</div>','<div class="prodid-answer">
  <p> The onscreen calculator performs basic functions and can be accessed by clicking
					an icon on the screen. An onscreen calculator is available for the Integrated
					Reasoning section <i>only</i>. (Test takers may not bring their own calculators
					into the testing center, and the Quantitative section must be completed without
					a calculator.) </p>
  <p>
    <img src="/assets/contents/GMATcard034.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 36,'035',3,NULL,'<div class="prodid-question">
  <p>When should the onscreen calculator be used?</p>
</div>','<div class="prodid-answer">
  <p>Use the calculator only when it’s necessary to carry out precise calculations
					with difficult numbers.</p>
  <p>Be cautious when you use the onscreen calculator. There is a risk of entering
					information incorrectly, resulting in wrong answers.</p>
  <p>Furthermore, rounding and estimation are often much faster than the
					time-consuming process of entering multiple large numbers into the
					calculator.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 37,'036',3,NULL,'<div class="prodid-question">
  <p>What kind of note taking is most helpful for Multi-Source Reasoning
					questions?</p>
</div>','<div class="prodid-answer">
  <p>Your notes should summarize the main points of the material on each tab, much
					like the Passage Map for Reading Comprehension questions. (See the Verbal
					flashcards for more details on Passage Mapping.)</p>
  <p>Since the given information is spread across multiple tabs that cannot be viewed
					all at once, taking notes is extremely useful on Multi-Source Reasoning
					questions.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 38,'037',3,NULL,'<div class="prodid-question">
  <p>What are the best ways to analyze the information given on multiple tabbed
					pages?</p>
</div>','<div class="prodid-answer">
  <p>After taking notes that summarize what is on each tabbed page, isolate the
					information relevant to each individual question asked.</p>
  <p>Quick simplification of numerical data is often sufficient to answer Yes/No
					multiple–dichotomous choice questions.</p>
  <p> For True/False multiple–dichotomous choice questions, a “True” answer means the
					statement <b>must be true</b> or <b>must be inferable</b> based on the
					information on the tabbed pages. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 39,'038',3,NULL,'<div class="prodid-question">
  <p>How does the sortable table work in Table Analysis questions?</p>
  <p>When should tables be sorted?</p>
</div>','<div class="prodid-answer">
  <p>Above each table, there is a drop-down menu following the words “Sort By.” This
					drop-down menu gives you the choice to sort by each column header. Sorting the
					table by a given column will arrange that column’s entries in ascending
					order.</p>
  <p>Tables should be sorted to answer questions for which sorting is useful, such as
					questions that ask about the median, range, or highest and lowest values.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 40,'039',3,NULL,'<div class="prodid-question">
  <p>What is a “supported inference”?</p>
  <p>What question formats may ask you to identify a supported inference?</p>
</div>','<div class="prodid-answer">
  <p> Many questions will ask for a supported inference, using words like
					<i>infer</i>, <i>imply</i>, and <i>suggest</i> in reference to conclusions that
					can be drawn from the information directly provided. To be a supported
					inference, an answer choice <b>must be true </b>based on the information given.
					This is the same definition of <i>inference</i> that you will use for Reading
					Comprehension and Critical Reasoning questions on the Verbal section. </p>
  <p>Multiple–dichotomous choice and multiple-choice question formats often ask for
					you to determine whether the given information supports a particular
					inference.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 41,'040',3,NULL,'<div class="prodid-question">
  <p>Define mean, median, mode, range, and standard deviation.</p>
</div>','<div class="prodid-answer">
  <p> The <b>mean</b> is the arithmetic average of a set of numbers. </p>
  <p> The <b>median</b> is the middle value when numbers in a set are arranged in
					ascending or descending order. In a set with an even number of values, the
					median is the mean of the two middle numbers. </p>
  <p> The <b>mode</b> of a set is the value that appears the greatest number of times.
					If two or more values tie for the greatest number of occurrences, such a set has
					multiple modes. </p>
  <p> The <b>range</b> is the positive difference between the smallest and largest
					numbers in a set. </p>
  <p>
    <b>Standard deviation</b> measures the dispersion of a set of numbers around the
					mean. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 42,'041',3,NULL,'<div class="prodid-question">
  <p>What are the defining characteristics of the four quadrants of a coordinate
					plane?</p>
  <p>
    <img src="/assets/contents/GMATcard041.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>The four quadrants of a coordinate plane are defined by their positions relative
					to the <i>x</i>- (horizontal) and <i>y</i>- (vertical) axes. Points to the right
					of the <i>y</i>-axis have positive <i>x</i>-coordinates, and those to the left
					of the <i>y</i>-axis have negative <i>x</i>-coordinates. Points above the
						<i>x</i>-axis have positive <i>y</i>-coordinates, and those below the
						<i>x</i>-axis have negative <i>y</i>-coordinates.</p>
  <p>In (<i>x</i>, <i>y</i>) notation, the points within each quadrant have the
					following signs:</p>
  <p>I. (+, +)</p>
  <p>II. (−, +)</p>
  <p>III. (−, −)</p>
  <p>IV. (+, −)</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 43,'042',3,NULL,'<div class="prodid-question">
  <p>What is a scatter plot?</p>
</div>','<div class="prodid-answer">
  <p>A scatter plot shows individual data points plotted on a coordinate plane.</p>
  <p>The scatter plot below shows quiz scores achieved by 30 different students
					relative to their hours of study.</p>
  <p>
    <img src="/assets/contents/GMATcard042.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 44,'043',3,NULL,'<div class="prodid-question">
  <p>What is a regression line?</p>
</div>','<div class="prodid-answer">
  <p>A regression line “averages” the points on a scatter plot in order to show
					general trends. A regression line isn’t necessarily a straight line. The
					regression line below shows a positive linear correlation between the two
					variables indicated by the axes.</p>
  <p>
    <img src="/assets/contents/GMATcard043.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 45,'044',3,NULL,'<div class="prodid-question">
  <p>What is a pie chart?</p>
</div>','<div class="prodid-answer">
  <p>A pie chart shows parts (slices) of a whole (the entire pie). Values for
					individual slices, if they are given, can be stated as percents or actual
					amounts. Regardless, the individual slices will add up to the entire whole, or
					100%.</p>
  <p>
    <img src="/assets/contents/GMATcard044.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 46,'045',3,NULL,'<div class="prodid-question">
  <p>What is a bar graph?</p>
</div>','<div class="prodid-answer">
  <p>A bar graph (or bar chart) uses horizontal or vertical rectangular bars to show
					comparisons among categories. A simple bar chart shows one observation for each
					bar:</p>
  <p>
    <img src="/assets/contents/GMATcard045_01.png" />
  </p>
  <p>A stacked bar chart can show multiple observations for each bar, with colors or
					shading to differentiate the separate data:</p>
  <p>
    <img src="/assets/contents/GMATcard045_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 47,'046',3,NULL,'<div class="prodid-question">
  <p>What is a line graph?</p>
</div>','<div class="prodid-answer">
  <p>A line graph shows data that are connected in some way, usually sequentially over
					time. A line graph can have one or multiple lines, with each line representing a
					specific type of observed data.</p>
  <p>The line graph below shows actual revenue and inflation-adjusted revenue, by
					year.</p>
  <p>
    <img src="/assets/contents/GMATcard046.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 48,'047',3,NULL,'<div class="prodid-question">
  <p>What is a histogram?</p>
</div>','<div class="prodid-answer">
  <p>Histograms, which are usually presented in bar chart form, show density of data.
					Each bar represents a range of data, and the height of each bar shows the number
					of occurrences within the indicated range.</p>
  <p>The histogram below shows the number of people at a party within each of several
					age ranges.</p>
  <p>
    <img src="/assets/contents/GMATcard047.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 49,'048',3,NULL,'<div class="prodid-question">
  <p>What is a pictograph?</p>
</div>','<div class="prodid-answer">
  <p>A pictograph uses images or symbols to represent large numbers of a type of
					data.</p>
  <p>The pictograph below shows the number of bananas purchased by month at a certain
					store, with each picture of a whole banana representing 1,000 bananas.</p>
  <p>
    <img src="/assets/contents/GMATcard048.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(0, 50,'049',3,NULL,'<div class="prodid-question">
  <p>What is a process diagram or flow chart?</p>
</div>','<div class="prodid-answer">
  <p>A process diagram or flow chart provides a visual representation of steps in a
					process.</p>
  <p>The flow chart below shows the procedure used to process an order. The columns
					indicate that different departments are responsible for various steps.</p>
  <p>
    <img src="/assets/contents/GMATcard049.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',1,'skipped');
INSERT INTO flashcards VALUES(1, 51,'050',4,NULL,'<div class="prodid-question">
  <p>What is the approximate mix of question types in the Verbal section?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard050.png" />
  </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 52,'051',4,NULL,'<div class="prodid-question">
  <p>What is the average amount of time that you should spend on each of the Verbal
					questions, by question type?</p>
</div>','<div class="prodid-answer">
  <p>Sentence Correction questions should average 1 minute per question.</p>
  <p>Critical Reasoning questions should average 2 minutes per question.</p>
  <p>Reading Comprehension should average about 4 minutes per passage plus a little
					less than 1.5 minutes per question.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 53,'052',5,NULL,'<div class="prodid-question">
  <p>What is the Kaplan Method for Critical Reasoning?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Identify the question type.</li>
    <li>Untangle the stimulus.</li>
    <li>Predict the answer.</li>
    <li>Evaluate the choices.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 54,'053',5,NULL,'<div class="prodid-question">
  <p>What are the common types of Critical Reasoning questions?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>Assumption</li>
    <li>Strengthen</li>
    <li>Weaken</li>
    <li>Evaluation</li>
    <li>Flaw</li>
    <li>Inference</li>
    <li>Explain</li>
    <li>Bolded Statement</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 55,'054',5,NULL,'<div class="prodid-question">
  <p>What is the approximate distribution of Critical Reasoning question types on
					the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>11% Assumption</p>
  <p>19% Strengthen</p>
  <p>37% Weaken</p>
  <p>3% Flaw</p>
  <p>15% Inference</p>
  <p>8% Explain</p>
  <p>6% Other</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 56,'055',5,NULL,'<div class="prodid-question">
  <p>Which Critical Reasoning question types make up the “assumption family”?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>Assumption</li>
    <li>Strengthen</li>
    <li>Weaken</li>
    <li>Evaluation</li>
    <li>Flaw</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 57,'056',5,NULL,'<div class="prodid-question">
  <p>What must you do to untangle the stimulus for questions in the Assumption
					family?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Identify the conclusion.</li>
    <li>Identify the evidence.</li>
    <li>Figure out the central assumption.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 58,'057',5,NULL,'<div class="prodid-question">
  <p>What is a conclusion?</p>
</div>','<div class="prodid-answer">
  <p> The conclusion is the <i>point of the author’s argument</i>. </p>
  <p>The conclusion is a statement of opinion.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 59,'058',5,NULL,'<div class="prodid-question">
  <p>What is evidence?</p>
</div>','<div class="prodid-answer">
  <p> Evidence is what is provided to <i>support the conclusion</i>. </p>
  <p>All evidence given on the GMAT is to be taken as true, even if it seems to be
					conjecture.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 60,'059',5,NULL,'<div class="prodid-question">
  <p>What is an assumption?</p>
</div>','<div class="prodid-answer">
  <p> An assumption is <i>something unwritten that must be true for the conclusion to
						be true</i>. The most important part of this definition is that it <i>must
						be true</i>. If the assumption is shown to be false, then the entire
					argument falls apart. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 61,'060',5,NULL,'<div class="prodid-question">
  <p>How do you recognize an Assumption question?</p>
</div>','<div class="prodid-answer">
  <p> An Assumption question will very often have the term <i>assumption</i> or
						<i>presupposition</i> in it, but even if it doesn’t, it will ask what is
						<i>needed</i> for the argument to hold. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 62,'061',5,NULL,'<div class="prodid-question">
  <p>What is the first thing you should look for when trying to identify the
					assumption?</p>
</div>','<div class="prodid-answer">
  <p> If a term or concept is mentioned in the conclusion but does not show up in the
					evidence, that means it is unsupported. In this case, the assumption, which
						<i>must be true</i> for the argument to hold<i>,</i> will build a bridge
					from the evidence to the conclusion in order to support the conclusion. Looking
					for mismatched terms between the evidence and the conclusion should be your
					first step. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 63,'062',5,NULL,'<div class="prodid-question">
  <p>How do you recognize a Strengthen question?</p>
</div>','<div class="prodid-answer">
  <p> A Strengthen question will ask you for a fact that makes the argument in the
					stimulus <i>more likely to be true</i>. The correct answer will likely do this
					by confirming or supporting the central assumption. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 64,'063',5,NULL,'<div class="prodid-question">
  <p>How do you recognize a Weaken question?</p>
</div>','<div class="prodid-answer">
  <p> A Weaken question will ask you for a fact that makes the argument in the
					stimulus <i>less likely to be true</i>. The correct answer will likely do this
					by denying the central assumption. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 65,'064',5,NULL,'<div class="prodid-question">
  <p>How do you recognize a Flaw question?</p>
</div>','<div class="prodid-answer">
  <p> A Flaw question will ask you how the argument is flawed, or, more specifically,
					why the argument is vulnerable to criticism. Usually, the flaw in the argument
					has to do with an <i>unwarranted assumption.</i></p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 66,'065',5,NULL,'<div class="prodid-question">
  <p>How do you recognize an Inference question?</p>
</div>','<div class="prodid-answer">
  <p> An Inference question will ask you what <i>must be true based on the information
						in the stimulus.</i></p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 67,'066',5,NULL,'<div class="prodid-question">
  <p>How do you recognize an Explain question?</p>
</div>','<div class="prodid-answer">
  <p>An Explain question will present you with a paradox or with seemingly
					contradictory statements, and ask you to explain how to resolve the paradox or
					how the statements can coexist.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 68,'067',5,NULL,'<div class="prodid-question">
  <p>How do you recognize a Bolded Statement question?</p>
</div>','<div class="prodid-answer">
  <p>Besides the bolded text in the stimulus, Bolded Statement questions can be
					recognized because they ask about the function of different parts of the
					stimulus. Correct answers will characterize the function of the bolded sentences
					rather than discuss the details of the stimulus.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 69,'068',5,NULL,'<div class="prodid-question">
  <p> What two types of questions can the word <i>support</i> in the question stem
					indicate? How can you tell the difference? </p>
</div>','<div class="prodid-answer">
  <p>
    <i>Support</i> can be used in a Strengthen question or an Inference question. In
					a Strengthen question, the correct answer choice will support the assumption,
					and thus bolster the argument. In an Inference question, the stimulus will
					support the correct answer choice. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 70,'069',5,NULL,'<div class="prodid-question">
  <p>If the argument depends on causation, what is the most likely assumption?</p>
</div>','<div class="prodid-answer">
  <p>“There is no other possible cause.” This applies whether the causation is in the
					evidence or the conclusion.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 71,'070',5,NULL,'<div class="prodid-question">
  <p>What are the three most common assumption patterns in the Assumption family of
					questions?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Causation</li>
    <li>Representativeness</li>
    <li>Plans, proposals, and predictions</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 72,'071',5,NULL,'<div class="prodid-question">
  <p>What are the alternative possibilities when causation is assumed?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Reverse causation, in which instead of <i>X</i> causing <i>Y</i>, <i>Y</i>
						causes <i>X</i></li>
    <li>Correlation, in which both <i>X</i> and <i>Y</i> are caused by <i>Z</i></li>
    <li>Coincidence, in which <i>X</i> and <i>Y</i> happened to occur at the same
						time but are unrelated</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 73,'072',5,NULL,'<div class="prodid-question">
  <p>What is the representativeness pattern?</p>
</div>','<div class="prodid-answer">
  <p>The representativeness pattern occurs when all of the evidence is about an
					individual or a small group, and the conclusion is about a larger group of which
					the small group is a part. The pattern can appear in the reverse direction as
					well, but this is less common.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 74,'073',5,NULL,'<div class="prodid-question">
  <p>What keywords in a stimulus indicate that representativeness may be part of an
					argument’s central assumption?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Survey</i>, <i>study</i>, <i>poll</i>, <i>experiment</i></p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 75,'074',5,NULL,'<div class="prodid-question">
  <p>If the argument depends on representativeness, what is the most common
					assumption?</p>
</div>','<div class="prodid-answer">
  <p>It is that the group used as evidence is indeed representative of the group to
					which the conclusion applies.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 76,'075',5,NULL,'<div class="prodid-question">
  <p>What pattern emerges with conclusions that consist of plans, proposals, or
					predictions?</p>
</div>','<div class="prodid-answer">
  <p>Whenever there is a plan, proposal, or prediction in the conclusion, there is
					usually a temporal shift from the evidence to the conclusion—either the evidence
					is based on the past and the conclusion relates to the present, or the evidence
					is based on the present and the conclusion relates to the future.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 77,'076',5,NULL,'<div class="prodid-question">
  <p>If the conclusion is a plan, proposal, or prediction, what are the most common
					assumptions?</p>
</div>','<div class="prodid-answer">
  <p>Whenever there is a temporal shift, the assumption is that conditions will remain
					(or have remained) the same. Also, the author assumes that there is not some
					outside confounding factor that would cause the plan, proposal, or prediction to
					fail.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 78,'077',5,NULL,'<div class="prodid-question">
  <p>What is the Why question that is used to answer Explain questions?</p>
</div>','<div class="prodid-answer">
  <p>“Why, even though [Thing One], is it true that [Thing Two]?” The answer choice
					that answers this question convincingly will be the correct one.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 79,'078',5,NULL,'<div class="prodid-question">
  <p>What are the steps for answering Explain questions?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Find the two seemingly contradictory facts.</li>
    <li>Plug them into the Why question (“Why, even though [Thing One], is it true
						that [Thing Two]?”).</li>
    <li>Find the answer to the Why question among the answer choices.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 80,'079',5,NULL,'<div class="prodid-question">
  <p>How do you approach Inference questions?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Summarize and catalog the information in the stimulus.</li>
    <li>Go through each answer choice, asking, “Based on the information in the
						stimulus, is this <i>completely supported</i>, and does it <i>have to be
							true</i>?” </li>
    <li>Find the answer choice about which you can answer “Yes” to both
						questions.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 81,'080',5,NULL,'<div class="prodid-question">
  <p>What should you watch out for in the answer choices for Inference questions?</p>
</div>','<div class="prodid-answer">
  <p> Statements that are <i>out of scope</i> or <i>extreme</i>. Because the correct
					answer must be true and will be completely supported, no additional information
					can be necessary to support the correct answer. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 82,'081',5,NULL,'<div class="prodid-question">
  <p>How should you approach Bolded Statement questions?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Determine whether the two bolded statements are going in the same or
						opposite directions and eliminate answer choices that say otherwise.</li>
    <li>Determine the purpose of the first statement and eliminate answer choices
						that contradict it.</li>
    <li>Determine which of the remaining answer choices matches the purpose of the
						second statement.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 83,'082',5,NULL,'<div class="prodid-question">
  <p>What is an Evaluation question?</p>
</div>','<div class="prodid-answer">
  <p>An Evaluation question is a rare Argument question type that asks you to identify
					information that would help you assess an argument’s strength. The correct
					answer won’t strengthen or weaken the author’s reasoning or supply a missing
					assumption. Instead, the right answer will specify the kind of evidence that
					would help you judge the validity of an argument.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 84,'083',5,NULL,'<div class="prodid-question">
  <p>What are the most common wrong-answer traps on Critical Reasoning questions?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>
      <b>180:</b> The answer choice uses terms you may be looking for, but does so
						in the opposite of the way needed. </li>
    <li>
      <b>Faulty Use of Detail:</b> Something mentioned in the stimulus is used in
						an incorrect manner. </li>
    <li>
      <b>Extreme:</b> The answer choice goes beyond what is needed or what is
						acceptable. </li>
    <li>
      <b>Distortion:</b> The answer choice takes facts from the stimulus out of
						context. </li>
    <li>
      <b>Out of Scope:</b> The answer choice goes beyond what is mentioned in the
						stimulus. </li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 85,'084',5,NULL,'<div class="prodid-question">
  <p>What are some keywords that indicate the conclusion is to follow?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Thus</i>, <i>Therefore</i>, <i>In conclusion</i>, <i>So</i>, <i>Hence</i>,
						<i>Clearly</i></p>
  <p>Looking for keywords is the most efficient first step to unravelling the
					stimulus. Remember that the conclusion will always be the opinion supported by
					the given evidence.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 86,'085',5,NULL,'<div class="prodid-question">
  <p>What two keywords, when they appear, will generally precede the most important
					piece of evidence?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Since</i> and <i>Because</i></p>
  <p>Whatever follows one of these words tends to be the piece of evidence that
					directly leads to the conclusion. In addition, the conclusion will very often be
					in the same sentence as this piece of evidence, separated by a comma.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 87,'086',5,NULL,'<div class="prodid-question">
  <p>What are the two most common flaws on GMAT Flaw questions?</p>
</div>','<div class="prodid-answer">
  <p>The two most common flaws are mistaking correlation for causation and confusing
					actual value with percentages.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 88,'087',5,NULL,'<div class="prodid-question">
  <p>
    <b>What is the flaw in the following argument?</b>
  </p>
  <p>In 1995, 20% of all beach visitors purchased an ice-cream cone. In 2005, only 15%
					of all beach visitors purchased an ice-cream cone. Therefore, fewer ice-cream
					cones were purchased at the beach in 2005 than in 1995.</p>
</div>','<div class="prodid-answer">
  <p>This argument confuses actual values with percentages. It’s possible that twice
					as many people went to the beach in 2005 as did in 1995, so 15% of the 2005
					number would be much greater than 20% of the smaller, 1995 number.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 89,'088',5,NULL,'<div class="prodid-question">
  <p>What are some words that might indicate that an answer choice is too extreme?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>All</i>, <i>Always</i>, <i>Definitely</i>, <i>Must</i>, <i>Never</i>,
						<i>No</i>, <i>None</i>, <i>Only</i>, <i>Will</i></p>
  <p>Any word that doesn’t allow for a single counterexample should be a red flag as
					you read the answer choices. It doesn’t mean the choice is definitely wrong,
					just that you need to watch out.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 90,'089',6,NULL,'<div class="prodid-question">
  <p>What is the Kaplan Method for Reading Comprehension?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Read the passage strategically.</li>
    <li>Analyze the question stem.</li>
    <li>Research the relevant text.</li>
    <li>Make a prediction.</li>
    <li>Evaluate the answer choices.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 91,'090',6,NULL,'<div class="prodid-question">
  <p>True or False: All GMAT Reading Comprehension passages will be between four and
					five paragraphs long.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. GMAT Reading Comprehension passages can be between one and five
					paragraphs; usually, test takers can expect two passages to be short and two to
					be long. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 92,'091',6,NULL,'<div class="prodid-question">
  <p>What is the approximate breakdown of Reading Comprehension passage topics?</p>
</div>','<div class="prodid-answer">
  <p>30% Social Sciences</p>
  <p>25% Biological Sciences</p>
  <p>25% Business</p>
  <p>10% Physical Sciences</p>
  <p>10% Other (Humanities, Law, etc.)</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 93,'092',6,NULL,'<div class="prodid-question">
  <p>How do the Reading Comprehension passages and questions appear on the screen?</p>
</div>','<div class="prodid-answer">
  <p>The passage is on the left side of the screen with a scroll bar between it and
					the question on the right side of the screen. The passage remains on the screen
					for the entire question set. Questions appear one at a time.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 94,'093',6,NULL,'<div class="prodid-question">
  <p>When reading strategically, what three sets of questions should you ask
					yourself?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>What is the author likely to discuss next? What will the author’s opinion
						likely be? (Ask such predictive questions throughout the passage to keep you
						engaged and focused on what the author considers important. Make predictions
						by identifying structural and emphasis keywords in the passage.)</li>
    <li>What is the main idea and function of each paragraph? (Ask this question at
						the end of each paragraph and summarize the answer in your Passage
						Map.)</li>
    <li>What are the Topic, Scope, and Purpose of the passage as a whole? (Answer
						these questions in your Passage Map once you have finished reading the
						passage.)</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 95,'094',6,NULL,'<div class="prodid-question">
  <p>What are the Topic and Scope of a Reading Comprehension passage?</p>
</div>','<div class="prodid-answer">
  <p> The Topic is <i>the broad subject of the passage</i>—for example, the California
					condor. </p>
  <p> The Scope is <i>the narrow focus within that subject</i>—for example, efforts to
					protect the endangered California condor population. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 96,'095',6,NULL,'<div class="prodid-question">
  <p>What is the Purpose of a Reading Comprehension passage?</p>
</div>','<div class="prodid-answer">
  <p> The Purpose is <i>why the author is writing the passage</i>. Your description of
					the author’s Purpose should always begin with a verb. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 97,'096',6,NULL,'<div class="prodid-question">
  <p>What verbs can help you identify the author’s Purpose?</p>
</div>','<div class="prodid-answer">
  <p>The author of a GMAT passage will likely be writing the passage for one of six
					reasons. You can remember these as the ARCADE verbs:</p>
  <ul>
    <li>Advocate</li>
    <li>Rebut</li>
    <li>Compare</li>
    <li>Analyze</li>
    <li>Describe</li>
    <li>Explain</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 98,'097',6,NULL,'<div class="prodid-question">
  <p>When should you determine the Topic, Scope, and Purpose of a passage?</p>
</div>','<div class="prodid-answer">
  <p>The Topic and Scope are usually clear by the end of the first paragraph.
					Determine the Purpose after you’ve finished the passage, but before you answer
					the first question.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 99,'098',6,NULL,'<div class="prodid-question">
  <p>What is a Passage Map, and why should you make one when you read each Reading
					Comprehension passage?</p>
</div>','<div class="prodid-answer">
  <p> A Passage Map is <i>a guide to the organization of the passage.</i> Making a
					Passage Map focuses your attention on the <i>structure of the passage</i>, the
						<i>author’s point of view</i>, and the <i>main idea of each paragraph</i>.
					Doing so saves you time and helps you answer the questions correctly. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 100,'099',6,NULL,'<div class="prodid-question">
  <p>What is the most useful format for a Passage Map?</p>
</div>','<div class="prodid-answer">
  <p> First, make a <i>brief summary</i> of each paragraph and its function in the
					passage’s structure. Write these short notes after you read each paragraph. </p>
  <p> Then, identify the broad <i>Topic</i> of the passage, the <i>Scope</i> (the
					specific part of the Topic that the author focuses on) of the passage, and the
					author’s <i>Purpose</i> (identified by an active verb) for writing the passage.
				</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 101,'100',6,NULL,'<div class="prodid-question">
  <p>What is the biggest trap that test takers fall into on Reading Comprehension?</p>
</div>','<div class="prodid-answer">
  <p> Reading for details is the biggest trap. You should <i>not</i> read these
					passages the way you read for work, school, or pleasure. Instead, <i>focus on
						the main ideas</i> as you create your Passage Map. You can always research
					details in the passage if a question requires. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 102,'101',6,NULL,'<div class="prodid-question">
  <p> If not details, then what <i>should</i> you focus on when reading passages on
					the GMAT? </p>
</div>','<div class="prodid-answer">
  <p> You should read for <i>structure</i>. An understanding of the organization of
					the passage, along with recognizing the author’s point of view, will be the most
					helpful when answering the questions. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 103,'102',6,NULL,'<div class="prodid-question">
  <p>What are the four most common Reading Comprehension question types?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>Global</li>
    <li>Detail</li>
    <li>Inference</li>
    <li>Logic</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 104,'103',6,NULL,'<div class="prodid-question">
  <p>What do Global questions ask about, and how can their answers be found?</p>
</div>','<div class="prodid-answer">
  <p> Global questions ask about <i>the passage as a whole.</i> Their answers can be
					found <i>in your Topic, Scope, and Purpose summaries.</i> If you successfully
					completed Step 1 of the Kaplan Method for Reading Comprehension, you will
					already have the answers to most Global questions. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 105,'104',6,NULL,'<div class="prodid-question">
  <p>How can you quickly eliminate wrong answer choices to Global questions?</p>
</div>','<div class="prodid-answer">
  <p> You can quickly eliminate incorrect answers to Global questions by <i>reading
						the first few words of each answer choice</i> and eliminating answer choices
					that contain verbs inconsistent with the author’s Purpose. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 106,'105',6,NULL,'<div class="prodid-question">
  <p>What do Detail questions ask about, and how can their answers be found?</p>
</div>','<div class="prodid-answer">
  <p> Detail questions ask about <i>specific parts of the passage.</i> They will use
					categorical language such as “the author makes which of the following
					statements” or “according to the passage.” Their answers can be found by
						<i>researching the passage,</i> and the Passage Map you wrote in Step 1 of
					the Kaplan Method will focus your <i>search for the detail.</i></p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 107,'106',6,NULL,'<div class="prodid-question">
  <p>How should you deal with the two common Detail question variants, EXCEPT
					questions and Roman numeral questions?</p>
</div>','<div class="prodid-answer">
  <p>For Detail EXCEPT questions, search for each answer choice in the text and
					eliminate that choice when you find it.</p>
  <p>For Roman numeral questions, try to find each statement in the text; then
					eliminate answers based on whether or not you’ve found it.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 108,'107',6,NULL,'<div class="prodid-question">
  <p>What is the biggest mistake a test taker can make on Detail questions?</p>
</div>','<div class="prodid-answer">
  <p>Relying on memory is a big mistake. Wrong answer choices are written to look
					tempting to people who think they remember what the passage says. The answer
					will be directly in the text—do the research!</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 109,'108',6,NULL,'<div class="prodid-question">
  <p>What do Inference questions ask about, and how can their answers be found?</p>
</div>','<div class="prodid-answer">
  <p> Inference questions are about things that <i>must be true</i> based on the
					passage, but that <i>aren’t explicitly stated</i>. They will use language such
					as <i>likely</i>, <i>probably</i>, <i>implies</i>, or <i>suggests</i> to let you
					know the answer is not directly stated. Their answers can be found by
						<i>checking the answer choices one at a time against your Scope, Purpose,
						Passage Map, or the passage itself.</i></p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 110,'109',6,NULL,'<div class="prodid-question">
  <p>What constitutes a correct answer to a Reading Comprehension Inference
					question?</p>
</div>','<div class="prodid-answer">
  <p> The correct answer to an Inference question is a statement that <i>must be
						true</i> based on the information given in the passage. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 111,'110',6,NULL,'<div class="prodid-question">
  <p>What are the most common wrong-answer traps on Inference questions?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>
      <b>Extreme:</b> These choices are too strong to be supported by the passage.
						Beware of words such as <i>always</i> and <i>never</i>; they may indicate
						this trap. </li>
    <li>
      <b>Out of Scope:</b> Check answer choices against the Scope you wrote down
						in Step 1 of the Kaplan Method. A choice inconsistent with the Scope will
						not be the correct answer to an Inference question. </li>
    <li>
      <b>180:</b> Some choices can be tempting because they have all the right
						details, but they are connected in a way opposite to that of the passage. </li>
    <li>
      <b>Distortion:</b> These “trap” answer choices take a detail out of context
						to make a choice that sounds good but is incorrect. These choices prey on
						people who rely on their memory, so remember to do your research when
						answering! </li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 112,'111',6,NULL,'<div class="prodid-question">
  <p>What do Logic questions ask about, and how can their answers be found?</p>
</div>','<div class="prodid-answer">
  <p> Logic questions ask about the <i>author’s reason for including ideas and
						details.</i> Their answers can be identified by using <i>your Passage
						Map</i> and <i>keywords from the text</i> to identify the context of a
					specific detail or section and its relationship to the rest of the passage. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 113,'112',6,NULL,'<div class="prodid-question">
  <p>What are some of the less common Reading Comprehension question types?</p>
</div>','<div class="prodid-answer">
  <p>Parallelism questions ask you to take the ideas in a passage and apply them
					through analogy to a new situation.</p>
  <p>Application questions ask you to identify an example or application of something
					described in the passage.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 114,'113',6,NULL,'<div class="prodid-question">
  <p>What other types of questions can appear on Reading Comprehension passages, and
					how should you attack them?</p>
</div>','<div class="prodid-answer">
  <p>Anything that appears as a Critical Reasoning question can theoretically appear
					as a Reading Comprehension question. These should be attacked as if they were
					Critical Reasoning questions, using the passage or part of the passage as the
					stimulus.</p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 115,'114',6,NULL,'<div class="prodid-question">
  <p>True or False: You should look at the question first to save time when reading
					the passage.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False!</i> This will only lead to careless reading. Focus on structure and
					points of view. Don’t let yourself get bogged down with details, and the passage
					will go fast enough (and the questions will go even faster). </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 116,'115',7,NULL,'<div class="prodid-question">
  <p>What are the three categories of errors tested by GMAT Sentence Correction
					questions?</p>
</div>','<div class="prodid-answer">
  <p>GMAT Sentence Correction questions test the following concepts:</p>
  <ul>
    <li>Grammar</li>
    <li>Style</li>
    <li>Idioms</li>
  </ul>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 117,'116',7,NULL,'<div class="prodid-question">
  <p>On Sentence Correction questions, what does answer choice (A) always
					represent?</p>
</div>','<div class="prodid-answer">
  <p> On Sentence Correction, answer choice (A) is always the underlined portion of
					the sentence <i>as originally written in the original question</i>. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 118,'117',7,NULL,'<div class="prodid-question">
  <p>True or False: Although it’s helpful to spot an error on the first read-through
					of a Sentence Correction question, it’s not a big deal if you don’t see one.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True</i>. Don’t spend more than a few seconds looking over the question once
					you’re done reading it; if the error doesn’t jump out at you, you can zero in on
					it using the Kaplan Method to analyze the answer choices. Plus, sometimes there
						<i>is no</i> error—and on Test Day, the last thing you want to do is waste
					time searching for a mistake that isn’t there. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 119,'118',7,NULL,'<div class="prodid-question">
  <p>How can you use the answer choices to identify the grammar at issue and narrow
					the number of options?</p>
</div>','<div class="prodid-answer">
  <p> On Sentence Correction questions, it is helpful to <i>split</i> the answer
					choices into <i>groups</i> to identify the grammar at issue and narrow the
					number of options. If you notice that half of the choices have <i>it</i> and
					half have <i>their,</i> you will know pronouns are at issue and you’ll able to
					eliminate several answer choices at once. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 120,'119',7,NULL,'<div class="prodid-question">
  <p>What are the two most important verb rules in GMAT Sentence Correction?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>The verb must agree with the subject of the sentence; plural subjects take
						plural verbs, and singular subjects take singular verbs.</li>
    <li>The verb tense must match the meaning of the sentence as a whole.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 121,'120',7,NULL,'<div class="prodid-question">
  <p>What are the two most important pronoun rules in GMAT Sentence Correction?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>The pronoun must refer unambiguously to a single noun elsewhere in the
						sentence.</li>
    <li>The pronoun must agree in number with the noun it replaces.</li>
  </ol>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 122,'121',7,NULL,'<div class="prodid-question">
  <p> On the GMAT, the word <i>which</i> must be preceded by a comma, and it must
					always refer to what? </p>
  <p>What are the exceptions to this rule?</p>
</div>','<div class="prodid-answer">
  <p> On the GMAT, the word <i>which</i> must be preceded by a comma, and it always
					refers to the <i>noun or phrase</i> immediately <i>preceding the comma</i>. </p>
  <p> The exceptions to this rule are prepositional phrases such as <i>of which,
						without which, </i>and <i>at which</i> and questions that use the word
						<i>which</i>. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.504962',0,'skipped');
INSERT INTO flashcards VALUES(1, 123,'122',7,NULL,'<div class="prodid-question">
  <p>On the GMAT, collective noun phrases such as “group of students,” “chain of
					restaurants,” or “collection of coins” usually take which kind of pronouns and
					verbs: singular or plural?</p>
</div>','<div class="prodid-answer">
  <p> On the GMAT, collective noun phrases such as “group of students,” “chain of
					restaurants,” or “collection of coins” usually take <i>singular</i> pronouns and
					verbs. </p>
</div>','','2014-06-16 12:25:50.504962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 124,'123',7,NULL,'<div class="prodid-question">
  <p>True or False: If there are pronouns in the question stem, you must always figure
					out how to make those pronouns unambiguous.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. If you use pronouns, they must be unambiguous. However, it is not
					always possible to give every pronoun a clear antecedent; sometimes there is no
					choice but to replace one or more pronouns with nouns to preserve clarity. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 125,'124',7,NULL,'<div class="prodid-question">
  <p>If a descriptive phrase is set off by commas, what should you check?</p>
</div>','<div class="prodid-answer">
  <p> If a descriptive phrase is set off by commas, <i>check whether the thing that
						the phrase logically modifies is immediately adjacent to the modifying
						phrase.</i> If it’s not, then the phrase is probably modifying the wrong
					thing and the sentence is incorrect. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 126,'125',7,NULL,'<div class="prodid-question">
  <p>If a sentence contains a comparison word such as <i>like, unlike,</i> or
						<i>more/less than,</i> what should you check? </p>
</div>','<div class="prodid-answer">
  <p> If a sentence contains a comparison word, check <i>to see if the items compared
						are similar grammatically and logically.</i> You can compare the state of
					California to the state of Massachusetts, and you can compare California’s
					population to Massachusetts’s population, but you can’t compare the state of
					California to the population of Massachusetts. If there is a comparison error,
					eliminate choice (A) and any other answer choices that repeat the mistake. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 127,'126',7,NULL,'<div class="prodid-question">
  <p>What is the passive voice, and should you use it?</p>
</div>','<div class="prodid-answer">
  <p> The passive voice is a construction in which<i> the subject is the one who
						receives or is affected by the verb, and the object is the one who acts.
					</i>For example, <i>I was bitten by the dog</i> is passive. You should <i>avoid
						the passive voice when possible,</i> using active sentences like <i>The dog
						bit me.</i> However, the passive voice may occasionally appear in correct
					answer choices. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 128,'127',7,NULL,'<div class="prodid-question">
  <p>True or False: You should memorize as many idioms as you can before Test Day.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. Even for non-native speakers of English, the majority of idioms
					tested on the GMAT are intuitive and easily recognized. Focus your studies on
					the idioms that you get wrong, or the ones that don’t sound natural to your ear.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 129,'128',7,NULL,'<div class="prodid-question">
  <p> On the GMAT, what must <i>where</i> refer to? What must <i>when</i> refer to?
				</p>
</div>','<div class="prodid-answer">
  <p> On the GMAT, <i>where</i> must refer to a <i>place</i>, and <i>when</i> must
					refer to a <i>time</i>. If you need to refer to an item that is neither a place
					nor a time, you should instead use <i>in which</i> (or <i>on which</i>, <i>for
						which</i>, etc.). </p>
  <p> Wrong: The GMAT is a test <i>where</i> critical thinking is useful. </p>
  <p> Correct: The GMAT is a test <i>for which</i> critical thinking is useful. </p>
  <p> Correct: The ice rink is a place <i>where</i> you can learn to skate. </p>
  <p> Wrong: The most popular book in the series is the one <i>when</i> aliens attack
					earth. </p>
  <p> Correct: The most popular book in the series is the one <i>in which</i> aliens
					attack earth. </p>
  <p> Correct: Jim wakes up <i>when</i> his dog barks in the morning. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 130,'129',7,NULL,'<div class="prodid-question">
  <p>What is a run-on sentence?</p>
</div>','<div class="prodid-answer">
  <p> A run-on sentence is <i>a sentence in which clauses</i> (groups of words
					containing a subject and a verb) <i>are not connected properly.</i></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 131,'130',7,NULL,'<div class="prodid-question">
  <p>What are three ways to fix a run-on sentence?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Join the two clauses with a conjunction (usually preceded by a comma): I sat
						on the porch to watch the sunset, <i>and</i> my dog Bruno slept contentedly
						at my feet. </li>
    <li>Change the comma to a semicolon: I sat on the porch to watch the sunset; my
						dog Bruno slept contentedly at my feet.</li>
    <li>Break the sentence into two separate sentences: I sat on the porch to watch
						the sunset. My dog Bruno slept contentedly at my feet.</li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 132,'131',7,NULL,'<div class="prodid-question">
  <p>True or False: Phrases set off by commas affect subject-verb agreement outside
					the commas.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. In fact, it can be a good idea to read sentences without paying
					attention to the phrases between commas to look for subject-verb errors. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 133,'132',7,NULL,'<div class="prodid-question">
  <p>Can a comma be used to join two independent clauses?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>A comma can be used to join two independent clauses only if a coordinating
						conjunction is present.</i>
  </p>
  <p>Examples:</p>
  <p> I am going to the carnival<i>, and</i> I intend to stay there. </p>
  <p> It rained heavily during the morning<i>, but</i> we managed to have our track
					meet anyway. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 134,'133',7,NULL,'<div class="prodid-question">
  <p>When is a semicolon used?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>A semicolon joins two independent clauses when a coordinating conjunction is
						not present.</i>
  </p>
  <p>Examples:</p>
  <p> I am going to the carnival<i>;</i> I intend to stay there. </p>
  <p> It rained heavily during the morning<i>;</i> we managed to have our track meet
					anyway. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 135,'134',7,NULL,'<div class="prodid-question">
  <p>What must a subject and its verb agree in?</p>
</div>','<div class="prodid-answer">
  <p> A subject and its verb must agree in <i>number</i>. Number refers to whether a
					subject or its verb is <i>singular</i> or <i>plural</i>. </p>
  <p> A subject and its verb must also agree in <i>person</i> (first person/second
					person/third person). Subject-verb agreement errors relating to person are rare
					on the GMAT. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 136,'135',7,NULL,'<div class="prodid-question">
  <p>What is an intervening phrase?</p>
</div>','<div class="prodid-answer">
  <p> An intervening phrase is <i>a phrase or a relative clause that adds additional
						information about the subject of the sentence</i>. The intervening phrase
					itself is not part of the subject.</p>
  <p>Example: Jim, <i>as well as your brother and I,</i> is invited. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 137,'136',7,NULL,'<div class="prodid-question">
  <p>
    <b>Which words in the underlined portion are NOT part of the subject?</b>
  </p>
  <p>
    <u>The Geneva Bible, which was first published in 1560</u>, was the version of
					the Bible that Shakespeare knew best. </p>
</div>','<div class="prodid-answer">
  <p> The phrase <i>which was first published in 1560</i> is not part of the subject. </p>
  <p>When a set of words is set off from the rest of the sentence by commas, those
					words are not part of the subject. That makes checking for subject-verb
					agreement much easier: just ignore the words set off by commas and concentrate
					on the subject and the verb.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 138,'137',7,NULL,'<div class="prodid-question">
  <p>True or False: When checking for subject-verb disagreement, remember that the
					subject doesn’t always appear before the verb.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True.</i> When checking for subject-verb disagreement, remember that the
					subject doesn’t always appear <i>before</i> the verb. </p>
  <p>Example: On the fence post <i>hung</i> an old <i>saddle</i>, its leather dull and
					withered from age. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 139,'138',7,NULL,'<div class="prodid-question">
  <p> When words in the subject position are connected by <i>either … or</i> or
						<i>neither … nor</i>, what happens to the verb? </p>
</div>','<div class="prodid-answer">
  <p>
    <i>The verb agrees with the last word in the pair.</i>
  </p>
  <p>Examples: </p>
  <p> Neither Thomas Jefferson nor Alexander <i>Hamilton was</i> supportive of Aaron
					Burr’s political ambitions. </p>
  <p> Neither Thomas Jefferson nor the <i>Federalists were</i> supportive of Aaron
					Burr’s political ambitions. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 140,'139',7,NULL,'<div class="prodid-question">
  <p>What is a gerund?</p>
</div>','<div class="prodid-answer">
  <p> A <i>gerund</i> is the noun form of a verb, ending in <i>-ing</i>. </p>
  <p> Knowing the difference between gerunds and verb forms that use <i>-ing</i>
					endings is important because, on the GMAT, verb tenses that end in <i>-ing</i>
					are almost never part of a correct answer. </p>
  <p> Correct: <i>Cooking</i> is an art. </p>
  <p> Incorrect: His friends would come to visit if he were <i>cooking</i> a delicious
					meal. </p>
  <p> Correct: His friends would come to visit if he were <i>to cook</i> a delicious
					meal. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 141,'140',7,NULL,'<div class="prodid-question">
  <p>What is a modifier?</p>
</div>','<div class="prodid-answer">
  <p> A modifier is <i>a word, phrase, or sentence element that illuminates, limits,
						or qualifies the sense of another word, phrase, or element in the same
						construction</i>.</p>
  <p>Example: The Black Card, <i>a symbol of high status</i>, is coveted by millions.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 142,'141',7,NULL,'<div class="prodid-question">
  <p>What is a misplaced modifier?</p>
</div>','<div class="prodid-answer">
  <p> A misplaced modifier is a <i>modifying word</i>, <i>clause</i>, or <i>phrase</i>
					placed in a way that <i>creates ambiguity or a nonsensical sentence</i>. </p>
  <p>Example: </p>
  <p>Incorrect: The doctor sprinted to the field to help the injured football player
					wearing high heels.</p>
  <p>Correct: The doctor, wearing high heels, sprinted to the field to help the
					injured football player.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 143,'142',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p> Desiring to free his readers from superstition, the theories of Epicurus are
					expounded in Lucretius’s poem <i>De rerum natura.</i></p>
</div>','<div class="prodid-answer">
  <p> The introductory modifying phrase is a misplaced modifier. It is modifying
						<i>theories</i>, the noun after the comma, but this is the wrong noun to
					modify. In this sentence there is nowhere the modifier can be placed to make it
					work properly, and no noun to which it can reasonably refer. (<i>Lucretius’s,
					</i>the possessive, is functioning as an adjective modifying <i>poem.</i>) You
					must rearrange the sentence to fix the problem.</p>
  <p>Correct: Desiring to free his readers from superstition, Lucretius wrote the poem
						<i>De rerum natura</i> in which he expounded upon the theories of Epicurus.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 144,'143',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence? </b>
  </p>
  <p>That night they sat discussing when the cow might calve in the kitchen.</p>
</div>','<div class="prodid-answer">
  <p> Modifying phrases inside a sentence can also be misplaced. The problem here is
					the phrase <i>in the kitchen,</i> which seems to refer to where the cow might
					give birth to her calf.</p>
  <p>Correct: That night they sat in the kitchen discussing when the cow might
					calve.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 145,'144',7,NULL,'<div class="prodid-question">
  <p>What is a pronoun?</p>
</div>','<div class="prodid-answer">
  <p> A pronoun is <i>a word that is used in place of a noun or another pronoun</i>. </p>
  <p>Example: As Nancy ran, <i>she</i> became short of breath. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 146,'145',7,NULL,'<div class="prodid-question">
  <p>What is the antecedent of a pronoun?</p>
</div>','<div class="prodid-answer">
  <p> The antecedent of a pronoun is <i>the word to which the pronoun refers</i>. It
					can precede or follow the pronoun.</p>
  <p>Examples: </p>
  <p>
    <i>Henry David Thoreau</i> went to jail because <i>he</i> opposed the
					Mexican-American War and the Fugitive Slave Act. </p>
  <p> Because <i>he</i> opposed the Mexican-American War and the Fugitive Slave Act,
						<i>Henry David Thoreau</i> went to jail. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 147,'146',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>To plaster a wall, a mason puts some on a trowel and smoothes it over the
					laths.</p>
</div>','<div class="prodid-answer">
  <p> There is no clear antecedent for the pronouns <i>some</i> and <i>it</i> in this
					sentence. It’s clear that the pronoun <i>some</i> is intended to refer to the
					noun <i>plaster,</i> but <i>plaster</i> occurs in this sentence only in the
					infinitive form <i>to plaster</i>, not as a noun. The pronoun <i>it</i> refers
					to <i>some</i>, which is itself ambiguous. </p>
  <p>Correct: To plaster a wall, a mason puts some <i>plaster</i> on a trowel and
					smoothes it over the laths. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 148,'147',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p> The pro-slavery writer A. C. C. Thompson questioned Frederick Douglass’s
					authorship of <i>The Narrative,</i> claiming that he was too uneducated to have
					written such an eloquent book. </p>
</div>','<div class="prodid-answer">
  <p> The antecedent of <i>he</i> in this sentence is unclear: it could be either
					Thompson and Douglass. Logically, the antecedent should be <i>Frederick
						Douglass</i>, but the sentence as written contains only the possessive form
						<i>Douglass’s</i>, and a possessive cannot be the antecedent of a pronoun.
					Correct this problem by eliminating the pronoun entirely and restating the noun. </p>
  <p>Correct: The pro-slavery writer A. C. C. Thompson questioned whether Frederick
					Douglass actually wrote <i>The Narrative</i>, claiming that Douglass was too
					uneducated to have written such an eloquent book. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 149,'148',7,NULL,'<div class="prodid-question">
  <p>What is a verb?</p>
</div>','<div class="prodid-answer">
  <p> A verb is <i>a word that expresses an action or a state of being</i>.</p>
  <p>Examples: </p>
  <p> She <i>kicked</i> the ball. </p>
  <p> He <i>lives</i> in England. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 150,'149',7,NULL,'<div class="prodid-question">
  <p>What is a participle?</p>
</div>','<div class="prodid-answer">
  <p> A participle is <i>a form of a verb that usually ends in -ing or -ed.</i> It is
					used as an adjective or adverb in a sentence.</p>
  <p>Examples: </p>
  <p> Let <i>sleeping</i> dogs lie. </p>
  <p> It is difficult to calm a <i>frightened</i> child. </p>
  <p>
    <i>Scrutinizing</i> his textbook, the boy found an error. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 151,'150',7,NULL,'<div class="prodid-question">
  <p> What is most important to know about <i>-ing</i> forms of verbs on GMAT Sentence
					Correction questions? </p>
</div>','<div class="prodid-answer">
  <p> As far as the GMAT is concerned, the only reason to use an <i>-ing</i> form of a
					verb is to emphasize that an action is continuing. Generally, <i>-ing</i> forms
					of verbs are found in incorrect answer choices. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 152,'151',7,NULL,'<div class="prodid-question">
  <p>What does the subject do in a passive-voice sentence?</p>
</div>','<div class="prodid-answer">
  <p> In the passive voice, the <i>subject is acted upon.</i> It <i>receives the
						action</i> expressed in the verb. </p>
  <p>Example: The girl <i>was bitten by</i> the dog. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 153,'152',7,NULL,'<div class="prodid-question">
  <p>What does the subject do in an active-voice sentence?</p>
</div>','<div class="prodid-answer">
  <p> In the active voice, the <i>subject performs the action expressed in the verb.
					</i></p>
  <p>Example: The dog <i>bit</i> the girl. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 154,'153',7,NULL,'<div class="prodid-question">
  <p>
    <b>Change the passive voice of this sentence into active voice:</b>
  </p>
  <p>A cabin was built near Walden Pond by Henry David Thoreau in 1845.</p>
</div>','<div class="prodid-answer">
  <p> In the active voice, the subject <i>Thoreau</i> does something. </p>
  <p>Correct: Henry David Thoreau <i>built a cabin</i> near Walden Pond in 1845. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 155,'154',7,NULL,'<div class="prodid-question">
  <p>How is the future tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p> Use the future tense <i>for intended actions or actions expected in the
						future.</i></p>
  <p>Example: The 22nd century <i>will begin</i> in the year 2101. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 156,'155',7,NULL,'<div class="prodid-question">
  <p>How is the future perfect tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p> Use the future perfect tense<i> for a future state or event that will take place
						before another future event</i>. </p>
  <p>Example: By next October, the candidates <i>will have debated</i> at least once.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 157,'156',7,NULL,'<div class="prodid-question">
  <p>What tense do you use for the earlier of two past events, the past perfect tense
					or the simple past tense? What tense do you use for the later event?</p>
</div>','<div class="prodid-answer">
  <p> Use the <i>past perfect</i> tense for the earlier of two past events and the
						<i>simple past</i> tense for the later event. </p>
  <p>Example: By the time he <i>opened</i> the store yesterday morning, he <i>had</i>
					already <i>completed</i> a 10-mile run. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 158,'157',7,NULL,'<div class="prodid-question">
  <p>What do you use the future perfect for, the earlier or later of two future
					events?</p>
</div>','<div class="prodid-answer">
  <p> Use the future perfect for the <i>earlier</i> of two future events. </p>
  <p>Example: I <i>will have taken</i> the GMAT by the time I celebrate my next
					birthday. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 159,'158',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>By the time I write to Leo, he will probably move.</p>
</div>','<div class="prodid-answer">
  <p>The point the author is trying to get across is not that Leo will move when he
					gets the letter, but that by the time the letter arrives, Leo will likely be
					living somewhere else. </p>
  <p>Correct: By the time I write to Leo, he <i>will</i> probably <i>have moved.</i></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 160,'159',7,NULL,'<div class="prodid-question">
  <p>What is the subjunctive mood?</p>
</div>','<div class="prodid-answer">
  <p> The subjunctive mood is used <i>to express a wish, probability, thought,
						condition contrary-to-fact, or requirement</i>. </p>
  <p> The subjunctive form <i>were</i> is used in statements that express a wish or
					situations that are <i>contrary to fact.</i></p>
  <p>Example: I wish I <i>were</i> rich. (But I’m not.) </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 161,'160',7,NULL,'<div class="prodid-question">
  <p>What is parallelism (or parallel structure)?</p>
</div>','<div class="prodid-answer">
  <p> Parallelism is<i> the expressing of ideas of equal importance and function in
						the same sentence and setting them all in the same grammatical form</i>
					(that is, all nouns, all adjectives, all gerunds, all clauses, etc.). </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 162,'161',7,NULL,'<div class="prodid-question">
  <p>If a sentence contains a list, what should you immediately check?</p>
</div>','<div class="prodid-answer">
  <p> If a sentence contains a list, you should immediately check <i>whether all items
						in the list are grammatically parallel.</i> If they are not, then eliminate
					choice (A) and any other answer choices that repeat this mistake. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 163,'162',7,NULL,'<div class="prodid-question">
  <p> If a sentence contains a two-part construction such as <i>either</i> X <i>or</i>
						Y<i>, not only</i> X <i>but also</i> Y<i>,</i> or X <i>and</i> Y<i>,</i>
					what should you immediately check? </p>
</div>','<div class="prodid-answer">
  <p> If a sentence contains a two-part construction, you should immediately check
						<i>whether</i> X <i>and</i> Y <i>are grammatically parallel.</i> If they are
					not, then eliminate choice (A) and any other answer choices that repeat this
					mistake. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 164,'163',7,NULL,'<div class="prodid-question">
  <p>Besides conjunctions and verbs, what are some other types of words to which
					parallelism applies?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Prepositions (<i>in, on, by, with,</i> etc.) </li>
    <li>Articles (<i>the, a, an</i>) </li>
    <li>Helping verbs (<i>had, has, would</i>, etc.) </li>
    <li>Possessive pronouns (<i>his, her</i>, etc.) </li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 165,'164',7,NULL,'<div class="prodid-question">
  <p>What are some two-part constructions that require parallelism?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>both … and</i>
  </p>
  <p>
    <i>either … or</i>
  </p>
  <p>
    <i>neither … nor</i>
  </p>
  <p>
    <i>not only … but also</i>
  </p>
  <p>
    <i>just as … so …</i>
  </p>
  <p>
    <i>whether … or …</i>
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 166,'165',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Isaac Newton not only studied physics but also theology.</p>
</div>','<div class="prodid-answer">
  <p> The problem here is that the author intends to coordinate the two nouns
						<i>physics</i> and <i>theology,</i> but makes the mistake of putting the
					verb of the sentence (<i>studied</i>) after the first element of the two-part
					construction (<i>not only</i>). The solution to an error like this is usually to
					move the verb. </p>
  <p>Correct: Isaac Newton <i>studied not only</i> physics but also theology. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 167,'166',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>To drive while intoxicated is risking grave injury and criminal charges.</p>
</div>','<div class="prodid-answer">
  <p>Use the same parts of speech for parallel phrases. In this case, use either the
					gerund or the infinitive form of both verbs. </p>
  <p>Correct: <i>To drive</i> while intoxicated is <i>to risk</i> grave injury and
					criminal charges. </p>
  <p>or</p>
  <p>
    <i>Driving</i> while intoxicated is <i>risking</i> grave injury and criminal
					charges. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 168,'167',7,NULL,'<div class="prodid-question">
  <p>To be considered correct, a sentence that makes a comparison must do what three
					things?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>It must make clear what is being compared.</li>
    <li>It must compare things that logically can be compared.</li>
    <li>It must use parallel structure.</li>
  </ol>
  <p>A sentence that makes an unclear, illogical, or incomplete comparison is
					grammatically unacceptable.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 169,'168',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The peaches here are riper than any other fruit stand.</p>
</div>','<div class="prodid-answer">
  <p> This illogical comparison is comparing <i>peaches</i> to <i>any other fruit
						stand,</i> even though that’s clearly not the author’s intention. Correct
					the sentence so that you’re comparing peaches to peaches by inserting the phrase
						<i>those at</i>. </p>
  <p>Correct: The peaches here are riper than <i>those at</i> any other fruit stand.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 170,'169',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Fred Astaire danced better than any man in the world.</p>
</div>','<div class="prodid-answer">
  <p> This is wrong because the phrase <i>any man in the world</i> presumably includes
					Astaire, and even Fred Astaire couldn’t have danced better than himself. </p>
  <p> This error is corrected by inserting either the word <i>other</i> or the word
						<i>else</i>. </p>
  <p>Correct: Fred Astaire danced better than any <i>other</i> man in the world. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 171,'170',7,NULL,'<div class="prodid-question">
  <p>What is redundancy?</p>
</div>','<div class="prodid-answer">
  <p> Redundancy is <i>the use of multiple words or phrases that have the same meaning
						in a situation in which one would be sufficient to get the point across.</i></p>
  <p>Redundant: Let me know your future plans for later this week.</p>
  <p>Preferred: Let me know your plans for this week.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 172,'171',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The school was established and founded by Quakers in 1906.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Established</i> and <i>founded</i> both have the same meaning in this
					sentence: set up, created. One or the other is acceptable—using both results in
					redundancy. </p>
  <p>Correct: The school was <i>established</i> by Quakers in 1906. </p>
  <p>or</p>
  <p> The school was <i>founded</i> by Quakers in 1906. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 173,'172',7,NULL,'<div class="prodid-question">
  <p>Why is wordiness a problem?</p>
</div>','<div class="prodid-answer">
  <p> Wordiness is a problem because <i>having extra words in a sentence is
						repetitious when the thought could be expressed more concisely</i>. On the
					GMAT, versions of Sentence Correction questions can be unacceptable partly or
					entirely because they’re too wordy; choose shorter versions as long as no
					essential words have been left out of the sentence. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 174,'173',7,NULL,'<div class="prodid-question">
  <p>
    <b>Make this wordy sentence more concise:</b>
  </p>
  <p>The dwindling supply of musical instruments that are antique is increasingly
					limited, making their soaring value go up each year.</p>
</div>','<div class="prodid-answer">
  <p>Because the supply of antique musical instruments is diminishing, their value
					increases each year.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 175,'174',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>John Clare, like Edith May and Charles Fenno Hoffman, were confined for decades
					to an insane asylum.</p>
</div>','<div class="prodid-answer">
  <p> The subject, <i>John Clare,</i> is singular, but the verb, <i>were</i>, is
					plural. The phrase set off by commas with two other nouns distracts you from the
					fact that the subject and verb don’t agree. </p>
  <p>Correct: John Clare, like Edith May and Charles Fenno Hoffman, <i>was</i>
					confined for decades to an insane asylum. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 176,'175',7,NULL,'<div class="prodid-question">
  <p> True or False: <i>And</i> is the only word that makes two singular nouns take a
					plural verb. </p>
</div>','<div class="prodid-answer">
  <p>
    <i>True</i>. <i>And</i> creates a <i>compound subject</i> that takes a plural
					verb. <i>Or</i> and <i>nor,</i> and phrases such as <i>as well as</i> or <i>in
						addition to</i>, do not create compound subjects. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 177,'176',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>There is only a few dozen tigers left in southern China.</p>
</div>','<div class="prodid-answer">
  <p> In <i>there is/are</i> sentences, the subject comes after the verb. <i>A few
						dozen tigers,</i> plural, is the subject of this sentence, so the verb
					should also be plural. </p>
  <p>Correct: There <i>are</i> only a few dozen tigers left in southern China. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 178,'177',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Sifting the sand in a riverbed, gold was discovered by prospectors in California
					in 1848.</p>
</div>','<div class="prodid-answer">
  <p> Because the introductory modifying phrase is next to the word <i>gold,</i> this
					sentence says that the <i>gold was sifting sand</i>. The author obviously meant
					to say that the <i>prospectors were sifting sand</i>. Modifying phrases must be
					as close as possible to the noun or phrase they modify. </p>
  <p>Correct: Sifting the sand in a riverbed, <i>prospectors</i> discovered gold in
					California in 1848. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 179,'178',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Pennsylvania governor William Keith encouraged the young Benjamin Franklin to
					open his own printing shop because he perceived that the quality of printing in
					Philadelphia was poor.</p>
</div>','<div class="prodid-answer">
  <p> The sentence is structured such that the pronoun <i>he</i> could refer to either
					Keith or Franklin, which prevents the reader from knowing what the author
					intended. </p>
  <p>Correct: Pennsylvania governor William Keith encouraged the young Benjamin
					Franklin to open his own printing shop because <i>the governor</i> perceived
					that the quality of printing in Philadelphia was poor. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 180,'179',7,NULL,'<div class="prodid-question">
  <p>Is “the number” always plural or always singular? Is “a number” always plural or
					always singular?</p>
</div>','<div class="prodid-answer">
  <p> “The number” is always <i>singular</i>. “A number” is always <i>plural</i>. </p>
  <p>Examples: </p>
  <p>
    <i>The number</i> of cookies he ate <i>was</i> impressive. </p>
  <p>
    <i>A number</i> of turkeys <i>were</i> gathered outside the shed. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 181,'180',7,NULL,'<div class="prodid-question">
  <p>How is the present tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p>A verb’s present tense is used:</p>
  <ol>
    <li>
      <i>To describe a state or action occurring in the present time</i>: Congress
							<i>is debating</i> about health policy this session. </li>
    <li>
      <i>To describe habitual action</i>: Many Americans <i>jog</i> every day. </li>
    <li>
      <i>To describe “general truths”</i>: The earth <i>is</i> round and
							<i>rotates</i> on its axis. </li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 182,'181',7,NULL,'<div class="prodid-question">
  <p>How is the past tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p> A verb’s past tense is used <i>to describe an event or state that took place at
						a specific time in the past and is now complete.</i></p>
  <p>Example: Few people <i>bought</i> new cars last year. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 183,'182',7,NULL,'<div class="prodid-question">
  <p>How is the present perfect tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p>Present perfect tense is used:</p>
  <ol>
    <li>For <i>actions and states that started in the past and continue up to and
							into the present time</i>: Kevin <i>has been</i> a trustee since
						December. </li>
    <li>For <i>actions and states that happened a number of times in the past and
							may happen again in the future</i>: The Modern Language Association
							<i>has awarded</i> a prize for independent scholars every year since
						1983. </li>
    <li>For <i>something that happened at an unspecified time in the past</i>: Susan
						Sontag <i>has written</i> critical essays about Leni Riefenstahl. </li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 184,'183',7,NULL,'<div class="prodid-question">
  <p>How is the past perfect tense of a verb used?</p>
</div>','<div class="prodid-answer">
  <p> Past perfect tense is used <i>to represent past actions or states that were
						completed before other past actions or states</i>. </p>
  <p>Example: After he came to America, Vladimir Nabokov translated novels that he
						<i>had written</i> in Russian while he was living in Europe. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 185,'184',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p> Mozart finished about two-thirds of the <i>Requiem</i> when he died. </p>
</div>','<div class="prodid-answer">
  <p> Putting both verbs of the sentence in the simple past tense makes it sound as if
					Mozart wrote two-thirds of the <i>Requiem</i> during the moment of his death. If
					you put the first verb into the past perfect, the sentence makes much more
					sense. </p>
  <p>Correct: Mozart <i>had finished</i> about two-thirds of the <i>Requiem</i> when
					he died. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 186,'185',7,NULL,'<div class="prodid-question">
  <p>What subjunctive structure is used to express an order or recommendation?</p>
</div>','<div class="prodid-answer">
  <p> To express an order or recommendation, a special subjunctive structure is used.
					The structure is: verb (e.g., <i>order</i>, <i>recommend</i>, <i>demand</i>,
						<i>insist</i>) + new subject + <i>that</i> + base form of the verb. The base
					form of a verb is the infinitive without the word <i>to</i> (e.g., <i>be</i>,
						<i>run</i>, <i>work</i>). </p>
  <p>Example: Airlines <i>insist that each passenger pass</i> through a metal
					detector. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 187,'186',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Byron admired Dryden more than Wordsworth.</p>
</div>','<div class="prodid-answer">
  <p>There are two ways to interpret this unclear comparison: either that Dryden meant
					more to Byron than Wordsworth did, or that Byron thought more highly of Dryden
					than Wordsworth did. Whichever meaning you choose, the problem can be cleared up
					by adding more words to the sentence. </p>
  <p>Correct: Byron admired Dryden more than <i>he did </i>Wordsworth. </p>
  <p>or</p>
  <p> Byron admired Dryden more than <i>Wordsworth did.</i></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 188,'187',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The amount of protesters who showed up to the rally was astounding.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Amount </i>should be used to refer to a singular or non-countable word. The
					proper word for this sentence would be <i>number</i>, which refers to a plural
					or countable word. </p>
  <p>Correct: The <i>number</i> of protesters who showed up to the rally was
					astounding. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 189,'188',7,NULL,'<div class="prodid-question">
  <p> In constructing a comparison, when should you use <i>like</i> versus <i>as</i>?
				</p>
</div>','<div class="prodid-answer">
  <p> Use <i>like</i> to compare nouns; use <i>as</i> when a verb accompanies the
					comparison. </p>
  <p>Example: </p>
  <p> His favorite shirt is <i>like</i> a baby’s blanket. </p>
  <p> He hangs the shirt outside to dry <i>as</i> his grandmother used to do. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 190,'189',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>I regard you to be a close friend.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Regard as </i>is the correct idiom; <i>regard to be </i>is wrong. </p>
  <p>Correct: I <i>regard </i>you <i>as </i>a close friend. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 191,'190',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Brigitte Bardot joined an organization that is concerned in preventing cruelty to
					animals.</p>
</div>','<div class="prodid-answer">
  <p> The correct idiom is <i>concerned about </i>or <i>concerned with, </i>not
						<i>concerned in.</i></p>
  <p>Correct: Brigitte Bardot joined an organization that is <i>concerned about
					</i>preventing cruelty to animals. </p>
  <p>or</p>
  <p> Brigitte Bardot joined an organization that is <i>concerned with </i>preventing
					cruelty to animals. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 192,'191',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>When Walt Whitman’s family moved to Brooklyn, there were no bridges nor tunnels
					across the East River.</p>
</div>','<div class="prodid-answer">
  <p> The phrase <i>no bridges nor tunnels </i>is just not idiomatic—it contains a
					double negative. Remove the second negative. </p>
  <p>Correct: When Walt Whitman’s family moved to Brooklyn, there were <i>no bridges
						or tunnels</i> across the East River. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 193,'192',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Neither the basketball player nor his agent were available for comment.</p>
</div>','<div class="prodid-answer">
  <p> When two or more singular nouns or pronouns are connected by <i>or</i> or
						<i>nor</i>, use a singular verb. </p>
  <p>Correct: Neither the basketball player nor his agent <i>was</i> available for
					comment. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 194,'193',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The competition among the two candidates grew intense in the weeks before the
					presidential election.</p>
</div>','<div class="prodid-answer">
  <p> Use <i>between </i>for two items and <i>among </i>for more than two. </p>
  <p>Correct: The competition <i>between</i> the two candidates grew intense in the
					weeks before the presidential election. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 195,'194',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>After the hurricane, many people were left without homes, businesses, and huge
					bills to replace all they had lost.</p>
</div>','<div class="prodid-answer">
  <p> The preposition <i>without </i>is used in front of only the first member of a
					series, which implies that people were left without homes, without businesses,
					and without huge bills to replace what they had lost, which makes no sense.</p>
  <p>Correct: After the hurricane, many people were left <i>without </i>homes,
						<i>without </i>businesses, and <i>with </i>huge bills to replace all they
					had lost. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 196,'195',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>That suit is as expensive than this one.</p>
</div>','<div class="prodid-answer">
  <p> The idiom is <i>as … as</i>. </p>
  <p>Correct: That suit is <i>as </i>expensive <i>as </i>this one. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 197,'196',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Jenny Lind was said to sing like a nightingale sings.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Like</i> is a preposition that is used to compare nouns. It introduces a
					prepositional phrase. <i>As</i>, when functioning as a conjunction, introduces a
					subordinate clause. Either <i>like</i> or <i>as</i> could be used correctly in
					this sentence, but some rearrangement of the rest of the sentence may be
					necessary. </p>
  <p>Correct: Jenny Lind was said to sing <i>like</i> a nightingale. </p>
  <p>or</p>
  <p> Jenny Lind was said to sing <i>as</i> a nightingale sings. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 198,'197',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The reason many high schools use metal detectors is because some students bring
					weapons to school.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Reason is because</i> is a redundancy. <i>Because</i> already means “for the
					reason that.”</p>
  <p>Correct: The reason many high schools use metal detectors is <i>that </i>some
					students bring weapons to school. </p>
  <p>or</p>
  <p> Many high schools use metal detectors <i>because </i>some students bring weapons
					to school. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 199,'198',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Beside the team, there are often reporters in the locker room.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Beside</i>, which means “next to,” doesn’t make sense in this sentence. The
					correct word to use is <i>besides</i>, which means “in addition to.” </p>
  <p>Correct: <i>Besides</i> the team, there are often reporters in the locker room.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 200,'199',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Call between five to six o’clock.</p>
</div>','<div class="prodid-answer">
  <p> The idiom is <i>between … and</i>. </p>
  <p>Correct: Call <i>between </i>five <i>and</i> six o’clock. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 201,'200',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>If Cleopatra’s nose would have been shorter, the whole face of the world would
					have changed.</p>
</div>','<div class="prodid-answer">
  <p> Contrary-to-fact and improbable conditional sentences use the helping verb
						<i>would </i>in the <i>then </i>clause, but never in the <i>if </i>clause: </p>
  <p>Correct: If Cleopatra’s nose <i>had been</i> shorter, the whole face of the world
						<i>would have </i>changed. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 202,'201',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Of the four televisions in my home, the older one is not able to be repaired.</p>
</div>','<div class="prodid-answer">
  <p> There are two problems with this sentence. First, the narrator is comparing four
					televisions, so the superlative <i>oldest </i>should be used. </p>
  <p> Second, it is incorrect to use a form of <i>to be able </i>preceding the passive
					form of an infinitive. </p>
  <p>Correct: Of the four televisions in my home, the <i>oldest</i> one <i>cannot</i>
					be repaired. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 203,'202',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Harvard’s MBA degree requirements are different than Columbia’s.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Different </i>is usually used with the preposition <i>from,
						</i>not <i>than</i>. </p>
  <p>Correct: Harvard’s MBA degree requirements are different <i>from</i> Columbia’s.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 204,'203',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>A diagram is when a sketch is made to illustrate the parts of something.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>When</i> on the GMAT must always refer to a literal time. <i>When</i> is
					incorrect if it is used to describe a situation or a definition; in those cases
					it can be replaced either by a prepositional <i>which</i> phrase (<i>in which,
						at which</i>) or by the word <i>that</i>. </p>
  <p>Correct: A diagram is <i>a sketch that is</i> made to illustrate the parts of
					something. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 205,'204',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The aggregate of recent polling numbers suggest that the upcoming election will
					be tough for incumbents.</p>
</div>','<div class="prodid-answer">
  <p>The subject of a sentence determines the correct verb form; a prepositional
					phrase between a subject and a verb has no effect on which form of the verb is
					correct. Because “aggregate” is singular, a singular verb is needed. </p>
  <p>Correct: The aggregate of recent polling numbers <i>suggests</i> that the
					upcoming election will be tough for incumbents. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 206,'205',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The football team’s playoff chances have looked good until the team’s recent loss
					in overtime.</p>
</div>','<div class="prodid-answer">
  <p>The past perfect tense is used to indicate something that happened before
					something else in the past. Because the team’s loss was in the past, the time
					when its chances looked good must have occurred before that. </p>
  <p>Correct: The football team’s playoff chances <i>had</i> looked good until the
					team’s recent loss in overtime. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 207,'206',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Whenever my friend quotes their favorite book, they sound extremely
					pretentious.</p>
</div>','<div class="prodid-answer">
  <p> A pronoun must agree in number with its antecedent. Because only one friend is
					referenced, <i>their</i> and <i>they</i> are incorrect. These pronouns should be
					replaced with <i>his</i> and <i>he</i> or <i>her</i> and <i>she</i> depending on
					the gender of the friend. </p>
  <p>Correct: Whenever my friend quotes <i>her</i> favorite book, <i>she </i>sounds
					extremely pretentious. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 208,'207',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Whenever Dyer and Jon argue, he comes away feeling mentally exhausted.</p>
</div>','<div class="prodid-answer">
  <p>A pronoun must clearly refer to an antecedent. Because “he” could refer to either
					Dyer or Jon, it cannot be used. The correct answer would likely remove the
					pronoun entirely and use the individual’s name. </p>
  <p>Correct: Whenever Dyer and Jon argue, <i>Jon</i> comes away feeling mentally
					exhausted. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 209,'208',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>In order for an athlete to perform at his peak, it is imperative that he does
					regular exercise.</p>
</div>','<div class="prodid-answer">
  <p>Adjectives modify nouns; adverbs modify verbs, adjectives, or other adverbs.
					Here, “regular” is an adjective, so it is describing the type of exercise:
					“regular” exercise as opposed to “weird” exercise. </p>
  <p>Correct: In order for an athlete to perform at his peak, it is imperative <i>that
						he exercise regularly</i>. </p>
  <p>(Note that the corrected sentence uses the subjunctive “that he exercise” to
					express an order or recommendation.)</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 210,'209',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Attempting to reconcile their differences quickly, arbitration was agreed to by
					both parties.</p>
</div>','<div class="prodid-answer">
  <p>A modifying phrase must go directly next to the thing it modifies. Here,
					“arbitration” is not attempting to reconcile their differences; “both parties”
					are. </p>
  <p>Correct: Attempting to reconcile their differences quickly, <i>both parties
						agreed to arbitration</i>. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 211,'210',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The purchase of balloons, decorating the backyard, and hiring the clown were all
					done the day before Alex’s birthday party.</p>
</div>','<div class="prodid-answer">
  <p>All of the terms in a list must be parallel. </p>
  <p>Correct: <i>Purchasing</i><i>the</i> balloons, decorating the backyard, and hiring the clown were all done
					the day before Alex’s birthday party. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 212,'211',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Waiting for Christmas morning is to be reminded of the joy of being a child.</p>
</div>','<div class="prodid-answer">
  <p>Analogies, metaphors, similes, and other comparisons all require parallel
					structure. </p>
  <p>Correct: <i>To wait</i> for Christmas morning is to be reminded of the joy of
					being a child. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 213,'212',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Jed’s head, while bigger than most cats, is still in proportion to his body.</p>
</div>','<div class="prodid-answer">
  <p>When two things are being compared, it’s important to make sure that they make
					sense logically as well as grammatically. Here, it’s highly unlikely that Jed’s
					head is bigger than most whole cats. </p>
  <p>Correct: Jed’s head, while bigger than <i>those of</i> most cats, is still in
					proportion to his body. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 214,'213',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Marlon Brando is considered to be the finest actor of his generation.</p>
</div>','<div class="prodid-answer">
  <p>“Considered” doesn’t need to be followed by a verb form. </p>
  <p>Correct: Marlon Brando is <i>considered</i> the finest actor of his generation.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 215,'214',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Despite what television commercials say, affluence is different than
					happiness.</p>
</div>','<div class="prodid-answer">
  <p>The construction “different than” is not idiomatic. </p>
  <p>Correct: Despite what television commercials say, affluence is different
						<i>from</i> happiness. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 216,'215',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>While the show is primarily aimed for children, parents often enjoy it more.</p>
</div>','<div class="prodid-answer">
  <p>The construction “aimed for” is not idiomatic. </p>
  <p>Correct: While the show is primarily aimed <i>at</i> children, parents often
					enjoy it more. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 217,'216',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Because their membership had been dwindling for decades, the VFW post was
					disbanded.</p>
</div>','<div class="prodid-answer">
  <p> Even though the antecedent comes later in the sentence, the pronoun “their”
					refers to the VFW post, which is a singular entity. The correct pronoun is
						<i>its</i>. </p>
  <p>Correct: Because <i>its</i> membership had been dwindling for decades, the VFW
					post was disbanded. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 218,'217',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Each of the members agree that a vote of no-confidence should be taken.</p>
</div>','<div class="prodid-answer">
  <p>Even though “members” is plural, it follows a preposition, and “each” stands for
					“each one,” which is singular. </p>
  <p>Correct: Each of the members <i>agrees</i> that a vote of no-confidence should be
					taken. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 219,'218',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>People were lined up around the block to buy tickets to the movie which had won
					the “Best Picture” award the night before.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Which</i> is used only when what follows is not necessary to identify the
					subject of the clause. Here, the movie is unknown at first, so the sentence
					should use <i>that</i> instead of <i>which</i>. </p>
  <p>Correct: People were lined up around the block to buy tickets to the movie
						<i>that</i> had won the “Best Picture” award the night before. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 220,'219',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Dancers must always remember to practice, eat well, and to respect their
					teachers.</p>
</div>','<div class="prodid-answer">
  <p>Lists must be parallel, and every term in the list can refer back to the
					preposition that began the list. The “to” before “respect” is unnecessary and
					incorrect. </p>
  <p>Correct: Dancers must always remember to practice, eat well, and <i>respect</i>
					their teachers. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 221,'220',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Because she didn’t want to hurt either of them, so Emily delayed informing her
					potential suitors of her decision.</p>
</div>','<div class="prodid-answer">
  <p>One and only one connector must be used to connect two clauses. Either “because”
					or “so” can be removed here, but one of them has to go. </p>
  <p>Correct: <i>Because</i> she didn’t want to hurt either of them, Emily delayed
					informing her potential suitors of her decision. </p>
  <p>or</p>
  <p> She didn’t want to hurt either of them, <i>so</i> Emily delayed informing her
					potential suitors of her decision. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 222,'221',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Many personal trainers recommend that their clients should focus as much on
					nutrition as on exercise.</p>
</div>','<div class="prodid-answer">
  <p> You can <i>recommend</i> that something happen, or state that something
						<i>should</i> happen, but doing both is redundant and incorrect. </p>
  <p>Correct: Many personal trainers <i>recommend that their clients focus</i> as much
					on nutrition as on exercise. </p>
  <p>or</p>
  <p> Many personal trainers <i>say that their clients should focus</i> as much on
					nutrition as on exercise. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 223,'222',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Many bullies pick fights that are as much about their own insecurities than they
					are about the person being picked on.</p>
</div>','<div class="prodid-answer">
  <p>“As … than” is always incorrect. </p>
  <p>Correct: Many bullies pick fights that are as much about their own insecurities
						<i>as</i> they are about the person being picked on. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 224,'223',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>To hurry her order, Genevieve must decide between one dessert or the other.</p>
</div>','<div class="prodid-answer">
  <p>“Between … or” is not idiomatic. </p>
  <p>Correct: To hurry her order, Genevieve must decide between one dessert <i>and</i>
					the other. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 225,'224',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Neither the teacher nor the students was happy with the result of the pop
					quiz.</p>
</div>','<div class="prodid-answer">
  <p> When a singular subject and a plural one are connected with <i>or</i> or
						<i>nor</i>, the verb must agree in number with the closest subject. Here,
					this is the plural “students.” </p>
  <p>Correct: Neither the teacher nor the students <i>were</i> happy with the result
					of the pop quiz. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 226,'225',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The football player couldn’t decide if he wanted to play next season.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>If</i> is used to set up a hypothetical situation; <i>whether</i> is used to
					set up a decision. </p>
  <p>Correct: The football player couldn’t decide <i>whether</i> he wanted to play
					next season. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 227,'226',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>There are less people in the audience for tonight’s show than there were for
					yesterday’s show.</p>
</div>','<div class="prodid-answer">
  <p> If you can count the number of things, use <i>fewer</i>. If you can’t count
					them, use <i>less</i>. </p>
  <p>Correct: There are <i>fewer</i> people in the audience for tonight’s show than
					there were for yesterday’s show. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 228,'227',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Children under 48 inches tall are prohibited to ride the roller coasters at the
					amusement park.</p>
</div>','<div class="prodid-answer">
  <p> You are prohibited <i>from</i> doing something, not <i>to </i>do it. </p>
  <p>Correct: Children under 48 inches tall are <i>prohibited from riding</i> the
					roller coasters at the amusement park. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 229,'228',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Because of his poor vision, George is forbidden from driving at night.</p>
</div>','<div class="prodid-answer">
  <p> You are forbidden <i>to</i> do something, not <i>from </i>doing it. </p>
  <p>Correct: Because of his poor vision, George is <i>forbidden to drive</i> at
					night. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 230,'229',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Jessica said she would try and let Mark know when she got to Virginia.</p>
</div>','<div class="prodid-answer">
  <p> You try <i>to</i> do something, you do not try <i>and</i> do something. </p>
  <p>Correct: Jessica said she would <i>try to</i> let Mark know when she got to
					Virginia. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 231,'230',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>It is unclear about who James is talking.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Who</i> is a subject, and <i>whom</i> is an object. Here, you need the
					object. </p>
  <p>Correct: It is unclear about <i>whom</i> James is talking. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 232,'231',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The skeleton key will only open doors on the first floor.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Only</i> must be put directly next to the word or phrase it is modifying.
					While it may be true that the key will “only open doors,” the purpose of the
					sentence is to convey that the doors the key will open are “on the first floor
					only.” </p>
  <p>Correct: The skeleton key will open doors on the first floor <i>only</i>. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 233,'232',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Although both Rachele and Samantha are up for a promotion, Rachele is having a
					much better relationship with her supervisor.</p>
</div>','<div class="prodid-answer">
  <p>The present progressive is rarely correct on the GMAT. Here, the simple present
					is clear and concise.</p>
  <p>Correct: Although both Rachele and Samantha are up for a promotion, Rachele
						<i>has</i> a much better relationship with her supervisor. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 234,'233',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Unlike football players, baseball has drawn a lot of criticism for steroid
					use.</p>
</div>','<div class="prodid-answer">
  <p>Make sure you’re comparing things that are logically comparable. Here, football
					players are being compared to the game of baseball.</p>
  <p>Correct: Unlike football players, <i>baseball players have</i> drawn a lot of
					criticism for steroid use. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 235,'234',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>The superhero, along with his trusty sidekick, capture the city’s most nefarious
					criminals.</p>
</div>','<div class="prodid-answer">
  <p> Only the word <i>and</i> can turn two singular subjects into a plural subject. </p>
  <p>Correct: The superhero, along with his trusty sidekick, <i>captures</i> the
					city’s most nefarious criminals. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 236,'235',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>In the fall, students can play either football and soccer, but not both.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Either … or</i> is the correct idiom, not <i>either … and</i>. </p>
  <p>Correct: In the fall, students can play <i>either</i> football <i>or</i> soccer,
					but not both. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 237,'236',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Professional football games are decided in “sudden death,” where the first team
					to score is the winner.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Where</i> on the GMAT must always refer to a literal place. </p>
  <p>Correct: Professional football games are decided in “sudden death,” <i>in
						which</i> the first team to score is the winner. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 238,'237',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Tom has never been able to distinguish whiskey and scotch.</p>
</div>','<div class="prodid-answer">
  <p> The correct idiom is either <i>distinguish … from</i> or <i>distinguish between
						… and</i>. </p>
  <p>Correct: Tom has never been able to distinguish whiskey <i>from</i> scotch. </p>
  <p>or</p>
  <p> Tom has never been able to distinguish <i>between</i> whiskey <i>and</i> scotch.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 239,'238',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>At the carnival, the barker estimated Navin’s weight at 200 pounds.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Estimate … at</i> is not idiomatic. </p>
  <p>Correct: At the carnival, the barker estimated Navin’s weight <i>to be</i> 200
					pounds. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 240,'239',7,NULL,'<div class="prodid-question">
  <p>
    <b>What is wrong with this sentence?</b>
  </p>
  <p>Rachel is regarded to be the best singer in the glee club.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Regarded to be </i>is not idiomatic. </p>
  <p>Correct: Rachel is regarded <i>as</i> the best singer in the glee club. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 241,'240',8,NULL,'<div class="prodid-question">
  <p>What is the average amount of time that you should spend on each Quantitative
					question?</p>
</div>','<div class="prodid-answer">
  <p>With 75 minutes to complete 37 Quantitative questions, you should spend an
					average of 2 minutes on each.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 242,'241',8,NULL,'<div class="prodid-question">
  <p>What is the approximate mix of question types on the Quantitative section?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard241.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 243,'242',8,NULL,'<div class="prodid-question">
  <p>What are the major topics tested on the Quantitative section?</p>
</div>','<div class="prodid-answer">
  <p>Algebra, Arithmetic, Number Properties, Proportions, Statistics, Math Formulas,
					and Geometry</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 244,'243',9,NULL,'<div class="prodid-question">
  <p>What is Picking Numbers?</p>
</div>','<div class="prodid-answer">
  <p>Most people find it easier to perform calculations with numbers than to perform
					calculations with variables. Picking Numbers is a strategy to replace the
					variables with numbers and make the math easier.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 245,'244',9,NULL,'<div class="prodid-question">
  <p>What are the two criteria that must be remembered when Picking Numbers?</p>
</div>','<div class="prodid-answer">
  <p> The numbers must be <i>permissible</i> and <i>manageable</i>. When Picking
					Numbers, make those numbers as easy to work with as possible, remembering any
					rules that the question has given you. Remember that this doesn’t mean the
					numbers need to be realistic. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 246,'245',9,NULL,'<div class="prodid-question">
  <p>What are clues that you can look for to help you pick manageable numbers?</p>
</div>','<div class="prodid-answer">
  <p>If there are fractions in the answer choices, try to pick numbers that work with
					the denominators of the fractions.</p>
  <p>If there are percents in the question, 100 is usually the easiest number to work
					with.</p>
  <p>If you need to divide one variable by another, make sure one number is a multiple
					of the other.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 247,'246',9,NULL,'<div class="prodid-question">
  <p>Can you use Picking Numbers on Data Sufficiency questions?</p>
</div>','<div class="prodid-answer">
  <p> Yes. When Picking Numbers on Data Sufficiency questions, you must always <i>pick
						at least two different sets of permissible numbers</i>, trying to prove that
					the statements are insufficient by producing two different results. (If you try
					several sets of numbers and get the same result each time, you will often be
					able to identify a pattern or rule that proves sufficiency.) </p>
  <p> Remember that <i>picking different types of numbers</i> is more likely to
					produce different results: positives vs. negatives, fractions vs. integers, odds
					vs. evens, 0 and 1, etc. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 248,'247',9,NULL,'<div class="prodid-question">
  <p>What is Backsolving?</p>
</div>','<div class="prodid-answer">
  <p>Backsolving is a form of Picking Numbers, only you “pick” the numbers given to
					you in the answer choices and plug them into the scenario in the question stem.
					Because one of those answer choices must be correct, Backsolving can often help
					you identify the right answer quickly and with little extra work.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 249,'248',9,NULL,'<div class="prodid-question">
  <p>When should you think about using Backsolving?</p>
</div>','<div class="prodid-answer">
  <p>You should consider Backsolving whenever there are numbers, but no variables, in
					the answer choices.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 250,'249',9,NULL,'<div class="prodid-question">
  <p>Which answer choice do you want to test first when Backsolving?</p>
</div>','<div class="prodid-answer">
  <p>Since answer choices are usually in ascending (occasionally descending) order, it
					is most efficient to test either (B) or (D) first. There’s usually some hint as
					to whether the answer is more likely to be high or low; if not, just pick
					whichever seems easier. If you pick (D) and that works, great. If it’s too low
					(and answer choices are in ascending order), then (E) must be the answer. If
					it’s too high, eliminate (D) and (E), then test (B), which will either be
					correct, too high, or too low. In any case, you will need to test at most only
					two answer choices.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 251,'250',9,NULL,'<div class="prodid-question">
  <p>Are there any variations to the Backsolving strategy of starting with (B) or
					(D)?</p>
</div>','<div class="prodid-answer">
  <p>Yes. First, eliminate any answers that you can clearly and quickly determine are
					incorrect. Then, Backsolve strategically using the choices that remain.</p>
  <p>Also, if (B) or (D) is unwieldy (say, involving a radical or complex fraction),
					start with (C).</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 252,'251',10,NULL,'<div class="prodid-question">
  <p>What is the Kaplan Method for Problem Solving?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Analyze the question.</li>
    <li>State the task.</li>
    <li>Approach strategically.</li>
    <li>Confirm your answer.</li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 253,'252',10,NULL,'<div class="prodid-question">
  <p>What does Step 1 of the Kaplan Method for Problem Solving, “Analyze the
					question,” mean in practical terms?</p>
</div>','<div class="prodid-answer">
  <p>First, determine the type of problem (word problem? overlapping sets? algebra?).
					Organize your thinking and know which rules or formulas you need.</p>
  <p>Second, simplify what you can (but don’t start solving yet!).</p>
  <p>Finally, glance at the answer choices to determine whether you can use a
					strategy.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 254,'253',10,NULL,'<div class="prodid-question">
  <p>What does Step 2 of the Kaplan Method for Problem Solving, “State the task,” mean
					in practical terms?</p>
</div>','<div class="prodid-answer">
  <p>Before you choose your approach, make sure you know what you’re solving for. In
					other words, what does the correct answer choice represent?</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 255,'254',10,NULL,'<div class="prodid-question">
  <p>What does Step 3 of the Kaplan Method for Problem Solving, “Approach
					strategically,” mean in practical terms?</p>
</div>','<div class="prodid-answer">
  <p>Use your analysis from Steps 1 and 2 to find the most straightforward approach
					that allows you to make sense of the problem. Given the current problem, choose
					the easiest approach or combination of approaches for you:</p>
  <ol>
    <li>A Kaplan strategy, such as Picking Numbers or Backsolving</li>
    <li>Straightforward math</li>
    <li>Strategic guessing</li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 256,'255',10,NULL,'<div class="prodid-question">
  <p>What does Step 4 of the Kaplan Method for Problem Solving, “Confirm your answer,”
					mean in practical terms?</p>
</div>','<div class="prodid-answer">
  <p>Reread the question stem as you select your answer. Make sure you are answering
					the question asked. If you notice a wrinkle in the problem that you missed
					earlier, rework the problem (if you have time) or make a strategic guess.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 257,'256',10,NULL,'<div class="prodid-question">
  <p>If a question asks you what CANNOT be true, what does that mean about the
					incorrect answers?</p>
</div>','<div class="prodid-answer">
  <p>It means they could be true under some circumstances. It’s important to note that
					it doesn’t mean the wrong answers MUST be true.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 258,'257',10,NULL,'<div class="prodid-question">
  <p>What answer choices are more likely to be correct when the question asks “Which
					of the following . . . ?”</p>
</div>','<div class="prodid-answer">
  <p>“Which of the following” questions often require that you evaluate the answer
					choices individually, which can be time-consuming. However, (D) and (E) are
					disproportionately the correct answers to questions of this type. Therefore,
					evaluating the answer choices starting with (E) and then (D), working your way
					through the choices in reverse order, will save you time.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 259,'258',11,NULL,'<div class="prodid-question">
  <p>What is the Kaplan Method for Data Sufficiency?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Analyze the question stem.</li>
    <li>Evaluate the statements using 12TEN.</li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 260,'259',11,NULL,'<div class="prodid-question">
  <p>What is the most important question to ask before evaluating the statements?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>What information would be sufficient to answer this question?</i> The biggest
					mistake people make on Data Sufficiency questions is doing more work than they
					have to. Taking the time to figure out what is needed before looking at the
					statements helps to avoid that pitfall. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 261,'260',11,NULL,'<div class="prodid-question">
  <p>What does 12TEN stand for?</p>
</div>','<div class="prodid-answer">
  <p>12TEN is a mnemonic used to remember the order of answer choices in Data
					Sufficiency questions. It stands for:</p>
  <ul>
    <li>
      <b>1</b> alone is sufficient </li>
    <li>
      <b>2</b> alone is sufficient </li>
    <li>
      <b>T</b>ogether the statements are sufficient (individually they are
						insufficient) </li>
    <li>
      <b>E</b>ither statement alone is sufficient </li>
    <li>
      <b>N</b>either statement is sufficient, either alone or together </li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 262,'261',11,NULL,'<div class="prodid-question">
  <p>What are the two types of Data Sufficiency questions?</p>
</div>','<div class="prodid-answer">
  <p>
    <b>Value</b>—The question is asking for a specific value for a variable. For
					sufficiency, you must be able to solve for a single value. </p>
  <p>
    <b>Yes/No</b>—The question is asking whether or not a statement is true. </p>
  <p>Value questions make up about two-thirds of the Data Sufficiency questions you’ll
					see on Test Day.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 263,'262',11,NULL,'<div class="prodid-question">
  <p>True or False: To be sufficient, a statement must lead to an answer of “yes” to a
					Yes/No Data Sufficiency question.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. A statement is insufficient when it leads to the answer being
					“sometimes yes / sometimes no.” As long as the statement leads to a single
					answer (either “always yes” or “always no”), it is sufficient. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 264,'263',11,NULL,'<div class="prodid-question">
  <p>What is the most common mistake rookie test takers make with Data Sufficiency
					questions?</p>
</div>','<div class="prodid-answer">
  <p>Rookies make the mistake of not forgetting about Statement (1) before evaluating
					Statement (2). Remember, you first have to determine whether each statement is
					sufficient individually, without regard to the other statement.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 265,'264',11,NULL,'<div class="prodid-question">
  <p>When should you combine the statements in Data Sufficiency?</p>
</div>','<div class="prodid-answer">
  <p>Combine the statements only if each of them, separately, has been proven
					insufficient. If that is the case, you have to determine whether the answer is
					choice T (together) or choice N (neither).</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 266,'265',11,NULL,'<div class="prodid-question">
  <p>What is the best way to combine statements in Data Sufficiency?</p>
</div>','<div class="prodid-answer">
  <p>Treat them as if they are one long statement.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 267,'266',11,NULL,'<div class="prodid-question">
  <p>True or False: A statement in a Value question is sufficient only if it leads to
					a singular value.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True</i>. If, for example, a statement leads to <i>x</i> = 2 or <i>x</i> =
					–2, it is not sufficient to determine the value of <i>x</i>. It must lead to a
					single value. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 268,'267',12,NULL,'<div class="prodid-question">
  <p>What is a coefficient?</p>
</div>','<div class="prodid-answer">
  <p> A coefficient is <i>the numerical constant by which one or more variables are
						multiplied. </i></p>
  <p> For example, the coefficient of 3<i>x</i><sup /><sup>2</sup> is 3. A variable
					(or product of variables) without a numerical coefficient, such as <i>z</i> or
						<i>xy</i><i /><sup>3</sup>, is understood to have a coefficient of 1. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 269,'268',12,NULL,'<div class="prodid-question">
  <p>What is a coefficient?</p>
</div>','<div class="prodid-answer">
  <p> A coefficient is <i>the numerical constant by which one or more variables are
						multiplied. </i></p>
  <p> For example, the coefficient of 3<i>x</i><sup /><sup>2</sup> is 3. A variable
					(or product of variables) without a numerical coefficient, such as <i>z</i> or
						<i>xy</i><i /><sup>3</sup>, is understood to have a coefficient of 1. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 270,'269',12,NULL,'<div class="prodid-question">
  <p>In a word problem, what are some keywords and phrases that indicate the need for
					an addition symbol (+)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>add</li>
    <li>sum</li>
    <li>increased by</li>
    <li>more than</li>
    <li>plus</li>
    <li>exceeds</li>
    <li>combined</li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 271,'270',12,NULL,'<div class="prodid-question">
  <p>In a word problem, what are some keywords and phrases that indicate the need for
					a subtraction symbol (–)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>subtract</li>
    <li>difference</li>
    <li>decreased by</li>
    <li>less than</li>
    <li>minus</li>
    <li>reduced</li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 272,'271',12,NULL,'<div class="prodid-question">
  <p>In a word problem, what are some keywords and phrases that indicate the need for
					a multiplication symbol (×)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>multiply</li>
    <li>product</li>
    <li>multiplied by</li>
    <li>of</li>
    <li>times</li>
    <li>twice</li>
    <li>double</li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 273,'272',12,NULL,'<div class="prodid-question">
  <p>In a word problem, what are some keywords and phrases that indicate the need for
					a fraction or a division symbol (÷)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>divide</li>
    <li>quotient</li>
    <li>divided by</li>
    <li>into</li>
    <li>per</li>
    <li>ratio</li>
    <li>out of</li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 274,'273',12,NULL,'<div class="prodid-question">
  <p>In a word problem, what are some keywords and phrases that indicate the need for
					an equal symbol (=)?</p>
</div>','<div class="prodid-answer">
  <ul>
    <li>equal</li>
    <li>is, was, will be</li>
    <li>result</li>
    <li>total</li>
    <li>equal to</li>
    <li>has</li>
    <li>costs</li>
    <li>the same as</li>
    <li>as much as</li>
  </ul>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 275,'274',12,NULL,'<div class="prodid-question">
  <p>What are the steps for isolating a variable?</p>
</div>','<div class="prodid-answer">
  <ol>
    <li>Eliminate any <b>fractions</b> by multiplying both sides and/or eliminate
							<b>radicals</b> by squaring both sides. </li>
    <li>Put all terms with the variable you’re solving for on one <b>side</b> by
						adding or subtracting on both sides. </li>
    <li>
      <b>Combine</b> like terms. </li>
    <li>
      <b>Factor</b> out the desired variable (if necessary). </li>
    <li>
      <b>Divide</b> by everything else, leaving the desired variable by itself.
					</li>
  </ol>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 276,'275',12,NULL,'<div class="prodid-question">
  <p> Factor the expression 2<i>a</i> + 6<i>ac</i>.</p>
</div>','<div class="prodid-answer">
  <p>Factoring a polynomial means expressing it as a product of two or more simpler
					expressions. Common factors can be factored out using the distributive law.</p>
  <p> The greatest common factor of 2<i>a</i> + 6<i>ac</i> is 2<i>a</i>. Using the
					distributive law, you can factor out 2<i>a</i>, so that the expression becomes
						2<i>a</i>(1 + 3<i>c</i>). </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 277,'276',12,NULL,'<div class="prodid-question">
  <p> Express <img src="/assets/contents/GMATcard276_01.png" /> in terms of <i>x</i> if
						<i>a</i> = 2<i>x</i> and <i>b</i> = 3. </p>
</div>','<div class="prodid-answer">
  <p> Substitution, <i>a process of plugging values into equations</i>, is used to
					evaluate an algebraic expression or to express it in terms of other variables.
					Replace every variable in the expression with the number or quantity you are
					told is its equivalent. </p>
  <p> Here, you will replace every <i>a</i> with 2<i>x</i> and every <i>b</i> with 3: </p>
  <p>
    <img src="/assets/contents/GMATcard276_02.png" />
  </p>
  <p>Without more information, you can’t simplify or evaluate this expression
					further.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 278,'277',12,NULL,'<div class="prodid-question">
  <p> If 4<i>x</i> – 7 = 2<i>x</i> + 5, what is the value of <i>x</i>? </p>
</div>','<div class="prodid-answer">
  <p>4<i>x</i> – 7 = 2<i>x</i> + 5</p>
  <p>Add 7 to both sides:</p>
  <p>4<i>x</i> = 2<i>x</i> + 12</p>
  <p>Subtract 2<i>x</i> from both sides: </p>
  <p>2<i>x</i> = 12 </p>
  <p>Divide both sides by 2:</p>
  <p>
    <i>x</i> = 6 </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 279,'278',12,NULL,'<div class="prodid-question">
  <p> Simplify (3<i>x</i><i /><sup>2</sup> + 5<i>x</i>)(<i>x</i> – 1). </p>
</div>','<div class="prodid-answer">
  <p>To simplify this equation, multiply using FOIL:</p>
  <p>
    <b>F</b>irst terms: (3<i>x</i><sup /><sup>2</sup>)(<i>x</i>) =
						3<i>x</i><sup>3</sup></p>
  <p>
    <b>O</b>uter terms: (3<i>x</i><sup /><sup>2</sup>)(–1) =
						–3<i>x</i><sup /><sup>2</sup></p>
  <p>
    <b>I</b>nner terms: (5<i>x</i>)(<i>x</i>) = 5<i>x</i><sup /><sup>2</sup></p>
  <p>
    <b>L</b>ast terms: (5<i>x</i>)(–1) = –5<i>x</i></p>
  <p>Now combine like terms:</p>
  <p> 3<i>x</i><sup>3</sup> – 3<i>x</i><sup /><sup>2</sup> +
						5<i>x</i><sup /><sup>2</sup> – 5<i>x </i>= 3<i>x</i><i /><sup>3</sup> +
						2<i>x</i><sup /><sup>2</sup> – 5<i>x.</i></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 280,'279',12,NULL,'<div class="prodid-question">
  <p>What are the three classic quadratics?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>a</i>
    <sup>2</sup> + 2<i>ab</i> + <i>b</i><sup>2</sup> = (<i>a</i> +
						<i>b</i>)<sup>2</sup></p>
  <p>
    <i>a</i>
    <sup>2</sup> – 2<i>ab</i> + <i>b</i><sup>2</sup> = (<i>a</i> –
						<i>b</i>)<sup>2</sup></p>
  <p>
    <i>a</i>
    <sup>2</sup> – <i>b</i><sup>2</sup> = (<i>a</i> – <i>b</i>)(<i>a</i> +
						<i>b</i>) </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 281,'280',12,NULL,'<div class="prodid-question">
  <p> Factor the quadratic expression
					<br /><i>x</i><sup>2</sup> – 3<i>x</i> + 2. </p>
</div>','<div class="prodid-answer">
  <p>Many of the quadratic expressions that you’ll see on the GMAT can be factored
					into the product of two binomials by using the FOIL method backward, factoring
					the given polynomial into two binomials.</p>
  <p> Start by writing down what you know: <br /><i>x</i><sup>2</sup> – 3<i>x</i> + 2 =
						(<i>x </i><i></i><i></i>)(<i>x </i><i></i><i></i>). </p>
  <p>You’ll need to fill in the missing term in each binomial factor. The sum of the
					missing terms must be the coefficient of the polynomial’s second term, and the
					product of the missing terms needs to be the polynomial’s third term.</p>
  <p> In this problem, the missing terms must sum to –3 and have a product of 2. Only
					–1 and –2 fit these restrictions, so they must be the missing terms. Thus,
						<br /><i>x</i><sup>2</sup> – 3<i>x</i> + 2 = (<i>x</i> – 1)(<i>x</i> – 2).
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 282,'281',12,NULL,'<div class="prodid-question">
  <p> Factor the quadratic expression 9<i>x</i><sup>2</sup> – 1. </p>
</div>','<div class="prodid-answer">
  <p> A common quadratic expression on the GMAT is the difference of two squares. Once
					you recognize a polynomial as the difference of two squares, you’ll be able to
					factor it automatically, because any polynomial of the form <i>a</i><sup>2</sup>
					– <i>b</i><sup>2</sup> can be factored into a product of the form <br />(<i>a</i>
					+ <i>b</i>)(<i>a</i> – <i>b</i>). </p>
  <p> If 9<i>x</i><sup>2</sup> = (3<i>x</i>)<sup>2</sup> and 1 = 1<sup>2</sup>, then
						9<i>x</i><sup>2</sup> – 1 is the difference of two squares. </p>
  <p> Therefore, 9<i>x</i><sup>2</sup> – 1 = (3<i>x </i>+ 1)(3<i>x </i>– 1). </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 283,'282',12,NULL,'<div class="prodid-question">
  <p> Factor the quadratic expression
					<i>x</i><sup>2</sup> + 6<i>x</i> + 9. </p>
</div>','<div class="prodid-answer">
  <p>
    <i>x</i>
    <sup>2</sup> and 9 are both perfect squares, and 6<i>x </i>is twice the
					product of<i> x </i>and 3, so this polynomial is a classic quadratic in the form
						<i>a</i><sup>2</sup> + 2<i>ab</i> + <i>b</i><sup>2</sup>. Since there is a
					plus sign in front of the 6<i>x</i>,<br /><i> x</i><sup>2</sup> + 6<i>x </i>+ 9 =
						(<i>x </i>+ 3)<sup>2</sup>. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 284,'283',12,NULL,'<div class="prodid-question">
  <p> Solve for<i> x</i>: </p>
  <p>
    <i>x</i>
    <sup>2</sup> + 6 = 5<i>x</i></p>
</div>','<div class="prodid-answer">
  <p>To solve for a variable in a quadratic equation, first set one side of the
					equation equal to zero. Then factor the quadratic, using reverse FOIL, into two
					binomial expressions. Set each expression equal to zero to find the solution(s)
					for the variable.</p>
  <p>
    <i>
      <img src="/assets/contents/GMATcard283.png" />
    </i>
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 285,'284',12,NULL,'<div class="prodid-question">
  <p>How many linear equations are needed to solve for each of three variables?</p>
</div>','<div class="prodid-answer">
  <p> To solve for the individual values of multiple variables in a system of
					equations, you need <i>at least as many distinct linear equations as you have
						variables</i>—in this case, three. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 286,'285',12,NULL,'<div class="prodid-question">
  <p>What are the two ways to solve a system of linear equations?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Substitution</i>—Isolate one variable in one equation, then substitute for
					all instances of that variable in the other equation. </p>
  <p>
    <i>Combination</i>—Add or subtract whole equations. Use this when it will allow
					you to eliminate a variable or get directly to the equation you want. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 287,'286',12,NULL,'<div class="prodid-question">
  <p> If <i>x</i> – 15 = 2<i>y</i> and 6<i>y</i> + 2<i>x</i> = –10, what is the value
					of <i>y</i>? </p>
</div>','<div class="prodid-answer">
  <p> One method of solving simultaneous equations is through <i>substitution</i>. </p>
  <p> The first step is <i>to solve one equation for one variable in terms of the
						second</i>. The second step is to <i>substitute the result into the other
						equation and solve</i>. </p>
  <p> Solve the first equation for <i>x</i> by adding 15 to both sides: <i>x</i> =
						2<i>y</i> + 15. </p>
  <p> Now substitute 2<i>y</i> + 15 for <i>x</i> in the second equation: </p>
  <p>
    <img src="/assets/contents/GMATcard286.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 288,'287',12,NULL,'<div class="prodid-question">
  <p> Let <i>x</i><i />* be defined by the equation <img src="/assets/contents/GMATcard287_02.png" />. </p>
  <p> Evaluate <img src="/assets/contents/GMATcard287_01.png" />. </p>
</div>','<div class="prodid-answer">
  <p> Symbolism problems usually <i>require nothing more than substitution</i>. Read
					the question stem carefully for a definition of the symbols and for any examples
					of how to use them. Then, follow the given model, substituting the numbers that
					are in the question stem. </p>
  <p>
    <img src="/assets/contents/GMATcard287_03.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 289,'288',13,NULL,'<div class="prodid-question">
  <p>What is the product of zero and any number?</p>
</div>','<div class="prodid-answer">
  <p> The product of zero and any number is <i>zero. </i>Example: </p>
  <p>
    <img src="/assets/contents/GMATcard288.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 290,'289',13,NULL,'<div class="prodid-question">
  <p>What is division by zero?</p>
</div>','<div class="prodid-answer">
  <p> Division by zero is <i>undefined</i>. For GMAT purposes, that translates as “it
					can’t be done.” Because fractions are essentially division (that is, <img src="/assets/contents/GMATcard289.png" /> means 1 ÷ 4), any fraction with
					zero in the denominator is also undefined. So when you are given a fraction that
					has an algebraic expression in the denominator, be sure that the expression
					cannot equal zero. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 291,'290',13,NULL,'<div class="prodid-question">
  <p>Does multiplying or dividing a number by 1 change the number?</p>
</div>','<div class="prodid-answer">
  <p> Multiplying or dividing a number by 1 <i>does not </i>change the number.
					Example: </p>
  <p> (<i>a</i>)(1) = <i>a</i></p>
  <p> (1)(<i>a</i>) = <i>a</i></p>
  <p>
    <i>a</i> ÷ 1 = <i>a</i></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 292,'291',13,NULL,'<div class="prodid-question">
  <p>What is the order of operations represented by the acronym PEMDAS?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard291.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 293,'292',13,NULL,'<div class="prodid-question">
  <p>How do you subtract a negative number?</p>
</div>','<div class="prodid-answer">
  <p>Subtracting a negative is the same as adding a positive. For example:</p>
  <p>12 – (–3) = 12 + 3 = 15</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 294,'293',13,NULL,'<div class="prodid-question">
  <p>Describe the distributive law of multiplication.</p>
</div>','<div class="prodid-answer">
  <p> In multiplication, the distributive law of multiplication allows you to
					“distribute” a factor over numbers that are added or subtracted, by
						<i>multiplying that factor by each number in the group. </i>Example: </p>
  <p>
    <img src="/assets/contents/GMATcard293.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 295,'294',13,NULL,'<div class="prodid-question">
  <p>What are the steps when adding or subtracting fractions?</p>
</div>','<div class="prodid-answer">
  <p>When adding or subtracting fractions:</p>
  <p> First, <i>convert the fractions</i> if they don’t already share a common
						<i>denominator</i> (the bottom number of the fraction): </p>
  <p>
    <img src="/assets/contents/GMATcard294_01.png" />
  </p>
  <p> Then, add the <i>numerators</i> (the top numbers of the fractions): </p>
  <p>
    <img src="/assets/contents/GMATcard294_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 296,'295',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard295_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p> Because the numerator and the denominator are each considered distinct
					quantities, you would rewrite the fraction as (5 + 2) ÷ (7 – 3). The order of
					operations tells us that operations in parentheses must be performed first. That
					gives you 7 ÷ 4. Your final answer can be expressed as <img src="/assets/contents/GMATcard295_02.png" /> or <img src="/assets/contents/GMATcard295_03.png" />. </p>
  <p>You can divide the numerator of a fraction by the denominator to get an
					equivalent decimal: 1.75.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 297,'296',13,NULL,'<div class="prodid-question">
  <p> Change <img src="/assets/contents/GMATcard296_1.png" /> into an equivalent
					fraction with a denominator of 4. </p>
</div>','<div class="prodid-answer">
  <p>Because multiplying or dividing a number by 1 does not change the number,
					multiplying the numerator and denominator of a fraction by the same nonzero
					number doesn’t change the value of the fraction—it’s the same as multiplying the
					entire fraction by 1.</p>
  <p>To change the denominator from 2 to 4, you’ll have to multiply it by 2. But to
					keep the value of the fraction the same, you’ll also have to multiply the
					numerator by 2.</p>
  <p>
    <img src="/assets/contents/GMATcard296.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 298,'297',13,NULL,'<div class="prodid-question">
  <p> Change <img src="/assets/contents/GMATcard297_01.png" /> into an equivalent
					fraction with a denominator of 10. </p>
</div>','<div class="prodid-answer">
  <p>Dividing the numerator and denominator by the same nonzero number leaves the
					value of the fraction unchanged.</p>
  <p>To change the denominator from 20 to 10, you have to divide it by 2. But to keep
					the value of the fraction the same, you have to divide the numerator by the same
					number.</p>
  <p>
    <img src="/assets/contents/GMATcard297_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 299,'298',13,NULL,'<div class="prodid-question">
  <p> What is the least common denominator of <img src="/assets/contents/GMATcard298_01.png" /> and <img src="/assets/contents/GMATcard298_02.png" />? </p>
</div>','<div class="prodid-answer">
  <p> The least common denominator of these two fractions will be the least common
					multiple of 15 and 10. Because 15 = (5)(3) and 10 = (5)(2), the least common
					multiple of the two numbers is (5)(3)(2), or 30. That makes 30 the least common
					denominator of <img src="/assets/contents/GMATcard298_01.png" /> and <img src="/assets/contents/GMATcard298_02.png" />. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 300,'299',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard299_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>Before multiplying, reduce the fractions by canceling common factors in the
					numerators and denominators. First, cancel a 5 out of the 10 and the 15, a 3 out
					of the 3 and the 9, and a 4 out of the 8 and the 4:</p>
  <p>
    <img src="/assets/contents/GMATcard299_02.png" />
  </p>
  <p> Then multiply the numerators together and denominators together: <img src="/assets/contents/GMATcard299_03.png" /></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 301,'300',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard300_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>To divide fractions, multiply the first fraction by the reciprocal of the number
					or fraction that follows the division sign. The operation of division produces
					the same result as the inverse of multiplication.</p>
  <p>
    <img src="/assets/contents/GMATcard300_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 302,'301',13,NULL,'<div class="prodid-question">
  <p> Which is greater: <img src="/assets/contents/GMATcard301_01.png" />? </p>
</div>','<div class="prodid-answer">
  <p>If neither the numerators nor the denominators are the same, you have three
					options:</p>
  <ol>
    <li>Turn both fractions into their decimal equivalents: <img src="/assets/contents/GMATcard301_06.png" /></li>
    <li>Express both fractions in terms of some common denominator, and then see
						which new equivalent fraction has the largest numerator: <img src="/assets/contents/GMATcard301_02.png" /></li>
    <li>Cross-multiply the numerator of each fraction by the denominator of the
						other. The greater result will wind up next to the greater fraction.</li>
  </ol>
  <p>
    <img src="/assets/contents/GMATcard301_03.png" />
  </p>
  <p> Since <img src="/assets/contents/GMATcard301_04.png" />. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 303,'302',13,NULL,'<div class="prodid-question">
  <p>Which is greater: 0.254 or 0.3?</p>
</div>','<div class="prodid-answer">
  <p> 0.3 is equivalent to <img src="/assets/contents/GMATcard302_01.png" />, while
					0.254 is equivalent to only <img src="/assets/contents/GMATcard302_02.png" />.
					Since <img src="/assets/contents/GMATcard302_03.png" />, 0.3 is greater than
					0.254. </p>
  <p>Here’s the simplest way to compare decimals: Add zeros after the last digit to
					the right of the decimal point in each decimal fraction until all the decimals
					you’re comparing have the same number of digits (so the example would be 0.254
					and 0.300). Essentially, what you’re doing is giving all the fractions the same
					denominator so that you can just compare their numerators.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 304,'303',13,NULL,'<div class="prodid-question">
  <p>How do you change a fraction to its percent equivalent?</p>
</div>','<div class="prodid-answer">
  <p> To change a fraction to its percent equivalent, <i>multiply</i> by 100%. </p>
  <p> For example, the percent equivalent of <img src="/assets/contents/GMATcard303_01.png" /> is <img src="/assets/contents/GMATcard303_02.png" />. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 305,'304',13,NULL,'<div class="prodid-question">
  <p> Translate <img src="/assets/contents/GMATcard304_01.png" /> and <img src="/assets/contents/GMATcard304_02.png" /> into their decimal equivalents.
				</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard304_03.png" />. </p>
  <p>Know the decimal equivalents for these and other common fractions.</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 306,'305',13,NULL,'<div class="prodid-question">
  <p>How do you change a percent to its fractional equivalent?</p>
</div>','<div class="prodid-answer">
  <p> To change a percent to its fractional equivalent, <i>divide </i>by 100%. </p>
  <p> For example, the fractional equivalent of 32% is <img src="/assets/contents/GMATcard305.png" />. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 307,'306',13,NULL,'<div class="prodid-question">
  <p>What is a cubed number?</p>
</div>','<div class="prodid-answer">
  <p> A cubed number is <i>a number raised to the third power</i>. For example,
						4<sup>3</sup>= (4)(4)(4) = 64, and 64 is the cube of 4. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 308,'307',13,NULL,'<div class="prodid-question">
  <p> 4<sup>5</sup> × 4<sup>2</sup> = </p>
  <p> 4<sup>5</sup> ÷ 4<sup>2</sup> = </p>
</div>','<div class="prodid-answer">
  <p> When two exponent terms with the same base are multiplied, <i>add </i>the
					exponents: </p>
  <p> 4<sup>5</sup> × 4<sup>2</sup> = (4)(4)(4)(4)(4) × (4)(4) = 4<sup>7</sup></p>
  <p>or</p>
  <p> 4<sup>5</sup> × 4<sup>2</sup> = 4<sup>5 + 2</sup> = 4<sup>7</sup></p>
  <p> When two exponent terms with the same base are divided, <i>subtract</i> the
					exponents: </p>
  <p>
    <img src="/assets/contents/GMATcard307.png" />
  </p>
  <p>or</p>
  <p> 4<sup>5</sup> ÷ 4<sup>2</sup> = 4<sup>5 – 2</sup> = 4<sup>3</sup></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 309,'308',13,NULL,'<div class="prodid-question">
  <p> (3<sup>2</sup>)<sup>4 </sup>= </p>
</div>','<div class="prodid-answer">
  <p> When you raise an exponent to an exponent, <i>multiply</i> the exponents: </p>
  <p> (3<sup>2</sup>)<sup>4</sup> = (3 × 3)<sup>4</sup> = (3 × 3)(3 × 3)(3 × 3)(3 ×
						3)<sup></sup>= 3<sup>8</sup></p>
  <p>or</p>
  <p> (3<sup>2</sup>)<sup>4 </sup>= 3<sup>2 × 4 </sup>= 3<sup>8</sup></p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 310,'309',13,NULL,'<div class="prodid-question">
  <p> 3<sup>0</sup> = </p>
</div>','<div class="prodid-answer">
  <p> 3<sup>0</sup> = 1 </p>
  <p>Any nonzero number raised to the exponent 0 equals 1.</p>
  <p> Zero raised to the 0 power is <i>undefined</i>. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 311,'310',13,NULL,'<div class="prodid-question">
  <p>Does raising a fraction between 0 and 1 to an integer exponent greater than 1
					produce a result that is smaller or larger than the original fraction?</p>
</div>','<div class="prodid-answer">
  <p> Raising a fraction between 0 and 1 to an integer exponent greater than 1
					produces a result that is <i>smaller</i> than the original. Example: </p>
  <p>
    <img src="/assets/contents/GMATcard310.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 312,'311',13,NULL,'<div class="prodid-question">
  <p> 5<sup>–3 </sup>= </p>
</div>','<div class="prodid-answer">
  <p> A number raised to the exponent –<i>x</i> is the reciprocal of that number
					raised to the exponent <i>x</i>. Example: </p>
  <p>
    <img src="/assets/contents/GMATcard311.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 313,'312',13,NULL,'<div class="prodid-question">
  <p> 5.6 × 10<sup>6</sup> = </p>
</div>','<div class="prodid-answer">
  <p> When 10 is raised to a positive integer exponent, that exponent tells how many
						<i>zeros </i>the number would contain if it were written out. When another
					number is multiplied by such a power of 10, this is called <i>scientific
						notation</i>. </p>
  <p> In this case, the exponent 10<sup>6</sup> indicates that you must <i>move the
						decimal point</i> in the number 5.6 <i>six places to the right</i>. This is
					the equivalent of multiplying by 1,000,000, or one million. That’s because
						10<sup>6</sup> means six factors of 10; that is, (10)(10)(10)(10)(10)(10). </p>
  <p> 5.6 × 10<sup>6</sup> = 5,600,000, or 5.6 million </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 314,'313',13,NULL,'<div class="prodid-question">
  <p> If 5<sup><i>x</i></sup> × 5<sup>5</sup><sup><i>x</i></sup> = 125, what is the value of<i> x</i><i />? </p>
</div>','<div class="prodid-answer">
  <p>First, use exponent rules to make both sides of the equation comparable:</p>
  <p>
    <img src="/assets/contents/GMATcard311_01.png" />
  </p>
  <p> Now that you have a common base for the powers, you can solve for <i>x</i> by
					setting the exponents equal to each other: </p>
  <p>
    <img src="/assets/contents/GMATcard311_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 315,'314',13,NULL,'<div class="prodid-question">
  <p>What do fractional exponents relate to?</p>
</div>','<div class="prodid-answer">
  <p> Fractional exponents relate to <i>roots</i>. Examples: </p>
  <p>
    <img src="/assets/contents/GMATcard314.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 316,'315',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard315_1.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard315.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 317,'316',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard316_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p> On the GMAT, the square root sign always designates the positive square root. So
						<img src="/assets/contents/GMATcard316_02.png" /> when <i>a</i> is positive,
					and <img src="/assets/contents/GMATcard316_03.png" /> when <i>a</i> is negative.
				</p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 318,'317',13,NULL,'<div class="prodid-question">
  <p>
    <b>Try this Data Sufficiency question:</b>
  </p>
  <p> What is the value of<i> x</i>? </p>
  <p> (1) <i>x</i> = <img src="/assets/contents/GMATcard317.png" /></p>
  <p> (2) <i>x</i><sup>2</sup> = 16 </p>
</div>','<div class="prodid-answer">
  <p>When applying the four basic arithmetic operations, radicals (roots written with
					the radical symbol) are treated in much the same way as variables.</p>
  <p> Statement (1) is sufficient, because there is only one possible value for <img src="/assets/contents/GMATcard317.png" />, positive 4. Statement (2) is
					insufficient, because <i>x </i>could be 4 or –4. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 319,'318',13,NULL,'<div class="prodid-question">
  <p>What happens when a negative number is raised to an even exponent? To an odd
					exponent?</p>
</div>','<div class="prodid-answer">
  <p> A negative number raised to an even exponent is always <i>positive</i>. A
					negative number raised to an odd exponent is always <i>negative</i>. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 320,'319',13,NULL,'<div class="prodid-question">
  <p>Of addition, subtraction, multiplication, and division, with which two can the
					radicals be combined or split? With which two can the radicals not be combined
					or split?</p>
</div>','<div class="prodid-answer">
  <p> Radicals <b>can</b> be combined or split when the operations involved are
						<i>multiplication</i> and <i>division</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard319_01.png" />
  </p>
  <p> Radicals <b>cannot</b> be combined or split when the operations involved are
						<i>addition</i> and <i>subtraction</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard319_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 321,'320',13,NULL,'<div class="prodid-question">
  <p>What is a cube root?</p>
</div>','<div class="prodid-answer">
  <p> The cube root of <i>x</i> is <i>the number that, multiplied by itself 3
						times</i> (i.e., cubed), gives you <i>x</i>. Both positive and negative
					numbers have one and only one cube root, denoted by the symbol <img src="/assets/contents/GMATcard320_01.png" />, and the cube root of a number
					is always the same sign as the number itself. </p>
  <p> For example, (–5) × (–5) × (–5) = –125, so <img src="/assets/contents/GMATcard320_02.png" />. </p>
</div>','','2014-06-16 12:25:50.505962','2014-06-16 12:25:50.505962',0,'skipped');
INSERT INTO flashcards VALUES(1, 322,'321',13,NULL,'<div class="prodid-question">
  <p>If the number inside the radical is a multiple of a perfect square, what will you
					accomplish by factoring out the perfect square?</p>
</div>','<div class="prodid-answer">
  <p> If the number inside the radical is a multiple of a perfect square, the
					expression can be <i>simplified</i> by factoring out the perfect square. </p>
  <p> For example, <img src="/assets/contents/GMATcard321.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 323,'322',13,NULL,'<div class="prodid-question">
  <p>How do you multiply square roots?</p>
</div>','<div class="prodid-answer">
  <p> The <i>product</i> of more than one square root is equal to the square root of
					their total product. Example: </p>
  <p>
    <img src="/assets/contents/GMATcard322.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 324,'323',13,NULL,'<div class="prodid-question">
  <p>How do you divide square roots?</p>
</div>','<div class="prodid-answer">
  <p> The <i>quotient </i>of more than one square root is equal to the square root of
					their total quotient. Example: </p>
  <p>
    <img src="/assets/contents/GMATcard323.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 325,'324',13,NULL,'<div class="prodid-question">
  <p>Since square roots aren’t permitted as denominators in fractions, how do you
					convert such a fraction to have an integer in the denominator?</p>
</div>','<div class="prodid-answer">
  <p>Multiply the numerator and denominator by the square root in the denominator. For
					example:</p>
  <p>
    <img src="/assets/contents/GMATcard324.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 326,'325',13,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard325_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard325_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 327,'326',13,NULL,'<div class="prodid-question">
  <p>What does absolute value represent on a number line?</p>
</div>','<div class="prodid-answer">
  <p> Absolute value represents <i>the distance of a value from zero</i> on the number
					line. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 328,'327',13,NULL,'<div class="prodid-question">
  <p>True or False: The value inside an absolute value sign must be positive.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. The <i>final result</i> of an absolute value must be non-negative,
					but any numbers or variables inside the absolute value sign can be positive,
					negative, or zero. Watch out for this common Test Day trap. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 329,'328',13,NULL,'<div class="prodid-question">
  <p>How many solutions do absolute value problems normally have?</p>
</div>','<div class="prodid-answer">
  <p> Absolute value problems normally have <i>two</i> solutions, depending on whether
					the value inside the absolute value signs is positive or negative. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 330,'329',13,NULL,'<div class="prodid-question">
  <p> |<i>x </i>– 3| = 17 </p>
  <p> What are the possible values of<i> x</i>? </p>
</div>','<div class="prodid-answer">
  <p> When dealing with absolute value, you need to consider both the positive and
					negative interpretations of what is contained within the absolute value signs.
					Here, you have that the absolute value of<i> x </i>– 3 is equal to 17. This
					could mean that<i> x </i>– 3 is equal to 17 or –17. If<i> x </i>– 3 = 17,
						then<i> x </i>= 20. If<i> x </i>– 3 = –17, then<i> x </i>= –14. Thus, the
					two possible values for<i> x </i>are 20 and –14. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 331,'330',13,NULL,'<div class="prodid-question">
  <p>What happens when both sides of an inequality are multiplied or divided by
					a negative number?</p>
</div>','<div class="prodid-answer">
  <p>You have to reverse the direction of the inequality sign.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 332,'331',13,NULL,'<div class="prodid-question">
  <p>What extra caution is needed when considering the solution to an inequality?</p>
</div>','<div class="prodid-answer">
  <p> Remember that the solution to an inequality describes a <i>range</i> of values,
					not merely a single value. A number line is a great tool for visualizing such a
					range; for instance, this line represents all the values of <i>x</i> &gt; 3: </p>
  <p>
    <img src="/assets/contents/GMATcard331.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 333,'332',13,NULL,'<div class="prodid-question">
  <p>In inequalities, what must you know in order to multiply or divide by
					variables?</p>
</div>','<div class="prodid-answer">
  <p> In inequalities, you cannot multiply or divide by variables unless you know
						<i>whether they are positive or negative</i>. Remember, positives preserve
					the direction of the inequality sign but negatives reverse it; if you don’t know
					the sign of a variable, you won’t know whether the inequality sign should stay
					the same or be reversed. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 334,'333',13,NULL,'<div class="prodid-question">
  <p>Rather than being a single value, the solution to an inequality is almost always
					what?</p>
</div>','<div class="prodid-answer">
  <p> The solution to an inequality is almost always a <i>range</i> of possible
					values, rather than a single value. </p>
  <p>For example:</p>
  <p>
    <img src="/assets/contents/GMATcard333.png" />
  </p>
  <p> The shaded portion of the number line above shows the set of all numbers greater
					than –1, up to and including 4; this range would be expressed algebraically by
					the inequality –1 &lt; <i>x</i> ≤ 4. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 335,'334',13,NULL,'<div class="prodid-question">
  <p> Rewrite 7 – 3<i>x</i> &gt; 2 in its simplest form. </p>
</div>','<div class="prodid-answer">
  <p>Treat this inequality much like an equation—adding, subtracting, multiplying, and
					dividing both sides by the same thing. Just remember to reverse the inequality
					sign if you multiply or divide by a negative quantity.</p>
  <p>Subtract 7 from each side:</p>
  <p>
    <img src="/assets/contents/GMATcard334_01.png" />
  </p>
  <p>Divide both sides by –3 and reverse the inequality sign:</p>
  <p>
    <img src="/assets/contents/GMATcard334_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 336,'335',13,NULL,'<div class="prodid-question">
  <p> Solve for <i>x</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard335_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p> To eliminate the fraction, multiply both sides of the inequality by 4: 12 –
						<i>x</i> ≥ 8. </p>
  <p> Subtract 12 from both sides: –<i>x</i> ≥ –4. </p>
  <p> Multiply (or divide) both sides by –1 and change the direction of the inequality
					sign: <i>x</i> ≤ 4. </p>
  <p>
    <img src="/assets/contents/GMATcard335_02.png" />
  </p>
  <p> As you can see from the number line, the range of values that satisfies this
					inequality includes 4 and all numbers less than 4.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 337,'336',14,NULL,'<div class="prodid-question">
  <p>What is an integer?</p>
</div>','<div class="prodid-answer">
  <p> An integer is a <i>positive whole number, a negative whole number, </i>or
						<i>zero</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 338,'337',14,NULL,'<div class="prodid-question">
  <p>True or False: When an integer is added to, subtracted from, or multiplied by
					another integer, the result must be an integer.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True.</i> When only integers are involved in addition, subtraction, and
					multiplication, the resulting values will also be integers. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 339,'338',14,NULL,'<div class="prodid-question">
  <p>What is the only operation that can produce a noninteger from two integers?</p>
</div>','<div class="prodid-answer">
  <p> The only operation that can produce a noninteger from two integers is
						<i>division</i>. When two integers are multiplied, added, or subtracted, the
					result will always be an integer. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 340,'339',14,NULL,'<div class="prodid-question">
  <p> Which of the following is not an integer: <img src="/assets/contents/GMATcard339_01.png" />? </p>
</div>','<div class="prodid-answer">
  <p> An integer is defined as a whole number (without decimal or fraction parts). Of
					the listed radicals, the only number that cannot be simplified to a whole number
					is <img src="/assets/contents/GMATcard339_02.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 341,'340',14,NULL,'<div class="prodid-question">
  <p>Complete the following rules for odds and evens:</p>
  <p>Odd + Odd =</p>
  <p>Even + Even =</p>
  <p>Odd + Even =</p>
  <p>Odd × Odd =</p>
  <p>Even × Even =</p>
  <p>Odd × Even =</p>
</div>','<div class="prodid-answer">
  <p>Odd + Odd = Even</p>
  <p>Even + Even = Even</p>
  <p>Odd + Even = Odd</p>
  <p>Odd × Odd = Odd</p>
  <p>Even × Even = Even</p>
  <p>Odd × Even = Even</p>
  <p> Note that multiplying any even number by <i>any</i> integer always produces
					another even number. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 342,'341',14,NULL,'<div class="prodid-question">
  <p>Is the result positive or negative when two numbers with the same sign are
					multiplied or divided? When two numbers with different signs are multiplied or
					divided?</p>
</div>','<div class="prodid-answer">
  <p> When two numbers with the same sign are multiplied or divided, the result is
						<i>positive</i>. When two numbers with different signs are multiplied or
					divided, the result is <i>negative</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 343,'342',14,NULL,'<div class="prodid-question">
  <p> Do <i>x </i>and <i>y </i>have identical or different signs if <i>xy</i> &gt; 0?
					If <i>xy</i> &lt; 0? </p>
</div>','<div class="prodid-answer">
  <p> If <i>xy</i> &gt; 0, then <i>x</i> and <i>y</i> have <i>identical </i>signs. </p>
  <p> If<i> xy</i> &lt; 0, then <i>x</i> and <i>y</i> have <i>different</i> signs.
				</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 344,'343',14,NULL,'<div class="prodid-question">
  <p>What is a multiple?</p>
</div>','<div class="prodid-answer">
  <p> A multiple is <i>the product of a specified number and an integer</i>. For
					example, 3, 12, and 90 are all multiples of 3: 3 = (3)(1); 12 = (3)(4); and 90 =
					(3)(30). The number 4, for example, is not a multiple of 3 because there is no
					integer that can be multiplied by 3 and yield 4. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 345,'344',14,NULL,'<div class="prodid-question">
  <p>What are the factors of 36?</p>
</div>','<div class="prodid-answer">
  <p>Thirty-six has nine distinct factors: 1, 2, 3, 4, 6, 9, 12, 18, and 36.</p>
  <p>You can group these factors in pairs:</p>
  <p>(1)(36) = (2)(18) = (3)(12) = (4)(9) = (6)(6)</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 346,'345',14,NULL,'<div class="prodid-question">
  <p>What is the greatest factor of 16 that is also a factor of 80?</p>
</div>','<div class="prodid-answer">
  <p>16</p>
  <p>The greatest factor of any number is itself. Because 16 is also a factor of 80,
					16 is the greatest common factor of both numbers.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 347,'346',14,NULL,'<div class="prodid-question">
  <p>How can you determine whether a number is divisible by 2? 3? 4? 5? 6? 9? 10?</p>
</div>','<div class="prodid-answer">
  <p>A number is divisible by . . .</p>
  <ul>
    <li>2 if the number is even;</li>
    <li>3 if the sum of the digits is divisible by 3;</li>
    <li>4 if the two-digit number formed by the last two digits is divisible by
						4;</li>
    <li>5 if the last digit is 0 or 5;</li>
    <li>6 if the number is divisible by both 2 and 3;</li>
    <li>9 if the sum of the digits is divisible by 9; and</li>
    <li>10 if the last digit is zero.</li>
  </ul>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 348,'347',14,NULL,'<div class="prodid-question">
  <p>True or False: Many complex multiplication and division problems can be
					simplified by reducing the terms to their prime factors.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True</i>. On the GMAT, test takers are rarely expected to do complex
					arithmetic. Most scary-looking division and multiplication will end up reducing
					to a simpler form once the prime factors of the terms are identified and
					canceled out. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 349,'348',14,NULL,'<div class="prodid-question">
  <p>What is the smallest prime number?</p>
</div>','<div class="prodid-answer">
  <p>2</p>
  <p>A prime number is a positive integer with exactly two factors: 1 and itself.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 350,'349',14,NULL,'<div class="prodid-question">
  <p>True or False: All prime numbers are odd.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. There is one even prime number: 2. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 351,'350',14,NULL,'<div class="prodid-question">
  <p>True or False: 1 is a prime number.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. A prime number is divisible only by “1 and itself”—in the case of
					the number 1, “1 and itself” are the same number. Thus, the smallest prime is 2.
				</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 352,'351',14,NULL,'<div class="prodid-question">
  <p>How do you find the least common multiple (LCM) of two or more numbers?</p>
</div>','<div class="prodid-answer">
  <p>To find the LCM of two or more numbers, take the prime factorization of each
					number, then find the product of each prime factor raised to the highest power
					in each of the numbers. For 60 and 96:</p>
  <p> 60 = 2<sup>2</sup> × 3 × 5 </p>
  <p> 96 = 2<sup>5</sup> × 3 </p>
  <p> LCM = 2<sup>5</sup> × 3 × 5 = 480 </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 353,'352',14,NULL,'<div class="prodid-question">
  <p>What is the least common multiple of 6 and 8?</p>
</div>','<div class="prodid-answer">
  <p>Start by finding the prime factors of 6 and 8:</p>
  <p>6 = (2)(3)</p>
  <p>8 = (2)(2)(2)</p>
  <p>You see 2 appears as a factor three times in the prime factorization of 8, while
					3 appears as only a single factor of 6. So the least common multiple of 6 and 8
					will be (2)(2)(2)(3), or 24.</p>
  <p>Note that the least common multiple of two integers is smaller than their product
					if they have any factors in common. For instance, the product of 6 and 8 is 48,
					but their least common multiple is only 24.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 354,'353',14,NULL,'<div class="prodid-question">
  <p>What is the prime factorization of 210?</p>
</div>','<div class="prodid-answer">
  <p>Use a factor tree to find the prime factorization of 210:</p>
  <p>
    <img src="/assets/contents/GMATcard353.png" />
  </p>
  <p>Expressed as the product of its prime factors, 210 = 2 × 3 × 5 × 7.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 355,'354',14,NULL,'<div class="prodid-question">
  <p> How can you Pick Numbers for a number with remainder<i> n </i>when divided by
						<i>k</i>? </p>
</div>','<div class="prodid-answer">
  <p> To Pick Numbers for a number with remainder<i> n </i>when divided by <i>k</i>,
					take a multiple of<i> k </i>and add <i>n</i>. Because every number is a multiple
					of itself,<i> k </i>+<i> n </i>is often the easiest number to pick. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 356,'355',14,NULL,'<div class="prodid-question">
  <p> When<i> n </i>is divided by 7, the remainder is 5. What is the remainder when
						2<i>n</i> is divided by 7? </p>
</div>','<div class="prodid-answer">
  <p> You can answer this question using Picking Numbers. Find a number that leaves a
					remainder of 5 when divided by 7. A good choice would be 12, which is simply 5
					more than 7, or <img src="/assets/contents/GMATcard355.png" /> plus a remainder
					of 5. If<i> n </i>= 12, then 2<i>n</i> = 24, which, when divided by 7, leaves a
					remainder of 3. No matter what permissible value you pick for <i>n</i>, the
					result would be the same. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 357,'356',14,NULL,'<div class="prodid-question">
  <p>Does a prime number multiplied by another prime number always equal an odd
					number?</p>
</div>','<div class="prodid-answer">
  <p>Usually this is true, because all prime numbers greater than 2 are odd, but 2 is
					also a prime number (the smallest, and only even, prime). Thus, a prime number
					multiplied by 2 (another prime) would be even.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 358,'357',14,NULL,'<div class="prodid-question">
  <p>What is the value of a number in a sequence related to?</p>
</div>','<div class="prodid-answer">
  <p> The value of a number in a sequence is related to its <i>position</i> in the
					list. Sequences are often represented on the GMAT with subscripts as follows:
						<i>S</i><sub>1</sub>, <i>S</i><sub>2</sub>, <i>S</i><sub>3</sub>,…<i>S</i><sub><i>n</i></sub>,… </p>
  <p> The subscript part of the notation gives you the position of each term in the
					series. <i>S</i><sub>1</sub> is the first term in the list, <i>S</i><sub>2</sub>
					is the second term in the list, and so on. You will be given a formula that
					defines each term. </p>
  <p> For example, if you are told that <i>S</i><sub><i>n</i></sub> = 2<i>n</i> + 1,
					then the sequence would be (2 × 1) + 1, (2 × 2) + 1, (2 × 3) + 1, ..., or 3, 5,
					7, .... </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 359,'358',15,NULL,'<div class="prodid-question">
  <p>True or False:</p>
  <ol>
    <li>You can determine a ratio from two quantities.</li>
    <li>You need at least one quantity in a ratio to determine the other
						quantities.</li>
  </ol>
</div>','<div class="prodid-answer">
  <ol>
    <li>
      <i>True</i>. Any two quantities form a ratio. </li>
    <li>
      <i>True</i>. A ratio can have any number of numerical values. You need at
						least one value, along with the ratio itself, to determine the other
						values.</li>
  </ol>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 360,'359',15,NULL,'<div class="prodid-question">
  <p>The ratio of boys to girls is 3 to 4. If there are 135 boys, how many girls are
					there?</p>
</div>','<div class="prodid-answer">
  <p> When using a ratio to determine an actual number, set up a proportion, with the
					number of girls represented by <i>x</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard359.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 361,'360',15,NULL,'<div class="prodid-question">
  <p>The ratio of domestic sales revenue to foreign sales revenue of a certain product
					is 3:5. What fraction of the total sales revenue comes from domestic sales?</p>
</div>','<div class="prodid-answer">
  <p>In a part-to-part ratio, the total number of parts in the whole is determined by
					adding all the parts together. In this example, the parts are domestic sales
					revenue and foreign sales revenue; the ratio of 3:5 means there are 3 + 5 = 8
					parts in the total revenue.</p>
  <p> The fraction of the total sales revenue that comes from domestic revenue is
					therefore 3 of the 8 parts, or <img src="/assets/contents/GMATcard360.png" /> of
					the total revenue. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 362,'361',15,NULL,'<div class="prodid-question">
  <p> The ratio of <i>a</i> to <i>b</i> is 7:3. The ratio of <i>b</i> to <i>c</i> is
					2:5. What is the ratio of <i>a</i> to <i>c</i>? </p>
</div>','<div class="prodid-answer">
  <p> To solve combined ratios, <i>multiply one or both ratios by whatever you need to
						in order to get the terms they have in common to match</i>. That is, find
					the least common multiple of the common term. </p>
  <p> Multiply each member of <i>a</i>:<i>b</i> by 2 and multiply each member of
						<i>b</i>:<i>c</i> by 3 and you get <i>a</i>:<i>b</i> = 14:6 and
						<i>b</i>:<i>c</i> = 6:15. Now that the <i>b</i><i />’s match, you can just
					take <i>a</i> and <i>c</i> and say <i>a</i>:<i>c</i> = 14:15. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 363,'362',15,NULL,'<div class="prodid-question">
  <p> The ratio of <i>x</i> to <i>y</i> is 5:4. The ratio of <i>y</i> to <i>z</i> is
					1:2. What is the ratio of <i>x</i> to <i>z</i>? </p>
</div>','<div class="prodid-answer">
  <p> You want the <i>y</i><i />’s in the two ratios to equal each other, because then
					you can combine the <i>x</i>:<i>y</i> ratio and the <i>y</i>:<i>z</i> ratio to
					form the <i>x</i>:<i>y</i>:<i>z</i> ratio that you need to answer this question.
					To make the <i>y</i><i />’s equal, you can multiply the second ratio by 4. When
					you do so, you must perform the multiplication on both components of the ratio. </p>
  <p> Because a ratio is a constant proportion, it can be multiplied or divided by any
					number without losing its meaning, as long as the multiplication and division
					are applied to all the components of the ratio. In this case, you find that the
					new ratio for <i>y</i> to <i>z</i> is 4:8. You can combine this with the first
					ratio to find a new <i>x</i> to <i>y</i> to <i>z</i> ratio of 5:4:8. Therefore,
					the ratio of <i>x</i> to <i>z</i> is 5:8. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 364,'363',15,NULL,'<div class="prodid-question">
  <p>If the ratio of loaves of bread to bagels in a bakery is 1:3, and there are 60
					bagels, how many loaves of bread are there?</p>
</div>','<div class="prodid-answer">
  <p> Translate the ratio into algebra: The ratio of loaves of bread to bagels can be
					expressed as 1<i>x</i>:3<i>x</i>, where <i>x</i> is the constant by which the
					ratio is multiplied. </p>
  <p> The question states that there are 60 bagels. Therefore, 3<i>x</i> = 60. Solve
					for <i>x</i> by dividing both sides by 3; you get <i>x</i> = 20. Now, use this
					value of <i>x</i> to solve for the number of loaves of bread: </p>
  <p> 1<i>x</i> = 1(20) = 20 loaves of bread </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 365,'364',15,NULL,'<div class="prodid-question">
  <p>If the ratio of sparrows to the total number of birds in an aviary is 3:7, and
					there are 12 sparrows in the aviary, how many total birds does the aviary
					contain, not including sparrows?</p>
</div>','<div class="prodid-answer">
  <p>The ratio of sparrows to total birds is 3:7, and there are 12 sparrows. Since 3 ×
					4 = 12, this means that each number in the ratio is multiplied by 4 to yield the
					actual number of birds. Therefore, there are 7 × 4 = 28 total birds. Subtract
					the 12 sparrows from the 28 total birds to find that there are a total of 16
					birds in the aviary that aren’t sparrows.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 366,'365',15,NULL,'<div class="prodid-question">
  <p>What is the percent formula?</p>
</div>','<div class="prodid-answer">
  <p>
    <sub>
      <img src="/assets/contents/GMATcard365.png" />
    </sub>
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 367,'366',15,NULL,'<div class="prodid-question">
  <p>What is 12 percent of 25?</p>
</div>','<div class="prodid-answer">
  <p>Find the part:</p>
  <p>
    <img src="/assets/contents/GMATcard366.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 368,'367',15,NULL,'<div class="prodid-question">
  <p> Fifteen is <img src="/assets/contents/GMATcard367_01.png" /> percent of what
					number? </p>
</div>','<div class="prodid-answer">
  <p>Find the whole:</p>
  <p>
    <img src="/assets/contents/GMATcard367_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 369,'368',15,NULL,'<div class="prodid-question">
  <p>Ben spends $30 of his annual gardening budget on seed. If his total annual
					gardening budget is $150, what percentage of his budget does he spend on
					seed?</p>
</div>','<div class="prodid-answer">
  <p>This problem specifies the whole ($150) and the part ($30) and asks for the
					percentage. Plugging those numbers into the percent formula gives you:</p>
  <p>
    <img src="/assets/contents/GMATcard368.png" />
  </p>
  <p>Ben spends 20% of his annual gardening budget on seed.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 370,'369',15,NULL,'<div class="prodid-question">
  <p>What is the formula for percent change?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard369.png" />
  </p>
  <p> In the case of a percent increase, the percent change from the original value to
					the new value will always be exactly 100% less than the new value <i>as a
						percentage of</i> the original value. On the GMAT, beware of answers exactly
					100% apart. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 371,'370',15,NULL,'<div class="prodid-question">
  <p> What are the formulas for <i>percent increase</i> and <i>percent decrease</i>?
				</p>
</div>','<div class="prodid-answer">
  <p>The formulas for percent increase and percent decrease are alternative ways of
					understanding and applying the percent change formula:</p>
  <p>
    <img src="/assets/contents/GMATcard370.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 372,'371',15,NULL,'<div class="prodid-question">
  <p>Two years ago, 450 seniors graduated from Inman High School. Last year, 600
					seniors graduated. By what percentage did the number of graduating seniors
					increase?</p>
</div>','<div class="prodid-answer">
  <p>To find the difference, just subtract the original from the new. Note that the
					“original” is the base from which change occurs. It may or may not be the first
					number mentioned in the problem.</p>
  <p> The original is the figure from the earlier time (two years ago): 450. The
					increase is 600 – 450, or 150. So the percentage increase is <img src="/assets/contents/GMATcard371.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 373,'372',15,NULL,'<div class="prodid-question">
  <p>If the price of a $120 dress is increased by 25 percent, what is the new selling
					price?</p>
</div>','<div class="prodid-answer">
  <p>To find the new whole, you’ll first have to find the amount of increase. The
					original whole is $120, and the percent increase is 25%. Plugging in, you find
					that:</p>
  <p>
    <img src="/assets/contents/GMATcard372.png" />
  </p>
  <p>The amount of increase is $30, so the new selling price is $120 + $30, or
					$150.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 374,'373',15,NULL,'<div class="prodid-question">
  <p>The price of an antique is reduced by 20 percent, and then this price is reduced
					by 10 percent. If the antique originally cost $200, what is its final price?</p>
</div>','<div class="prodid-answer">
  <p> The most common mistake in this kind of problem is to reduce the original price
					by a total of 20% + 10%, or 30%. That would make the final price 70 percent of
					the original, or 70% ($200) = $140. This is <i>not</i> the correct answer. In
					this example, the second (10%) price reduction is taken off of the first sale
					price—the new whole, not the original whole. </p>
  <p>To get the correct answer, first find the new whole. You can find it by
					calculating either [$200 – (20% of $200)] or [80% ($200)]. Either way, you will
					find that the first sale price is $160. That price then has to be reduced by 10
					percent. Either calculate [$160 – (10% of $160)] or [90% ($160)]. In either
					case, the final price of the antique is $144.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 375,'374',15,NULL,'<div class="prodid-question">
  <p>A price rises by 10 percent one year and by 20 percent the next. What’s the total
					percent increase?</p>
</div>','<div class="prodid-answer">
  <p>On percent problems with unspecified values, use Picking Numbers and choose 100
					as the original value. Here, say the original price is $100.</p>
  <p>Year one:</p>
  <p>$100 + (10% of $100) = $100 + $10 = $110</p>
  <p>Year two:</p>
  <p>$110 + (20% of $110) = $110 + $22 = $132</p>
  <p>From $100 to $132—that’s a 32 percent increase.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 376,'375',15,NULL,'<div class="prodid-question">
  <p>After a change in management, production at a factory decreases 30%. Two weeks
					later, a new system of incentives is introduced, increasing production by 80%.
					What is the total percentage change in production?</p>
</div>','<div class="prodid-answer">
  <p>When dealing with an unknown number in a percent problem, use the Picking Numbers
					strategy and pick the number 100. Here, 100 would be equal to the original level
					of production.</p>
  <p>
    <img src="/assets/contents/GMATcard375.png" />
  </p>
  <p>Picking 100 as the original level of production allows you calculate the total
					percent change easily: 126 – 100 = 26, which is 26% of 100.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 377,'376',16,NULL,'<div class="prodid-question">
  <p>Define median, mode, and range.</p>
</div>','<div class="prodid-answer">
  <p> The median is the <i>middle number</i>, or the <i>average of the two middle
						numbers</i>, of a list of numbers arranged in <i>ascending or descending
						order</i>. </p>
  <p> The mode is the <i>number that appears most often on the list</i>. </p>
  <p> The range is <i>the positive difference between the largest number and smallest
						number on the list.</i></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 378,'377',16,NULL,'<div class="prodid-question">
  <p>What is a standard deviation?</p>
</div>','<div class="prodid-answer">
  <p>Standard deviation is a measure of the dispersion of a group of numbers. The
					standard deviation is found using a formula based on the distances of the
					numbers from the average.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 379,'378',16,NULL,'<div class="prodid-question">
  <p>How is standard deviation tested on the GMAT?</p>
</div>','<div class="prodid-answer">
  <p>Standard deviation measures the dispersal, or spread, of numbers around the
					average. You will almost never be required to calculate standard deviation on
					the GMAT, but you do need to have a strong conceptual understanding of how
					standard deviation works. To calculate the standard deviation of a set of
					numbers:</p>
  <ol>
    <li>Find the mean.</li>
    <li>Find the differences between the mean and each of the numbers.</li>
    <li>Square each of the differences.</li>
    <li>Find the average of the squared differences.</li>
    <li>Take the square root of the average.</li>
  </ol>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 380,'379',16,NULL,'<div class="prodid-question">
  <p>What are the median, mode, and range of the following set of numbers?</p>
  <p>{1, 3, 1, 17, –4}</p>
</div>','<div class="prodid-answer">
  <p>Put the numbers in order: –4, 1, 1, 3, 17.</p>
  <p>The median is the middle of the list, 1.</p>
  <p>The mode is the number that shows up the most, 1.</p>
  <p>The range is the biggest number minus the smallest number: 17 – (–4) = 21.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 381,'380',16,NULL,'<div class="prodid-question">
  <p>What are the mean, median, and mode of the following set of numbers?</p>
  <p>{5, 8, 23, 47, 47, 62}</p>
</div>','<div class="prodid-answer">
  <p>The mean of a set is the set’s average. This set’s mean is</p>
  <p>
    <img src="/assets/contents/GMATcard380_01.png" />, or 32. </p>
  <p>The median of a set is the set’s middle number when arranged in ascending or
					descending order. When a set has an even number of integers, the median is the
					average of the two middle numbers. This set’s median is:</p>
  <p>
    <img src="/assets/contents/GMATcard380_02.png" />, or 35. </p>
  <p>The mode of a set is the most frequently occurring number of the set. This set’s
					mode is 47.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 382,'381',16,NULL,'<div class="prodid-question">
  <p>What are consecutive numbers?</p>
</div>','<div class="prodid-answer">
  <p> Consecutive numbers are <i>numbers of a certain type, following one another
						without interruption</i>. Numbers may be consecutive in ascending or
					descending order. The GMAT prefers to test consecutive integers (e.g., –2, –1,
					0, 1, 2, 3, ...), but you may encounter other types of consecutive numbers. </p>
  <p>For example, –4, –2, 0, 2, 4, 6, ... is a series of consecutive even numbers. The
					sequence –3, 0, 3, 6, 9, ... is a series of consecutive multiples of 3. The
					sequence 2, 3, 5, 7, 11, ... is a series of consecutive prime numbers. Most
					consecutive sequences contain evenly spaced terms, although certain sequences,
					such as consecutive primes or consecutive perfect squares, are not evenly
					spaced.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 383,'382',16,NULL,'<div class="prodid-question">
  <p>True or False: In a list of consecutive integers, the mean equals the median.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>True.</i>
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 384,'383',16,NULL,'<div class="prodid-question">
  <p>What must you multiply to find the sum of the terms in a consecutive
					sequence?</p>
</div>','<div class="prodid-answer">
  <p> To find the sum of the terms in a consecutive sequence (or any sequence of
					evenly spaced terms), multiply the<i> average of the smallest and largest
						numbers in the sequence </i>by the <i>number of terms in the sequence</i>.
				</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 385,'384',16,NULL,'<div class="prodid-question">
  <p>What is the sum of the integers 105 through 225 inclusive?</p>
</div>','<div class="prodid-answer">
  <p> The average of the smallest and largest terms in this set of consecutive
					integers is <img src="/assets/contents/GMATcard384.png" />. </p>
  <p>The number of terms in this set is 225 – 105 + 1 = 121.</p>
  <p>The sum of the integers is thus 165 × 121, or 19,965.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 386,'385',16,NULL,'<div class="prodid-question">
  <p>What is a factorial?</p>
</div>','<div class="prodid-answer">
  <p> A factorial is a mathematical operation designated by an exclamation point (!).
						If<i> n </i>is an integer greater than 1, then<i> n </i>factorial, denoted
					by <i>n</i>!, is defined as the product of all the integers from 1 to <i>n</i>. </p>
  <p>For example:</p>
  <p>2! = 2 × 1 = 2</p>
  <p>3! = 3 × 2 × 1 = 6</p>
  <p>4! = 4 × 3 × 2 × 1 = 24</p>
  <p>By definition, 0! = 1! = 1.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 387,'386',16,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard386_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard386_02.png" />
  </p>
  <p>Also note: 6! = 6 × 5! = 6 × 5 × 4!, etc. Most GMAT factorial problems can be
					handled more efficiently by factoring and/or canceling.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 388,'387',16,NULL,'<div class="prodid-question">
  <p>What does a combination question ask for?</p>
</div>','<div class="prodid-answer">
  <p> A combination question asks you <i>how many unordered subgroups can be formed
						from a larger group</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 389,'388',16,NULL,'<div class="prodid-question">
  <p>What is the combination formula?</p>
</div>','<div class="prodid-answer">
  <p>Some combination questions use numbers that make quick, noncomputational solving
					difficult. In these cases, use the combination formula:</p>
  <p>
    <img src="/assets/contents/GMATcard388.png" />
  </p>
  <p> where<i> n </i>is the number of items in the group as a whole and<i> k </i>is
					the number of items in each subgroup formed. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 390,'389',16,NULL,'<div class="prodid-question">
  <p>What are permutations?</p>
</div>','<div class="prodid-answer">
  <p>
    <i>Within any group of items or people, there are multiple arrange­ments</i>, or
					permutations, possible. For instance, within a group of three items <i>A</i>,
						<i>B</i>, and <i>C</i>, there are six permutations: <i>ABC</i>, <i>ACB</i>,
						<i>BAC</i>, <i>BCA</i>, <i>CAB</i>, and <i>CBA</i>. </p>
  <p> Permutations differ from combinations in that permutations are ordered—that is,
					the same <i>combination </i>of entities chosen in a different order counts as a
					separate <i>permutation</i>. </p>
  <p>Often, permutations questions ask for the number of differently ordered subgroups
					that can be drawn from a larger group.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 391,'390',16,NULL,'<div class="prodid-question">
  <p>What is the formula for permutations?</p>
</div>','<div class="prodid-answer">
  <p>If you’re asked to find the number of ways to arrange a smaller group that’s
					being drawn from a larger group, use the permutation formula:</p>
  <p>
    <img src="/assets/contents/GMATcard390.png" />
  </p>
  <p> where <i>n</i> is the number of items in the group as a whole and <i>k</i> is
					the number of items in each subgroup formed. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 392,'391',16,NULL,'<div class="prodid-question">
  <p>How many distinct three-digit numbers can be formed with the digits 1, 3, and 5
					using each only once?</p>
</div>','<div class="prodid-answer">
  <p> For a permutation question in which all items in the group are used in each
					arrangement (i.e., there are no subgroups), use the arrangement formula:
						<i>n</i>!. </p>
  <p> In this case, there are three distinct digits, so <i>n</i> = 3. The number of
					arrangements is 3! = 3 × 2 × 1 = 6. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 393,'392',16,NULL,'<div class="prodid-question">
  <p> Restaurant A has 5 appetizers, 20 main courses, and 4 desserts. If a meal
					consists of 1 appetizer, 1 main course, and 1 dessert, how many different meals
					can be ordered at Restaurant A? </p>
</div>','<div class="prodid-answer">
  <p>Because you’re only choosing one of each item, the number of possible outcomes
					from each set is the number of items in that set. There are 5 possible
					appetizers, 20 possible main courses, and 4 possible desserts. The number of
					different meals that can be ordered is (5)(20)(4) = 400.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 394,'393',16,NULL,'<div class="prodid-question">
  <p>A customer wants to buy three different fruits. If there are seven different
					fruits available at the market, how many different ways can he choose his
					fruit?</p>
</div>','<div class="prodid-answer">
  <p> Because the order of the fruits chosen does not matter, this is a combination
					question. The combinations formula is: <img src="/assets/contents/GMATcard393_01.png" />, where<i> n </i>= the number of
					fruits available, and<i> k </i>is the number of fruits being picked. In this
					case, <i>n</i> = 7 and <i>k</i> = 3. </p>
  <p>
    <img src="/assets/contents/GMATcard393_02.png" />
  </p>
  <p>There are 35 ways to choose 3 different fruits from 7 options.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 395,'394',16,NULL,'<div class="prodid-question">
  <p>What formula is used in all probability questions?</p>
</div>','<div class="prodid-answer">
  <p>Every probability question involves the formula:</p>
  <p>
    <img src="/assets/contents/GMATcard394.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 396,'395',16,NULL,'<div class="prodid-question">
  <p> How do you determine the probability of something <i>not</i> happening? </p>
</div>','<div class="prodid-answer">
  <p> To find the probability that an event does <i>not</i> happen, subtract the
					probability that the event happens from 1: </p>
  <p>
    <img src="/assets/contents/GMATcard395.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 397,'396',16,NULL,'<div class="prodid-question">
  <p>What are the different ways in which probability can be expressed? What are the
					maximum and minimum probabilities of a given event occurring?</p>
</div>','<div class="prodid-answer">
  <p> The probability formula is expressed as a ratio of the number of desired
					outcomes to the total number of possible outcomes. Probability is usually
					expressed as a fraction (for example “the probability of event A occurring is
						<img src="/assets/contents/GMATcard396.png" />”), a decimal (0.25), or a
					percentage (25%), but it can also be expressed in words (“the probability of
					event A occurring is 1 in 4”). </p>
  <p>The probability of any event occurring cannot exceed 1 (a probability of 1
					represents a 100% chance of an event occurring) and cannot be less than 0
					(a probability of 0 represents a 0% chance of an event occurring).</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 398,'397',16,NULL,'<div class="prodid-question">
  <p>What is the probability of throwing a 5 on a fair six-sided die?</p>
</div>','<div class="prodid-answer">
  <p> There is 1 favorable outcome—throwing a 5. There are 6 possible outcomes—one for
					each side of the die. The probability is <img src="/assets/contents/GMATcard397.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 399,'398',16,NULL,'<div class="prodid-question">
  <p>A fair coin is flipped twice. What is the probability of it landing with the
					heads side facing up on both flips?</p>
</div>','<div class="prodid-answer">
  <p> When events are independent, that is, <i>the chance of an event occurring does
						not depend on the result of other event(s)</i>, the probability that several
					events all occur is the product of the probability of each event occurring
					individually. </p>
  <p> Multiply the probability for each flip: <img src="/assets/contents/GMATcard398.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 400,'399',16,NULL,'<div class="prodid-question">
  <p>If 2 students are chosen at random, without replacement, from a class with 5
					girls and 5 boys, what is the probability that both students chosen will
					be girls?</p>
</div>','<div class="prodid-answer">
  <p> The probability that the first student chosen will be a girl is <img src="/assets/contents/GMATcard399_01.png" />. Because there would be 4 girls
					and 5 boys remaining (9 total students), the probability that the second student
					chosen will be a girl (given that the first student chosen is a girl) is <img src="/assets/contents/GMATcard399_02.png" />. Thus the probability that both
					students chosen will be girls is <img src="/assets/contents/GMATcard399_03.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 401,'400',16,NULL,'<div class="prodid-question">
  <p>What is the probability of throwing a 2, a 5, and a 3, in that order, on a fair
					six-sided die?</p>
</div>','<div class="prodid-answer">
  <p> There are 3 independent trials with 6 possible outcomes—one for each side of the
					die. The probability of throwing any one particular number is <img src="/assets/contents/GMATcard400_01.png" />. Thus, the probability of the
					desired outcome on three consecutive trials is <img src="/assets/contents/GMATcard400_02.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 402,'401',16,NULL,'<div class="prodid-question">
  <p>What is the probability of throwing a 2, a 5, and a 3, in no particular order, on
					a fair six-sided die?</p>
</div>','<div class="prodid-answer">
  <p> There are 3 dependent trials, each with 6 possible outcomes—one for each side of
					the die. The probability of the first trial is <img src="/assets/contents/GMATcard401_01.png" /> (3 desired outcomes—2, 5, or 3—
					out of 6 possible outcomes). The probability of the second trial is <img src="/assets/contents/GMATcard401_02.png" /> (the 2 remaining desired
					outcomes out of 6 possible outcomes). The probability of third trial is <img src="/assets/contents/GMATcard401_03.png" />, as only one desired outcome
					remains. Thus, the probability of the desired outcomes occurring in no
					particular order is <img src="/assets/contents/GMATcard401_04.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 403,'402',16,NULL,'<div class="prodid-question">
  <p>A bucket has 50 red marbles, 25 green marbles, 15 blue marbles, and 10 orange
					marbles. If a marble is picked at random, what is the probability of picking a
					marble that is neither red nor orange?</p>
</div>','<div class="prodid-answer">
  <p>There are a total of 50 + 25 + 15 + 10 = 100 marbles in the bucket. Red and
					orange marbles make up 60 of these, so the remaining marbles would amount to
					40.</p>
  <p>
    <img src="/assets/contents/GMATcard402.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 404,'403',17,NULL,'<div class="prodid-question">
  <p>What formula is used to find an average (arithmetic mean)?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard403.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 405,'404',17,NULL,'<div class="prodid-question">
  <p>When is it possible to determine the average without knowing the exact number of
					terms?</p>
</div>','<div class="prodid-answer">
  <p> You can use the <i>weighted average formula</i> if you have averages for
					proportions of the population, even if you don’t have data specific to the
					individual members of the population. </p>
  <p>Weighted average = (Avg. of A)(Percent that are A) + (Avg. of B)(Percent that are
					B) ... + (Avg. of N)(Percent that are N)</p>
  <p>Note that to use the weighted average formula, you must have averages for all
					portions adding up to 100 percent of the whole.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 406,'405',17,NULL,'<div class="prodid-question">
  <p>What is the sum of the differences of each term on a list from the average of
					that list?</p>
</div>','<div class="prodid-answer">
  <p> The sum of the differences of each term on a list from the average of that list
					is <i>0</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 407,'406',17,NULL,'<div class="prodid-question">
  <p>June pays an average price of $14.50 for six articles of clothing. What is the
					total price of all six articles?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard406.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 408,'407',17,NULL,'<div class="prodid-question">
  <p>The total weight of the licorice sticks in a jar is 30 ounces. If the average
					weight of each licorice stick is 2 ounces, how many licorice sticks are there in
					the jar?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard407.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 409,'408',17,NULL,'<div class="prodid-question">
  <p>The average annual rainfall in Boynton for 1976–1979 was 26 inches per year.
					Boynton received 24 inches of rain in 1976, 30 inches in 1977, and 19 inches in
					1978. How many inches of rainfall did Boynton receive in 1979?</p>
</div>','<div class="prodid-answer">
  <p>If you’re given the average, the total number of terms, and all but one of the
					actual numbers, you can find the missing number.</p>
  <p>You know that total rainfall equals 24 + 30 + 19 + (inches of rain in 1979).</p>
  <p>You know that the average rainfall was 26 inches per year for 4 years.</p>
  <p>So, plug these numbers into the average formula to find that Sum of terms =
					(Average)(Number of terms):</p>
  <p>
    <img src="/assets/contents/GMATcard408.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 410,'409',17,NULL,'<div class="prodid-question">
  <p> The average of 63, 64, 85, and <i>x</i> is 80. What is the value of <i>x</i>?
				</p>
</div>','<div class="prodid-answer">
  <p> When finding an average, the <i>combined distance of the numbers above the
						average from the mean must be balanced with the combined distance of the
						numbers below the average from the mean.</i></p>
  <p>Think of each value in terms of its position relative to the average, 80:</p>
  <p>63 is 17 less than 80</p>
  <p>64 is 16 less than 80</p>
  <p>85 is 5 greater than 80</p>
  <p> So these three terms are a total of 17 + 16 – 5, or 28, less than the average.
					Therefore, <i>x</i> must be 28 greater than the average to restore the balance
					at 80. So <i>x</i> = 28 + 80 = 108. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 411,'410',17,NULL,'<div class="prodid-question">
  <p>The average individual test score for a group of girls is 30. The average
					individual test score for a group of boys is 24. If there are twice as many boys
					as girls, what is the overall average?</p>
</div>','<div class="prodid-answer">
  <p>Since you are given the average for various portions of the group, according to a
					ratio, you can use Picking Numbers to suppose that there are exactly 3 total
					students: 2 boys and 1 girl.</p>
  <p>
    <img src="/assets/contents/GMATcard410.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 412,'411',17,NULL,'<div class="prodid-question">
  <p>The average (arithmetic mean) of 24 numbers is 15. What is the sum?</p>
</div>','<div class="prodid-answer">
  <p>When using the average to find a sum, use this formula: </p>
  <p>Sum of terms = (Average)(Number of terms)</p>
  <p>Sum = 15 × 24 = 360</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 413,'412',17,NULL,'<div class="prodid-question">
  <p>Michael’s average score after four tests is 80. If he scores 100 on the fifth
					test, what’s his new average?</p>
</div>','<div class="prodid-answer">
  <p>Use the sum of the terms of the old average to help you find the new average when
					a number is added or deleted.</p>
  <p>Find the original sum from the original average:</p>
  <p>Original sum = 4 × 80 = 320</p>
  <p>Add the fifth score to make the new sum:</p>
  <p>New sum = 320 + 100 = 420</p>
  <p>Find the new average from the new sum:</p>
  <p>
    <img src="/assets/contents/GMATcard412.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 414,'413',17,NULL,'<div class="prodid-question">
  <p>How many parts must you know to solve for all three parts of a three-part
					equation (such as the percent formula, speed formula, or average formula)?</p>
</div>','<div class="prodid-answer">
  <p> You can solve for all three parts of a three-part equation (such as the percent
					formula, speed formula, or average formula) if you know the values for any
						<i>two</i> parts. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 415,'414',17,NULL,'<div class="prodid-question">
  <p> When two objects are traveling in <i>opposite</i> directions, how do you find
					their relative speed (the speed at which they move closer to or farther away
					from each other)? </p>
  <p> How do you find the relative speed of two objects traveling in the <i>same</i>
					direction? </p>
</div>','<div class="prodid-answer">
  <p> When two objects are traveling in <i>opposite</i> directions, <i>add</i> their
					speeds to find their relative speed. </p>
  <p> When two objects are traveling in the <i>same</i> direction, <i>subtract</i>
					their speeds to find their relative speed. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 416,'415',17,NULL,'<div class="prodid-question">
  <p>As an alternative to using algebra, what is a good strategy for solving a
					question that involves two objects moving toward or away from each other?</p>
</div>','<div class="prodid-answer">
  <p>Draw a picture and Backsolve: Move the objects in the picture according to the
					values given in the answer choices to see which one works to answer the question
					correctly.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 417,'416',17,NULL,'<div class="prodid-question">
  <p>What is the speed formula?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard416.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 418,'417',17,NULL,'<div class="prodid-question">
  <p>What is the average speed formula?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard417.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 419,'418',17,NULL,'<div class="prodid-question">
  <p>True or False: An easy shortcut to average-speed problems is to simply take the
					average of the given speeds.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. Averaging the given speeds is, in fact, a common wrong-answer
					trap; test takers who make this error are failing to account for the different
					amounts of time spent at each speed. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 420,'419',17,NULL,'<div class="prodid-question">
  <p>How can you rapidly eliminate answer choices on average speed questions?</p>
</div>','<div class="prodid-answer">
  <p> On average speed questions, you can rapidly eliminate answer choices by
						<i>recognizing which way the speeds are weighted</i>. If a problem involves
					more time spent at a fast speed than at a slower speed, the average speed
					will be higher than the average of the two speeds, and vice versa. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 421,'420',17,NULL,'<div class="prodid-question">
  <p>How far do you drive if you travel for 5 hours at 60 miles per hour?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard420.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 422,'421',17,NULL,'<div class="prodid-question">
  <p>How long does it take to drive 520 miles at 40 miles per hour?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard421.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 423,'422',17,NULL,'<div class="prodid-question">
  <p>How many hours will it take to fill a 500-liter tank at a rate of 2 liters per
					minute?</p>
</div>','<div class="prodid-answer">
  <p>Plug the numbers into the rate formula:</p>
  <p>
    <img src="/assets/contents/GMATcard422_01.png" />
  </p>
  <p> Now convert 250 minutes to hours: 250 minutes ÷ 60 minutes per hour = <img src="/assets/contents/GMATcard422_02.png" /> hours, or 4 hours 10 minutes, to
					fill the tank. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 424,'423',17,NULL,'<div class="prodid-question">
  <p>If 350 widgets cost $20, how much will 1,400 widgets cost at the same rate?</p>
</div>','<div class="prodid-answer">
  <p> Set up a proportion using the fraction <img src="/assets/contents/GMATcard423_01.png" />. </p>
  <p>
    <img src="/assets/contents/GMATcard423_02.png" />
  </p>
  <p> Solving algebraically, you get 350<i>x</i> = (1,400)(20), which simplifies to: </p>
  <p>
    <img src="/assets/contents/GMATcard423_03.png" />
  </p>
  <p>So, 1,400 widgets will cost $80 at the given rate.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 425,'424',17,NULL,'<div class="prodid-question">
  <p>A bus travels from Station B to Station A at 50 mph, and returns from Station A
					to Station B at 30 mph along the same path. What is the average speed of the bus
					in mph?</p>
</div>','<div class="prodid-answer">
  <p>To find the average speed, you cannot simply average the two speeds; rather, you
					must calculate the weighted average of the two legs of the journey. To do this
					you must find the total distance traveled, and divide that by the total time
					traveled. You are not given a distance here, so your best approach is to pick a
					number for the distance. Use 150 miles because 150 is a multiple of both 30 and
					50. This would give you a total distance traveled of 300 miles (150 miles in
					each direction). The time from B to A would be 150 miles divided by 50 mph, or 3
					hours. The time from A to B would be 150 miles divided by 30 mph, or 5 hours.
					This gives you a total time of 8 hours. The average speed of the bus would then
					be 300 miles divided by 8 hours, or 37.5 mph.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 426,'425',17,NULL,'<div class="prodid-question">
  <p>What is the combined work formula for two workers?</p>
</div>','<div class="prodid-answer">
  <p> The combined work formula is <img src="/assets/contents/GMATcard425.png" />.
						<i>T</i> is the total time it will take to complete one task if the two
					workers work together, and <i>A</i> and <i>B</i> are the amounts of time it
					takes each worker to complete the task alone. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 427,'426',17,NULL,'<div class="prodid-question">
  <p>What is the combined work formula for three or more workers?</p>
</div>','<div class="prodid-answer">
  <p> For three or more workers, the combined work formula is <img src="/assets/contents/GMATcard426_01.png" />. <i>T</i> is the total time it
					will take all the workers working together to complete one task, and <i>A</i>,
						<i>B</i>, ...<i> n </i>are the amounts of time it takes each of the
					individual workers to complete one task working alone. Note that solving this
					formula results in <img src="/assets/contents/GMATcard426_02.png" />, the
					reciprocal of the total time. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 428,'427',17,NULL,'<div class="prodid-question">
  <p>What is the difference between simple and compound interest?</p>
</div>','<div class="prodid-answer">
  <p>In simple interest, interest is paid only on the principal. In compound interest,
					interest is paid both on the principal and on any previously accrued
					interest.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 429,'428',17,NULL,'<div class="prodid-question">
  <p>What is the simple interest formula?</p>
</div>','<div class="prodid-answer">
  <p> (Total of principal and interest) = Principal × (1 + <i>rt</i>), where <i>r</i>
					equals the interest rate per time period, expressed as a decimal, and <i>t</i>
					equals the number of time periods. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 430,'429',17,NULL,'<div class="prodid-question">
  <p>What is the compound interest formula?</p>
</div>','<div class="prodid-answer">
  <p> (Total of principal and interest) = Principal (1 + <i>r</i>)<sup><i>t</i></sup>, where <i>t</i> is the number of times the interest is compounded and
						<i>r</i> is the interest rate per time period, expressed as a decimal. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 431,'430',17,NULL,'<div class="prodid-question">
  <p>What is the overlapping sets formula involving two groups?</p>
</div>','<div class="prodid-answer">
  <p>Total = Group 1 + Group 2 – Both + Neither</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 432,'431',17,NULL,'<div class="prodid-question">
  <p>What are the three ways to solve overlapping sets problems?</p>
</div>','<div class="prodid-answer">
  <p>1. Use the overlapping sets formula:</p>
  <p> Total = Group 1 + Group 2 <i>–</i> Both + Neither </p>
  <p>2. Draw a Venn diagram: <img src="/assets/contents/GMATcard431_01.png" /></p>
  <p>3. Make a chart: <img src="/assets/contents/GMATcard431_02.png" /></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 433,'432',18,NULL,'<div class="prodid-question">
  <p>What is an angle bisector?</p>
</div>','<div class="prodid-answer">
  <p> A line or line segment bisects an angle if it <i>splits the angle into two
						smaller</i>, <i>equal angles</i>. Line segment <i>BD</i> below bisects
						∠<i>ABC</i>, so ∠<i>ABD</i> has the same measure as ∠<i>DBC</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard432.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 434,'433',18,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard433_01.png" />
  </p>
  <p> In the diagram above, line <img src="/assets/contents/GMATcard433_02.png" /> is
					parallel to line <img src="/assets/contents/GMATcard433_03.png" />. What is the
					value of<i> x</i>? </p>
</div>','<div class="prodid-answer">
  <p> The angle marked<i> x</i>° corresponds to the angle adjacent to and to the left
					of the 70° angle on line <img src="/assets/contents/GMATcard433_02.png" />.
					Therefore, it must be supplementary to the 70° angle. Because a straight line
					equals 180°, 70° +<i> x</i>° = 180°, and <i>x </i>equals 110. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 435,'434',18,NULL,'<div class="prodid-question">
  <p>A pair of intersecting lines makes an angle of 37°. What is measurement of the
					adjacent (supplementary) angle?</p>
</div>','<div class="prodid-answer">
  <p>Supplementary angles always add up to 180°. The supplementary angle of a 37°
					angle would thus be 143°.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 436,'435',18,NULL,'<div class="prodid-question">
  <p>A pair of intersecting lines makes an angle of 37°. What is the measurement of
					its vertical angle?</p>
</div>','<div class="prodid-answer">
  <p>Vertical angles (angles formed by two intersecting lines that share a vertex but
					no common side) are always equal. Thus the vertical angle of a 37° angle would
					be 37°.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 437,'436',18,NULL,'<div class="prodid-question">
  <p>What are parallel lines?</p>
  <p>What are perpendicular lines?</p>
</div>','<div class="prodid-answer">
  <p> Parallel lines are <i>lines with the same slope</i>. If continued infinitely on
					the same plane, they will never meet. </p>
  <p> Perpendicular lines are <i>lines that meet at 90° angles</i>. Their slopes are
					negative reciprocals of each other. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 438,'437',18,NULL,'<div class="prodid-question">
  <p>What is a triangle?</p>
  <p>What is a right triangle?</p>
</div>','<div class="prodid-answer">
  <p> A triangle is <i>a polygon with three straight sides and three interior
						angles.</i></p>
  <p> A right triangle is <i>a triangle with one interior angle of 90</i>° (a right
					angle). </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 439,'438',18,NULL,'<div class="prodid-question">
  <p>What are isosceles triangles?</p>
</div>','<div class="prodid-answer">
  <p> Isosceles triangles are <i>triangles that have two equal sides</i>. The angles
					opposite these sides, or base angles, are also equal. </p>
  <p>
    <img src="/assets/contents/GMATcard438.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 440,'439',18,NULL,'<div class="prodid-question">
  <p>What are equilateral triangles?</p>
</div>','<div class="prodid-answer">
  <p> Equilateral triangles are <i>triangles with three equal sides, </i>which means
					they also have <i>three equal </i>60° <i>angles</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard439.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 441,'440',18,NULL,'<div class="prodid-question">
  <p>Given the lengths of two sides of a triangle, how do you find the minimum and
					maximum lengths for the third side?</p>
</div>','<div class="prodid-answer">
  <p> If you know the lengths of two sides of a triangle, you know that <i>the third
						side is between their positive difference and their sum</i>. </p>
  <p>For example, the lengths of two of the sides of a triangle are 7 and 3. What is
					the range of possible lengths for the third side?</p>
  <p>The third side is greater than the difference (7 – 3 = 4) and less than the sum
					(7 + 3 = 10). In other words, 4 &lt; side &lt; 10.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 442,'441',18,NULL,'<div class="prodid-question">
  <p>What does the acronym BDSS mean?</p>
</div>','<div class="prodid-answer">
  <p>BDSS is an easy way to remember the Triangle Inequality Theorem:</p>
  <p> Each side of a triangle is <b>B</b>igger than the <b>D</b>ifference (of the
					other side lengths) and <b>S</b>maller than the <b>S</b>um (of the other side
					lengths). </p>
  <p>You can remember the initials BDSS with the mnemonic “Big Daddy Says So.”</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 443,'442',18,NULL,'<div class="prodid-question">
  <p>What is the sum of the two non-right interior angles of a right triangle?</p>
</div>','<div class="prodid-answer">
  <p>All triangles have interior angles that sum to exactly 180°. A right angle
					measures 90°, so the sum of the other two interior angles of a right triangle is
					180 – 90 = 90°.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 444,'443',18,NULL,'<div class="prodid-question">
  <p>What do the interior angles of a triangle sum up to? For every additional side in
					a polygon, how many degrees must you add to the sum of the interior angles? For
					example, what will the interior angles total in a five-sided figure?</p>
</div>','<div class="prodid-answer">
  <p> The interior angles of a triangle sum up to <i>180 degrees</i>. For every side
					beyond three, add another 180 degrees. For example, a five-sided figure will
					have interior angles totaling 180° + 2(180°) = 540°. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 445,'444',18,NULL,'<div class="prodid-question">
  <p>What is the height of a triangle?</p>
</div>','<div class="prodid-answer">
  <p> The height of a triangle is <i>the perpendicular distance from a vertex to the
						side opposite the vertex</i>. The height may fall inside or outside the
					triangle, or it may coincide with one of the sides. </p>
  <p>
    <img src="/assets/contents/GMATcard444.png" />
  </p>
  <p> In the diagrams above, <i>AD</i>, <i>LK</i>, and <i>EH</i> are heights. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 446,'445',18,NULL,'<div class="prodid-question">
  <p>What is the formula for the area of a triangle?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard445.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 447,'446',18,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard446_01.png" />
  </p>
  <p>In the diagram above, the base has length 4 and the height has length 3. What is
					the area of the triangle, in square units?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard446_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 448,'447',18,NULL,'<div class="prodid-question">
  <p>How do you find the area of a right triangle?</p>
</div>','<div class="prodid-answer">
  <p> In a right triangle, think of one leg as the base and the other as the height.
					The area is one-half the product of the legs, or <img src="/assets/contents/GMATcard447_01.png" />. </p>
  <p>
    <img src="/assets/contents/GMATcard447_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 449,'448',18,NULL,'<div class="prodid-question">
  <p> Angle <i>BAC </i>of triangle <i>ABC </i>is a right angle. Is side <i>BC
					</i>longer or shorter than side <i>AB</i>? </p>
</div>','<div class="prodid-answer">
  <p> This question may seem abstract, until you draw a picture of a right triangle,
					labeling the vertex with the 90° angle as point <i>A</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard448.png" />
  </p>
  <p> Line segment <i>BC </i>has to be longer than line segment <i>AB</i>, since <i>BC
					</i>is opposite the triangle’s largest angle. </p>
  <p>The hypotenuse (the side of a right triangle opposite the right angle) is always
					the longest of a triangle’s three sides.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 450,'449',18,NULL,'<div class="prodid-question">
  <p>What is the Pythagorean theorem?</p>
</div>','<div class="prodid-answer">
  <p> The Pythagorean theorem states that for a <i>right triangle </i>(a triangle that
					contains one 90° angle)<i>, a</i><sup>2</sup> + <i>b</i><sup>2</sup> =
						<i>c</i><sup>2</sup>, where <i>a </i>and <i>b </i>are the lengths of the
					shorter two sides of the right triangle, known as legs, and <i>c </i>is the
					length of the longest side, or hypotenuse. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 451,'450',18,NULL,'<div class="prodid-question">
  <p>If a right triangle has legs (short sides) of lengths 3 and 4, what will the
					length of the longest side, or hypotenuse, be?</p>
</div>','<div class="prodid-answer">
  <p>5</p>
  <p>This is a 3:4:5 triangle, a ratio of side lengths that satisfies the Pythagorean
					theorem. Such ratios are called Pythagorean triples. These special right
					triangles occur frequently on the GMAT.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 452,'451',18,NULL,'<div class="prodid-question">
  <p>If the length of the hypotenuse (longest side) of a right triangle is 26 and one
					of the legs (short sides) has a length of 10, what is the length of the other
					leg?</p>
</div>','<div class="prodid-answer">
  <p>24</p>
  <p>10 = 5 × 2 and 26 = 13 × 2, so this right triangle is a 5:12:13 right triangle
					and the last side should be 12 × 2 = 24. Remember, the special right triangles
					are ratios; learn to recognize them even when they are multiplied by various
					constants.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 453,'452',18,NULL,'<div class="prodid-question">
  <p>What is the ratio of the lengths of the sides of a 45°-45°-90° triangle?</p>
</div>','<div class="prodid-answer">
  <p> A 45°-45°-90° triangle has sides in the ratio of <img src="/assets/contents/GMATcard452.png" />. This type of special right
					triangle occurs frequently on the GMAT. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 454,'453',18,NULL,'<div class="prodid-question">
  <p>What is the length of the hypotenuse of an isosceles right triangle with legs of
					length 4?</p>
</div>','<div class="prodid-answer">
  <p> You can use the Pythagorean theorem to find the hypotenuse, but it’s quicker to
					use the special right triangle ratios. In an isosceles right triangle, the ratio
					of leg to leg to hypotenuse is <img src="/assets/contents/GMATcard453_01.png" />.
					Because the length of a leg is 4, the length of the hypotenuse must be <img src="/assets/contents/GMATcard453_02.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 455,'454',18,NULL,'<div class="prodid-question">
  <p>What is the ratio of the lengths of the sides of a 30°-60°-90° triangle?</p>
</div>','<div class="prodid-answer">
  <p> A 30°-60°-90° triangle has sides in the ratio of <i><img src="/assets/contents/GMATcard454.png" /></i>. This type of special right triangle occurs frequently on the GMAT. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 456,'455',18,NULL,'<div class="prodid-question">
  <p>
    <b>Try this Data Sufficiency question:</b>
  </p>
  <p> What is the area of right triangle <i>ABC</i>? </p>
  <p> (1) <i>AB </i>= 5 </p>
  <p> (2) <i>BC </i>= 4 </p>
</div>','<div class="prodid-answer">
  <p> In this Data Sufficiency question, neither statement alone is sufficient. You
					may think at first that together the two statements are enough, because it looks
					like <i>ABC </i>is a 3:4:5 right triangle. Not so fast! You’re given two sides,
					but you don’t know which sides they are. If <i>AB </i>is the hypotenuse, then it
					is a 3:4:5 triangle, and the area is <img src="/assets/contents/GMATcard455.png" />(3 × 4) = 6, but it’s also possible that <i>AC</i>, the missing side, is the
					hypotenuse. In that case, the area would be <img src="/assets/contents/GMATcard455.png" />(4 × 5) = 10. Statements (1) and (2)
					together are NOT sufficient. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 457,'456',18,NULL,'<div class="prodid-question">
  <p>What is a polygon?</p>
</div>','<div class="prodid-answer">
  <p> A polygon is <i>a closed figure whose sides are straight line segments</i>. </p>
  <p> Families or classes of polygons are named according to the number of sides: A
					triangle has <i>three sides</i>; a quadrilateral has <i>four sides</i>; a
					pentagon has <i>five sides</i>; and a hexagon has <i>six sides</i>. </p>
  <p>Triangles and quadrilaterals are by far the most important polygons on the GMAT;
					other polygons appear only occasionally.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 458,'457',18,NULL,'<div class="prodid-question">
  <p>What is the perimeter of a polygon?</p>
</div>','<div class="prodid-answer">
  <p> The perimeter is <i>the distance around a polygon, or the sum of the lengths of
						its sides.</i></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 459,'458',18,NULL,'<div class="prodid-question">
  <p>What is a vertex of a polygon?</p>
</div>','<div class="prodid-answer">
  <p> A vertex (plural: vertices) of a polygon is <i>a point where two sides
						intersect</i>. Polygons are named by assigning each vertex a letter and
					listing them in order, as in pentagon <i>ABCDE </i>below. </p>
  <p>
    <img src="/assets/contents/GMATcard458.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 460,'459',18,NULL,'<div class="prodid-question">
  <p>What is a regular polygon?</p>
</div>','<div class="prodid-answer">
  <p> A regular polygon is <i>a polygon with sides of equal length and interior angles
						of equal measure</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 461,'460',18,NULL,'<div class="prodid-question">
  <p>What is the formula for finding the degree measure of one angle in a regular
					polygon?</p>
</div>','<div class="prodid-answer">
  <p> The total degree measure of the angles in a polygon is 180°(<i>n </i>– 2), where
						<i>n </i>is the number of sides in the polygon. Therefore, the degree
					measure of each angle in a regular polygon is <img src="/assets/contents/GMATcard460.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 462,'461',18,NULL,'<div class="prodid-question">
  <p>What is the name for any four-sided figure in which both pairs of opposite sides
					are parallel? What is the formula for the figure’s area?</p>
</div>','<div class="prodid-answer">
  <p> Any four-sided figure in which both pairs of opposite sides are parallel is a
						<i>parallelogram</i>. Parallelograms include <i>squares</i> and
						<i>rectangles</i>. The area of a parallelogram is its <i>base </i>times
						its <i>height</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 463,'462',18,NULL,'<div class="prodid-question">
  <p> Find the perimeter of rectangle <i>WXYZ</i> below: </p>
  <p>
    <img src="/assets/contents/GMATcard462.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>Perimeter of a rectangle = 2(length + width)</p>
  <p>The perimeter of a 5-by-2 rectangle is 2(5 + 2) = 14.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 464,'463',18,NULL,'<div class="prodid-question">
  <p> Find the perimeter of square <i>EFGH </i>below: </p>
  <p>
    <img src="/assets/contents/GMATcard463.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>Because all four sides of a square are the same length, Perimeter = 4(side). If
					the length of one side of a square is 3, the perimeter is 4 × 3 = 12.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 465,'464',18,NULL,'<div class="prodid-question">
  <p> Find the area of rectangle <i>ABCD</i> below: </p>
  <p>
    <img src="/assets/contents/GMATcard464.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>Area of a rectangle = (length)(width)</p>
  <p>The area of a 5-by-2 rectangle is 2 × 5 = 10.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 466,'465',18,NULL,'<div class="prodid-question">
  <p>What are the characteristics of a trapezoid?</p>
</div>','<div class="prodid-answer">
  <p> A trapezoid is a quadrilateral with exactly one pair of parallel sides. To find
					the area of a trapezoid, either use the formula <img src="/assets/contents/GMATcard465_01.png" />, where base<sub>1</sub> and
						base<sub>2</sub> are the lengths of the parallel sides, or break the
					trapezoid into rectangles and triangles, as shown below: </p>
  <p>
    <img src="/assets/contents/GMATcard465_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 467,'466',18,NULL,'<div class="prodid-question">
  <p>What is the sum of the angles of a polygon with 7 sides?</p>
</div>','<div class="prodid-answer">
  <p> The sum of the angles of a polygon with <i>n </i>sides is equal to 180°(<i>n</i>
					– 2). For a polygon with 7 sides, you have: </p>
  <p>
    <img src="/assets/contents/GMATcard466.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 468,'467',18,NULL,'<div class="prodid-question">
  <p>True or False: Oddly shaped figures on the GMAT often require obscure formulas to
					solve.</p>
</div>','<div class="prodid-answer">
  <p>
    <i>False</i>. Oddly shaped figures on the GMAT can almost always be divided into
					smaller, easily recognizable figures—most often right triangles and rectangles.
				</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 469,'468',18,NULL,'<div class="prodid-question">
  <p>What are the radius and diameter of a circle?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard468.png" />
  </p>
  <p> A diameter is a <i>line segment that connects two points on the circle and
						passes through the center of the circle</i>. <i>AB </i>is a diameter of
					circle <i>O </i>above. </p>
  <p> A radius (plural: radii) is <i>a line segment that connects the center of the
						circle with any point on the circle</i>. The radius of a circle is one-half
					the length of the diameter. In circle <i>O </i>above, <i>OA</i>, <i>OB</i>, and
						<i>OC </i>are radii. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 470,'469',18,NULL,'<div class="prodid-question">
  <p>What is the circumference of a circle?</p>
  <p>What is the formula for circumference?</p>
</div>','<div class="prodid-answer">
  <p> The circumference of a circle is the <i>distance around the circle</i>. The
					formula for circumference is <i>C </i>= 2<img src="/assets/contents/GMATcard_pi.png" /><i>r</i>, where <i>r </i>is the
					radius of the circle. The formula can also be expressed as <i>C </i>= <img src="/assets/contents/GMATcard_pi.png" /><i>d</i>, where <i>d </i>is the
					diameter of the circle (as the diameter is twice the length of the radius). </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 471,'470',18,NULL,'<div class="prodid-question">
  <p>What is the formula for the area of a circle?</p>
</div>','<div class="prodid-answer">
  <p> The formula for the area of a circle is <i>A</i> = <img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup>, where
					<i>r</i> is the radius of the circle. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 472,'471',18,NULL,'<div class="prodid-question">
  <p>What is the area of a circle with a radius of 4?</p>
</div>','<div class="prodid-answer">
  <p> The area of a circle is found with <img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup>. Here the radius is 4, so the area is: </p>
  <p>
    <img src="/assets/contents/GMATcard_pi.png" /> (4)<sup>2</sup> = 16<img src="/assets/contents/GMATcard_pi.png" /></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 473,'472',18,NULL,'<div class="prodid-question">
  <p>What do you call an arc that is exactly half the circumference of its circle?</p>
</div>','<div class="prodid-answer">
  <p> An arc that is exactly half the circumference of its circle is called a
						<i>semicircle</i>. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 474,'473',18,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard473.png" />
  </p>
  <p> In circle <i>O </i>above, what is the length of arc <i>ABC</i>? </p>
</div>','<div class="prodid-answer">
  <p> The formula for circumference is C = 2<img src="/assets/contents/GMATcard_pi.png" /><i>r</i>; therefore, if <i>r </i>=
					6, <i>C </i>= 2 × <img src="/assets/contents/GMATcard_pi.png" /> × 6 = 12<img src="/assets/contents/GMATcard_pi.png" />. Since angle <i>AOC </i>measures
					60°, arc <i>ABC </i>is <img src="/assets/contents/GMATcard473_02.png" /> or <img src="/assets/contents/GMATcard473_03.png" /> of the circumference. Thus, the
					length of arc <i>ABC </i>is <img src="/assets/contents/GMATcard473_03.png" /> ×
						12<img src="/assets/contents/GMATcard_pi.png" />, or 2<img src="/assets/contents/GMATcard_pi.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 475,'474',18,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard473.png" />
  </p>
  <p> In circle <i>O </i>above, what is the area of sector <i>AOC </i>? </p>
</div>','<div class="prodid-answer">
  <p> The formula for the area of a circle is <i>A</i> = <img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup>; therefore, if
						<i>r </i>= 6, then <i>A</i> = <img src="/assets/contents/GMATcard_pi.png" />
					× 6² = 36<img src="/assets/contents/GMATcard_pi.png" />. Because ∠<i>AOC</i>
					measures 60°, a 60° “slice” of the circle is <img src="/assets/contents/GMATcard473_02.png" /> or <img src="/assets/contents/GMATcard473_03.png" /> of the total area of the circle.
					Therefore, the area of the sector is <img src="/assets/contents/GMATcard473_03.png" />(36<img src="/assets/contents/GMATcard_pi.png" />) = 6<img src="/assets/contents/GMATcard_pi.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 476,'475',18,NULL,'<div class="prodid-question">
  <p> A circle has an inscribed angle of 60°, with a corresponding arc of length 3<img src="/assets/contents/GMATcard_pi.png" />. What is the area of the circle?
				</p>
</div>','<div class="prodid-answer">
  <p> Because the arc of length 3<img src="/assets/contents/GMATcard_pi.png" />
					corresponds to a 60° inscribed angle, you know that 3<img src="/assets/contents/GMATcard_pi.png" /> is <img src="/assets/contents/GMATcard473_03.png" /> of the total circumference (as
					60° is <img src="/assets/contents/GMATcard473_03.png" /> of the total 360° in the
					circle). This gives you a circumference of 18<img src="/assets/contents/GMATcard_pi.png" />. Since the circumference formula is
						2<img src="/assets/contents/GMATcard_pi.png" /><i>r</i>, a circumference of
					18 tells you that the radius is 9. Using the formula for the area of a circle
						<img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup>), you get
					an area of 81<img src="/assets/contents/GMATcard_pi.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 477,'476',18,NULL,'<div class="prodid-question">
  <p>At what angle does a line tangent to a circle meet that circle’s radius at the
					point of tangency?</p>
</div>','<div class="prodid-answer">
  <p> Lines tangent to circles and those circles’ radii meet at a <i>right </i>angle.
					A line that is <i>tangent </i>to a circle <i>meets the circle at exactly one
						point</i>, called the <i>point of tangency. </i>In this diagram, line <i>AC
					</i>is tangent to circle <i>O </i>at point <i>B</i>. Line <i>AC </i>is
					perpendicular to radius <i>OB</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard476.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 478,'477',18,NULL,'<div class="prodid-question">
  <p> What does it mean for a polygon to be <i>inscribed </i>in a circle and for a
					polygon to be <i>circumscribed </i>about a circle? </p>
</div>','<div class="prodid-answer">
  <p> A polygon is <i>inscribed </i>in a circle if all the vertices of the polygon lie
					on the circle. A polygon is <i>circumscribed </i>about a circle if all the sides
					of the polygon are tangent to the circle. </p>
  <p> Square <i>ABCD</i>, below, is inscribed in circle <i>O</i>. You can also say
					that circle <i>O </i>is circumscribed about square <i>ABCD</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard477_01.png" />
  </p>
  <p> Square <i>PQRS</i>, below, is circumscribed about circle <i>O</i>. You can also
					say that circle <i>O </i>is inscribed in square <i>PQRS</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard477_02.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 479,'478',18,NULL,'<div class="prodid-question">
  <p>If a triangle has all three sides inscribed in a circle, and one of its sides is
					the diameter of the circle, what is the angle opposite the diagonal?</p>
</div>','<div class="prodid-answer">
  <p> The angle opposite the diagonal will be a <i>right </i>angle. In this diagram,
					line <i>AB </i>is a diameter of circle <i>O</i>, and <i>AB </i>is also the
					hypotenuse of right triangle <i>ABC</i>. Angle <i>ACB </i>is a right angle. </p>
  <p>
    <img src="/assets/contents/GMATcard478.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 480,'479',18,NULL,'<div class="prodid-question">
  <p>The area of a square circumscribed about a circle is 36. What is the
					circumference of the circle?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard479_01.png" />
  </p>
  <p>To get the circumference, you need the diameter or radius. Look for the
					connection between the shapes. Is the diameter the same as a side or a
					diagonal?</p>
  <p> In this case, the circle’s diameter is also the square’s edge, which is <img src="/assets/contents/GMATcard479_02.png" />, or 6. </p>
  <p> Circumference = <img src="/assets/contents/GMATcard_pi.png" /> (diameter) = 6
						<img src="/assets/contents/GMATcard_pi.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 481,'480',18,NULL,'<div class="prodid-question">
  <p> What is the diameter of the circle below, with center <i>O</i>? </p>
  <p>
    <img src="/assets/contents/GMATcard480.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>When a triangle is inscribed in a circle in such a way that one side of the
					triangle coincides with the diameter of the circle, the triangle is a right
					triangle.</p>
  <p>
    <i>AC </i>is a diameter of circle <i>O</i>, because it passes through center
					point <i>O</i>. So triangle <i>ABC </i>fits the description given of a right
					triangle. Moreover, triangle <i>ABC </i>is a special 5:12:13 right triangle,
					with a hypotenuse of 13. </p>
  <p> Therefore, the length of diameter <i>AC </i>is 13. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 482,'481',18,NULL,'<div class="prodid-question">
  <p> Triangle <i>ABC </i>is inscribed in circle <i>O</i>, with segment <i>BC
					</i>passing through the circle’s center at point <i>O</i>. If <i>AB </i>= <i>AC
					</i>= 3, what is the circumference of circle <i>O</i>? </p>
</div>','<div class="prodid-answer">
  <p> Because <i>ABC </i>is an inscribed triangle with one of its sides (<i>BC</i>)
					passing through the circle’s center, it must be a right triangle, with a right
					angle at the vertex opposite the hypotenuse <i>BC</i>. You are also given <i>AB
					</i>= <i>AC </i>= 3, so you know <i>ABC </i>is an isosceles right triangle,
					which has side lengths in the ratio <img src="/assets/contents/GMATcard481_01.png" /><i>.</i> The hypotenuse
						(<i>BC</i>, which is also the diameter of the circle) will therefore be <img src="/assets/contents/GMATcard481_02.png" />. The formula for the
					circumference of a circle is 2<img src="/assets/contents/GMATcard_pi.png" /><i>r</i>, or <img src="/assets/contents/GMATcard_pi.png" /><i>d</i>; thus,
					the circumference of circle <i>O </i>is <img src="/assets/contents/GMATcard481_03.png" />, or <img src="/assets/contents/GMATcard481_04.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 483,'482',18,NULL,'<div class="prodid-question">
  <p>What is a uniform solid, and what is the formula for its volume?</p>
</div>','<div class="prodid-answer">
  <p> A uniform solid is a <i>solid with the same dimensions throughout the entire
						object</i>. The overwhelming majority of GMAT solids are uniform. </p>
  <p>Volume = (Area of base)(Height)</p>
  <p>Be prepared to use the formulas for triangles, circles, and rectangles to
					calculate the area of the base of a GMAT solid.</p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 484,'483',18,NULL,'<div class="prodid-question">
  <p>What is a rectangular solid?</p>
</div>','<div class="prodid-answer">
  <p> A rectangular solid is <i>a solid with six rectangular faces</i>. All edges meet
					at right angles. Real-life examples of rectangular solids are cereal boxes,
					bricks, etc. </p>
  <p>
    <img src="/assets/contents/GMATcard483.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 485,'484',18,NULL,'<div class="prodid-question">
  <p>How do you find the volume of a uniform rectangular solid?</p>
</div>','<div class="prodid-answer">
  <p> A uniform rectangular solid is simply a rectangle with a third dimension. To
					find the volume, take the area of the base (<i>length </i>× <i>width</i>), and
					multiply it by the third dimension (<i>height</i>): </p>
  <p>
    <img src="/assets/contents/GMATcard_length.png" /> × <i>w</i> × <i>h</i></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 486,'485',18,NULL,'<div class="prodid-question">
  <p> What is the length of <i>AG</i>? </p>
  <p>
    <img src="/assets/contents/GMATcard485_01.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>To find the diagonal of a rectangular solid, use the Pythagorean theorem
					twice.</p>
  <p> Draw diagonal <i>AC</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard485_02.png" />
  </p>
  <p>
    <i>ABC </i>is a 3:4:5 triangle, so <i>AC </i>= 5. </p>
  <p> Now look at triangle <i>ACG</i>: </p>
  <p>
    <img src="/assets/contents/GMATcard485_03.png" />
  </p>
  <p>
    <i>ACG </i>happens to be another special triangle, so you don’t need to use the
					Pythagorean theorem. Because the lengths of both legs of the right triangle are
					equal, <i>ACG </i>is a 45°-45°-90° triangle, so <i>AG</i> = <img src="/assets/contents/GMATcard485_04.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 487,'486',18,NULL,'<div class="prodid-question">
  <p>What is the surface area of a rectangular solid with the dimensions 4 × 3 ×
					2?</p>
</div>','<div class="prodid-answer">
  <p>A uniform rectangular solid has three pairs of equal, rectangular sides. The
					total surface area is the sum of the areas of these six sides. Therefore, the
					total surface area would be:</p>
  <p>
    <img src="/assets/contents/GMATcard486.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 488,'487',18,NULL,'<div class="prodid-question">
  <p>What is a cube?</p>
</div>','<div class="prodid-answer">
  <p> A cube is <i>a special rectangular solid in which all edges are of equal length,
						and therefore all faces are squares</i>. </p>
  <p>Sugar cubes and dice without rounded corners are real-life examples of cubes.</p>
  <p>
    <img src="/assets/contents/GMATcard487.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 489,'488',18,NULL,'<div class="prodid-question">
  <p>How do you find the volume of a cube?</p>
  <p>How do you find the surface area of a cube?</p>
</div>','<div class="prodid-answer">
  <p> Since a cube is a rectangular solid for which <img src="/assets/contents/GMATcard_length.png" /> = <i>w</i> = <i>h</i>, the
					formula for its volume can be stated in terms of any edge: </p>
  <p> Volume of a cube = <img src="/assets/contents/GMATcard_length.png" /><i>wh</i> =
					(edge)(edge)(edge) = <i>e</i><sup>3</sup></p>
  <p> Surface area of a cube = sum of areas of faces = 6<i>e</i><sup>2</sup></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 490,'489',18,NULL,'<div class="prodid-question">
  <p>What are the characteristics of a cylinder?</p>
</div>','<div class="prodid-answer">
  <p> A cylinder is a <i>uniform solid whose horizontal cross section is a
					circle</i>—for example, a soup can, or a pipe that is closed at both ends. A
					cylinder’s measurements are generally given in terms of its radius, <i>r</i>,
					and its height, <i>h</i>. </p>
  <p>
    <img src="/assets/contents/GMATcard489.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 491,'490',18,NULL,'<div class="prodid-question">
  <p>How do you find the volume of a cylinder?</p>
  <p>How do you find the surface area of a cylinder?</p>
</div>','<div class="prodid-answer">
  <p>To find the volume or surface area of a cylinder, you need two pieces of
					information—the height of the cylinder and the radius of its base.</p>
  <p> Volume of a cylinder = (area of base)(height) + <img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup><i>h</i></p>
  <p> Lateral surface area of a cylinder = (circumference of base)(height) = 2<img src="/assets/contents/GMATcard_pi.png" /><i>rh</i></p>
  <p> Total surface area of a cylinder = areas of circular ends + lateral surface area
					= 2<img src="/assets/contents/GMATcard_pi.png" /><i>r</i><sup>2</sup> + 2<img src="/assets/contents/GMATcard_pi.png" /><i>rh</i></p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 492,'491',18,NULL,'<div class="prodid-question">
  <p>What is coordinate geometry?</p>
</div>','<div class="prodid-answer">
  <p> Coordinate geometry is a system in which the location of a point in a plane is
					shown by an <i>x</i>-coordinate for the horizontal position and a
					<i>y</i>-coordinate for the vertical position. </p>
  <p>
    <img src="/assets/contents/GMATcard491.png" />
  </p>
  <p> A point is described by its <i>x</i>- and <i>y</i>-coordinates: (<i>x</i>,
						<i>y</i>). </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 493,'492',18,NULL,'<div class="prodid-question">
  <p> When you start at the origin (the place where the <i>x- </i>and <i>y-</i>axes
					cross) of the coordinate plane and move in the directions listed, what happens
					to <i>x </i>or <i>y </i>? </p>
  <p>
    <img src="/assets/contents/GMATcard492.png" />
  </p>
</div>','<div class="prodid-answer">
  <p>When you start at the origin of the coordinate plane and move:</p>
  <p> to the right → <i>x </i>is positive </p>
  <p> to the left → <i>x </i>is negative </p>
  <p> up → <i>y </i>is positive </p>
  <p> down → <i>y </i>is negative </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 494,'493',18,NULL,'<div class="prodid-question">
  <p>What is the slope of a line?</p>
</div>','<div class="prodid-answer">
  <p> The slope of a line is <i>the measure of a line’s incline on the coordinate
						plane</i>. It is measured by <img src="/assets/contents/GMATcard493_01.png" />, where the Δ means “change.” Slope can also be expressed as <img src="/assets/contents/GMATcard493_02.png" /> or <img src="/assets/contents/GMATcard493_03.png" />. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 495,'494',18,NULL,'<div class="prodid-question">
  <p> In the formula <i>y </i>= <i>mx </i>+ <i>b</i>, what do <i>m </i>and <i>b
					</i>represent? What does this entire equation represent? </p>
</div>','<div class="prodid-answer">
  <p> In coordinate geometry, the slope-intercept equation of a line is expressed
					using the formula <i>y </i>= <i>mx </i>+ <i>b</i>, where <i>m </i>is the
						<i>slope of the line </i>and <i>b </i>is the y<i>-intercept</i>, or the
					point where the line crosses the <i>y</i>-axis. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 496,'495',18,NULL,'<div class="prodid-question">
  <p>What is the distance <br />from (2, 3) to (–7, 3)?</p>
</div>','<div class="prodid-answer">
  <p>
    <img src="/assets/contents/GMATcard495.png" />
  </p>
  <p> If two points have the same <i>x</i>-coordinates or the same
					<i>y</i>-coordinates—that is, they make a line segment that is parallel to an
					axis—all you have to do is subtract the numbers that are different. </p>
  <p> Here, the <i>y</i>-coordinates are the same, so just subtract the
					<i>x</i>-coordinates: <br />2 –(–7) = 9. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 497,'496',18,NULL,'<div class="prodid-question">
  <p> If the coordinates of point <i>A </i>are (3, 4), and the coordinates of point
						<i>B </i>are (6, 8), what is the distance between points <i>A </i>and
						<i>B</i>? </p>
</div>','<div class="prodid-answer">
  <p> If two points have different <i>x</i>-coordinates and different
					<i>y</i>-coordinates, draw a right triangle, calculate the leg lengths, and use
					the Pythagorean theorem to find the distance between the two points. </p>
  <p>
    <img src="/assets/contents/GMATcard496.png" />
  </p>
  <p> So <i>AC </i>= 6 – 3 = 3, and <i>BC </i>= 8 – 4 = 4. If you recognize these as
					the legs of a 3:4:5 right triangle, you’ll know immediately that the distance
					between points <i>A </i>and <i>B </i>must be 5. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 498,'497',18,NULL,'<div class="prodid-question">
  <p> What is the distance from point <i>P </i>(2, 3) to point <i>Q
					</i>(–1, –1)? </p>
</div>','<div class="prodid-answer">
  <p> If two points have different <i>x</i>-coordinates and different
					<i>y</i>-coordinates, draw a right triangle, calculate the leg lengths, and use
					the Pythagorean theorem to find the distance between the two points. </p>
  <p>
    <img src="/assets/contents/GMATcard497.png" />
  </p>
  <p>It’s a 3:4:5 triangle!</p>
  <p>
    <i>PQ</i> = 5 </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 499,'498',18,NULL,'<div class="prodid-question">
  <p>
    <img src="/assets/contents/GMATcard498_01.png" />
  </p>
  <p> Line <i>r</i> is a straight line as shown. Which of the following points lies on
					line <i>r</i>? </p>
  <p>
    <img src="/assets/contents/GMATcard498_02.png" />
  </p>
</div>','<div class="prodid-answer">
  <p> Line <i>r </i>intercepts the <i>y</i>-axis at (0, –2), so you can plug –2 in for
						<i>b </i>in the slope-intercept form of a linear equation. Line <i>r</i> has
					a rise (Δ<i>y</i>) of 2 and a run (Δ<i>x</i>) of 5, so its slope is <img src="/assets/contents/GMATcard498_03.png" />. </p>
  <p> That makes the slope-intercept equation <img src="/assets/contents/GMATcard498_04.png" />. </p>
  <p> The easiest way to proceed from here is to substitute the coordinates of each
					answer choice into the equation in place of <i>x</i> and <i>y</i>; only the
					coordinates that satisfy the equation can lie on the line. The only
					<i>x</i>-coordinate that will not create a fraction on the right side of the
					equals sign is 10. </p>
  <p> Plugging in (10, 2) for <i>x</i> and <i>y</i> in the slope-intercept equation
					gives you <img src="/assets/contents/GMATcard498_05.png" />, which simplifies to
					2 = 4 – 2, a true statement. </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
INSERT INTO flashcards VALUES(1, 500,'499',18,NULL,'<div class="prodid-question">
  <p> A line is expressed as <img src="/assets/contents/GMATcard499_01.png" />. Which
					is greater, the <i>x</i>-intercept or the <i>y</i>-intercept? </p>
</div>','<div class="prodid-answer">
  <p> The <i>y</i>-intercept is given in the equation as 5. You can derive the
						<i>x</i>-intercept by solving for the value of <i>x </i>when <i>y </i>= 0: </p>
  <p>
    <img src="/assets/contents/GMATcard499_02.png" />
  </p>
</div>','','2014-06-16 12:25:50.506962','2014-06-16 12:25:50.506962',0,'skipped');
