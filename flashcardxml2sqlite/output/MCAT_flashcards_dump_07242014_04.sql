DELETE FROM categories;
DELETE FROM flashcards;
UPDATE sqlite_sequence SET seq = 0 WHERE name = 'categories';
INSERT INTO categories VALUES(1,'behavioral-sciences','Behavioral Sciences','2014-07-24 12:15:09.647793','2014-07-24 12:15:09.654793');
INSERT INTO categories VALUES(2,'biochemistry','Biochemistry','2014-07-24 12:15:09.654793','2014-07-24 12:15:09.654793');
INSERT INTO categories VALUES(3,'biology','Biology','2014-07-24 12:15:09.654793','2014-07-24 12:15:09.654793');
INSERT INTO categories VALUES(4,'general-chemistry','General Chemistry','2014-07-24 12:15:09.654793','2014-07-24 12:15:09.654793');
INSERT INTO categories VALUES(5,'organic-chemistry','Organic Chemistry','2014-07-24 12:15:09.654793','2014-07-24 12:15:09.654793');
INSERT INTO categories VALUES(6,'physics','Physics','2014-07-24 12:15:09.654793','2014-07-24 12:15:09.654793');
INSERT INTO flashcards VALUES(0, 1,'MCAT1',1,NULL,'<div class="prodid-question">
  <p>
    <b>Absolute Threshold</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The minimum of stimulus energy needed to activate a sensory system.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 2,'MCAT10',1,NULL,'<div class="prodid-question">
  <p>
    <b>Altruism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of helping behavior in which the intent is to benefit someone else at
						a cost to oneself.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 3,'MCAT100',1,NULL,'<div class="prodid-question">
  <p>
    <b>Halo Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cognitive bias in which judgments of an individual’s character can
						be affected by the overall impression of the individual.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 4,'MCAT101',1,NULL,'<div class="prodid-question">
  <p>
    <b>Heuristic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A rule of thumb or shortcut that is used to make decisions.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 5,'MCAT102',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hindbrain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brain that controls balance, motor coordination, breathing,
						digestion, and general arousal processes.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 6,'MCAT103',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hippocampus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the limbic system that is important for memory and learning.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 7,'MCAT104',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hypnosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An altered state of consciousness in which a person appears to be awake but
						is, in fact, in a highly suggestible state in which another person or event
						may trigger actions by the person.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 8,'MCAT105',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hypothalamus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the forebrain that controls homeostatic and endocrine functions
						by controlling the release of pituitary hormones.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 9,'MCAT106',1,NULL,'<div class="prodid-question">
  <p>
    <b>Id</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Freudian psychoanalysis, the part of the unconscious resulting from basic,
						instinctual urges for sexuality and survival; operates under the pleasure
						principle and seeks instant gratification.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 10,'MCAT107',1,NULL,'<div class="prodid-question">
  <p>
    <b>Identity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A part of an individual’s self-concept based on the groups to which
						that person belongs and his or her relationships to others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 11,'MCAT108',1,NULL,'<div class="prodid-question">
  <p>
    <b>Implicit Memory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Memory that does not require conscious recall; consists of skills and
						conditioned behaviors.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 12,'MCAT109',1,NULL,'<div class="prodid-question">
  <p>
    <b>Implicit Personality Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that states that people tend to associate traits and behavior in
						others, and that people have the tendency to attribute their own beliefs,
						opinions, and ideas onto others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 13,'MCAT11',1,NULL,'<div class="prodid-question">
  <p>
    <b>Amygdala</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the limbic system that is important for memory and emotion,
						especially fear.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 14,'MCAT110',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impression Management</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Behaviors that are intended to influence the perceptions of other people
						about a person, object, or event.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 15,'MCAT111',1,NULL,'<div class="prodid-question">
  <p>
    <b>Incidence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The number of new cases of a disease per population at risk in a given period
						of time; usually, new cases per 1000 at-risk people per year.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 16,'MCAT112',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inclusive Fitness</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A measure of reproductive success; depends on the number of offspring an
						individual has, how well they support their offspring, and how well their
						offspring can support others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 17,'MCAT113',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inductive Reasoning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of cognition that utilizes generalizations to develop a theory.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 18,'MCAT114',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ingratiation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An impression management strategy that uses flattery to increase social
						acceptance.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 19,'MCAT115',1,NULL,'<div class="prodid-question">
  <p>
    <b>Instinctive Drift</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency of animals to resist learning when a conditioned behavior
						conflicts with the animal’s instinctive behaviors.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 20,'MCAT116',1,NULL,'<div class="prodid-question">
  <p>
    <b>Intelligence Quotient</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Numerical measurement of intelligence, usually accomplished by some form of
						standardized testing.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 21,'MCAT117',1,NULL,'<div class="prodid-question">
  <p>
    <b>Interference</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A retrieval error caused by the learning of information; can be proactive
						(old information causes difficulty learning new information) or retroactive
						(new information interferes with older learning).</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 22,'MCAT118',1,NULL,'<div class="prodid-question">
  <p>
    <b>Intuition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Perceptions about a situation that may or may not be supported by available
						evidence but are nonetheless perceived as information that may be used to
						make a decision.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 23,'MCAT119',1,NULL,'<div class="prodid-question">
  <p>
    <b>James–Lange Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of emotion that states that a stimulus results in physiological
						arousal, which then leads to a secondary response in which emotion is
						consciously experienced.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 24,'MCAT12',1,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Anomie</i>
    </b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A state of normlessness; anomic conditions erode social solidarity by means
						of excessive individualism, social inequality, and isolation.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 25,'MCAT120',1,NULL,'<div class="prodid-question">
  <p>
    <b>Just-Noticeable Difference (jnd)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The minimum difference in magnitude between two stimuli before one can
						perceive this difference; also called a difference threshold.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 26,'MCAT121',1,NULL,'<div class="prodid-question">
  <p>
    <b>Just-World Hypothesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The cognitive bias that good things happen to good people and bad things
						happen to bad people.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 27,'MCAT122',1,NULL,'<div class="prodid-question">
  <p>
    <b>Justice</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In medical ethics, the tenet that the physician has a responsibility to treat
						similar patients with similar care, and to distribute healthcare resources
						fairly.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 28,'MCAT123',1,NULL,'<div class="prodid-question">
  <p>
    <b>Learning (Behaviorist) Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that attitudes are developed through forms of learning (direct
						contact, direct interaction, direct instruction, and conditioning).</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 29,'MCAT124',1,NULL,'<div class="prodid-question">
  <p>
    <b>Limbic System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebrum that is associated with emotion and memory;
						includes the amygdala and hippocampus.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 30,'MCAT125',1,NULL,'<div class="prodid-question">
  <p>
    <b>Linguistic Relativity Hypothesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A hypothesis suggesting that one’s perception of reality is largely
						determined by the content, form, and structure of language; also known as
						the Whorfian hypothesis.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 31,'MCAT126',1,NULL,'<div class="prodid-question">
  <p>
    <b>Locus of Control</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The characterization of the source of influences on the events in
						one’s life; can be internal or external.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 32,'MCAT127',1,NULL,'<div class="prodid-question">
  <p>
    <b>Long-Term Potentiation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strengthening of neural connections due to rehearsal or relearning;
						thought to be the neurophysiological basis of long-term memory.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 33,'MCAT128',1,NULL,'<div class="prodid-question">
  <p>
    <b>Maintenance Rehearsal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Repetition of a piece of information to either keep it within working memory
						or store it.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 34,'MCAT129',1,NULL,'<div class="prodid-question">
  <p>
    <b>Managing Appearances</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An impression management strategy in which one uses props, appearance,
						emotional expression, or associations with others to create a positive
						image.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 35,'MCAT13',1,NULL,'<div class="prodid-question">
  <p>
    <b>Anxiety Disorders</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Disorders that involve worry, unease, fear, and apprehension about future
						uncertainties based on real or imagined events that can impair physical and
						psychological health.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 36,'MCAT130',1,NULL,'<div class="prodid-question">
  <p>
    <b>Manic Episode</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A period of at least one week with prominent and persistent elevated or
						expansive mood and at least two other manic symptoms.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 37,'MCAT131',1,NULL,'<div class="prodid-question">
  <p>
    <b>Master Status</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A status with which a person is most identified.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 38,'MCAT132',1,NULL,'<div class="prodid-question">
  <p>
    <b>Material Culture</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The physical items one associates with a given cultural group.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 39,'MCAT133',1,NULL,'<div class="prodid-question">
  <p>
    <b>Medulla Oblongata</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brainstem that regulates vital functions, including
						breathing, heart rate, and blood pressure.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 40,'MCAT134',1,NULL,'<div class="prodid-question">
  <p>
    <b>Melatonin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A serotonin derivative secreted by the pineal gland that is associated with
						sleepiness.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 41,'MCAT135',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mental Set</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A tendency to repeat solutions that have yielded positive results at some
						time in the past.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 42,'MCAT136',1,NULL,'<div class="prodid-question">
  <p>
    <b>Meritocracy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A society in which advancement up the social ladder is based on intellectual
						talent and achievement.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 43,'MCAT137',1,NULL,'<div class="prodid-question">
  <p>
    <b>Midbrain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brainstem that manages sensorimotor reflexes to visual and
						auditory stimuli and gives rise to some cranial nerves.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 44,'MCAT138',1,NULL,'<div class="prodid-question">
  <p>
    <b>Misinformation Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A phenomenon in which memories are altered by misleading information provided
						at the point of encoding or recall.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 45,'MCAT139',1,NULL,'<div class="prodid-question">
  <p>
    <b>Narcolepsy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sleep disorder characterized by a lack of voluntary control over the onset
						of sleep; also involves cataplexy and hypnagogic and hypnopompic
						hallucinations.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 46,'MCAT14',1,NULL,'<div class="prodid-question">
  <p>
    <b>Appraisal Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A similar theory to the basic model, accepting that there are biologically
						predetermined expressions once an emotion is experienced; accepts that there
						is a cognitive antecedent to emotional expression.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 47,'MCAT140',1,NULL,'<div class="prodid-question">
  <p>
    <b>Network</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A term used to describe the observable pattern of social relationships among
						individual units of analysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 48,'MCAT141',1,NULL,'<div class="prodid-question">
  <p>
    <b>Neuroplasticity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Change in neural connections caused by learning or a response to injury.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 49,'MCAT142',1,NULL,'<div class="prodid-question">
  <p>
    <b>Neuropsychology</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The study of functions and behaviors associated with specific regions of the
						brain.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(0, 50,'MCAT143',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nonmaleficence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ethical tenet that the physician has a responsibility to avoid
						interventions in which the potential for harm outweighs the potential for
						benefit.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',1,'skipped');
INSERT INTO flashcards VALUES(1, 51,'MCAT144',1,NULL,'<div class="prodid-question">
  <p>
    <b>Non-Rapid Eye Movement (NREM) Sleep</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stages 1 through 4 of sleep; contains everslowing brain waves as one gets
						deeper into sleep.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 52,'MCAT145',1,NULL,'<div class="prodid-question">
  <p>
    <b>Norms</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Societal rules that define the boundaries of acceptable behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 53,'MCAT146',1,NULL,'<div class="prodid-question">
  <p>
    <b>Obedience</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The changing of behavior of an individual based on a command from someone
						seen as an authority figure.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 54,'MCAT147',1,NULL,'<div class="prodid-question">
  <p>
    <b>Object Permanence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Knowledge that an object does not cease to exist even when the object cannot
						be seen; a milestone in cognitive development.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 55,'MCAT148',1,NULL,'<div class="prodid-question">
  <p>
    <b>Observational Learning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of learning in which behavior is modified as a result of watching
						others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 56,'MCAT149',1,NULL,'<div class="prodid-question">
  <p>
    <b>Occipital Lobe</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebral cortex that controls visual processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 57,'MCAT15',1,NULL,'<div class="prodid-question">
  <p>
    <b>Archetype</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Jungian psychoanalysis, a thought or image that has an emotional element
						and is a part of the collective unconscious.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 58,'MCAT150',1,NULL,'<div class="prodid-question">
  <p>
    <b>Operant Conditioning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of associative learning in which the frequency of a behavior is
						modified using reinforcement or punishment.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 59,'MCAT151',1,NULL,'<div class="prodid-question">
  <p>
    <b>Opponent-Process Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that states that the body will adapt to counteract repeated exposure
						to stimuli, such as seeing afterimages or ramping up the sympathetic nervous
						system in response to a depressant.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 60,'MCAT152',1,NULL,'<div class="prodid-question">
  <p>
    <b>Parallel Processing</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ability to simultaneously analyze and combine information regarding
						multiple aspects of a stimulus, such as color, shape, and motion.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 61,'MCAT153',1,NULL,'<div class="prodid-question">
  <p>
    <b>Parietal Lobe</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebral cortex that controls somatosensory and spatial
						processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 62,'MCAT154',1,NULL,'<div class="prodid-question">
  <p>
    <b>Personality Disorders</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Disorders that involve patterns of behavior that are inflexible and
						maladaptive, causing distress or impaired function in at least two of the
						following: cognition, emotion, interpersonal functioning, or impulse
						control.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 63,'MCAT155',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pineal Gland</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A brain structure located near the thalamus that secretes melatonin.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 64,'MCAT156',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pons</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brainstem that relays information between the cortex and
						medulla, regulates sleep, and carries some motor and sensory information
						from the head and neck.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 65,'MCAT157',1,NULL,'<div class="prodid-question">
  <p>
    <b>Poverty</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A socioeconomic condition of low resource availability; in the United States,
						the poverty line is determined by the government’s calculation of the
						minimum income requirements for families to acquire the minimum necessities
						of life.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 66,'MCAT158',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prejudice</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An irrationally based positive or negative attitude toward a person, group,
						or thing, formed prior to actual experience.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 67,'MCAT159',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prevalence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The number of cases of a disease per population in a given period of time;
						usually, cases per 1000 people per year.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 68,'MCAT16',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arcuate Fasciculus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A bundle of axons that connects Wernicke’s Area (language
						comprehension) with Broca’s Area (motor function of speech). Damage
						causes conduction aphasia, characterized by the inability to repeat words
						with intact spontaneous speech production and comprehension.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 69,'MCAT160',1,NULL,'<div class="prodid-question">
  <p>
    <b>Primacy Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon of first impressions of a person being more important than
						subsequent impressions.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 70,'MCAT161',1,NULL,'<div class="prodid-question">
  <p>
    <b>Priming</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A retrieval cue by which recall is aided by a word or phrase that is
						semantically related to the desired memory.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 71,'MCAT162',1,NULL,'<div class="prodid-question">
  <p>
    <b>Projection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which individuals attribute their undesired feelings
						to others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 72,'MCAT163',1,NULL,'<div class="prodid-question">
  <p>
    <b>Projection Area</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebral cortex that analyzes sensory input.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 73,'MCAT164',1,NULL,'<div class="prodid-question">
  <p>
    <b>Proprioception</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ability to tell where one’s body is in space.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 74,'MCAT165',1,NULL,'<div class="prodid-question">
  <p>
    <b>Punishment</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In operant conditioning, the use of an aversive stimulus designed to decrease
						the frequency of an undesired behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 75,'MCAT166',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rapid Eye Movement (REM) Sleep</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Sleep stage in which the eyes move rapidly back and forth and physiological
						arousal levels are more similar to wakefulness than sleep; dreaming occurs
						during this stage.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 76,'MCAT167',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rationalization</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which individuals explain undesirable behaviors in a
						way that is self-justifying and socially acceptable.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 77,'MCAT168',1,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Formation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which individuals suppress urges by unconsciously
						converting them into their exact opposites.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 78,'MCAT169',1,NULL,'<div class="prodid-question">
  <p>
    <b>Recency Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon in which the most recent information we have about an
						individual is most important in forming our impressions.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 79,'MCAT17',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arousal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A psychological and physiological state of being awake and reactive to
						stimuli; nearly synonymous with alertness.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 80,'MCAT170',1,NULL,'<div class="prodid-question">
  <p>
    <b>Reciprocal Determinism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In the social cognitive perspective, the notion that thoughts, feelings,
						behaviors, and environment interact to determine behavior in a given
						situation.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 81,'MCAT171',1,NULL,'<div class="prodid-question">
  <p>
    <b>Recognition-Primed Decision Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decision-making model in which experience and recognition of similar
						situations one has already experienced play a large role in decision-making
						and actions; also one of the explanations for the experience of
						intuition.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 82,'MCAT172',1,NULL,'<div class="prodid-question">
  <p>
    <b>Regression</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which an individual deals with stress by reverting to
						an earlier developmental state.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 83,'MCAT173',1,NULL,'<div class="prodid-question">
  <p>
    <b>Reinforcement</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In operant conditioning, the use of a stimulus designed to increase the
						frequency of a desired behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 84,'MCAT174',1,NULL,'<div class="prodid-question">
  <p>
    <b>Representativeness Heuristic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A shortcut in decision-making that relies on categorizing items on the basis
						of whether they fit the prototypical, stereotypical, or representative image
						of the category.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 85,'MCAT175',1,NULL,'<div class="prodid-question">
  <p>
    <b>Repression</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which the ego forces undesired thoughts and urges into
						the unconscious mind.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 86,'MCAT176',1,NULL,'<div class="prodid-question">
  <p>
    <b>Response Bias</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency of subjects to systematically respond to a stimulus in a
						particular way due to nonsensory factors.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 87,'MCAT177',1,NULL,'<div class="prodid-question">
  <p>
    <b>Retrieval</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process of demonstrating that information has been retained in memory;
						includes recall, recognition, and relearning.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 88,'MCAT178',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ritual</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A formalized ceremony that usually involves specific material objects,
						symbolism, and additional mandates on acceptable behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 89,'MCAT179',1,NULL,'<div class="prodid-question">
  <p>
    <b>Role</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A set of beliefs, values, attitudes, and norms that define expectations of
						behaviors associated with a given status.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 90,'MCAT18',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arousal Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of motivation that states that there is particular level of arousal
						required in order to perform actions optimally; summarized by the
						Yerkes–Dodson law.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 91,'MCAT180',1,NULL,'<div class="prodid-question">
  <p>
    <b>Schachter–Singer Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of emotion that states that both physiological arousal and cognitive
						appraisal must occur before an emotion is consciously experienced.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 92,'MCAT181',1,NULL,'<div class="prodid-question">
  <p>
    <b>Schema</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An organized pattern of thought and behavior; one of the central concepts of
						Piaget’s stages of cognitive development.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 93,'MCAT182',1,NULL,'<div class="prodid-question">
  <p>
    <b>Schizophrenia</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A psychotic disorder characterized by gross distortions of reality and
						disturbances in the content and form of thought, perception, and
						behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 94,'MCAT183',1,NULL,'<div class="prodid-question">
  <p>
    <b>Selective Attention</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ability to focus on a single stimulus even while other stimuli are
						occurring simultaneously.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 95,'MCAT184',1,NULL,'<div class="prodid-question">
  <p>
    <b>Self-Disclosure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An aspect of interpersonal attraction or impression management in which one
						shares his or her fears, thoughts, and goals with another person in the
						hopes of being met with empathy and nonjudgment.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 96,'MCAT185',1,NULL,'<div class="prodid-question">
  <p>
    <b>Self-Fulfilling Prophecy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon of a stereotype creating an expectation of a particular group,
						which creates conditions that lead to confirmation of this stereotype.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 97,'MCAT186',1,NULL,'<div class="prodid-question">
  <p>
    <b>Self-Handicapping</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An impression management strategy where one creates obstacles to avoid
						self-blame when he or she does not meet expectations.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 98,'MCAT187',1,NULL,'<div class="prodid-question">
  <p>
    <b>Self-Serving Bias</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The idea that individuals will view their own success as being based on
						internal factors, while viewing failures as being based on external
						factors.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 99,'MCAT188',1,NULL,'<div class="prodid-question">
  <p>
    <b>Semantic Network</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Organization of information in the brain by linking concepts with similar
						characteristics and meaning.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 100,'MCAT189',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sensation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Transduction of physical stimuli into neurological signals.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 101,'MCAT19',1,NULL,'<div class="prodid-question">
  <p>
    <b>Assimilation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In psychology, the process by which new information is interpreted in terms
						of existing schemata; in sociology, the process by which the behavior and
						culture of a group or an individual begins to merge with that of another
						group.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 102,'MCAT190',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sensitive Period</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A time during which environmental input has a maximal impact on the
						development of a particular ability.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 103,'MCAT191',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sensory Memory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Visual (iconic) and auditory (echoic) stimuli briefly stored in memory; fades
						very quickly unless attention is paid to the information.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 104,'MCAT192',1,NULL,'<div class="prodid-question">
  <p>
    <b>Serial Position Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency to better remember items presented at the beginning or end of a
						list; related to the primary and recency effects.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 105,'MCAT193',1,NULL,'<div class="prodid-question">
  <p>
    <b>Shaping</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In operant conditioning, the process of conditioning a complex behavior by
						rewarding successive approximations of the behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 106,'MCAT194',1,NULL,'<div class="prodid-question">
  <p>
    <b>Signal Detection Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of perception in which internal (psychological) and external
						(environmental) context both play a role in our perception of stimuli.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 107,'MCAT195',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sleep Apnea</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Sleep disorder in which a person may cease to breathe while sleeping; may be
						due to obstruction or a central (neurological) cause.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 108,'MCAT196',1,NULL,'<div class="prodid-question">
  <p>
    <b>Slow-Wave Sleep</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Consists of NREM sleep stages 3 and 4; also called delta-wave sleep.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 109,'MCAT197',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Action</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Actions and behaviors that individuals are conscious of and performing
						because others are around.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 110,'MCAT198',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Capital</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The investment people make in their society in return for economic or
						collective rewards.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 111,'MCAT199',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Construction Model of Emotion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of emotional expression that assumes there are no biologically wired
						emotions; rather, they are based on experiences and situational context
						alone.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 112,'MCAT2',1,NULL,'<div class="prodid-question">
  <p>
    <b>Accommodation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Process by which existing schemata are modified to encompass new
						information.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 113,'MCAT20',1,NULL,'<div class="prodid-question">
  <p>
    <b>Associative Learning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process by which a connection is made between two stimuli or a stimulus
						and a response; examples include classical conditioning and operant
						conditioning.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 114,'MCAT200',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Constructionism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theoretical approach that uncovers the ways in which individuals and groups
						participate in the formation of their perceived social reality.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 115,'MCAT201',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Facilitation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency to perform at a different level based on the fact that others
						are around.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 116,'MCAT202',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Mobility</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of individuals in the social hierarchy through changes in
						income, education, or occupation.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 117,'MCAT203',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Movements</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Philosophies that drive large numbers of people to organize to promote or
						resist social change.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 118,'MCAT204',1,NULL,'<div class="prodid-question">
  <p>
    <b>Social Perception</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Understanding the thoughts and motives of other people present in the social
						world; also referred to as social cognition.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 119,'MCAT205',1,NULL,'<div class="prodid-question">
  <p>
    <b>Somatosensation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The sense of “touch,” which contains multiple modalities:
						pressure, vibration, pain, and temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 120,'MCAT206',1,NULL,'<div class="prodid-question">
  <p>
    <b>Somnambulism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Sleep disorder in which one carries out actions in his or her sleep; also
						called sleepwalking.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 121,'MCAT207',1,NULL,'<div class="prodid-question">
  <p>
    <b>Source Amnesia</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A memory error by which a person remembers the details of an event but
						confuses the context by which the details were gained; often causes a person
						to remember events that happened to someone else as having happened to him-
						or herself.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 122,'MCAT208',1,NULL,'<div class="prodid-question">
  <p>
    <b>Spacing Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon of retaining larger amounts of information when the amount of
						time between sessions of relearning is increased.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 123,'MCAT209',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stereotypes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Attitudes and impressions that are made based on limited and superficial
						information about a person or a group of individuals.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 124,'MCAT21',1,NULL,'<div class="prodid-question">
  <p>
    <b>Attachment</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A very deep emotional bond to another person, particularly a parent or
						caregiver.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 125,'MCAT210',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stigma</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The extreme disapproval or dislike of a person or group based on perceived
						differences in social characteristics from the rest of society.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 126,'MCAT211',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stimulus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Any energy pattern that is sensed in some way by the body; includes visual,
						auditory, and physical sensations, among others.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 127,'MCAT212',1,NULL,'<div class="prodid-question">
  <p>
    <b>Storage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The retention of encoded information; divided into sensory, short-term, and
						long-term memory.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 128,'MCAT213',1,NULL,'<div class="prodid-question">
  <p>
    <b>Subcultures</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Groups of people within a culture that distinguish themselves from the
						primary culture to which they belong.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 129,'MCAT214',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sublimation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which unacceptable urges are transformed into socially
						acceptable behaviors.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 130,'MCAT215',1,NULL,'<div class="prodid-question">
  <p>
    <b>Subliminal Perception</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Perception of a stimulus below a threshold (usually the threshold of
						conscious perception).</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 131,'MCAT216',1,NULL,'<div class="prodid-question">
  <p>
    <b>Superego</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Freudian psychoanalysis, the part of the unconscious mind focused on
						idealism, perfectionism, and societal norms.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 132,'MCAT217',1,NULL,'<div class="prodid-question">
  <p>
    <b>Symbolic Culture</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The nonmaterial culture that represents a group of people; expressed through
						ideas and concepts.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 133,'MCAT218',1,NULL,'<div class="prodid-question">
  <p>
    <b>Symbolic Ethnicity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An ethnic identity that is only relevant on special occasions or in specific
						circumstances and that does not impact everyday life.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 134,'MCAT219',1,NULL,'<div class="prodid-question">
  <p>
    <b>Symbolic Interactionism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theoretical framework that studies the ways individuals interact through a
						shared understanding of words, gestures, and other symbols.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 135,'MCAT22',1,NULL,'<div class="prodid-question">
  <p>
    <b>Attitude</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A tendency toward expression of positive or negative feelings or evaluations
						of a person, place, thing, or situation.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 136,'MCAT220',1,NULL,'<div class="prodid-question">
  <p>
    <b>Syntax</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The way in which words are organized to create meaning.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 137,'MCAT221',1,NULL,'<div class="prodid-question">
  <p>
    <b>Temporal Lobe</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebral cortex that controls auditory processing, memory
						processing, emotional control, and language.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 138,'MCAT222',1,NULL,'<div class="prodid-question">
  <p>
    <b>Thalamus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the forebrain that serves as a relay and sorting station for
						sensory information, and then transmits the information to the cerebral
						cortex.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 139,'MCAT223',1,NULL,'<div class="prodid-question">
  <p>
    <b>Theory of Mind</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ability to sense how another’s mind works.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 140,'MCAT224',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tolerance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Decreased response to a drug after physiological adaptation.</b>
  </p>
</div>','','2014-07-24 12:15:09.655793','2014-07-24 12:15:09.655793',0,'skipped');
INSERT INTO flashcards VALUES(1, 141,'MCAT225',1,NULL,'<div class="prodid-question">
  <p>
    <b>Transduction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Conversion of physical, electromagnetic, auditory, and other stimuli to
						electrical signals in the nervous system.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 142,'MCAT226',1,NULL,'<div class="prodid-question">
  <p>
    <b>Two-Point Threshold</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The minimum distance necessary between two points of stimulation on the skin
						such that the points will be felt as two distinct stimuli.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 143,'MCAT227',1,NULL,'<div class="prodid-question">
  <p>
    <b>Universal Emotions</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Emotions that are recognized by all cultures; includes happiness, sadness,
						anger, fear, disgust, contempt, and surprise.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 144,'MCAT228',1,NULL,'<div class="prodid-question">
  <p>
    <b>Weber’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of perception that states that there is a constant ratio between the
						change in stimulus intensity needed to produce a just-noticeable difference
						and the intensity of the original stimulus.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 145,'MCAT229',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wernicke’s Area</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A brain region located in the superior temporal gyrus of the temporal lobe
						(usually in the left hemisphere); largely responsible for language
						comprehension. Damage causes Wernicke’s aphasia, a loss of language
						comprehension, resulting in fluid production of language without
						meaning.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 146,'MCAT23',1,NULL,'<div class="prodid-question">
  <p>
    <b>Attribute Substitution</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A phenomenon observed when individuals must make judgments that are complex
						but instead substitute a simpler solution or perception.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 147,'MCAT230',1,NULL,'<div class="prodid-question">
  <p>
    <b>Zone of Proximal Development</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Those skills which a child has not yet mastered but can accomplish with the
						help of a more knowledgeable other.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 148,'MCAT24',1,NULL,'<div class="prodid-question">
  <p>
    <b>Attribution Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that focuses on the tendency for individuals to infer the causes of
						other people’s behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 149,'MCAT25',1,NULL,'<div class="prodid-question">
  <p>
    <b>Autonomy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ethical tenet that the physician has the responsibility to respect
						patients’ choices about their own healthcare.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 150,'MCAT26',1,NULL,'<div class="prodid-question">
  <p>
    <b>Availability Heuristic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A shortcut in decision-making that relies on the information that is most
						readily available, rather than the total body of information on a
						subject.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 151,'MCAT27',1,NULL,'<div class="prodid-question">
  <p>
    <b>Avoidance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of negative reinforcement in which one eschews the unpleasantness of
						something that has yet to happen.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 152,'MCAT28',1,NULL,'<div class="prodid-question">
  <p>
    <b>Back Stage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In the dramaturgical approach, the setting where players are free from their
						role requirements and not in front of the audience; back stage behaviors may
						not be deemed appropriate or acceptable and are thus kept invisible from the
						audience.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 153,'MCAT29',1,NULL,'<div class="prodid-question">
  <p>
    <b>Basal Ganglia</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the forebrain that coordinates muscle movement and routes
						information from the cortex to the brain and spinal cord.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 154,'MCAT3',1,NULL,'<div class="prodid-question">
  <p>
    <b>Acquisition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In classical conditioning, the process of taking advantage of reflexive
						responses to turn a neutral stimulus into a conditioned stimulus.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 155,'MCAT30',1,NULL,'<div class="prodid-question">
  <p>
    <b>Beneficence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ethical tenet that the physician has a responsibility to act in the
						patient’s best interest.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 156,'MCAT31',1,NULL,'<div class="prodid-question">
  <p>
    <b>Brainstem</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The most primitive portion of the brain, which includes the midbrain and
						hindbrain; controls the autonomic nervous system and communication between
						the spinal cord, cranial nerves, and brain.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 157,'MCAT32',1,NULL,'<div class="prodid-question">
  <p>
    <b>Broca’s Area</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A brain region located in the inferior frontal gyrus of the frontal lobe
						(usually in the left hemisphere); largely responsible for the motor function
						of speech. Damage causes Broca’s aphasia, a loss of the motor
						function of speech, resulting in intact understanding with an inability to
						correctly produce spoken language.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 158,'MCAT33',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bystander Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The observation that, when in a group, individuals are less likely to respond
						to a person in need.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 159,'MCAT34',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cannon–Bard Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory of emotion that states that a stimulus is first received and is then
						simultaneously processed physiologically and cognitively, allowing for the
						conscious emotion to be experienced.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 160,'MCAT35',1,NULL,'<div class="prodid-question">
  <p>
    <b>Catatonia</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Disorganized motor behavior characterized by various unusual physical
						movements or stillness.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 161,'MCAT36',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cerebellum</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the hindbrain that maintains posture and balance and coordinates
						body movements.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 162,'MCAT37',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cerebral Cortex</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The outermost layer of the cerebrum, responsible for complex perceptual,
						behavioral, and cognitive processes.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 163,'MCAT38',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cerebrum</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brain that contains the cerebral cortex, limbic system, and
						basal ganglia.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 164,'MCAT39',1,NULL,'<div class="prodid-question">
  <p>
    <b>Circadian Rhythm</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The alignment of physiological processes with the 24-hour day, including
						sleep–wake cycles and some elements of the endocrine system.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 165,'MCAT4',1,NULL,'<div class="prodid-question">
  <p>
    <b>Adaptation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In perception, a decrease in stimulus perception after a long duration of
						exposure; in learning, the process by which new information is processed;
						consists of assimilation and accommodation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 166,'MCAT40',1,NULL,'<div class="prodid-question">
  <p>
    <b>Circular Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A repetitive action that achieves a desired response; seen during
						Piaget’s sensorimotor stage.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 167,'MCAT41',1,NULL,'<div class="prodid-question">
  <p>
    <b>Classical Conditioning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of associative learning in which a neutral stimulus becomes associated
						with an unconditioned stimulus such that the neutral stimulus alone produces
						the same response as the unconditioned stimulus; the neutral stimulus thus
						becomes a conditioned stimulus.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 168,'MCAT42',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cognitive Dissonance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The simultaneous presence of two opposing thoughts or opinions.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 169,'MCAT43',1,NULL,'<div class="prodid-question">
  <p>
    <b>Collective Unconscious</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Jungian psychoanalysis, the part of the unconscious mind that is shared
						among all humans and is a result of our common ancestry.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 170,'MCAT44',1,NULL,'<div class="prodid-question">
  <p>
    <b>Compliance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A change of behavior of an individual at the request of another.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 171,'MCAT45',1,NULL,'<div class="prodid-question">
  <p>
    <b>Confirmation Bias</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cognitive bias in which one focuses on information that supports a given
						solution, belief, or hypothesis, and ignores evidence against it.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 172,'MCAT46',1,NULL,'<div class="prodid-question">
  <p>
    <b>Conflict Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theoretical framework that emphasizes the role of power differentials in
						producing social order.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 173,'MCAT47',1,NULL,'<div class="prodid-question">
  <p>
    <b>Conformity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The changing of beliefs or behaviors in order to fit into a group or
						society.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 174,'MCAT48',1,NULL,'<div class="prodid-question">
  <p>
    <b>Consciousness</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Awareness of oneself; can be used to describe varying levels of awareness
						that occur with wakefulness, sleep, dreaming, and druginduced states.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 175,'MCAT49',1,NULL,'<div class="prodid-question">
  <p>
    <b>Conservation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Concept seen in quantitative analysis performed by a child; develops when a
						child is able to identify the difference between quantity by number and
						actual amount, especially when faced with identical quantities separated
						into varying pieces.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 176,'MCAT5',1,NULL,'<div class="prodid-question">
  <p>
    <b>Adaptive Value</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The extent to which a trait benefits a species by influencing the
						evolutionary fitness of the species.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 177,'MCAT50',1,NULL,'<div class="prodid-question">
  <p>
    <b>Context Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A retrieval cue by which memory is aided when a person is in the location
						where encoding took place.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 178,'MCAT51',1,NULL,'<div class="prodid-question">
  <p>
    <b>Correspondent Inference Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that states that people pay closer attention to intentional behavior
						than accidental behavior when making attributions, especially if the
						behavior is unexpected.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 179,'MCAT52',1,NULL,'<div class="prodid-question">
  <p>
    <b>Critical Period</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A time during development during which exposure to language is essential for
						eventual development of effective use of language; between two years of age
						and puberty.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 180,'MCAT53',1,NULL,'<div class="prodid-question">
  <p>
    <b>Crystallized Intelligence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cognitive capacity to understand relationships or solve problems using
						information acquired during schooling and other experiences.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 181,'MCAT54',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cultural Relativism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The recognition that social groups and cultures must be studied on their own
						terms to be understood.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 182,'MCAT55',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deductive Reasoning</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of cognition that starts with general information and narrows down
						that information to create a conclusion.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 183,'MCAT56',1,NULL,'<div class="prodid-question">
  <p>
    <b>Defense Mechanism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A technique used by the ego that denies, falsifies, or distorts reality in
						order to resolve anxiety caused by undesirable urges of the id and
						superego.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 184,'MCAT57',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deindividuation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The idea that people will lose a sense of selfawareness and can act
						dramatically differently based on the influence of a group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 185,'MCAT58',1,NULL,'<div class="prodid-question">
  <p>
    <b>Delusions</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Fixed, false beliefs that are discordant with reality and not shared by
						one’s culture, but are maintained in spite of strong evidence to the
						contrary.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 186,'MCAT59',1,NULL,'<div class="prodid-question">
  <p>
    <b>Demographic Transition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transition from high birth and mortality rates to lower birth and
						mortality rates, seen as a country develops from a preindustrial to an
						industrialized economic system.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 187,'MCAT6',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aggression</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A behavior with the intention to cause harm or increase relative social
						dominance; can be physical or verbal.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 188,'MCAT60',1,NULL,'<div class="prodid-question">
  <p>
    <b>Demographics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The statistical arm of sociology, which attempts to characterize and explain
						populations by quantitative analysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 189,'MCAT61',1,NULL,'<div class="prodid-question">
  <p>
    <b>Depressive Episode</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A period of at least two weeks in which there is a prominent and persistent
						depressed mood or lack of interest and at least four other depressive
						symptoms.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 190,'MCAT62',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deviance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The violation of norms, rules, or expectations within a society.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 191,'MCAT63',1,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Diagnostic and Statistical Manual of Mental Disorders </i>(DSM)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The guide by which most psychological disorders are characterized, described,
						and diagnosed; currently in its fifth edition (DSM–5, published May
						2013).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 192,'MCAT64',1,NULL,'<div class="prodid-question">
  <p>
    <b>Diencephalon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the embryonic forebrain that becomes the thalamus, hypothalamus,
						posterior pituitary gland, and pineal gland.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 193,'MCAT65',1,NULL,'<div class="prodid-question">
  <p>
    <b>Disconfirmation Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Idea that states that if evidence obtained during testing does not confirm a
						hypothesis, then the hypothesis is discarded or revised.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 194,'MCAT66',1,NULL,'<div class="prodid-question">
  <p>
    <b>Discrimination</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In classical conditioning, the process by which two similar but distinct
						conditioned stimuli produce different responses; in sociology, when
						individuals of a particular group are treated differently from others based
						on their group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 195,'MCAT67',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dishabituation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sudden increase in response to a stimulus, usually due to a change in the
						stimulus or the addition of another stimulus; sometimes called
						resensitization.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 196,'MCAT68',1,NULL,'<div class="prodid-question">
  <p>
    <b>Displacement</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A defense mechanism by which undesired urges are transferred from one target
						to another, more acceptable one.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 197,'MCAT69',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dissociative Disorders</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Disorders that involve a perceived separation from identity or the
						environment.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 198,'MCAT7',1,NULL,'<div class="prodid-question">
  <p>
    <b>Alertness</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>State of consciousness in which one is aware, able to think, and able to
						respond to the environment; nearly synonymous with arousal.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 199,'MCAT70',1,NULL,'<div class="prodid-question">
  <p>
    <b>Divided Attention</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ability to attend to multiple stimuli simultaneously and to perform
						multiple tasks at the same time.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 200,'MCAT71',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dramaturgical Approach</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Impression management theory that represents the world as a stage and
						individuals as actors performing to an audience.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 201,'MCAT72',1,NULL,'<div class="prodid-question">
  <p>
    <b>Drive Reduction Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory that explains motivation as being based on the goal of eliminating
						uncomfortable internal states.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 202,'MCAT73',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ego</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Freudian psychoanalysis, the part of the unconscious mind that mediates
						the urges of the id and superego; operates under the reality principle.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 203,'MCAT74',1,NULL,'<div class="prodid-question">
  <p>
    <b>Egocentrism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Self-centered view of the world in which one is not necessarily able to
						understand the experience of another person; seen in Piaget’s
						preoperational stage.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 204,'MCAT75',1,NULL,'<div class="prodid-question">
  <p>
    <b>Elaboration Likelihood Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theory in which attitudes are formed and changed through different routes
						of informational processing based on the degree of deep thought given to
						persuasive information.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 205,'MCAT76',1,NULL,'<div class="prodid-question">
  <p>
    <b>Elaborative Rehearsal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The association of information in short-term memory to information already
						stored in longterm memory; aids in long-term storage.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 206,'MCAT77',1,NULL,'<div class="prodid-question">
  <p>
    <b>Encoding</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process of receiving information and preparing it for storage; can be
						automatic or effortful.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 207,'MCAT78',1,NULL,'<div class="prodid-question">
  <p>
    <b>Errors of Growth</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Misuse of grammar characterized by universal application of a rule,
						regardless of exceptions; seen in children during language development.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 208,'MCAT79',1,NULL,'<div class="prodid-question">
  <p>
    <b>Escape</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of negative reinforcement in which one reduces the unpleasantness of
						something that already exists.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 209,'MCAT8',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aligning Actions</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An impression management strategy in which one makes questionable behavior
						acceptable through excuses.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 210,'MCAT80',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ethnocentrism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The practice of making judgments about other cultures based on the values and
						beliefs of one’s own culture.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 211,'MCAT81',1,NULL,'<div class="prodid-question">
  <p>
    <b>Explicit Memory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Memory that requires conscious recall, divided into facts (semantic memory)
						and experiences (episodic memory); also known as declarative memory.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 212,'MCAT82',1,NULL,'<div class="prodid-question">
  <p>
    <b>Extinction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In classical conditioning, the decrease in response resulting from repeated
						presentation of the conditioned stimulus without the presence of the
						unconditioned stimulus.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 213,'MCAT83',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fixation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In Freudian psychoanalysis, the result of overindulgence or frustration
						during a psychosexual stage; causes a neurotic pattern of personality based
						on that stage.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 214,'MCAT84',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fluid Intelligence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Ability to quickly identify relationships and connections, and then use those
						relationships and connections to make correct deductions.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 215,'MCAT85',1,NULL,'<div class="prodid-question">
  <p>
    <b>Foraging</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The act of searching for and exploiting food resources.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 216,'MCAT86',1,NULL,'<div class="prodid-question">
  <p>
    <b>Forebrain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the brain that is associated with complex perceptual, cognitive, and behavioral processes such as emotion and memory.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 217,'MCAT87',1,NULL,'<div class="prodid-question">
  <p>
    <b>Front Stage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In the dramaturgical approach, the setting where players are in front of an
						audience and perform roles that are in keeping with the image they hope to
						project about themselves.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 218,'MCAT88',1,NULL,'<div class="prodid-question">
  <p>
    <b>Frontal Lobe</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of the cerebral cortex that controls motor processing, executive
						function, and the integration of cognitive and behavioral processes.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 219,'MCAT89',1,NULL,'<div class="prodid-question">
  <p>
    <b>Functional Fixedness</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The inability to identify uses for an object beyond its usual purpose.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 220,'MCAT9',1,NULL,'<div class="prodid-question">
  <p>
    <b>Alter-Casting</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An impression management strategy in which one imposes an identity onto
						another person.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 221,'MCAT90',1,NULL,'<div class="prodid-question">
  <p>
    <b>Functionalism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A theoretical framework that explains how parts of society fit together to
						create a cohesive whole.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 222,'MCAT91',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fundamental Attribution Error</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The general bias toward making dispositional attributions rather than
						situational attributions when analyzing another person’s
						behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 223,'MCAT92',1,NULL,'<div class="prodid-question">
  <p>
    <b>Game Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A model that explains social interaction and decision-making as a game,
						including strategies, incentives, and punishments.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 224,'MCAT93',1,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Gemeinschaft </i>and <i>Gesellschaft</i></b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Theory that distinguishes between two major types of groups: communities
							(<i>Gemeinschaften</i>), which share beliefs, ancestry, or geography;
						and society (<i>Gesellschaften</i>), which work together toward a common
						goal.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 225,'MCAT94',1,NULL,'<div class="prodid-question">
  <p>
    <b>Generalization</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In classical conditioning, the process by which two distinct but similar
						stimuli come to produce the same response.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 226,'MCAT95',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gestalt Principles</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Ways for the brain to infer missing parts of an image when the image is
						incomplete.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 227,'MCAT96',1,NULL,'<div class="prodid-question">
  <p>
    <b>Group Polarization</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency toward decisions that are more extreme than the individual
						thoughts of the group members.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 228,'MCAT97',1,NULL,'<div class="prodid-question">
  <p>
    <b>Groupthink</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency for groups to make decisions based on ideas and solutions that
						arise within the group without considering outside ideas and ethics; based
						on pressure to conform and remain loyal to the group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 229,'MCAT98',1,NULL,'<div class="prodid-question">
  <p>
    <b>Habituation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in response caused by repeated exposure to a stimulus.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 230,'MCAT99',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hallucinations</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Perceptions that are not due to external stimuli but have a compelling sense
						of reality; drugs that cause hallucinations, such as lysergic acid
						diethylamide (LSD) or psilocybin-containing mushrooms, are termed
						hallucinogens.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 231,'MCAT231',2,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>α</i>-Helix</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An element of secondary structure, marked by clockwise coiling of amino acids
						around a central axis.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 232,'MCAT232',2,NULL,'<div class="prodid-question">
  <p>
    <b>Acetyl-CoA</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An important metabolic intermediate that links glycolysis and
						<i>β</i>-oxidation to the citric acid cycle; can also be converted
						into ketone bodies.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 233,'MCAT233',2,NULL,'<div class="prodid-question">
  <p>
    <b>Activation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Conversion of a biomolecule to its active or usable form, such as activating
						tRNA with an amino acid or activating a fatty acid with CoA to form fatty
						acyl-CoA.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 234,'MCAT234',2,NULL,'<div class="prodid-question">
  <p>
    <b>Active Site</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The catalytically active portion of an enzyme.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 235,'MCAT235',2,NULL,'<div class="prodid-question">
  <p>
    <b>Active Transport</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of a molecule against its concentration gradient with energy
						investment; primary active transport uses ATP, whereas secondary active
						transport uses a favorable transport gradient of a different molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 236,'MCAT236',2,NULL,'<div class="prodid-question">
  <p>
    <b>Activity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Also sometimes called velocity or rate, this is a measure of the catalytic
						activity of an enzyme. It is often measured as a <i>v</i></b>
    <sub>
      <b>max</b>
    </sub>
    <b> and may be analyzed after protein isolation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 237,'MCAT237',2,NULL,'<div class="prodid-question">
  <p>
    <b>Adenosine Triphosphate (ATP)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The primary energy molecule of the body; it releases energy by breaking the
						bond with the terminal phosphate to form ADP and an inorganic phosphate.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 238,'MCAT238',2,NULL,'<div class="prodid-question">
  <p>
    <b>Aerobic Respiration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Energy-producing metabolic processes that require oxygen, including the
						citric acid cycle, electron transport chain, and oxidative
						phosphorylation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 239,'MCAT239',2,NULL,'<div class="prodid-question">
  <p>
    <b>Allosteric Enzymes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Enzymes that experience changes in their conformation as a result of
						interactions at sites other than the active site called allosteric sites;
						conformational changes may increase or decrease enzyme activity.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 240,'MCAT240',2,NULL,'<div class="prodid-question">
  <p>
    <b>Alternative Splicing</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The production of multiple different but related mRNA molecules from a single
						primary transcript of hnRNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 241,'MCAT241',2,NULL,'<div class="prodid-question">
  <p>
    <b>Amino Acid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A dipolar compound containing an amino group (–NH</b>
    <sub>
      <b>2</b>
    </sub>
    <b> ) and a carboxyl group (–COOH).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 242,'MCAT242',2,NULL,'<div class="prodid-question">
  <p>
    <b>Amplification</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Increased transcription (and translation) of a gene in response to hormones,
						growth factors, and other intracellular conditions.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 243,'MCAT243',2,NULL,'<div class="prodid-question">
  <p>
    <b>Anabolism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Metabolic processes that result in the consumption of energy and the synthesis of molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 244,'MCAT244',2,NULL,'<div class="prodid-question">
  <p>
    <b>Anaerobic Respiration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Energy-producing metabolic processes that do not require oxygen, including
						glycolysis and fermentation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 245,'MCAT245',2,NULL,'<div class="prodid-question">
  <p>
    <b>Anomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A subtype of epimers in which the chiral carbon with inverted configuration is the carbonyl carbon (anomeric carbon).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 246,'MCAT246',2,NULL,'<div class="prodid-question">
  <p>
    <b>Anticodon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A three-nucleotide sequence on a tRNA molecule that pairs with a corresponding mRNA codon during translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 247,'MCAT247',2,NULL,'<div class="prodid-question">
  <p>
    <b>Apoenzyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme devoid of the prosthetic group coenzyme, or cofactor necessary for normal activity.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 248,'MCAT248',2,NULL,'<div class="prodid-question">
  <p>
    <b>Apolipoprotein</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Protein molecules responsible for the interaction of lipoproteins with cells
						and the transfer of lipid molecules between lipoproteins; also called
						apoproteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 249,'MCAT249',2,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>β</i>-Oxidation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The catabolism of fatty acids to acetyl-CoA.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 250,'MCAT250',2,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>β</i>-Pleated Sheet</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An element of secondary structure, marked by peptide chains lying alongside
						one another, forming rows or strands.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 251,'MCAT251',2,NULL,'<div class="prodid-question">
  <p>
    <b>Basal Metabolic Rate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of energy consumed in a given period of time by an organism while
						resting.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 252,'MCAT252',2,NULL,'<div class="prodid-question">
  <p>
    <b>Bradford Protein Assay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A colorimetric method of determining the concentration of protein in an
						isolate against a protein standard; relies on a transition of absorption
						between bound and unbound Coomassie Brilliant Blue dye.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 253,'MCAT253',2,NULL,'<div class="prodid-question">
  <p>
    <b>Catabolism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Metabolic processes that result in the release of energy and the breakdown of
						molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 254,'MCAT254',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cell Adhesion Molecules</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Specialized structural proteins that are involved in cell-to-cell junctions
						as well as transient cellular interactions; common cell adhesion molecules
						include cadherins, integrins, and selectins.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 255,'MCAT255',2,NULL,'<div class="prodid-question">
  <p>
    <b>Central Dogma of Molecular Biology</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The major steps in the transfer of genetic information, from transcription of
						DNA to RNA and translation of that RNA to protein.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 256,'MCAT256',2,NULL,'<div class="prodid-question">
  <p>
    <b>Centrifugation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process of separating components on the basis of their density and
						resistance to flow by spinning a sample at very high speeds; the most dense
						components form a solid pellet and the least dense components remain in the
						supernatant (liquid portion).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 257,'MCAT257',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ceramide</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The simplest sphingolipid, with a single hydrogen as its head group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 258,'MCAT258',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cerebroside</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sphingolipid containing a carbohydrate as a head group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 259,'MCAT259',2,NULL,'<div class="prodid-question">
  <p>
    <b>Chaperones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Proteins that assist in protein folding during posttranslational
						processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 260,'MCAT260',2,NULL,'<div class="prodid-question">
  <p>
    <b>Chemiosmotic Coupling</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Utilization of the proton-motive force generated by the electron transport
						chain to drive ATP synthesis in oxidative phosphorylation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 261,'MCAT261',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cholesterol</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A molecule containing four linked aromatic rings; cholesterol provides both
						fluidity and stability to cell membranes and is the precursor for steroid
						hormones.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 262,'MCAT262',2,NULL,'<div class="prodid-question">
  <p>
    <b>Citric Acid Cycle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A metabolic pathway that produces GTP, energy carriers, and carbon dioxide as
						it burns acetyl-CoA; also called the Krebs cycle or tricarboxylic acid (TCA)
						cycle; can share intermediates with many other metabolic processes including
						fatty acid and cholesterol synthesis, gluconeogenesis, amino acid
						metabolism, and others.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 263,'MCAT263',2,NULL,'<div class="prodid-question">
  <p>
    <b>Coding Strand</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strand of DNA that is not used as a template during transcription; also
						called the sense strand.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 264,'MCAT264',2,NULL,'<div class="prodid-question">
  <p>
    <b>Codon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A three-nucleotide sequence in an mRNA molecule that pairs with an
						appropriate tRNA anticodon during translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 265,'MCAT265',2,NULL,'<div class="prodid-question">
  <p>
    <b>Coenzyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An organic molecule that helps an enzyme carry out its function.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 266,'MCAT266',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cofactor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An inorganic molecule or ion that helps an enzyme carry out its function.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 267,'MCAT267',2,NULL,'<div class="prodid-question">
  <p>
    <b>Competitive Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in enzyme activity that results from the interaction of an
						inhibitor with the active site of an enzyme; competitive inhibition can be
						overcome by addition of excess substrate.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 268,'MCAT268',2,NULL,'<div class="prodid-question">
  <p>
    <b>Conformational Coupling</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A less-accepted mechanism of ATP synthase activity in which the protons cause
						a conformational change that releases ATP from ATP synthase.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 269,'MCAT269',2,NULL,'<div class="prodid-question">
  <p>
    <b>Conjugated Protein</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A protein that derives part of its function from covalently attached
						molecules (prosthetic groups).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 270,'MCAT270',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cooperativity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The interaction between subunits of a multisubunit protein in which binding
						of substrate to one subunit increases the affinity of other subunits for the
						substrate; unbinding of substrate from one subunit decreases the affinity of
						other subunits for the substrate.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 271,'MCAT271',2,NULL,'<div class="prodid-question">
  <p>
    <b>Corepressor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A species that binds with a repressor, allowing the complex to bind to the
						operator region of an operon, stopping transcription of the relevant
						gene.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 272,'MCAT272',2,NULL,'<div class="prodid-question">
  <p>
    <b>Cristae</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Infoldings of the inner mitochondrial membrane that increase the surface area
						available for electron transport chain complexes.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 273,'MCAT273',2,NULL,'<div class="prodid-question">
  <p>
    <b>Degenerate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Description of the genetic code, in which more than one codon can specify a
						single amino acid.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 274,'MCAT274',2,NULL,'<div class="prodid-question">
  <p>
    <b>Denaturation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The loss of tertiary structure in a protein, leading to loss of function.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 275,'MCAT275',2,NULL,'<div class="prodid-question">
  <p>
    <b>Deoxyribonucleic Acid (DNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nucleic acid found exclusively in the nucleus that codes for all the of the
						genes necessary for life; transcribed to mRNA and always read 5&#39; to 3&#39;.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 276,'MCAT276',2,NULL,'<div class="prodid-question">
  <p>
    <b>Desmosomes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell-to-cell junctions that anchor layers of epithelial cells to one
						another.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 277,'MCAT277',2,NULL,'<div class="prodid-question">
  <p>
    <b>Disulfide Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A covalent interaction between the –SH groups of two cysteine
						residues; an element of tertiary and quaternary structure.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 278,'MCAT278',2,NULL,'<div class="prodid-question">
  <p>
    <b>Electrochemical Gradient</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An uneven separation of ions across a biological membrane, resulting in
						electrical potential energy.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 279,'MCAT279',2,NULL,'<div class="prodid-question">
  <p>
    <b>Electrophoresis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process of separating compounds on the basis of size and charge using a
						porous gel and an electric field; protein electrophoresis generally uses
						polyacrylamide, while nucleic acid electrophoresis generally uses
						agarose.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 280,'MCAT280',2,NULL,'<div class="prodid-question">
  <p>
    <b>Elongation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The three-step cycle repeated for each amino acid being added to a protein
						during translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 281,'MCAT281',2,NULL,'<div class="prodid-question">
  <p>
    <b>Endocytosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transport of molecules into a cell through invagination of the cell
						membrane and the formation of a vesicle; phagocytosis is the endocytosis of
						solids, pinocytosis is the endocytosis of liquids.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 282,'MCAT282',2,NULL,'<div class="prodid-question">
  <p>
    <b>Enhancer</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A collection of several response elements that allow for the control of one
						gene’s expression by multiple signals.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 283,'MCAT283',2,NULL,'<div class="prodid-question">
  <p>
    <b>Enzyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A biological molecule with catalytic activity; includes many proteins and some RNA molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 284,'MCAT284',2,NULL,'<div class="prodid-question">
  <p>
    <b>Epimers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A subtype of diastereomers that differ in absolute configuration at exactly
						one chiral carbon.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 285,'MCAT285',2,NULL,'<div class="prodid-question">
  <p>
    <b>Euchromatin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Looser, less dense collections of DNA that appear light-colored under the
						microscope; transcriptionally active.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 286,'MCAT286',2,NULL,'<div class="prodid-question">
  <p>
    <b>Exocytosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transport of molecules out of a cell by release from a transport vesicle;
						the vesicle fuses to the cell membrane during secretion.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 287,'MCAT287',2,NULL,'<div class="prodid-question">
  <p>
    <b>Exon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of hnRNA that is spliced together with other exons to form mature
						mRNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 288,'MCAT288',2,NULL,'<div class="prodid-question">
  <p>
    <b>Facilitated Diffusion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of solute molecules through the cell membrane down their
						concentration gradient through a transport protein or channel; used for ions
						and large or polar molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 289,'MCAT289',2,NULL,'<div class="prodid-question">
  <p>
    <b>Fatty Acid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A monocarboxylic acid without additional substituents; fatty acids may be
						saturated (all single bonds) or unsaturated (contain at least one double
						bond); natural fatty acids are in the cis conformation.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 290,'MCAT290',2,NULL,'<div class="prodid-question">
  <p>
    <b>Feedback Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The inhibition of an enzyme by its product (or a product further down in a
						metabolic pathway); used to maintain homeostasis.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 291,'MCAT291',2,NULL,'<div class="prodid-question">
  <p>
    <b>Feed-Forward Activation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The stimulation of an enzyme by an intermediate that precedes the enzyme in a
						metabolic pathway.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 292,'MCAT292',2,NULL,'<div class="prodid-question">
  <p>
    <b>Fermentation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The conversion of pyruvate to either ethanol and carbon dioxide (yeast) or
						lactic acid (animal cells); does not require oxygen.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 293,'MCAT293',2,NULL,'<div class="prodid-question">
  <p>
    <b>Fischer Projection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method of drawing organic molecules in which horizontal lines are coming
						out of the page (wedges) and vertical lines are going into the page
						(dashes).</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 294,'MCAT294',2,NULL,'<div class="prodid-question">
  <p>
    <b>Flavin Adenine Dinucleotide (FAD)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An energy carrier that accepts electrons and feeds them into the electron
						transport chain.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 295,'MCAT295',2,NULL,'<div class="prodid-question">
  <p>
    <b>Fluid Mosaic Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The representation of the plasma membrane as a dynamic phospholipid bilayer
						that interacts with cholesterol and proteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 296,'MCAT296',2,NULL,'<div class="prodid-question">
  <p>
    <b>Furanose</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A five-membered ring sugar.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 297,'MCAT297',2,NULL,'<div class="prodid-question">
  <p>
    <b>G Protein-Coupled Receptors</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A special class of membrane receptors with an associated GTP binding protein;
						activation of a G protein-coupled receptor involves dissociation and GTP
						hydrolysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 298,'MCAT298',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ganglioside</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sphingolipid with a head group containing an oligosaccharide and one or
						more <i>N</i> -acetylneuraminic acid (NANA) molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 299,'MCAT299',2,NULL,'<div class="prodid-question">
  <p>
    <b>Gap Junctions</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell-to-cell junctions that allow for the passage of small molecules between
						adjacent cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 300,'MCAT300',2,NULL,'<div class="prodid-question">
  <p>
    <b>Globoside</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sphingolipid with multiple carbohydrate groups attached as a head
						group.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 301,'MCAT301',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glucogenic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes amino acids that can be converted into intermediates that feed into
						gluconeogenesis; all amino acids except leucine and lysine.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 302,'MCAT302',2,NULL,'<div class="prodid-question">
  <p>
    <b>Gluconeogenesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The production of glucose from other biomolecules; carried out by the liver
						and kidney.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 303,'MCAT303',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glucose</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The primary monosaccharide used for fuel by all cells of the body; has the
						formula C</b>
    <sub>
      <b>6</b>
    </sub>
    <b> H</b>
    <sub>
      <b>12</b>
    </sub>
    <b> O</b>
    <sub>
      <b>6</b>
    </sub>
    <b> .</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 304,'MCAT304',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycerol</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A three-carbon alcohol that serves as the backbone for glycerophospholipids,
						sphingolipids, and triacylglycerols.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 305,'MCAT305',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycerophospholipid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A lipid containing a glycerol backbone with a phosphate group; bound by ester
						linkages to two fatty acids.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 306,'MCAT306',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycogen</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A branched polymer of glucose that represents a storage form of glucose.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 307,'MCAT307',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycolysis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The breakdown of glucose into two molecules of pyruvate with the formation of
						energy carriers (NADH); occurs under both aerobic and anaerobic
						conditions.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 308,'MCAT308',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycosidic Linkage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The bond between the anomeric carbon of a sugar and another molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 309,'MCAT309',2,NULL,'<div class="prodid-question">
  <p>
    <b>Glycosphingolipid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sphingolipid with a head group composed of sugars; includes cerebrosides
						and globosides.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 310,'MCAT310',2,NULL,'<div class="prodid-question">
  <p>
    <b>Helicase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that unwinds the double helix of a DNA molecule, allowing
						replication to take place.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 311,'MCAT311',2,NULL,'<div class="prodid-question">
  <p>
    <b>Heterochromatin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Dense, tightly coiled DNA that appears dark-colored under the microscope;
						transcriptionally inactive.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 312,'MCAT312',2,NULL,'<div class="prodid-question">
  <p>
    <b>Heterogeneous Nuclear RNA (hnRNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Preprocessed converted to mRNA by adding a poly-A tail and 5&#39; cap and mRNA;
						splicing out introns.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 313,'MCAT313',2,NULL,'<div class="prodid-question">
  <p>
    <b>Histone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A structural protein around which DNA is coiled in eukaryotic cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 314,'MCAT314',2,NULL,'<div class="prodid-question">
  <p>
    <b>Holoenzyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that has already bound a required prosthetic group, coenzyme, or
						cofactor.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.656793',0,'skipped');
INSERT INTO flashcards VALUES(1, 315,'MCAT315',2,NULL,'<div class="prodid-question">
  <p>
    <b>Hydrophilic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Being attracted to water; describes polar and charged compounds and those
						that can participate in hydrogen bonding.</b>
  </p>
</div>','','2014-07-24 12:15:09.656793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 316,'MCAT316',2,NULL,'<div class="prodid-question">
  <p>
    <b>Hydrophobic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Being repelled by water; describes nonpolar, uncharged compounds (usually
						lipids or certain R groups of amino acids).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 317,'MCAT317',2,NULL,'<div class="prodid-question">
  <p>
    <b>Hypertonic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution that has a greater concentration than the one to which it is being
						compared.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 318,'MCAT318',2,NULL,'<div class="prodid-question">
  <p>
    <b>Hypotonic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution that has a lower concentration than the one to which it is being compared.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 319,'MCAT319',2,NULL,'<div class="prodid-question">
  <p>
    <b>Induced Fit Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The best-supported of the most prominent theories of enzyme specificity;
						states that the enzyme and substrate experience a change in conformation
						during binding to increase complementarity. Usually contrasted with the lock
						and key theory.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 320,'MCAT320',2,NULL,'<div class="prodid-question">
  <p>
    <b>Inducible System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An operon that requires an inducer to remove a repressor protein from the
						operator site to begin transcription of the relevant gene; also called a
						positive control system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 321,'MCAT321',2,NULL,'<div class="prodid-question">
  <p>
    <b>Initiation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The start of translation, in which the small subunit of the ribosome binds to
						the mRNA molecule, and the first tRNA (methionine or
						<i>N</i>-formylmethionine) is bound to the start codon (AUG).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 322,'MCAT322',2,NULL,'<div class="prodid-question">
  <p>
    <b>Intron</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A portion of hnRNA that is spliced out to form mRNA; remains in the nucleus
						during processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 323,'MCAT323',2,NULL,'<div class="prodid-question">
  <p>
    <b>Irreversible Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in enzyme activity that results from the interaction of an
						inhibitor that binds permanently at either the active site or an allosteric
						site; in laboratory settings, irreversible inhibitors are sometimes called
						suicide substrates.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 324,'MCAT324',2,NULL,'<div class="prodid-question">
  <p>
    <b>Isoelectric Focusing</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A specialized method of separating proteins by their isoelectric point using
						electrophoresis; the gel is modified to possess a pH gradient.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 325,'MCAT325',2,NULL,'<div class="prodid-question">
  <p>
    <b>Isoelectric Point (pI)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The pH at which an amino acid is predominantly in zwitterionic form.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 326,'MCAT326',2,NULL,'<div class="prodid-question">
  <p>
    <b>Isoform</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A slightly different version of the same protein, often specific to a given
						tissue.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 327,'MCAT327',2,NULL,'<div class="prodid-question">
  <p>
    <b>Isotonic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution that has the same concentration as the one to which it is being
						compared.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 328,'MCAT328',2,NULL,'<div class="prodid-question">
  <p>
    <b>Jacob–Monod Model</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The description of the structure and function of operons in prokaryotes, in
						which operons have structural genes, an operator site, a promoter site, and
						a regulator gene.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 329,'MCAT329',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ketogenesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The synthesis of ketone bodies from the metabolic products of
						<i>β</i>-oxidation or amino acid metabolism; occurs under conditions
						of starvation.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 330,'MCAT330',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ketogenic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes amino acids that can be converted into intermediates that feed into
						ketogenesis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 331,'MCAT331',2,NULL,'<div class="prodid-question">
  <p>
    <b>Kinase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A specific transferase enzyme that catalyzes the movement of a phosphate
						group, generally from ATP, to a molecule of interest.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 332,'MCAT332',2,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>K</i>
    </b>
    <sub>
      <b>m</b>
    </sub>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The concentration of substrate at which an enzyme runs at half its maximal
						velocity; a measure of enzyme affinity (the higher the <i>K</i></b>
    <sub>
      <b>m</b>
    </sub>
    <b>, the lower the affinity).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 333,'MCAT333',2,NULL,'<div class="prodid-question">
  <p>
    <b>Lagging Strand</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strand of DNA that is synthesized in small fragments, called Okazaki
						fragments, and then ligated together. The Okazaki fragments are synthesized
						in the 5&#39; to 3&#39; direction, but the overall synthesis is in the 3&#39; to 5&#39;
						direction.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 334,'MCAT334',2,NULL,'<div class="prodid-question">
  <p>
    <b>Lariat</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The lasso-shaped structure formed during the removal of introns in mRNA
						processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 335,'MCAT335',2,NULL,'<div class="prodid-question">
  <p>
    <b>Leading Strand</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strand of DNA that is continuously synthesized in the 5&#39; to 3&#39; direction.
						The template strand is read in the 3&#39; to 5&#39; direction.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 336,'MCAT336',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ligase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that catalyzes the joining of large polymeric biomolecules, most
						commonly nucleic acids.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 337,'MCAT337',2,NULL,'<div class="prodid-question">
  <p>
    <b>Lipoprotein</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transport mechanism for lipids within the circulatory and lymphatic
						systems; includes chylomicrons and VLDL, which transport mostly
						triacylglycerols, and HDL, IDL, and LDL, which transport mostly cholesterol
						and cholesteryl esters.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 338,'MCAT338',2,NULL,'<div class="prodid-question">
  <p>
    <b>Lock and Key Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>One of the two most prominent theories of enzyme specificity; states that the
						enzyme and the substrate have a static but complimentary state. Less
						well-supported than the induced fit model.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 339,'MCAT339',2,NULL,'<div class="prodid-question">
  <p>
    <b>Lyase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that catalyzes the cleavage or synthesis of a molecule without the
						addition or loss of water.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 340,'MCAT340',2,NULL,'<div class="prodid-question">
  <p>
    <b>Matrix</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Contents of the inner mitochondrial membrane; includes soluble enzymes of the
						electron transport chain and mitochondrial DNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 341,'MCAT341',2,NULL,'<div class="prodid-question">
  <p>
    <b>Membrane Receptors</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Transmembrane protein molecules that act enzymatically or as ion channels to
						participate in signal transduction.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 342,'MCAT342',2,NULL,'<div class="prodid-question">
  <p>
    <b>Messenger RNA (mRNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strand of RNA formed after transcription of DNA; moves to the cytoplasm
						to be translated.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 343,'MCAT343',2,NULL,'<div class="prodid-question">
  <p>
    <b>Micelle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A collection of fatty acid or phospholipid molecules oriented to minimize
						free energy through hydrophobic and hydrophilic interactions; generally a
						sphere with a hydrophobic core and hydrophilic exterior.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 344,'MCAT344',2,NULL,'<div class="prodid-question">
  <p>
    <b>Mixed Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in enzyme activity that results from the interaction of an
						inhibitor with an allosteric site; mixed inhibitors bind to the free enzyme
						and to the substrate-bound enzyme with different affinities. They cannot be
						overcome by addition of substrate and impact both <i>K</i></b>
    <sub>
      <b>m</b>
    </sub>
    <b> and <i>v</i></b>
    <sub>
      <b>max</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 345,'MCAT345',2,NULL,'<div class="prodid-question">
  <p>
    <b>Monocistronic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The coding pattern of eukaryotes in which one mRNA molecule codes for only
						one protein.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 346,'MCAT346',2,NULL,'<div class="prodid-question">
  <p>
    <b>Motor Proteins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Proteins that are involved in cell motility through interactions with
						structural proteins; motor proteins have ATPase activity and include myosin,
						kinesin, and dynein.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 347,'MCAT347',2,NULL,'<div class="prodid-question">
  <p>
    <b>Mutarotation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The rapid interconversion between different anomers of a sugar.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 348,'MCAT348',2,NULL,'<div class="prodid-question">
  <p>
    <b>Nicotinamide Adenine Dinucleotide (NAD</b>
    <sup>
      <b>+</b>
    </sup>
    <b>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An energy carrier that accepts electrons and feeds them into the electron
						transport chain.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 349,'MCAT349',2,NULL,'<div class="prodid-question">
  <p>
    <b>Nicotinamide Adenine Dinucleotide Phosphate (NADP</b>
    <sup>
      <b>+</b>
    </sup>
    <b>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An electron donor important in the pentose phosphate pathway that is involved
						in biosynthesis, oxidative stress, and immune function.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 350,'MCAT350',2,NULL,'<div class="prodid-question">
  <p>
    <b>Noncompetitive Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in enzyme activity that results from the interaction of an
						inhibitor with an allosteric site; noncompetitive inhibitors bind equally
						well to free enzymes and to substratebound enzymes. They cannot be overcome
						by addition of substrate.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 351,'MCAT351',2,NULL,'<div class="prodid-question">
  <p>
    <b>Nontemplate Synthesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The method of <i>de</i><i>novo</i> synthesis of lipids and carbohydrates that relies on gene
						expression and enzyme specificity rather than the genetic template of DNA or
						RNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 352,'MCAT352',2,NULL,'<div class="prodid-question">
  <p>
    <b>Operator Site</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A component of the operon in prokaryotes; a nontranscribable region of DNA
						that is capable of binding a repressor protein.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 353,'MCAT353',2,NULL,'<div class="prodid-question">
  <p>
    <b>Operon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In prokaryotes, a cluster of genes transcribed as a single mRNA that can be
						regulated by repressors or inducers, depending on the system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 354,'MCAT354',2,NULL,'<div class="prodid-question">
  <p>
    <b>Osmosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The simple diffusion of water.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 355,'MCAT355',2,NULL,'<div class="prodid-question">
  <p>
    <b>Osmotic Pressure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The pressure necessary to counteract the effect of an osmotic gradient
						against pure water; one of the colligative properties; can be thought of as
						a “sucking” pressure created by solutes drawing in water.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 356,'MCAT356',2,NULL,'<div class="prodid-question">
  <p>
    <b>Oxidative Phosphorylation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transfer of a phosphate group, generally to ATP, which is powered by a
						gradient formed by oxidation–reduction reactions; occurs in the
						mitochondria.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 357,'MCAT357',2,NULL,'<div class="prodid-question">
  <p>
    <b>Pancreatic Proteases</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The enzymes that are primarily responsible for the digestion of proteins in
						the small intestine; they include trypsin, chymotrypsin, and
						carboxypeptidases A and B, all of which are secreted as zymogens.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 358,'MCAT358',2,NULL,'<div class="prodid-question">
  <p>
    <b>Paracellular Transport</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Transport of materials through the interstitial space without interactions
						with the cytoplasm or cell membrane.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 359,'MCAT359',2,NULL,'<div class="prodid-question">
  <p>
    <b>Passive Transport</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of a molecule down its concentration gradient without energy
						investment; includes simple and facilitated diffusion and osmosis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 360,'MCAT360',2,NULL,'<div class="prodid-question">
  <p>
    <b>Pentose Phosphate Pathway</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A metabolic process that produces NADPH and ribose 5-phosphate for nucleotide
						synthesis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 361,'MCAT361',2,NULL,'<div class="prodid-question">
  <p>
    <b>Peptide</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A molecule composed of more than one amino acid; can be subdivided into
						dipeptides (two amino acids), tripeptides (three), oligopeptides (up to 20)
						and polypeptides (more than 20).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 362,'MCAT362',2,NULL,'<div class="prodid-question">
  <p>
    <b>Peptide Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An amide bond between the carboxyl group of one amino acid and the amino
						group of another amino acid.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 363,'MCAT363',2,NULL,'<div class="prodid-question">
  <p>
    <b>Phospholipid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A lipid containing a phosphate and an alcohol (glycerol or sphingosine)
						joined to hydrophobic fatty acid tails.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 364,'MCAT364',2,NULL,'<div class="prodid-question">
  <p>
    <b>Polycistronic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The coding pattern of prokaryotes, in which one mRNA may code for multiple
						proteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 365,'MCAT365',2,NULL,'<div class="prodid-question">
  <p>
    <b>Polysaccharide</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A long chain of monosaccharides linked by glycosidic bonds; can be divided
						into homopolysaccharides (only one type of monosaccharide is used) and
						heteropolysaccharides (more than one type of monosaccharide is used).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 366,'MCAT366',2,NULL,'<div class="prodid-question">
  <p>
    <b>Primary Structure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The linear sequence of amino acids in a polypeptide.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 367,'MCAT367',2,NULL,'<div class="prodid-question">
  <p>
    <b>Promoter Region</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Portion of DNA upstream from a gene; contains the TATA box, which is the site
						where RNA polymerase II binds to start transcription.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 368,'MCAT368',2,NULL,'<div class="prodid-question">
  <p>
    <b>Prostaglandins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A group of 20-carbon molecules that are unsaturated carboxylic acids derived
						from arachidonic acid; act as paracrine or autocrine hormones.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 369,'MCAT369',2,NULL,'<div class="prodid-question">
  <p>
    <b>Prosthetic Group</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cofactor or coenzyme that is covalently bonded to a protein to permit its
						function.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 370,'MCAT370',2,NULL,'<div class="prodid-question">
  <p>
    <b>Proton-Motive Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The proton concentration gradient across the inner mitochondrial membrane,
						which is created in the electron transport chain and used in oxidative
						phosphorylation.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 371,'MCAT371',2,NULL,'<div class="prodid-question">
  <p>
    <b>Pyranose</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A six-membered ring sugar.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 372,'MCAT372',2,NULL,'<div class="prodid-question">
  <p>
    <b>Pyruvate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An important metabolic intermediate that can feed into the citric acid cycle,
						fermentation, or gluconeogenesis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 373,'MCAT373',2,NULL,'<div class="prodid-question">
  <p>
    <b>Quaternary Structure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The interaction between different subunits of a multisubunit protein;
						stabilized by R group interactions.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 374,'MCAT374',2,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Coupling</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The tendency of unfavorable biological reactions to occur concurrently with
						favorable reactions, often catalyzed by a single enzyme.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 375,'MCAT375',2,NULL,'<div class="prodid-question">
  <p>
    <b>Reducing Sugar</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sugar that can reduce other compounds and that can be picked up by
						Tollen’s or Benedict’s reagent.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 376,'MCAT376',2,NULL,'<div class="prodid-question">
  <p>
    <b>Release Factor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The protein that binds to the stop codon during termination of
						translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 377,'MCAT377',2,NULL,'<div class="prodid-question">
  <p>
    <b>Renaturation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Regaining the correct tertiary structure after denaturation of a protein.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 378,'MCAT378',2,NULL,'<div class="prodid-question">
  <p>
    <b>Repressible System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An operon that requires a repressor to bind to a corepressor before binding
						to the operator site to stop transcription of the relevant gene; also called
						a negative control system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 379,'MCAT379',2,NULL,'<div class="prodid-question">
  <p>
    <b>Respiratory Quotient</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A numerical representation that can be used to determine the prevalent type
						of biomolecule being used in metabolism; the ratio of carbon dioxide
						produced to oxygen consumed.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 380,'MCAT380',2,NULL,'<div class="prodid-question">
  <p>
    <b>Resting Membrane Potential</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrical potential that results from the unequal distribution of charge
						around the cell membrane; resting membrane potential characterizes a cell
						that has not been stimulated.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 381,'MCAT381',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ribosomal RNA (rRNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The structural and enzymatic RNA found in ribosomes that takes part in
						translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 382,'MCAT382',2,NULL,'<div class="prodid-question">
  <p>
    <b>Ribozyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An RNA molecule with enzymatic activity.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 383,'MCAT383',2,NULL,'<div class="prodid-question">
  <p>
    <b>Saponification</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The reaction between a fatty acid and a strong base, resulting in a
						negatively charged fatty acid anion bound to a metal ion; creates soap.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 384,'MCAT384',2,NULL,'<div class="prodid-question">
  <p>
    <b>Saturation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Refers to the presence or absence of double bonds in a fatty acid; saturated
						fatty acids have only single bonds, whereas unsaturated fatty acids have at
						least one double bond.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 385,'MCAT385',2,NULL,'<div class="prodid-question">
  <p>
    <b>Secondary Structure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The local structure of neighboring amino acids; most common are
							<i>α</i>-helices and <i>β</i>-pleated sheets.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 386,'MCAT386',2,NULL,'<div class="prodid-question">
  <p>
    <b>Shine–Dalgarno Sequence</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The site of initiation of translation in prokaryotes.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 387,'MCAT387',2,NULL,'<div class="prodid-question">
  <p>
    <b>Shuttle Mechanism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method of functionally transferring a compound across a membrane without
						the actual molecule crossing; common examples are the glycerol 3-phosphate
						shuttle and the malate–aspartate shuttle.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 388,'MCAT388',2,NULL,'<div class="prodid-question">
  <p>
    <b>Side Chain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The variable component of an amino acid that gives it its identity and
						chemical properties; also called an R group.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 389,'MCAT389',2,NULL,'<div class="prodid-question">
  <p>
    <b>Simple Diffusion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of solute molecules through the cell membrane down their
						concentration gradient without a transport protein; used for small,
						nonpolar, lipophilic molecules and water.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 390,'MCAT390',2,NULL,'<div class="prodid-question">
  <p>
    <b>Sphingolipid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A lipid containing a sphingosine or sphingoid backbone bound to fatty acid
						tails; includes ceramide, sphingomyelins, glycosphingolipids, and
						gangliosides.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 391,'MCAT391',2,NULL,'<div class="prodid-question">
  <p>
    <b>Sphingomyelin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sphingophospholipid containing a sphingosine backbone and a phosphate head
						group.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 392,'MCAT392',2,NULL,'<div class="prodid-question">
  <p>
    <b>Spliceosome</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The apparatus used for splicing out introns and bringing exons together
						during mRNA processing.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 393,'MCAT393',2,NULL,'<div class="prodid-question">
  <p>
    <b>Start Codon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The first codon in an mRNA molecule that codes for an amino acid (AUG for
						methionine or <i>N</i>-formylmethionine).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 394,'MCAT394',2,NULL,'<div class="prodid-question">
  <p>
    <b>Stop Codon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The last codon of translation (UAA, UGA, or UAG); release factor binds here,
						terminating translation.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 395,'MCAT395',2,NULL,'<div class="prodid-question">
  <p>
    <b>Structural Proteins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Proteins that are involved in the cytoskeleton and extracellular matrix; they
						are generally fibrous in nature and include collagen, elastin, keratin,
						actin, and tubulin.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 396,'MCAT396',2,NULL,'<div class="prodid-question">
  <p>
    <b>Substrate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The molecule upon which an enzyme acts.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 397,'MCAT397',2,NULL,'<div class="prodid-question">
  <p>
    <b>Substrate-Level Phosphorylation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The transfer of a phosphate group from a high-energy compound to ATP or
						another compound; occurs in glycolysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 398,'MCAT398',2,NULL,'<div class="prodid-question">
  <p>
    <b>Surfactant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that lowers surface tension by acting as a detergent or
						emulsifier.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 399,'MCAT399',2,NULL,'<div class="prodid-question">
  <p>
    <b>TATA box</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The site of binding for RNA polymerase II during transcription; named for its
						high concentration of thymine and adenine bases.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 400,'MCAT400',2,NULL,'<div class="prodid-question">
  <p>
    <b>Template Strand</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The strand of DNA that is transcribed to form mRNA; also called the antisense
						strand.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 401,'MCAT401',2,NULL,'<div class="prodid-question">
  <p>
    <b>Termination</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The end of translation, in which the ribosome finds a stop codon and release
						factor binds to it, allowing the peptide to be freed from the ribosome.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 402,'MCAT402',2,NULL,'<div class="prodid-question">
  <p>
    <b>Terpene</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A class of lipids built from isoprene moieties; have carbon groups in
						multiples of five.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 403,'MCAT403',2,NULL,'<div class="prodid-question">
  <p>
    <b>Tertiary Structure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The three-dimensional shape of a polypeptide, stabilized by numerous
						interactions between R groups.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 404,'MCAT404',2,NULL,'<div class="prodid-question">
  <p>
    <b>Tight Junctions</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell-to-cell junctions that prevent the paracellular transport of materials;
						tight junctions form a collar around cells and link cells within a single
						layer.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 405,'MCAT405',2,NULL,'<div class="prodid-question">
  <p>
    <b>Transcellular Transport</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Transport of materials through the cell; requires interaction with the
						cytoplasm and may require transport proteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 406,'MCAT406',2,NULL,'<div class="prodid-question">
  <p>
    <b>Transcription</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Production of an mRNA molecule from a strand of DNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 407,'MCAT407',2,NULL,'<div class="prodid-question">
  <p>
    <b>Transcription Factors</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Proteins that help RNA polymerase II locate and bind to the promoter region
						of DNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 408,'MCAT408',2,NULL,'<div class="prodid-question">
  <p>
    <b>Transfer RNA (tRNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A folded strand of RNA that contains a threenucleotide anticodon that pairs
						with an appropriate mRNA codon during translation and is charged with the
						corresponding amino acid.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 409,'MCAT409',2,NULL,'<div class="prodid-question">
  <p>
    <b>Transferase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that catalyzes the transfer of a functional group.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 410,'MCAT410',2,NULL,'<div class="prodid-question">
  <p>
    <b>Translation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Production of a protein from an mRNA molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 411,'MCAT411',2,NULL,'<div class="prodid-question">
  <p>
    <b>Triacylglycerol</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A glycerol molecule esterified to three fatty acid molecules; the most common
						form of fat storage within the body.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 412,'MCAT412',2,NULL,'<div class="prodid-question">
  <p>
    <b>Uncompetitive Inhibition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in enzyme activity that results from the interaction with an
						inhibitor at the allosteric site; uncompetitive inhibitors bind only to the
						substrate-bound enzyme and cannot be overcome by addition of substrate.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 413,'MCAT413',2,NULL,'<div class="prodid-question">
  <p>
    <b>Vitamin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An essential organic coenzyme that assists an enzyme in carrying out its
						action.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 414,'MCAT414',2,NULL,'<div class="prodid-question">
  <p>
    <b>Wax</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A lipid with a high melting point that is composed of a very long chain
						alcohol and a very long chain fatty acid.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 415,'MCAT415',2,NULL,'<div class="prodid-question">
  <p>
    <b>Wobble</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Description of the third nucleotide of a codon, which often plays no role in
						specifying an amino acid; an evolutionary development designed to protect
						against mutations.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 416,'MCAT416',2,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Zwitterion</i>
    </b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A molecule that contains charges, but is neutral overall. Most often used to
						describe amino acids.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 417,'MCAT417',2,NULL,'<div class="prodid-question">
  <p>
    <b>Zymogen</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme that is secreted in an inactive form and must be activated by
						cleavage; common examples are digestive enzymes.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 418,'MCAT418',3,NULL,'<div class="prodid-question">
  <p>
    <b>Acrosome</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The large vesicle at the head of a sperm cell containing enzymes that degrade
						the ovum cell membrane to allow fertilization.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 419,'MCAT419',3,NULL,'<div class="prodid-question">
  <p>
    <b>Action Potential</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A sharp change in the membrane potential of neurons or muscle cells caused by
						a change in the selective permeability to Na</b>
    <sup>
      <b>+</b>
    </sup>
    <b> and K</b>
    <sup>
      <b>+</b>
    </sup>
    <b> using voltage-gated ion channels. Action potentials are all-or-nothing
						events.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 420,'MCAT420',3,NULL,'<div class="prodid-question">
  <p>
    <b>Adaptive Immunity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A highly specific form of immunity that develops in response to exposure to
						pathogens; consists of both humoral immunity and cytotoxic immunity.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 421,'MCAT421',3,NULL,'<div class="prodid-question">
  <p>
    <b>Adrenal Cortex</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesizes and releases corticosteroids. Glucocorticoids are stimulated by
						adrenocorticotropic hormone (ACTH), whereas mineralocorticoids are
						stimulated by angiotensin II. Cortical sex hormones include androgens like
						testosterone.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 422,'MCAT422',3,NULL,'<div class="prodid-question">
  <p>
    <b>Adrenal Medulla</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesizes and releases epinephrine and norepinephrine, which stimulate an
						increase in the metabolic rate and blood glucose levels.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 423,'MCAT423',3,NULL,'<div class="prodid-question">
  <p>
    <b>Adrenocorticotropic Hormone (ACTH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, ACTH stimulates the
						adrenal cortex to synthesize and secrete glucocorticoids. ACTH is regulated
						by corticotropin releasing factor (CRF), which is released by the
						hypothalamus.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 424,'MCAT424',3,NULL,'<div class="prodid-question">
  <p>
    <b>Afferent Neurons</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Neurons that carry information to the central nervous system from the
						periphery. Also called sensory neurons.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 425,'MCAT425',3,NULL,'<div class="prodid-question">
  <p>
    <b>Aldosterone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A steroid hormone produced in the adrenal cortex that is responsible for
						reabsorption of sodium and water and excretion of potassium and hydrogen
						ions.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 426,'MCAT426',3,NULL,'<div class="prodid-question">
  <p>
    <b>Allantois</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The embryonic membrane that contains the growing embryo’s waste
						products.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 427,'MCAT427',3,NULL,'<div class="prodid-question">
  <p>
    <b>Alleles</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Genes coding for alternative forms of a given trait.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 428,'MCAT428',3,NULL,'<div class="prodid-question">
  <p>
    <b>Amino Acid-Derivative Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormones that are synthesized by modifying amino acids. Most amino
						acid-derivative hormones act via secondary messengers, while some act in a
						fashion similar to steroid hormones.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 429,'MCAT429',3,NULL,'<div class="prodid-question">
  <p>
    <b>Amnion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The innermost extraembryonic membrane; produces the amniotic fluid in which
						the growing fetus is suspended</b>.</p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 430,'MCAT430',3,NULL,'<div class="prodid-question">
  <p>
    <b>Anterior Pituitary</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesizes and releases many vital hormones, including follicle-stimulating
						hormone, luteinizing hormone, adrenocorticotropic hormone,
						thyroidstimulating hormone, prolactin, endorphins, and growth hormone
						(“FLAT PEG”). The anterior pituitary is under the hormonal
						control of the hypothalamus.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 431,'MCAT431',3,NULL,'<div class="prodid-question">
  <p>
    <b>Antidiuretic Hormone (ADH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A peptide hormone, also known as vasopressin, which acts on the collecting
						duct to increase water reabsorption. ADH is produced by the hypothalamus and
						stored in the posterior pituitary.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 432,'MCAT432',3,NULL,'<div class="prodid-question">
  <p>
    <b>Antigen</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A substance that is bound by an antibody, causing an immune reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 433,'MCAT433',3,NULL,'<div class="prodid-question">
  <p>
    <b>Appendicular Skeleton</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The bones of the pelvis, the pectoral girdles, and the limbs.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 434,'MCAT434',3,NULL,'<div class="prodid-question">
  <p>
    <b>Archenteron</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The central cavity in the gastrula stage of embryological development; it is
						lined by endoderm and ultimately gives rise to the adult digestive
						tract.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 435,'MCAT435',3,NULL,'<div class="prodid-question">
  <p>
    <b>Arteries</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Vessels that carry blood away from the heart. These vessels are muscular and
						do not have valves.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 436,'MCAT436',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ascending Limb of the Loop of Henle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Portion of the nephron not permeable to water. As the filtrate flows up the
						ascending limb through a decreasing concentration in the interstitium, NaCl
						is first passively, then actively removed from the filtrate, decreasing
						filtrate concentration.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 437,'MCAT437',3,NULL,'<div class="prodid-question">
  <p>
    <b>Atria</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The two thin-walled upper chambers of the heart. The right atrium receives
						deoxygenated blood from the venae cavae, while the left atrium receives
						oxygenated blood from the pulmonary veins.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 438,'MCAT438',3,NULL,'<div class="prodid-question">
  <p>
    <b>Atrioventricular Valves</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Valves located between the atria and the ventricles (tricuspid valve and
						mitral valve).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 439,'MCAT439',3,NULL,'<div class="prodid-question">
  <p>
    <b>Axial Skeleton</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The skull, vertebral column, ribcage, and hyoid bone.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 440,'MCAT440',3,NULL,'<div class="prodid-question">
  <p>
    <b>Axon Hillock</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The portion of the neuron that connects the cell body (soma) to the axon. The
						impulses the neuron receives from all the dendrites are summed up at the
						axon hillock to determine whether or not an action potential will be
						initiated.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 441,'MCAT441',3,NULL,'<div class="prodid-question">
  <p>
    <b>Bacteriophages</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Viruses that can only infect bacteria.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 442,'MCAT442',3,NULL,'<div class="prodid-question">
  <p>
    <b>Bile</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An alkaline fluid synthesized in the liver, stored in the gall bladder, and
						released into the duodenum. Bile aids in the emulsification, digestion, and
						absorption of fats.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 443,'MCAT443',3,NULL,'<div class="prodid-question">
  <p>
    <b>Binary Fission</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Method of asexual reproduction by which prokaryotes divide. The circular DNA
						molecule replicates and then moves to the edge of the cell. The cell then
						divides into two daughter cells of equal size.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 444,'MCAT444',3,NULL,'<div class="prodid-question">
  <p>
    <b>Blastulation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process by which a morula develops into a blastula with a fluid-filled
						cavity called a blastocoel.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 445,'MCAT445',3,NULL,'<div class="prodid-question">
  <p>
    <b>Blood Antigens</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Proteins found on the erythrocyte cell surface. Three antigens used to
						differentiate blood groups are A, B, and Rh. If a host organism is
						transfused with erythrocytes containing antigens that the host does not
						have, an immune response will be triggered, leading to hemolysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 446,'MCAT446',3,NULL,'<div class="prodid-question">
  <p>
    <b>Blood Buffer System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Relies primarily on the carbonic acid buffer system, demonstrated by the
						equation: </b>
  </p>
  <p>
    <img src="/assets/contents/MCAT446img1.png" />
  </p>
  <p>
    <b>Release of carbon dioxide causes increased formation of water and an increase
						in pH. Increased retention of <img src="/assets/contents/MCAT446img2.png" />
						causes the pH to rise as well.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 447,'MCAT447',3,NULL,'<div class="prodid-question">
  <p>
    <b>Calcitonin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormone synthesized and released by the thyroid gland that decreases plasma
						Ca</b>
    <sup>
      <b>2+</b>
    </sup>
    <b> concentration.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 448,'MCAT448',3,NULL,'<div class="prodid-question">
  <p>
    <b>Capillaries</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Blood vessels composed of a single layer of endothelial cells, facilitating
						exchange of gases, nutrients, and waste products between the blood and
						interstitial fluid.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 449,'MCAT449',3,NULL,'<div class="prodid-question">
  <p>
    <b>Carbonic Anhydrase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Enzyme that catalyzes the conversion of carbonic acid to carbon dioxide and
						water as well as the formation of carbonic acid from carbon dioxide and
						water. Important in the bicarbonate buffer system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 450,'MCAT450',3,NULL,'<div class="prodid-question">
  <p>
    <b>Cardiac Muscle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of muscle found within the heart; may contain one or two nuclei.
						Involuntary, like smooth muscle, but appears striated, like skeletal muscle.
						Able to depolarize independent of the nervous system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 451,'MCAT451',3,NULL,'<div class="prodid-question">
  <p>
    <b>Cartilage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A firm, elastic, translucent connective tissue consisting of collagenous
						fibers embedded in chondrin. Produced by cells called chondrocytes.
						Cartilage is the principal component of embryonic skeletons and can harden
						and calcify into bone (ossify).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 452,'MCAT452',3,NULL,'<div class="prodid-question">
  <p>
    <b>Catecholamines</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Norepinephrine, epinephrine, and dopamine—neurotransmitters; also,
						hormones produced by the adrenal cortex that play a significant role in the
						sympathetic nervous system.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 453,'MCAT453',3,NULL,'<div class="prodid-question">
  <p>
    <b>Cell-Mediated (Cytotoxic) Immunity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Branch of the immune system in which intracellular pathogens are eliminated
						by killing their host cells. T-cells are the primary mediators of cytotoxic
						immunity.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 454,'MCAT454',3,NULL,'<div class="prodid-question">
  <p>
    <b>Cell Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A foundational belief in modern biology that all living things are composed
						of cells, the cell is the basic functional unit of life, that all cells
						arise from preexisting cells, and that DNA is the genetic material.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 455,'MCAT455',3,NULL,'<div class="prodid-question">
  <p>
    <b>Central Nervous System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The central nervous system consists of the brain and the spinal cord.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 456,'MCAT456',3,NULL,'<div class="prodid-question">
  <p>
    <b>Centrosome</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The portion of the cell containing the centrioles.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 457,'MCAT457',3,NULL,'<div class="prodid-question">
  <p>
    <b>Chemical Digestion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Enzymatic breakdown of large molecules into smaller molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 458,'MCAT458',3,NULL,'<div class="prodid-question">
  <p>
    <b>Cholecystokinin (CCK)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A hormone that is secreted by the duodenum in response to the presence of
						chyme. CCK stimulates the release of bile and pancreatic enzymes into the
						small intestine, and promotes satiety.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 459,'MCAT459',3,NULL,'<div class="prodid-question">
  <p>
    <b>Chorion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The outermost extraembryonic membrane; contributes to the formation of the
						placenta.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 460,'MCAT460',3,NULL,'<div class="prodid-question">
  <p>
    <b>Chromatin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Chromosomes in their uncoiled state. Chromatin itself is not visible as
						organized chromosomes under a light microscope.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 461,'MCAT461',3,NULL,'<div class="prodid-question">
  <p>
    <b>Chyme</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Combination of partially digested food and acid that forms in the
						stomach.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 462,'MCAT462',3,NULL,'<div class="prodid-question">
  <p>
    <b>Codominance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes a situation in which an organism heterozygous for a trait will have
						a phenotype that expresses both alleles in full. Both alleles, therefore,
						are dominant.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 463,'MCAT463',3,NULL,'<div class="prodid-question">
  <p>
    <b>Collecting Duct</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Portion of the nephron permeable to water and ions. As the filtrate flows
						down the collecting duct through the increasing concentration of the
						interstitium, the filtrate is concentrated. The degree of water reabsorption
						in the collecting duct is controlled by the action of the hormone ADH.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 464,'MCAT464',3,NULL,'<div class="prodid-question">
  <p>
    <b>Compact Bone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Much more dense than spongy bone, compact bone consists of Haversian systems
						(osteons).</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 465,'MCAT465',3,NULL,'<div class="prodid-question">
  <p>
    <b>Conjugation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The temporary joining of two organisms via a tube called a pilus, through
						which genetic material is exchanged; a form of sexual reproduction used by
						bacteria.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 466,'MCAT466',3,NULL,'<div class="prodid-question">
  <p>
    <b>Corona Radiata</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Outer layer of cells surrounding the oocyte. These cells are derived from
						follicular cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 467,'MCAT467',3,NULL,'<div class="prodid-question">
  <p>
    <b>Corpus Luteum</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Tissue that forms from the collapsed ovarian follicle. Produces and secretes
						progesterone and estrogen.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 468,'MCAT468',3,NULL,'<div class="prodid-question">
  <p>
    <b>Crossing Over</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The exchange of genetic material between homologous chromosomes that occurs
						during prophase I of meiosis. Crossing over aids in evolution and genetic
						diversity by unlinking linked genes.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 469,'MCAT469',3,NULL,'<div class="prodid-question">
  <p>
    <b>Dendrite</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An extension of the neuron that transmits impulses toward the cell body.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 470,'MCAT470',3,NULL,'<div class="prodid-question">
  <p>
    <b>Depolarization</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process that occurs when the voltage-gated Na</b>
    <sup>
      <b>+</b>
    </sup>
    <b> channels open, allowing Na</b>
    <sup>
      <b>+</b>
    </sup>
    <b> to rush into the cell and depolarize it.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 471,'MCAT471',3,NULL,'<div class="prodid-question">
  <p>
    <b>Dermis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The layer of skin beneath the epidermis that is subdivided into the papillary
						layer and the reticular layer. It contains the sweat glands, sense organs,
						blood vessels, and the bulbs of hair follicles; it is derived from the
						mesoderm.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 472,'MCAT472',3,NULL,'<div class="prodid-question">
  <p>
    <b>Descending Limb of the Loop of Henle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Portion of the nephron permeable only to water. The filtrate becomes more
						concentrated (loses water) as it travels through the descending limb due to
						the increasing concentration of the interstitium.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 473,'MCAT473',3,NULL,'<div class="prodid-question">
  <p>
    <b>Determinate Cleavage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Division of cells during embryogenesis in which each cell specializes early
						in development. By extension, each cell is not necessarily able to
						differentiate into an entire organism on its own.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 474,'MCAT474',3,NULL,'<div class="prodid-question">
  <p>
    <b>Diaphysis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cylindrical shaft of a long bone. Filled with bone marrow for the production
						of blood cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 475,'MCAT475',3,NULL,'<div class="prodid-question">
  <p>
    <b>Diastole</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The stage of the cardiac cycle in which the heart muscle relaxes and collects
						blood into its chambers.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 476,'MCAT476',3,NULL,'<div class="prodid-question">
  <p>
    <b>Diploid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells with two copies of each chromosome, usually one from the mother and one
						from the father. Eukaryotic somatic cells are diploid.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 477,'MCAT477',3,NULL,'<div class="prodid-question">
  <p>
    <b>Direct Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormones that travel to a target tissue to causes an action without another
						hormone acting as an intermediary.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 478,'MCAT478',3,NULL,'<div class="prodid-question">
  <p>
    <b>Directional Selection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of natural selection where one extreme phenotype is favored over the
						average phenotype and other extreme phenotypes. </b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 479,'MCAT479',3,NULL,'<div class="prodid-question">
  <p>
    <b>Disjunction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The separation of homologous chromosomes during anaphase I of meiosis.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 480,'MCAT480',3,NULL,'<div class="prodid-question">
  <p>
    <b>Disruptive Selection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of natural selection where both phenotypic extremes are favored over the
						average phenotype.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 481,'MCAT481',3,NULL,'<div class="prodid-question">
  <p>
    <b>Dizygotic Twins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Results when two ova are fertilized by two different sperm. Because the two
						resulting embryos develop from distinct zygotes, they do not have identical
						alleles.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 482,'MCAT482',3,NULL,'<div class="prodid-question">
  <p>
    <b>Dominant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes an allele that requires only one copy to be expressed.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 483,'MCAT483',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ductus Arteriosus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A shunt that connects the pulmonary artery to the aorta in order to bypass
						the fetal lung.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 484,'MCAT484',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ductus Venosus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A shunt that connects the umbilical vein to the inferior vena cava in order
						to bypass the fetal liver.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 485,'MCAT485',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ectoderm</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The outermost of the three primary germ layers; gives rise to the skin,
						nervous system, lens of the eye, and inner ear.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 486,'MCAT486',3,NULL,'<div class="prodid-question">
  <p>
    <b>Efferent Neurons</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Neurons that carry information from the central nervous system to other parts
						of the body. Also called motor neurons.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 487,'MCAT487',3,NULL,'<div class="prodid-question">
  <p>
    <b>Endocrine Glands</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Glands that synthesize and secrete hormones into the circulatory system.
						Examples include the hypothalamus, pituitary gland, pineal gland, pancreas,
						testes, ovaries, adrenal glands, thyroid gland, and parathyroid glands.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 488,'MCAT488',3,NULL,'<div class="prodid-question">
  <p>
    <b>Endoderm</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The innermost of the three primary germ layers; gives rise to the linings of
						the digestive and respiratory tracts and to parts of the liver, pancreas,
						thyroid, and bladder.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 489,'MCAT489',3,NULL,'<div class="prodid-question">
  <p>
    <b>Endometrium</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The mucosal lining of the uterus where the embryo implants. Progesterone is
						necessary for the maintenance of the endometrium during pregnancy.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 490,'MCAT490',3,NULL,'<div class="prodid-question">
  <p>
    <b>Endorphins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, endorphins inhibit the
						perception of pain.</b>
  </p>
</div>','','2014-07-24 12:15:09.657793','2014-07-24 12:15:09.657793',0,'skipped');
INSERT INTO flashcards VALUES(1, 491,'MCAT491',3,NULL,'<div class="prodid-question">
  <p>
    <b>Endothelial Cells</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells that line blood vessels. These cells are able to produce and release
						chemicals that aid in vasodilation and vasoconstriction. In addition, damage
						to these cells results in the release of mediators that aid in clotting.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 492,'MCAT492',3,NULL,'<div class="prodid-question">
  <p>
    <b>Enteric Nervous System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A collection of millions of neurons within the gastrointestinal system that
						governs the function of the GI tract. This system is able to function
						independently of the brain and spinal cord.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 493,'MCAT493',3,NULL,'<div class="prodid-question">
  <p>
    <b>Enteropeptidase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Digestive enzyme secreted by cells in the duodenum. This enzyme converts
						trypsinogen to trypsin. Trypsin is then able to activate other pancreatic
						enzymes to allow digestion to continue within the duodenum.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 494,'MCAT494',3,NULL,'<div class="prodid-question">
  <p>
    <b>Epidermis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The outermost layer of skin, which is composed of the following sublayers:
						stratum basalis, stratum spinosum, stratum granulosum, stratum lucidum, and
						stratum corneum. Serves as a protective barrier against microbial attack.
						Derived from the ectoderm.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 495,'MCAT495',3,NULL,'<div class="prodid-question">
  <p>
    <b>Epiglottis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A flap of cartilage that covers the glottis when swallowing food in order to
						prevent food particles from entering the larynx.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 496,'MCAT496',3,NULL,'<div class="prodid-question">
  <p>
    <b>Epiphyseal Plate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Portion of the bone where growth occurs; located in the epiphysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 497,'MCAT497',3,NULL,'<div class="prodid-question">
  <p>
    <b>Epiphysis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Dilated ends of long bones in the appendicular skeleton.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 498,'MCAT498',3,NULL,'<div class="prodid-question">
  <p>
    <b>Episomes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Plasmids that have the ability to integrate into the host genome.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 499,'MCAT499',3,NULL,'<div class="prodid-question">
  <p>
    <b>Erythrocytes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The oxygen-carrying component of blood. These anaerobic cells, which lack
						organelles, are packed with hemoglobin and have a characteristic biconcave,
						disk-like shape that facilitates gas exchange and mobility within blood
						vessels. Also called red blood cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 500,'MCAT500',3,NULL,'<div class="prodid-question">
  <p>
    <b>Estrogen</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormone synthesized and released by the ovaries, ovarian follicles, corpus
						luteum, and placenta. Estrogen stimulates the development of the female
						reproductive tract and secondary sexual characteristics and is partly
						responsible for the LH spike that causes ovulation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 501,'MCAT501',3,NULL,'<div class="prodid-question">
  <p>
    <b>Exocrine Glands</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Glands that synthesize and secrete substances through ducts. The mammary
						glands and sweat glands are examples of exocrine glands.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 502,'MCAT502',3,NULL,'<div class="prodid-question">
  <p>
    <b>Expressivity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Refers to the variability in phenotypes (especially severity of a disease)
						that can occur with a given genotype.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 503,'MCAT503',3,NULL,'<div class="prodid-question">
  <p>
    <b>Facultative Anaerobe</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An organism that makes ATP by aerobic respiration if oxygen is present, but
						that can switch to fermentation for sufficient ATP when oxygen is not
						available.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 504,'MCAT504',3,NULL,'<div class="prodid-question">
  <p>
    <b>Filtrate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The material that passes from the blood vessels into Bowman’s
						space.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 505,'MCAT505',3,NULL,'<div class="prodid-question">
  <p>
    <b>Follicle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A multilayered sac of cells that protects and nourishes the developing
						ovum.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 506,'MCAT506',3,NULL,'<div class="prodid-question">
  <p>
    <b>Follicle-Stimulating Hormone (FSH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, FSH stimulates maturation
						of ovarian follicles in females and maturation of the seminiferous tubules
						and sperm production in males. FSH is regulated by estrogen and gonadotropin
						releasing hormone (GnRH).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 507,'MCAT507',3,NULL,'<div class="prodid-question">
  <p>
    <b>Foramen Ovale</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A shunt that connects the right atrium to the left atrium in order to bypass
						the fetal lung.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 508,'MCAT508',3,NULL,'<div class="prodid-question">
  <p>
    <b>Frameshift Mutation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Mutation in which a number of nucleotides (except multiples of three) are
						either deleted or inserted. Such mutations lead to a shift in the DNA
						reading frame and often result in the translation of nonfunctional
						proteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 509,'MCAT509',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ganglia</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A group of neuronal cell bodies in the peripheral nervous system. May be
						sensory or autonomic.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 510,'MCAT510',3,NULL,'<div class="prodid-question">
  <p>
    <b>Gastric Glands</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Located in the stomach; secrete HCl and various enzymes (such as pepsin) when
						stimulated by gastrin.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 511,'MCAT511',3,NULL,'<div class="prodid-question">
  <p>
    <b>Gastrulation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process by which a single-layered blastula becomes a three-layered
						gastrula.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 512,'MCAT512',3,NULL,'<div class="prodid-question">
  <p>
    <b>Gene</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A unit of DNA that encodes a specific protein or RNA molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 513,'MCAT513',3,NULL,'<div class="prodid-question">
  <p>
    <b>Genetic Drift</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Changes in the composition of the gene pool by chance; often more pronounced
						in small populations.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 514,'MCAT514',3,NULL,'<div class="prodid-question">
  <p>
    <b>Genetic Map</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Created by analyzing recombination frequencies of linked genes; a schematic
						that shows the distance between two genes or the order of several genes on a
						chromosome.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 515,'MCAT515',3,NULL,'<div class="prodid-question">
  <p>
    <b>Genotype</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The genetic makeup of an individual.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 516,'MCAT516',3,NULL,'<div class="prodid-question">
  <p>
    <b>Glomerulus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Network of capillaries within Bowman’s capsule that serves as the site
						of filtration. Blood cells and proteins are too large to be filtered, but
						ions, glucose, and amino acids readily pass into the filtrate.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 517,'MCAT517',3,NULL,'<div class="prodid-question">
  <p>
    <b>Glucagon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Produced and secreted by the <i>α</i>-cells of the pancreas, glucagon
						increases blood glucose concentration by promoting gluconeogenesis and the
						conversion of glycogen to glucose in the liver.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 518,'MCAT518',3,NULL,'<div class="prodid-question">
  <p>
    <b>Glucocorticoids</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the adrenal cortex, glucocorticoids raise blood
						glucose levels while decreasing protein synthesis.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 519,'MCAT519',3,NULL,'<div class="prodid-question">
  <p>
    <b>Golgi Apparatus</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An organelle that plays a role in the packaging and secretion of proteins and
						other molecules produced intracellularly.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 520,'MCAT520',3,NULL,'<div class="prodid-question">
  <p>
    <b>Growth Hormone (GH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, GH stimulates bone and
						muscle growth as well as glucose conservation. GH is inhibited by
						somatostatin and stimulated by growth hormone releasing hormone (secreted by
						the hypothalamus).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 521,'MCAT521',3,NULL,'<div class="prodid-question">
  <p>
    <b>Haploid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells with only one copy of each chromosome. Germ cells in humans are
						haploid.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 522,'MCAT522',3,NULL,'<div class="prodid-question">
  <p>
    <b>Hemoglobin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A protein found in erythrocytes made up of four polypeptide chains, each
						containing a heme group. Hemoglobin is responsible for transporting oxygen
						from the alveoli to the tissues.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 523,'MCAT523',3,NULL,'<div class="prodid-question">
  <p>
    <b>Hepatic Portal Vein</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Carries nutrients (monosaccharides, amino acids, and small fatty acids)
						absorbed in the small intestine to the liver, where they are modified to
						enter circulation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 524,'MCAT524',3,NULL,'<div class="prodid-question">
  <p>
    <b>Heterozygous</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Organisms that contain two different alleles for the same gene on homologous
						chromosomes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 525,'MCAT525',3,NULL,'<div class="prodid-question">
  <p>
    <b>Homologous Chromosomes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Chromosomes in a diploid cell that contain alleles for the same traits at
						corresponding loci.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 526,'MCAT526',3,NULL,'<div class="prodid-question">
  <p>
    <b>Homozygous</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Organisms that contain two identical alleles of the same gene on homologous
						chromosomes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 527,'MCAT527',3,NULL,'<div class="prodid-question">
  <p>
    <b>Humoral Immunity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The synthesis of specific antibodies by activated B-cells in response to an
						antigen. These antibodies bind to the antigen and either clump together to
						become insoluble, neutralize the antigen, or attract other cells that engulf
						and digest the pathogen.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 528,'MCAT528',3,NULL,'<div class="prodid-question">
  <p>
    <b>Hyperventilation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An increase in the rate of respiration or tidal volume. Lack of oxygen or a
						decrease in blood pH promotes hyperventilation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 529,'MCAT529',3,NULL,'<div class="prodid-question">
  <p>
    <b>Hypodermis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Layer of loose connective tissue below the dermis that binds the dermis to
						the body.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 530,'MCAT530',3,NULL,'<div class="prodid-question">
  <p>
    <b>Immunoglobulin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synonymous with antibody; produced in response to a specific foreign
						substance that recognizes and binds to that antigen and triggers an immune
						response.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 531,'MCAT531',3,NULL,'<div class="prodid-question">
  <p>
    <b>Incomplete Dominance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes a situation in which an organism heterozygous for a trait will have
						a phenotype that is intermediate between both homozygous phenotypes. Neither
						allele, therefore, is dominant or recessive.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 532,'MCAT532',3,NULL,'<div class="prodid-question">
  <p>
    <b>Indeterminate Cleavage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell division in embryogenesis that results in each cell maintaining its
						totipotency, or ability to develop into a complete organism by itself.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 533,'MCAT533',3,NULL,'<div class="prodid-question">
  <p>
    <b>Induction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The influence of a group of cells on the development of other cells.
						Induction is achieved by chemical substances known as inducers. The cells
						secreting these inducers are sometimes called organizers.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 534,'MCAT534',3,NULL,'<div class="prodid-question">
  <p>
    <b>Inferior Vena Cava</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A large vein that returns deoxygenated blood from the lower body and the
						lower extremities to the right atrium of the heart.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 535,'MCAT535',3,NULL,'<div class="prodid-question">
  <p>
    <b>Innate Immunity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Nonspecific immunity provided by structures and cells. Structures, such as
						the skin, and cells, such as macrophages, are able to recognize invaders and
						kill them. Some cells of the innate immune system, such as macrophages and
						dendritic cells, are able to signal the presence of an invader to the
						adaptive immune system.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 536,'MCAT536',3,NULL,'<div class="prodid-question">
  <p>
    <b>Inner Cell Mass</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The group of cells in a blastocyst (mammalian blastula) that develop into the
						embryo.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 537,'MCAT537',3,NULL,'<div class="prodid-question">
  <p>
    <b>Insulin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Produced and secreted by the <i>β</i>-cells of the pancreas, insulin
						decreases blood glucose concentrations by facilitating the uptake of glucose
						by muscle and adipose cells and the conversion of glucose to glycogen in
						muscle and liver cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 538,'MCAT538',3,NULL,'<div class="prodid-question">
  <p>
    <b>Interphase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Phase of the cell cycle in which cell division does not take place. Includes
						the G</b>
    <sub>
      <b>1</b>
    </sub>
    <b> phase, S phase, and G</b>
    <sub>
      <b>2</b>
    </sub>
    <b> phase. Cells in this phase may or may not be growing.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 539,'MCAT539',3,NULL,'<div class="prodid-question">
  <p>
    <b>Interstitial Cells</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Also called Leydig cells, interstitial cells are located in the testes and
						secrete testosterone and other androgens.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 540,'MCAT540',3,NULL,'<div class="prodid-question">
  <p>
    <b>Intestinal Glands</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Contains brush-border enzymes such as maltase, sucrase, and lactase to digest
						disaccharides. Other enzymes of these glands include aminopeptidase,
						dipeptidase, and enteropeptidase.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 541,'MCAT541',3,NULL,'<div class="prodid-question">
  <p>
    <b>Intrapleural Space</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The space between the two membranes (visceral pleura and parietal pleura)
						that cover the lungs.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 542,'MCAT542',3,NULL,'<div class="prodid-question">
  <p>
    <b>Intrinsic Factor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A protein secreted by parietal cells of the stomach that is necessary for
						vitamin B</b>
    <sub>
      <b>12</b>
    </sub>
    <b> absorption.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 543,'MCAT543',3,NULL,'<div class="prodid-question">
  <p>
    <b>Inversion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of chromosomal rearrangement in which a portion of a chromosome breaks
						off and rejoins the same chromosome in the reverse position.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 544,'MCAT544',3,NULL,'<div class="prodid-question">
  <p>
    <b>Large Intestine</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Section of the GI tract that consists of the cecum, the colon, and the
						rectum. The main function of the large intestine is to absorb salts, water,
						and some vitamins.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 545,'MCAT545',3,NULL,'<div class="prodid-question">
  <p>
    <b>Leukocytes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>White blood cells; the component of blood involved in cell defense and
						immunity. Neutrophils, basophils, eosinophils, lymphocytes, and monocytes
						are types of leukocytes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 546,'MCAT546',3,NULL,'<div class="prodid-question">
  <p>
    <b>Liver</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Essential organ of the human body responsible for the production of bile,
						detoxification of ingested substances, production of urea, and the
						processing and modification of nutrients for storage. The liver also
						produces albumin (a protein that maintains blood oncotic pressure) and
						clotting factors.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 547,'MCAT547',3,NULL,'<div class="prodid-question">
  <p>
    <b>Luteinizing Hormone (LH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, LH stimulates ovulation
						and the formation of the corpus luteum. LH is regulated by estrogen,
						progesterone, and gonadotropin releasing hormone (GnRH).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 548,'MCAT548',3,NULL,'<div class="prodid-question">
  <p>
    <b>Lymph Nodes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Swellings along the lymph vessels where lymph is filtered by leukocytes to
						remove antigens.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 549,'MCAT549',3,NULL,'<div class="prodid-question">
  <p>
    <b>Lymphatic System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A system of vessels and lymph nodes that collect interstitial fluids and
						return them to the circulatory system, thereby maintaining fluid balance.
						The lymphatic system is also involved in lipid absorption and lymphocyte
						activation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 550,'MCAT550',3,NULL,'<div class="prodid-question">
  <p>
    <b>Lysogenic Cycle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Phase of viral replication in which the DNA of the bacteriophage becomes
						integrated in the host’s genome and replicates as the bacteria
						replicates.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 551,'MCAT551',3,NULL,'<div class="prodid-question">
  <p>
    <b>Lysosome</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A membrane-bound vesicle that contains hydrolytic enzymes used for
						intracellular digestion.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 552,'MCAT552',3,NULL,'<div class="prodid-question">
  <p>
    <b>Lytic Cycle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Phase in viral replication in which the host cell is lysed and releases new
						virions.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 553,'MCAT553',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mean</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The average, calculated as the sum of observed values divided by the number
						of observed values.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 554,'MCAT554',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mechanical Digestion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Breakdown of food particles into smaller particles through such activities as
						biting, chewing, and churning.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 555,'MCAT555',3,NULL,'<div class="prodid-question">
  <p>
    <b>Median</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The simplest division of a set of values; the middle value that divides the
						values into the upper half and lower half.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 556,'MCAT556',3,NULL,'<div class="prodid-question">
  <p>
    <b>Meiosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A two-phase cell division in germ cells that results in the formation of up
						to four haploid cells from one diploid cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 557,'MCAT557',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mendel’s Law of Independent Assortment</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The alleles of different genes sort independently from one another during
						meiosis. We now know that this is true only for unlinked genes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 558,'MCAT558',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mendel’s Law of Segregation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Mendel’s postulation that there are alternate versions of genes that
						account for genetic variation. Each individual has two alleles for each
						gene, with one maternal and one paternal in origin. During meiosis, these
						two alleles separate into two gametes each.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 559,'MCAT559',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mesoderm</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Primary germ layer that lies between the ectoderm and the endoderm. Gives
						rise to the musculoskeletal system, circulatory system, excretory system,
						gonads, connective tissue throughout the body, and portions of the digestive
						and respiratory organs.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 560,'MCAT560',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mitochondria</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The site of aerobic respiration that provides the cell with a majority of its
						energy in the form of ATP. The mitochondrion is a semiautonomous organelle
						enclosed by two membranes with an intermembrane space between the two
						membranes and a mitochondrial matrix enclosed by the inner membrane.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 561,'MCAT561',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mitosis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell division or nuclear division in somatic cells that results in the
						daughter nucleus receiving a full complement of the organism’s
						genome.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 562,'MCAT562',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mitral Valve</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A valve located between the left atrium and the left ventricle. The valve
						consists of two cusps and prevents backflow of blood from the left ventricle
						to the left atrium.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 563,'MCAT563',3,NULL,'<div class="prodid-question">
  <p>
    <b>Mode</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The most frequently occurring value in a set of observations.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 564,'MCAT564',3,NULL,'<div class="prodid-question">
  <p>
    <b>Monohybrid Cross</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cross between two organisms where only one trait is being studied.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 565,'MCAT565',3,NULL,'<div class="prodid-question">
  <p>
    <b>Monosynaptic Reflex</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Reflex pathway that has only one synapse between the sensory neuron and the
						motor neuron (such as the knee-jerk reflex).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 566,'MCAT566',3,NULL,'<div class="prodid-question">
  <p>
    <b>Monozygotic Twins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Result when a zygote splits into two embryos. Because both embryos contain
						identical alleles, they are often called identical twins.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 567,'MCAT567',3,NULL,'<div class="prodid-question">
  <p>
    <b>Morula</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solid ball of cells that develops from the zygote through cleavage. When
						the interior of the morula becomes hollow, it becomes known as a
						blastula.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 568,'MCAT568',3,NULL,'<div class="prodid-question">
  <p>
    <b>Multipotent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stem cells that can differentiate into multiple cell types within a
						particular group. For example, hematopoietic stem cells are able to
						differentiate into many different types of blood cells, but not into any
						other cell type.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 569,'MCAT569',3,NULL,'<div class="prodid-question">
  <p>
    <b>Myelin Sheath</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Insulating structure that surrounds axons. Action potentials cannot take
						place in areas of the axon that are myelinated.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 570,'MCAT570',3,NULL,'<div class="prodid-question">
  <p>
    <b>Negative Pressure Breathing</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The contraction of the diaphragm and the intercostal muscles increases the
						volume of the thoracic cavity, reducing the pressure in the intrapleural
						space. This decrease in pressure creates a vacuum that causes the lungs to
						suck in air.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 571,'MCAT571',3,NULL,'<div class="prodid-question">
  <p>
    <b>Nephron</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The functional unit of the kidney. Can be subdivided into Bowman’s
						capsule, proximal convoluted tubule, descending limb of the loop of Henle,
						ascending limb of the loop of Henle, distal convoluted tubule, and
						collecting duct.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 572,'MCAT572',3,NULL,'<div class="prodid-question">
  <p>
    <b>Neural Crest Cells</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells at the tip of the neural fold; this group of cells gives rise to many
						components of the peripheral nervous system and a number of other cell types
						throughout the body.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 573,'MCAT573',3,NULL,'<div class="prodid-question">
  <p>
    <b>Neurotransmitters</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Chemical messengers released from synaptic terminals of a neuron that can
						bind to and stimulate a postsynaptic cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 574,'MCAT574',3,NULL,'<div class="prodid-question">
  <p>
    <b>Nodes of Ranvier</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Gaps between segments of myelin sheath where action potentials can take
						place, allowing for saltatory conduction.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 575,'MCAT575',3,NULL,'<div class="prodid-question">
  <p>
    <b>Nondisjunction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The failure of homologous chromosomes or sister chromatids to separate
						properly during meiosis I or meiosis II, respectively. This usually results
						in gametes that lack certain genes or have multiple copies of those
						genes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 576,'MCAT576',3,NULL,'<div class="prodid-question">
  <p>
    <b>Obligate Intracellular Organisms</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Organisms that require a host cell to express their genes and reproduce.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 577,'MCAT577',3,NULL,'<div class="prodid-question">
  <p>
    <b>Osmoregulation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Maintenance of water and solute concentrations.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 578,'MCAT578',3,NULL,'<div class="prodid-question">
  <p>
    <b>Osteoblasts</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells in the bone tissue that secrete the organic constituents of the bone
						matrix. Osteoblasts develop into osteocytes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 579,'MCAT579',3,NULL,'<div class="prodid-question">
  <p>
    <b>Osteoclasts</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells in the bone matrix that are involved in bone degradation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 580,'MCAT580',3,NULL,'<div class="prodid-question">
  <p>
    <b>Osteons</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The structural unit of compact bone that consists of a central canal (either
						a Haversian or Volkmann&#39;s canal) surrounded by a number of concentric rings
						of bony matrix called lamellae.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 581,'MCAT581',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pancreas</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Its exocrine functions include secreting pancreatic amylase, trypsinogen,
						chymotrypsinogen, procarboxypeptidases A and B, and pancreatic lipase into
						the small intestine. Its endocrine functions include secretion of insulin
						and glucagon.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 582,'MCAT582',3,NULL,'<div class="prodid-question">
  <p>
    <b>Parathyroid Hormone (PTH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the parathyroid gland, PTH increases blood Ca</b>
    <sup>
      <b>2+</b>
    </sup>
    <b> concentration by increasing Ca</b>
    <sup>
      <b>2+</b>
    </sup>
    <b> reabsorption in the kidneys and by stimulating calcium release from
						bone.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 583,'MCAT583',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pathway of the Electrical Impulse in the Heart</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrical impulse originates in the sinoatrial (SA) node, located in the
						right atrium. It then travels through the atrioventricular (AV) node, then
						through the bundle of His, and finally through the Purkinje fibers.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 584,'MCAT584',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pathway of the Respiratory Tract</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Air travels through the nasal or oral cavity, pharynx, larynx, trachea,
						bronchi, bronchioles, and finally the alveoli (site of gas exchange).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 585,'MCAT585',3,NULL,'<div class="prodid-question">
  <p>
    <b>Penetrance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The percentage of people in a population with a certain genotype who express
						the associated phenotype.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 586,'MCAT586',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pepsin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Secreted as pepsinogen by the chief cells of the stomach, this enzyme cleaves
						peptide bonds, starting the digestion of proteins into individual amino
						acids.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 587,'MCAT587',3,NULL,'<div class="prodid-question">
  <p>
    <b>Peptide Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Polar hormones incapable of permeating the cell membrane that bind to surface
						receptors and act through secondary messengers.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 588,'MCAT588',3,NULL,'<div class="prodid-question">
  <p>
    <b>Peripheral Nervous System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>All neurons that are not part of the central nervous system, including
						sensory and motor neurons that connect to the central nervous system. Can be
						divided into the somatic nervous system and the autonomic nervous
						system.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 589,'MCAT589',3,NULL,'<div class="prodid-question">
  <p>
    <b>Peristalsis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Involuntary muscular contractions that push food down the digestive
						tract.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 590,'MCAT590',3,NULL,'<div class="prodid-question">
  <p>
    <b>Peroxisome</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Organelle that contains hydrogen peroxide and participates in the breakdown
						of very long chain fatty acids.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 591,'MCAT591',3,NULL,'<div class="prodid-question">
  <p>
    <b>Phenotype</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The physical manifestation of an individual’s genotype.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 592,'MCAT592',3,NULL,'<div class="prodid-question">
  <p>
    <b>Placenta</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The organ formed by the uterus and the extraembryonic membranes of the fetus.
						The placenta contains a network of capillaries through which exchange
						between the fetal circulation and maternal circulation takes place.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 593,'MCAT593',3,NULL,'<div class="prodid-question">
  <p>
    <b>Plasma</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Liquid portion of blood, an aqueous mixture of nutrients, salts, respiratory
						gases, hormones, and blood proteins.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 594,'MCAT594',3,NULL,'<div class="prodid-question">
  <p>
    <b>Platelets</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cell fragments involved in the clotting process. Come from megakaryocytes in
						the bone marrow.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 595,'MCAT595',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pluripotent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stem cells that can become other cells within the same primary germ layer.
						For example, after gastrulation, cells of the endodermal layer can only
						become derivatives of endoderm.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 596,'MCAT596',3,NULL,'<div class="prodid-question">
  <p>
    <b>Point Mutation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Mutation in which one nucleotide base is substituted by another. The protein
						products may or may not be functional.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 597,'MCAT597',3,NULL,'<div class="prodid-question">
  <p>
    <b>Polar Body</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A small, short-lived haploid cell created during oogenesis that receives very
						little cytoplasm, organelles, or nutrients.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 598,'MCAT598',3,NULL,'<div class="prodid-question">
  <p>
    <b>Portal Systems</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Circulatory routes in which blood travels through two capillary beds before
						returning to the heart. Some examples include the hypophyseal portal system,
						the hepatic portal system, and the renal portal system.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 599,'MCAT599',3,NULL,'<div class="prodid-question">
  <p>
    <b>Posterior Pituitary</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stores and releases hormones (oxytocin and ADH) synthesized by the
						hypothalamus. The release of these hormones is triggered by an action
						potential that originates in the hypothalamus.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 600,'MCAT600',3,NULL,'<div class="prodid-question">
  <p>
    <b>Primary Response</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The initial response to a specific antigen. During a primary response, T- and
						B-cells are activated and specific antibodies and memory cells for the
						antigen are produced.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 601,'MCAT601',3,NULL,'<div class="prodid-question">
  <p>
    <b>Primary Spermatocytes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Diploid cells that undergo meiosis I to form two haploid secondary
						spermatocytes.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 602,'MCAT602',3,NULL,'<div class="prodid-question">
  <p>
    <b>Progesterone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormone synthesized and released by the ovaries, corpus luteum, and placenta.
						During the luteal phase of the menstrual cycle, the corpus luteum secretes
						progesterone, which, along with estrogen, stimulates the development and
						maintenance of the endometrial walls for implantation of the embryo.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 603,'MCAT603',3,NULL,'<div class="prodid-question">
  <p>
    <b>Prolactin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A hormone synthesized and released by the anterior pituitary that stimulates
						milk production and secretion in female mammary glands.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 604,'MCAT604',3,NULL,'<div class="prodid-question">
  <p>
    <b>Proximal Convoluted Tubule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Site where glucose, amino acids, and other important organic molecules are
						reabsorbed. The proximal convoluted tubules lie in the cortex of the
						kidney.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 605,'MCAT605',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pyloric Glands</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Glands located in the walls of the stomach that secrete the hormone gastrin
						to increase gastric acid production.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 606,'MCAT606',3,NULL,'<div class="prodid-question">
  <p>
    <b>Pyloric Sphincter</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A valve between the stomach and the small intestine that regulates the flow
						of chyme into the duodenum.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 607,'MCAT607',3,NULL,'<div class="prodid-question">
  <p>
    <b>Recessive</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes an allele that requires two copies to be expressed.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 608,'MCAT608',3,NULL,'<div class="prodid-question">
  <p>
    <b>Recombination Frequency</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The likelihood of two genes on the same chromosome being separated during
						crossing over onto two different chromosomes; equal to the proportion of
						gametes that receive these recombinant chromosomes. If the recombination
						frequency of two particular traits is high, it can be inferred that they lie
						far apart from each other.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 609,'MCAT609',3,NULL,'<div class="prodid-question">
  <p>
    <b>Red Fibers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Slow-twitch muscle fibers. They are primarily aerobic and contain many
						mitochondria and high levels of myoglobin.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 610,'MCAT610',3,NULL,'<div class="prodid-question">
  <p>
    <b>Refractory Period</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A short period of time immediately following an action potential in which
						neurons or muscle cells are unresponsive to a stimulus (absolute refractory
						period). In some cases, a stimulus that is much larger than usual causes an
						action potential in a cell in a refractory period (relative refractory
						period).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 611,'MCAT611',3,NULL,'<div class="prodid-question">
  <p>
    <b>Repolarization</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process that occurs when the voltage-gated Na</b>
    <sup>
      <b>+</b>
    </sup>
    <b> channels close and voltage-gated K</b>
    <sup>
      <b>+</b>
    </sup>
    <b> channels open during an action potential, allowing K</b>
    <sup>
      <b>+</b>
    </sup>
    <b> to rush out of the cell and repolarize it.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 612,'MCAT612',3,NULL,'<div class="prodid-question">
  <p>
    <b>Respiratory Rate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Expressed as breaths per minute. Raising the respiratory rate can decrease
						the amount of carbon dioxide in the blood, thus increasing the pH. Likewise,
						decreasing the respiratory rate increases the concentration of carbon
						dioxide in the blood, resulting in a lower pH.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 613,'MCAT613',3,NULL,'<div class="prodid-question">
  <p>
    <b>Resting Potential</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The charge difference across the cell membrane of a neuron or a muscle cell
						while at rest. Most often maintained by the sodium–potassium
						pump.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 614,'MCAT614',3,NULL,'<div class="prodid-question">
  <p>
    <b>Reverse Transcriptase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An enzyme in retroviruses that uses RNA strands as templates for synthesizing
						cDNA molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 615,'MCAT615',3,NULL,'<div class="prodid-question">
  <p>
    <b>Rh Factor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A surface protein expressed on red blood cells that can induce an immune
						response. Introduction of Rh factor into the blood of a person who is Rh</b>
    <sup>
      <b>–</b>
    </sup>
    <b>may result in a fatal hemolysis reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 616,'MCAT616',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ribonucleic Acid (RNA)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nucleic acid found in both the nucleus and cytoplasm and most closely
						linked with transcription and translation, as well as some gene
						regulation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 617,'MCAT617',3,NULL,'<div class="prodid-question">
  <p>
    <b>Saltatory Conduction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A means by which action potentials jump from node to node along an axon.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 618,'MCAT618',3,NULL,'<div class="prodid-question">
  <p>
    <b>Sarcomere</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The structural unit of striated muscle. It is composed of thin (mostly actin)
						and thick (mostly myosin) filaments.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 619,'MCAT619',3,NULL,'<div class="prodid-question">
  <p>
    <b>Sarcoplasmic Reticulum</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A modified form of endoplasmic reticulum; stores calcium that is used to
						trigger contraction when muscle is stimulated.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 620,'MCAT620',3,NULL,'<div class="prodid-question">
  <p>
    <b>Schwann Cells</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Cells that produce myelin in the peripheral nervous system.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 621,'MCAT621',3,NULL,'<div class="prodid-question">
  <p>
    <b>Second Messenger</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A small molecule that transduces a hormonal signal from the exterior of the
						cell to the interior. Usually released when a peptide hormone binds to its
						receptor; cAMP is a common example.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 622,'MCAT622',3,NULL,'<div class="prodid-question">
  <p>
    <b>Secondary Response</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Subsequent infections by pathogens that trigger a more immediate response
						from the memory cells produced during the primary immune response.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 623,'MCAT623',3,NULL,'<div class="prodid-question">
  <p>
    <b>Semen</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The fluid discharged during ejaculation. Semen consists of sperm cells and
						seminal fluid (fluid from the prostate and bulbourethral glands).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 624,'MCAT624',3,NULL,'<div class="prodid-question">
  <p>
    <b>Semilunar Valves</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Valves (the aortic valve and the pulmonic valve) that prevent backflow of
						blood from the arteries into the ventricles.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 625,'MCAT625',3,NULL,'<div class="prodid-question">
  <p>
    <b>Seminiferous Tubules</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Located in the testes, the seminiferous tubules are the site of sperm
						production.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 626,'MCAT626',3,NULL,'<div class="prodid-question">
  <p>
    <b>Signaling Cascade</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Series of events, starting with the binding of a peptide hormone to a surface
						receptor. This sequence of events ultimately results in a change in cellular
						behavior.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 627,'MCAT627',3,NULL,'<div class="prodid-question">
  <p>
    <b>Sister Chromatids</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The relationship between the strands of DNA after replication. Each
						chromosome consists of two identical chromatids held together at a central
						region called the centromere. After the mitotic spindle pulls the sister
						chromatids apart, each chromatid is referred to as a chromosome on its
						own.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 628,'MCAT628',3,NULL,'<div class="prodid-question">
  <p>
    <b>Skeletal Muscle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of muscle responsible for voluntary movement, consisting of
						multinucleated, striated (striped) muscle fibers.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 629,'MCAT629',3,NULL,'<div class="prodid-question">
  <p>
    <b>Small Intestine</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Section of the digestive tract that can be subdivided into three sections:
						duodenum, jejunum, and ileum. Most digestion takes place in the duodenum and
						most absorption takes place in the jejunum and the ileum.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 630,'MCAT630',3,NULL,'<div class="prodid-question">
  <p>
    <b>Smooth Muscle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Nonstriated muscle, responsible for involuntary action. Controlled by the
						autonomic nervous system.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 631,'MCAT631',3,NULL,'<div class="prodid-question">
  <p>
    <b>Sodium–Potassium Pump</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A protein that hydrolyzes one ATP to transport three Na</b>
    <sup>
      <b>+</b>
    </sup>
    <b> out of the cell for every two K</b>
    <sup>
      <b>+</b>
    </sup>
    <b> it transports into the cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 632,'MCAT632',3,NULL,'<div class="prodid-question">
  <p>
    <b>Somatic Cells</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>All cells excluding the germ (reproductive) cells.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 633,'MCAT633',3,NULL,'<div class="prodid-question">
  <p>
    <b>Somatic Nervous System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Division of the peripheral nervous system that is responsible for voluntary
						movement.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 634,'MCAT634',3,NULL,'<div class="prodid-question">
  <p>
    <b>Somatostatin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Produced and secreted by the <i>δ</i>-cells of the pancreas,
						somatostatin inhibits the release of glucagon and insulin.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 635,'MCAT635',3,NULL,'<div class="prodid-question">
  <p>
    <b>Spermatozoa</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Mature sperm specialized for transporting the genetic information from the
						male to the ovum.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 636,'MCAT636',3,NULL,'<div class="prodid-question">
  <p>
    <b>Spongy Bone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Lighter and less dense than compact bone, it consists of an interconnecting
						lattice of bony spicules (trabeculae). The cavities between the spicules
						contain bone marrow.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 637,'MCAT637',3,NULL,'<div class="prodid-question">
  <p>
    <b>Stabilizing Selection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of natural selection where the average phenotype is favored while those
						outside the norm are eliminated.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 638,'MCAT638',3,NULL,'<div class="prodid-question">
  <p>
    <b>Starling Forces</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A balance between hydrostatic and oncotic pressures on both sides of a
						membrane, essential for maintaining proper fluid volumes and solute
						concentrations inside and outside the vasculature.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 639,'MCAT639',3,NULL,'<div class="prodid-question">
  <p>
    <b>Steroid Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Nonpolar hormones that cross the cell membrane and act by binding
						intracellular receptors.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 640,'MCAT640',3,NULL,'<div class="prodid-question">
  <p>
    <b>Superior Vena Cava</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A large vein that returns deoxygenated blood from the head and neck regions,
						as well as the upper extremities, to the right atrium of the heart.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 641,'MCAT641',3,NULL,'<div class="prodid-question">
  <p>
    <b>Surfactant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A liquid substance produced by the lung that reduces surface tension in the
						alveoli. Surfactant prevents lung collapse and decreases the effort needed
						to expand the lungs (inhale).</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 642,'MCAT642',3,NULL,'<div class="prodid-question">
  <p>
    <b>Synapse</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The space between the axon terminal of one neuron and the dendrite of another
						neuron (or membrane of an effector organ) where neurotransmitters are
						released.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 643,'MCAT643',3,NULL,'<div class="prodid-question">
  <p>
    <b>Synaptic Terminals</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Ends of axons that form one side of the synaptic cleft; the location where
						vesicles of neurotransmitters are stored.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 644,'MCAT644',3,NULL,'<div class="prodid-question">
  <p>
    <b>Systole</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The stage of the cardiac cycle in which the heart muscle contracts and pumps
						blood.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 645,'MCAT645',3,NULL,'<div class="prodid-question">
  <p>
    <b>Test Cross</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cross between an organism of an undetermined genotype and another that is
						homozygous recessive for the trait of interest.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 646,'MCAT646',3,NULL,'<div class="prodid-question">
  <p>
    <b>Testosterone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormone secreted by the interstitial cells of the testes. Testosterone is
						responsible for embryonic sexual differentiation, male sexual development,
						and the maintenance of masculine secondary sexual characteristics.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 647,'MCAT647',3,NULL,'<div class="prodid-question">
  <p>
    <b>Tetrad</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Collectively, the four chromatids involved when a pair of homologous
						chromosomes synapse during prophase I of meiosis.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 648,'MCAT648',3,NULL,'<div class="prodid-question">
  <p>
    <b>Thermoregulation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Maintenance of a constant internal body temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 649,'MCAT649',3,NULL,'<div class="prodid-question">
  <p>
    <b>Threshold Voltage</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The minimal voltage that must be reached in order for an action potential to
						be fired at the axon hillock.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 650,'MCAT650',3,NULL,'<div class="prodid-question">
  <p>
    <b>Thyroid Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the thyroid gland, thyroid hormones
						triiodothyronine (T</b>
    <sub>
      <b>3</b>
    </sub>
    <b>) and thyroxine (T</b>
    <sub>
      <b>4</b>
    </sub>
    <b>) stimulate cellular respiration as well as protein and fatty acid synthesis
						and degradation.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 651,'MCAT651',3,NULL,'<div class="prodid-question">
  <p>
    <b>Thyroid-Stimulating Hormone (TSH)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Synthesized and released by the anterior pituitary, TSH stimulates the
						thyroid gland to absorb iodine and to synthesize and secrete thyroid
						hormones. TSH is regulated by thyroid releasing hormone (TRH), which is
						released by the hypothalamus.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 652,'MCAT652',3,NULL,'<div class="prodid-question">
  <p>
    <b>Totipotent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stem cells that have the ability to become any cell within any system of the
						body. Embryonic stem cells are totipotent.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 653,'MCAT653',3,NULL,'<div class="prodid-question">
  <p>
    <b>Translocation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of chromosomal rearrangement in which a portion of one chromosome
						swaps with a portion of a nonhomologous chromosome.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 654,'MCAT654',3,NULL,'<div class="prodid-question">
  <p>
    <b>Transverse Tubules (T-Tubules)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A system of tubules that provides channels for ion flow throughout muscle
						fibers to facilitate the propagation of an action potential.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 655,'MCAT655',3,NULL,'<div class="prodid-question">
  <p>
    <b>Tricuspid Valve</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A valve located between the right atria and the right ventricle. The valve
						consists of three cusps and prevents backflow of blood from the right
						ventricle to the right atrium.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 656,'MCAT656',3,NULL,'<div class="prodid-question">
  <p>
    <b>Tropic Hormones</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Hormones that travel to a target tissue and cause the release of another
						hormone. A hormone downstream will cause the physiological effect.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 657,'MCAT657',3,NULL,'<div class="prodid-question">
  <p>
    <b>Umbilical Cord</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Connects the vasculature of the fetus to the placenta.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 658,'MCAT658',3,NULL,'<div class="prodid-question">
  <p>
    <b>Vagus Nerve</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>One of the twelve cranial nerves that provides parasympathetic signaling to
						the thoracic and abdominal cavities.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 659,'MCAT659',3,NULL,'<div class="prodid-question">
  <p>
    <b>Veins</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Vessels that carry blood toward the heart. These vessels are thin-walled and
						have valves to prevent backflow.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 660,'MCAT660',3,NULL,'<div class="prodid-question">
  <p>
    <b>Ventricles</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The muscular lower chambers of the heart. The right ventricle pumps
						deoxygenated blood to the lungs through the pulmonary arteries, while the
						left ventricle pumps oxygenated blood throughout the body through the
						aorta.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 661,'MCAT661',3,NULL,'<div class="prodid-question">
  <p>
    <b>Villi</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Fingerlike projections that extend out of the small intestine in order to
						increase surface area for maximal absorption.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 662,'MCAT662',3,NULL,'<div class="prodid-question">
  <p>
    <b>White Fibers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Fast-twitch muscle fibers. They are primarily anaerobic and fatigue more
						easily than red fibers.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 663,'MCAT663',3,NULL,'<div class="prodid-question">
  <p>
    <b>Zona Pellucida</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Inner layer of glycoproteins surrounding the oocyte. These glycoproteins are
						secreted by follicular cells and the oocyte itself. Penetration of the zona
						pellucida by a sperm cell forces the secondary oocyte to undergo meiosis
						II.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 664,'MCAT664',3,NULL,'<div class="prodid-question">
  <p>
    <b>Zygote</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A fertilized egg. Develops into a morula after a number of rounds of
						cleavage.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 665,'MCAT665',4,NULL,'<div class="prodid-question">
  <p>
    <b>Acid Dissociation Constant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An equilibrium expression used to measure acid strength, given by the ratio
						of the product of the products’ molar concentrations to the product
						of the reactants’ molar concentrations, with each term raised to the
						power of its stoichiometric coefficient. Denoted <i>K</i></b>
    <sub>
      <b>a</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.658793',0,'skipped');
INSERT INTO flashcards VALUES(1, 666,'MCAT666',4,NULL,'<div class="prodid-question">
  <p>
    <b>Activation Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy barrier that must be overcome for a reaction to proceed; often
						denoted by <i>E</i></b>
    <sub>
      <b>a</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.658793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 667,'MCAT667',4,NULL,'<div class="prodid-question">
  <p>
    <b>Adiabatic Process</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process in which no heat is transferred to or from the system by its
						surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 668,'MCAT668',4,NULL,'<div class="prodid-question">
  <p>
    <b>Alkali Metals</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The highly reactive elements found in Group IA (Group 1) of the Periodic
						Table, except hydrogen.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 669,'MCAT669',4,NULL,'<div class="prodid-question">
  <p>
    <b>Alkaline Earth Metals</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Elements found in Group IIA (Group 2) of the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 670,'MCAT670',4,NULL,'<div class="prodid-question">
  <p>
    <b>Amphoteric</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A species capable of reacting as either an acid or a base. In the
						Brønsted–Lowry sense, a species that can pick up or give off a
						proton. In the Lewis sense, a species that can donate or accept a lone pair
						of electrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 671,'MCAT671',4,NULL,'<div class="prodid-question">
  <p>
    <b>Anode</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrode where oxidation occurs during a cell’s
						oxidation–reduction reaction. Electrons always flow from the anode in
						an electrochemical cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 672,'MCAT672',4,NULL,'<div class="prodid-question">
  <p>
    <b>Aqueous Solution</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution containing water as its solvent.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 673,'MCAT673',4,NULL,'<div class="prodid-question">
  <p>
    <b>Arrhenius Definition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A definition of acids as producers of excess H</b>
    <sup>
      <b>+</b>
    </sup>
    <b> and bases as producers of excess OH</b>
    <sup>
      <b>–</b>
    </sup>
    <b> in aqueous solutions.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 674,'MCAT674',4,NULL,'<div class="prodid-question">
  <p>
    <b>Atomic Radius</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The distance measured either between the nucleus and outermost electron of an
						atom or by the separation of the two nuclei in a diatomic element. Decreases
						from left to right and from bottom to top on the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 675,'MCAT675',4,NULL,'<div class="prodid-question">
  <p>
    <b>Avogadro’s Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the number of moles of a gas present is proportional to its
						volume, assuming constant pressure and temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 676,'MCAT676',4,NULL,'<div class="prodid-question">
  <p>
    <b>Boyle’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that at a constant temperature, the volume of an ideal gas is
						inversely proportional to its pressure.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 677,'MCAT677',4,NULL,'<div class="prodid-question">
  <p>
    <b>Brønsted–Lowry Definition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Common definition of acids as proton (H</b>
    <sup>
      <b>+</b>
    </sup>
    <b>) donors and bases as proton acceptors.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 678,'MCAT678',4,NULL,'<div class="prodid-question">
  <p>
    <b>Buffer</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution containing a weak acid or base coupled with its conjugate salt,
						acting to prevent changes to the solution’s pH upon the addition of
						acidic or basic substances.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 679,'MCAT679',4,NULL,'<div class="prodid-question">
  <p>
    <b>Cathode</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrode at which reduction occurs during a cell’s
						oxidation–reduction reaction. Electrons always flow towards the
						cathode in an electrochemical cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 680,'MCAT680',4,NULL,'<div class="prodid-question">
  <p>
    <b>Charles’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that at a constant pressure, the volume of an ideal gas is directly
						proportional to its temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 681,'MCAT681',4,NULL,'<div class="prodid-question">
  <p>
    <b>Chemical Kinetics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The study of reaction rates and the factors that affect them.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 682,'MCAT682',4,NULL,'<div class="prodid-question">
  <p>
    <b>Closed System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A system that allows for the exchange of energy, but not matter, across its
						boundaries.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 683,'MCAT683',4,NULL,'<div class="prodid-question">
  <p>
    <b>Colligative Properties</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The properties of solutions—such as vapor pressure lowering, freezing
						point depression, boiling point elevation, and osmotic pressure—that
						are affected only by the number of solute particles dissolved and not by
						their chemical identities.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 684,'MCAT684',4,NULL,'<div class="prodid-question">
  <p>
    <b>Collision Theory of Chemical Kinetics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Theory stating that the rate of a reaction is directly proportional to the
						number of collisions that take place between reactants per second.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 685,'MCAT685',4,NULL,'<div class="prodid-question">
  <p>
    <b>Combination Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction in which two or more reactants combine to form a product (such as
						A + B → C).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 686,'MCAT686',4,NULL,'<div class="prodid-question">
  <p>
    <b>Common Ion Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the molar solubility of one salt is reduced when another salt,
						having a common ion, is brought into the same solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 687,'MCAT687',4,NULL,'<div class="prodid-question">
  <p>
    <b>Concentration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ratio of the amount of solute to the amount of solution; quantified by
						mole fraction, molarity, molality, or normality, among other measures.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 688,'MCAT688',4,NULL,'<div class="prodid-question">
  <p>
    <b>Conjugate Acids and Bases</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A systematic pairing of a protonated species (acid) with its deprotonated
						form (conjugate base) or a deprotonated species (base) with its protonated
						form (conjugate acid). Conjugates appear on opposite sides of a chemical
						equation.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 689,'MCAT689',4,NULL,'<div class="prodid-question">
  <p>
    <b>Constant-Volume Calorimeter</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An apparatus commonly referred to as a bomb calorimeter; used to measure the
						amount of heat absorbed or released during a reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 690,'MCAT690',4,NULL,'<div class="prodid-question">
  <p>
    <b>Decomposition Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A chemical reaction in which one substance breaks down into two substances
						(such as A → B + C).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 691,'MCAT691',4,NULL,'<div class="prodid-question">
  <p>
    <b>Diamagnetic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An atom or a substance that contains no unpaired electrons and is
						consequently repelled by a magnet.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 692,'MCAT692',4,NULL,'<div class="prodid-question">
  <p>
    <b>Diffusion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Passive dispersion of a gas or solute throughout a medium by means of random
						motion.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 693,'MCAT693',4,NULL,'<div class="prodid-question">
  <p>
    <b>Dipole Moment</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The product of the magnitude of either partial charge in a dipole multiplied
						by the distance between the charges, given by the equationp = <i>q</i>d
						where p is the dipole moment, <i>q</i> is the partial charge, and d is the
						displacement vector separating the charges.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 694,'MCAT694',4,NULL,'<div class="prodid-question">
  <p>
    <b>Disproportionation (Dismutation)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An oxidation–reduction reaction in which the same species is both
						oxidized and reduced.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 695,'MCAT695',4,NULL,'<div class="prodid-question">
  <p>
    <b>Double-Displacement Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A chemical reaction in which two different compounds exchange an atom or ion
						to form two new compounds (such as AB + CD → AD + CB); also called a
						metathesis reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 696,'MCAT696',4,NULL,'<div class="prodid-question">
  <p>
    <b>Effective Nuclear Charge</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The resulting positive nuclear charge an outer electron senses after
						accounting for the shielding effect of inner core electrons. Abbreviated
							<i>Z</i></b>
    <sub>
      <b>eff</b>
    </sub>
    <b>. Increases from left to right and from bottom to top on the Periodic
						Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 697,'MCAT697',4,NULL,'<div class="prodid-question">
  <p>
    <b>Effusion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of gas from one compartment to another through a small opening
						under pressure.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 698,'MCAT698',4,NULL,'<div class="prodid-question">
  <p>
    <b>Electrochemical Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A chemical reaction that either is driven by or produces electricity.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 699,'MCAT699',4,NULL,'<div class="prodid-question">
  <p>
    <b>Electrolyte</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A ion in water that is capable of conducting electricity in that
						solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 700,'MCAT700',4,NULL,'<div class="prodid-question">
  <p>
    <b>Electrolytic Cell</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An electrochemical cell that uses an external electric source to drive a
						nonspontaneous (unfavorable) oxidation–reduction reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 701,'MCAT701',4,NULL,'<div class="prodid-question">
  <p>
    <b>Electron Affinity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy released when an atom or ion in the gaseous state gains an
						electron. Increases from left to right and from bottom to top on the
						Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 702,'MCAT702',4,NULL,'<div class="prodid-question">
  <p>
    <b>Electronegativity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A measure of an atom’s ability to pull electron density toward itself
						when involved in a chemical bond. Increases from left to right and from
						bottom to top on the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 703,'MCAT703',4,NULL,'<div class="prodid-question">
  <p>
    <b>Empirical Formula</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Chemical formula showing the smallest whole-number ratio of atoms in a
						compound.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 704,'MCAT704',4,NULL,'<div class="prodid-question">
  <p>
    <b>Endothermic Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction that proceeds with the net absorption of heat from the
						surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 705,'MCAT705',4,NULL,'<div class="prodid-question">
  <p>
    <b>Enthalpy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The total heat content of a system at a constant pressure, commonly denoted
						by <i>H</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 706,'MCAT706',4,NULL,'<div class="prodid-question">
  <p>
    <b>Entropy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The chaos or randomness of a system, often denoted by <i>S</i>.
							Δ<i>S</i> represents the change in entropy following a
						reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 707,'MCAT707',4,NULL,'<div class="prodid-question">
  <p>
    <b>Equilibrium</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A dynamic point reached by a reversible reaction in which the rate of the
						forward reaction is equal to the rate of the reverse reaction. There is no
						net change in the concentrations of the products and reactants over
						time.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 708,'MCAT708',4,NULL,'<div class="prodid-question">
  <p>
    <b>Equilibrium Constant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ratio of the concentrations of the products to the concentrations of the
						reactants at the point of equilibrium, where each reactant and product in
						the expression is raised to the power of its stoichiometric coefficient.
						Commonly denoted by <i>K</i></b>
    <sub>
      <b>eq</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 709,'MCAT709',4,NULL,'<div class="prodid-question">
  <p>
    <b>Equivalence Point</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The point in a titration at which an equimolar amount of titrant has been
						added to the unknown solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 710,'MCAT710',4,NULL,'<div class="prodid-question">
  <p>
    <b>Exothermic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction that proceeds with the net release of heat into the
						surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 711,'MCAT711',4,NULL,'<div class="prodid-question">
  <p>
    <b>Faraday’s Constant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Denoted by F, it equals 9.65 x 10</b>
    <sup>
      <b>4</b>
    </sup>
    <b>
      <img src="/assets/contents/MCAT711img1.png" />. Commonly used in the formula
							<i>It</i> = <i>n</i>F, where <i>I</i> is current, <i>t</i> is time (in
						seconds), and <i>n</i> is moles of electrons transferred.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 712,'MCAT712',4,NULL,'<div class="prodid-question">
  <p>
    <b>Formal Charge</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The charge assigned to an atom in a molecule or polyatomic ion, assuming even
						division of the electrons in a bond. Molecules containing atoms with lower
						formal charges tend to be more stable than those with higher formal
						charges.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 713,'MCAT713',4,NULL,'<div class="prodid-question">
  <p>
    <b>Free Radical</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An atom or molecule that has an unpaired electron in its outermost shell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 714,'MCAT714',4,NULL,'<div class="prodid-question">
  <p>
    <b>Galvanic Cell</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An electrochemical cell powered by a spontaneous oxidation–reduction
						reaction that produces an electric current; also called a voltaic cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 715,'MCAT715',4,NULL,'<div class="prodid-question">
  <p>
    <b>Gay-Lussac’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that at a constant volume, the pressure of an ideal gas is directly
						proportional to its temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 716,'MCAT716',4,NULL,'<div class="prodid-question">
  <p>
    <b>Gibbs Free Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy of a system available to do work.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 717,'MCAT717',4,NULL,'<div class="prodid-question">
  <p>
    <b>Graham’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Law stating that the rates at which two different gases effuse or diffuse are
						inversely proportional to the square root of their molar masses:<img src="/assets/contents/MCAT717img1.png" />.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 718,'MCAT718',4,NULL,'<div class="prodid-question">
  <p>
    <b>Half-Cell</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An electrode immersed in an electrolytic solution that is the site of either
						oxidation or reduction in an electrochemical cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 719,'MCAT719',4,NULL,'<div class="prodid-question">
  <p>
    <b>Half-Equivalence Point</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The point in a titration at which exactly half the molar equivalence of
						reactant is consumed by the titrant being added. At this point in an
						acid–base titration, the pH is equal to the p<i>K</i></b>
    <sub>
      <b>a</b>
    </sub>
    <b> of the titrand.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 720,'MCAT720',4,NULL,'<div class="prodid-question">
  <p>
    <b>Halogens</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Elements found in Group VIIA (Group 17) of the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 721,'MCAT721',4,NULL,'<div class="prodid-question">
  <p>
    <b>Heat</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A transfer of energy from a substance with a higher temperature to a
						substance with a lower temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 722,'MCAT722',4,NULL,'<div class="prodid-question">
  <p>
    <b>Heisenberg Uncertainty Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The quantum mechanical concept that we cannot measure the exact momentum and
						position of an orbiting electron simultaneously. That is, the more
						accurately we measure an electron’s momentum, the less we know about
						its exact position.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 723,'MCAT723',4,NULL,'<div class="prodid-question">
  <p>
    <b>Henderson–Hasselbalch Equation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An equation commonly used in titrationbased problems that relates the pH or
						pOH of a solution to the p<i>K</i></b>
    <sub>
      <b>a</b>
    </sub>
    <b> and the ratio
						of the dissociated species. pH = p<i>K</i></b>
    <sub>
      <b>a</b>
    </sub>
    <b> +
						log</b>.<img src="/assets/contents/MCAT723img1.png" /></p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 724,'MCAT724',4,NULL,'<div class="prodid-question">
  <p>
    <b>Henry’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the partial pressure of a gas dissolved in a solution is directly
						proportional to the partial pressure of this gas above the solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 725,'MCAT725',4,NULL,'<div class="prodid-question">
  <p>
    <b>Hess’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the enthalpy change of an overall reaction is equal to the sum of
						the standard heats of formation of the products minus the sum of the
						standard heats of formation of the reactants.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 726,'MCAT726',4,NULL,'<div class="prodid-question">
  <p>
    <b>Hund’s Rule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that electrons will first fill orbitals within a subshell unpaired and
						with parallel spins before being coupled with other electrons of opposite
						spins in the same orbital. This method of maximizing the number of
						half-filled orbitals allows for the most stable distribution of electrons
						within a subshell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 727,'MCAT727',4,NULL,'<div class="prodid-question">
  <p>
    <b>Hydrogen Bonding</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Very strong intermolecular force where a hydrogen covalently bonded to an N,
						O, or F is attracted to another N, O, or F.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 728,'MCAT728',4,NULL,'<div class="prodid-question">
  <p>
    <b>Ideal Gas</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A hypothetical gas containing particles with zero volume and with no
						attractive intermolecular forces.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 729,'MCAT729',4,NULL,'<div class="prodid-question">
  <p>
    <b>Ideal Gas Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A unification of Boyle’s law, Charles’s law,
						Gay-Lussac’s law, and Avogadro’s principle into the formula
						that describes the behavior of ideal gases: <i>PV</i> =
						<i>n</i>R<i>T</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 730,'MCAT730',4,NULL,'<div class="prodid-question">
  <p>
    <b>Indicator</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A chemical species that changes color when undergoing dissociation.
						Indicators are used to signal the end point of a titration.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 731,'MCAT731',4,NULL,'<div class="prodid-question">
  <p>
    <b>Ion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A single particle or polyatomic species with an electric charge. </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 732,'MCAT732',4,NULL,'<div class="prodid-question">
  <p>
    <b>Ion Product</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The product of the molar concentrations of dissociated ions in solution at
						any point in a dissociation reaction, where each ion is raised to the power
						of its stoichiometric coefficient. Denoted <i>IP</i>. </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 733,'MCAT733',4,NULL,'<div class="prodid-question">
  <p>
    <b>Ionization Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of energy required to remove an electron from orbit about a
						gaseous atom into free space. Increases from left to right and from bottom
						to top on the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 734,'MCAT734',4,NULL,'<div class="prodid-question">
  <p>
    <b>Isobaric Process</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process that occurs at a constant pressure.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 735,'MCAT735',4,NULL,'<div class="prodid-question">
  <p>
    <b>Isoelectronic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Two different elements that share the same electronic configuration (such as
						K</b>
    <sup>
      <b>+</b>
    </sup>
    <b> and Ar).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 736,'MCAT736',4,NULL,'<div class="prodid-question">
  <p>
    <b>Isolated System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A system that can exchange neither energy nor matter with its
						surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 737,'MCAT737',4,NULL,'<div class="prodid-question">
  <p>
    <b>Isothermal Process</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process in which temperature (and, therefore, internal energy) remain
						constant.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 738,'MCAT738',4,NULL,'<div class="prodid-question">
  <p>
    <b>Isovolumetric (Isochoric) Process</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process in which volume remains constant and no net pressure–volume
						work is done.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 739,'MCAT739',4,NULL,'<div class="prodid-question">
  <p>
    <b>Kinetic Molecular Theory of Gases</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A series of assumptions used to account for the behavior of ideal gases. The
						theory describes gases as volumeless particles in constant, random motion
						that exhibit no intermolecular attractions and undergo completely elastic
						collisions with each other and the walls of their container.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 740,'MCAT740',4,NULL,'<div class="prodid-question">
  <p>
    <b>Law of Conservation of Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Law stating that energy cannot be created or destroyed but only transferred
						and transformed. </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 741,'MCAT741',4,NULL,'<div class="prodid-question">
  <p>
    <b>Le Châtelier’s Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that when a system in equilibrium is placed under one of several
						stressors, it will react in order to regain equilibrium.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 742,'MCAT742',4,NULL,'<div class="prodid-question">
  <p>
    <b>Lewis Definition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A definition of acids as electron pair acceptors and bases as electron pair
						donors. </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 743,'MCAT743',4,NULL,'<div class="prodid-question">
  <p>
    <b>Limiting Reagent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The reactant in a chemical equation that, given nonstoichiometric amounts,
						determines the amount of product that can form; the reactant that runs out
						first.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 744,'MCAT744',4,NULL,'<div class="prodid-question">
  <p>
    <b>Metalloids</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Elements that have properties between those of metals and nonmetals; includes
						B, Si, Ge, As, Sb, Te, Po, and At.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 745,'MCAT745',4,NULL,'<div class="prodid-question">
  <p>
    <b>Metals</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Elements that are characteristically electropositive, malleable, and ductile.
						These elements tend to be found on the left side of the Periodic Table, are
						lustrous, and have relatively low ionization energies and electron
						affinities.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 746,'MCAT746',4,NULL,'<div class="prodid-question">
  <p>
    <b>Molality</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Concentration of a solution calculated by moles of solute per kilograms of
						solvent, often denoted by <i>m</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 747,'MCAT747',4,NULL,'<div class="prodid-question">
  <p>
    <b>Molar Mass</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The sum of all the masses present (in amu) in one molecule of a molecular
						compound.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 748,'MCAT748',4,NULL,'<div class="prodid-question">
  <p>
    <b>Molar Solubility</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The molarity of a solute such that a solution is saturated (at
						equilibrium).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 749,'MCAT749',4,NULL,'<div class="prodid-question">
  <p>
    <b>Molarity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Concentration of a solution calculated by moles of solute per liters of
						solution, often denoted by <i>M</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 750,'MCAT750',4,NULL,'<div class="prodid-question">
  <p>
    <b>Molecular Orbital</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The region in a molecule where atomic orbitals overlap, resulting in either a
						stable low-energy bonding orbital or an unstable high-energy antibonding
						orbital.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 751,'MCAT751',4,NULL,'<div class="prodid-question">
  <p>
    <b>Nernst Equation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An equation used to determine a cell’s electromotive force (emf) when
						conditions are not standard. <img src="/assets/contents/MCAT751img1.png" />
						where <i>E</i></b>
    <sub>
      <b>cell</b>
    </sub>
    <b> is the emf, <i><img src="/assets/contents/MCAT751img2.png" /></i>is the emf
						under standard conditions, <i>n</i> is the number of electrons transferred
						in the oxidation–reduction reaction, and <i>Q</i> is the reaction
						quotient.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 752,'MCAT752',4,NULL,'<div class="prodid-question">
  <p>
    <b>Net Ionic Equation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A representation of a displacement reaction showing only the reactive species
						and omitting the spectator ions.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 753,'MCAT753',4,NULL,'<div class="prodid-question">
  <p>
    <b>Neutralization Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction in which an acid and a base are combined to form a salt (and often
						water as well).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 754,'MCAT754',4,NULL,'<div class="prodid-question">
  <p>
    <b>Noble Gas</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Inert elements naturally existing in a gaseous state that comprise Group VIII
						(Group 18) of the Periodic Table.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 755,'MCAT755',4,NULL,'<div class="prodid-question">
  <p>
    <b>Nonmetals</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Elements that have characteristically high electronegativity, ionization
						energy, and electron affinity. These elements tend to be found on the right
						side of the Periodic Table and are poor conductors of electricity.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 756,'MCAT756',4,NULL,'<div class="prodid-question">
  <p>
    <b>Nonpolar Covalent Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of covalent bond between atoms with the same electronegativities,
						resulting in an even distribution of electron density along the bond.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 757,'MCAT757',4,NULL,'<div class="prodid-question">
  <p>
    <b>Normality</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Concentration of a solution calculated by gram equivalent weight of solute
						per liter of solution, often denoted by <i>N</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 758,'MCAT758',4,NULL,'<div class="prodid-question">
  <p>
    <b>Octet Rule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A rule stating that atoms tend to react in order to form a complete octet of
						valence electrons. Exceptions include hydrogen (stable with two electrons),
						Be (four), B (six); any element in Period 3 and lower (can have more than
						eight electrons and be stable); and molecules with an odd number of
						electrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 759,'MCAT759',4,NULL,'<div class="prodid-question">
  <p>
    <b>Open System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A system that allows for the exchange of energy and matter across its
						boundaries.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 760,'MCAT760',4,NULL,'<div class="prodid-question">
  <p>
    <b>Oxidation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction in which a species loses electrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 761,'MCAT761',4,NULL,'<div class="prodid-question">
  <p>
    <b>Oxidation–Reduction Half-Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A hypothetical equation showing only the species that is oxidized or reduced
						in a oxidation–reduction reaction and the correct number of electrons
						transferred between the species in the complete, balanced equation.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 762,'MCAT762',4,NULL,'<div class="prodid-question">
  <p>
    <b>Oxidizing Agent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A species that is reduced in the process of oxidizing another species.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 763,'MCAT763',4,NULL,'<div class="prodid-question">
  <p>
    <b>Paramagnetic</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An atom or a substance that contains unpaired electrons and is consequently
						attracted to a magnet.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 764,'MCAT764',4,NULL,'<div class="prodid-question">
  <p>
    <b>Partial Pressure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The pressure contribution of a single gas in a container holding a mixture of
						gases, as given by the equation <i>P</i></b>
    <sub>
      <b>A</b>
    </sub>
    <b> = <i>X</i></b>
    <sub>
      <b>A</b>
    </sub>
    <b>
      <i>P</i>
    </b>
    <sub>
      <b>total</b>
    </sub>
    <b>, where <i>X</i></b>
    <sub>
      <b>A</b>
    </sub>
    <b> is the mole fraction of gas A and <i>P</i></b>
    <sub>
      <b>total</b>
    </sub>
    <b> is the total pressure of the mixture.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 765,'MCAT765',4,NULL,'<div class="prodid-question">
  <p>
    <b>Pauli Exclusion Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that no two electrons in an atom can have the same set of four quantum
						number values.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 766,'MCAT766',4,NULL,'<div class="prodid-question">
  <p>
    <b>Percent Composition</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The percentages of the elements making up a compound (usually by mass in
						amu).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 767,'MCAT767',4,NULL,'<div class="prodid-question">
  <p>
    <b>Percent Yield</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A ratio (calculated as a percentage) of the actual mass of product yielded to
						the theoretical yield of product mass.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 768,'MCAT768',4,NULL,'<div class="prodid-question">
  <p>
    <b>pH</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Scaled value used to measure the acidic strength of a solution, calculated by
						taking the negative logarithm of the proton concentration in a solution: pH
						= –log [H</b>
    <sup>
      <b>+</b>
    </sup>
    <b>].</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 769,'MCAT769',4,NULL,'<div class="prodid-question">
  <p>
    <b>Phase Diagram</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A pressure–temperature plot showing the conditions under which a
						substance exists in pure phase or in equilibrium between different
						phases.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 770,'MCAT770',4,NULL,'<div class="prodid-question">
  <p>
    <b>Photon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A particle of light energy with a value equal to <i>hf</i>, where <i>h</i> is
						Planck’s constant and <i>f</i> is the frequency of radiation.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 771,'MCAT771',4,NULL,'<div class="prodid-question">
  <p>
    <b>Polar Covalent Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of covalent bond between atoms with different electronegativities that
						results in an unequal sharing of electron pairs, giving the bond partially
						positive and partially negative poles.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 772,'MCAT772',4,NULL,'<div class="prodid-question">
  <p>
    <b>Process Function</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A variable that depends on the path taken to get from one state to another.
						Includes work and heat.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 773,'MCAT773',4,NULL,'<div class="prodid-question">
  <p>
    <b>Raoult’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the vapor pressure of a solvent is proportional to the mole
						fraction of the solvent in the solution; provides an explanation for the
						boiling point elevation seen in solutions.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 774,'MCAT774',4,NULL,'<div class="prodid-question">
  <p>
    <b>Rate-Determining Step</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The slowest step in a reaction mechanism; determines the overall rate of the
						reaction.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 775,'MCAT775',4,NULL,'<div class="prodid-question">
  <p>
    <b>Rate Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An experimentally determined mathematical expression showing the rate of a
						reaction as a function of the concentrations of its reactants.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 776,'MCAT776',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Mechanism</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A “play-by-play” showing the individual steps of a reaction,
						including the formation and destruction of any reaction intermediates that
						may occur.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 777,'MCAT777',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Order</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The sum of the exponents in a rate law, where each exponent provides the
						reaction order with respect to its reactant.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 778,'MCAT778',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Quotient</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The ratio of the concentrations of the products to the concentrations of the
						reactants at any point during a reaction, where each reactant and product in
						the expression is raised to the power of its stoichiometric coefficient.
						Commonly denoted by <i>Q</i>. Usually compared to the equilibrium constant
						to determine the direction a reaction will proceed to regain
						equilibrium.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 779,'MCAT779',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reaction Rate</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The measure of how quickly reactants are consumed and products are formed,
						commonly expressed in terms of mol L</b>
    <sup>
      <b>–1</b>
    </sup>
    <b> s</b>
    <sup>
      <b>–1</b>
    </sup>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 780,'MCAT780',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reducing Agent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A species that is oxidized in the process of reducing another species.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 781,'MCAT781',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reduction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction in which a species gains electrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 782,'MCAT782',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reduction Potential</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A measure of the tendency of a species to be reduced, commonly used in
						identifying the anode and cathode of an electrochemical cell.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 783,'MCAT783',4,NULL,'<div class="prodid-question">
  <p>
    <b>Resonance Structures</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Alternate Lewis structures of the same molecule that show the delocalization
						of electrons within that molecule; Lewis structures that contribute to a
						resonancestabilized system. Resonance structures have the same atomic
						connectivity but differ in the distribution of electrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 784,'MCAT784',4,NULL,'<div class="prodid-question">
  <p>
    <b>Reversible Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A process that will proceed bidirectionally to form both product and
						reactant.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 785,'MCAT785',4,NULL,'<div class="prodid-question">
  <p>
    <b>Single-Displacement Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A chemical reaction in which an atom or ion of one compound is replaced by
						another atom or ion (such as A + BC → B + AC).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 786,'MCAT786',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solubility</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A ratio that measures how much solute can dissolve in a solvent at a given
						temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 787,'MCAT787',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solubility Product Constant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The product of the molar concentrations of dissociated ions in a saturated
						solution, where each ion is raised to the power of its stoichiometric
						coefficient. <br />Denoted <i>K</i></b>
    <sub>
      <b>sp</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 788,'MCAT788',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solute</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound, commonly an ion, dissolved in a solvent to create a solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 789,'MCAT789',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solution Equilibrium</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>When a solute is dissolved in a solvent, it will dissociate until reaching an
						equilibrium point at which the rate of dissociation equals the rate of
						precipitation of the solute, regardless of any additional solute introduced
						into the mixture.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 790,'MCAT790',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solvation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process of forming a cagelike network of solvent molecules around a
						solute in a solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 791,'MCAT791',4,NULL,'<div class="prodid-question">
  <p>
    <b>Solvent</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A medium, commonly a liquid, into which a solute is dissolved to create a
						solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 792,'MCAT792',4,NULL,'<div class="prodid-question">
  <p>
    <b>Specific Heat</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of heat required to raise one gram of a substance by one degree
						Celsius.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 793,'MCAT793',4,NULL,'<div class="prodid-question">
  <p>
    <b>Spontaneous Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction that will proceed or occur on its own without additional energy
						input from its surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 794,'MCAT794',4,NULL,'<div class="prodid-question">
  <p>
    <b>Standard Temperature and Pressure (STP) </b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>273 Kelvin (0° Celsius) and 1 atmosphere(760 mmHg).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 795,'MCAT795',4,NULL,'<div class="prodid-question">
  <p>
    <b>State Function</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A variable that depends only on the current state of a system, and not the
						path taken to get there. Includes pressure, density, temperature, volume,
						enthalpy, internal energy, Gibbs free energy, and entropy.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 796,'MCAT796',4,NULL,'<div class="prodid-question">
  <p>
    <b>Strong Acid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An acid that will completely dissociate in aqueous solution (such as HCl, HI,
						HNO</b>
    <sub>
      <b>3</b>
    </sub>
    <b>, and HClO</b>
    <sub>
      <b>4</b>
    </sub>
    <b>). </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 797,'MCAT797',4,NULL,'<div class="prodid-question">
  <p>
    <b>System</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The part of the universe under consideration that is separated by some real
						or imaginary boundary from its surroundings.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 798,'MCAT798',4,NULL,'<div class="prodid-question">
  <p>
    <b>Theoretical Yield</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The expected amount of product yielded in a reaction according to the
						reactants’ stoichiometry.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 799,'MCAT799',4,NULL,'<div class="prodid-question">
  <p>
    <b>Titrand</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution of unknown concentration to which small volumes of a solution of
						known concentration are added to reach the equivalence point.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 800,'MCAT800',4,NULL,'<div class="prodid-question">
  <p>
    <b>Titrant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A solution of known concentration added in small volumes to a solution of
						unknown concentration to reach the equivalence point.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 801,'MCAT801',4,NULL,'<div class="prodid-question">
  <p>
    <b>Titration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An analytical procedure in which a solution of known concentration is slowly
						added to a solution of unknown concentration to the point of molar
						equivalency, thereby providing the concentration of the unknown
						solution.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 802,'MCAT802',4,NULL,'<div class="prodid-question">
  <p>
    <b>Transition Elements</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The elements found in the B groups of the Periodic Table. These elements
						contain partially filled <i>d</i> subshells.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 803,'MCAT803',4,NULL,'<div class="prodid-question">
  <p>
    <b>Transition State</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A high-energy complex in which old bonds are partially broken and new bonds
						are partially formed. Charges existing only in the transition state are
						designated as partial charges.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 804,'MCAT804',4,NULL,'<div class="prodid-question">
  <p>
    <b>Triple Point</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A point on a phase diagram at which a substance exists in equilibrium between
						all three phases. </b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 805,'MCAT805',4,NULL,'<div class="prodid-question">
  <p>
    <b>Valence Electrons</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrons occupying the outermost electron shell of an atom that
						participate in chemical bonds. Atoms with the same number of valence
						electrons usually have similar properties.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 806,'MCAT806',4,NULL,'<div class="prodid-question">
  <p>
    <b>VSEPR Theory</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The acronym for valence shell electron pair repulsion theory, which states
						that the three-dimensional molecular geometry about some central atom is
						determined by the electronic repulsions between its bonding and nonbonding
						electron pairs.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 807,'MCAT807',4,NULL,'<div class="prodid-question">
  <p>
    <b>Water Dissociation Constant</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An expression of the autoionization of water into H</b>
    <sup>
      <b>+</b>
    </sup>
    <b> and OH</b>
    <sup>
      <b>–</b>
    </sup>
    <b> at a certain temperature, given by the product of the ions’ molar
						concentrations. Denoted by <i>K</i></b>
    <sub>
      <b>w</b>
    </sub>
    <b>, and equal to 10</b>
    <sup>
      <b>–14</b>
    </sup>
    <b> at 25°C. <i>K</i></b>
    <sub>
      <b>w</b>
    </sub>
    <b> = [H</b>
    <sup>
      <b>+</b>
    </sup>
    <b>][OH</b>
    <sup>
      <b>–</b>
    </sup>
    <b>].</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 808,'MCAT808',5,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>α</i>-Carbon</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The carbon adjacent to a carbonyl; in amino acids, the chiral stereocenter in
						all amino acids except glycine.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 809,'MCAT809',5,NULL,'<div class="prodid-question">
  <p>
    <b>Absolute Conformation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The exact spatial arrangement of atoms or groups in a chiral molecule around
						a single chiral atom, designated by (<i>R</i>) or (<i>S</i>).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 810,'MCAT810',5,NULL,'<div class="prodid-question">
  <p>
    <b>Acetal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A functional group that contains a carbon atom bonded to two –OR groups, an alkyl chain, and a hydrogen atom.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 811,'MCAT811',5,NULL,'<div class="prodid-question">
  <p>
    <b>Achiral</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A molecule that either does not contain a chiral center or contains chiral
						centers and a plane of symmetry; as such, it has a superimposable mirror
						image.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 812,'MCAT812',5,NULL,'<div class="prodid-question">
  <p>
    <b>Aldehyde</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that has a carbonyl as a terminal group. Aldehydes are named by
						replacing the –<i>e</i> in the corresponding alkane with
							–<i>al</i>. Formaldehyde, acetaldehyde, and propionaldehyde are
						common names used for the simplest aldehydes.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 813,'MCAT813',5,NULL,'<div class="prodid-question">
  <p>
    <b>Aldol Condensation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction in which an aldehyde or ketone acts as both the electrophile and
						nucleophile, resulting in the formation of a carbon–carbon bond in a
						new molecule called an aldol.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 814,'MCAT814',5,NULL,'<div class="prodid-question">
  <p>
    <b>Alkanes</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Compounds consisting of only carbons and hydrogens bonded with
							<i>σ</i> bonds. As chain length increases, boiling point, melting
						point, and density increase. However, chain branching decreases both boiling
						point and density.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 815,'MCAT815',5,NULL,'<div class="prodid-question">
  <p>
    <b>Amide</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that has a carbonyl group bonded to nitrogen. Amides are named by
						dropping the –<i>oic</i><i>acid</i> in the corresponding carboxylic acid and adding
							–<i>amide</i>. Substituents attached to nitrogen are listed as
						prefixes and are preceded by <i>N</i>–.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 816,'MCAT816',5,NULL,'<div class="prodid-question">
  <p>
    <b>Anhydride</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A functional group containing two carbonyls separated by an oxygen atom
						(RCOOCOR); often the condensation dimer of a carboxylic acid.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 817,'MCAT817',5,NULL,'<div class="prodid-question">
  <p>
    <b>Axial</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes groups on a cyclic molecule that are perpendicular to the plane of
						the molecule, pointing straight up or down.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 818,'MCAT818',5,NULL,'<div class="prodid-question">
  <p>
    <b>Azimuthal Quantum Number (<i>I</i>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes the subshell in which an electron is found; possible values range
						from 0 to <i>n</i> – 1, with <i>l</i> = 0 representing the <i>s</i>
						subshell, <i>l</i> = 1 representing <i>p</i>, <i>l</i> = 2 representing
							<i>d</i>, and <i>l</i> = 3 representing <i>f</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 819,'MCAT819',5,NULL,'<div class="prodid-question">
  <p>
    <b>Carbonyl</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A double bond between a carbon and an oxygen.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 820,'MCAT820',5,NULL,'<div class="prodid-question">
  <p>
    <b>Carboxylic Acid</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that has a –COOH terminal group. Carboxylic acids are named
						by replacing the –<i>e</i> in the corresponding alkane with
							–<i>oic acid</i>. Formic acid and acetic acid are common names
						for the simplest carboxylic acids: methanoic acid and ethanoic acid,
						respectively.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 821,'MCAT821',5,NULL,'<div class="prodid-question">
  <p>
    <b>Carboxylic Acid Derivative</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that can be created from a carboxylic acid by nucleophilic acyl
						substitution; includes anhydrides, esters, and amides.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 822,'MCAT822',5,NULL,'<div class="prodid-question">
  <p>
    <b>Chemical Properties</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Characteristics of compounds that change chemical composition during a
						reaction; determines how a molecule will react with other molecules.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 823,'MCAT823',5,NULL,'<div class="prodid-question">
  <p>
    <b>Chemical Shift (<i>δ</i>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An arbitrary variable used to plot NMR spectra; measured in parts per million
						(ppm).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 824,'MCAT824',5,NULL,'<div class="prodid-question">
  <p>
    <b>Chiral</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A molecule or carbon atom bonded to four different groups and without a plane
						of symmetry; thus, it is not superimposable upon its mirror image and has an
						enantiomer.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 825,'MCAT825',5,NULL,'<div class="prodid-question">
  <p>
    <b>Chromatography</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A separation technique that uses the retention time of a compound in the
						mobile phase as it travels through the stationary phase to separate
						compounds with different chemical properties.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 826,'MCAT826',5,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Cis–Trans</i> Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of diastereomers with different arrangements of substituents about an
						immovable bond.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 827,'MCAT827',5,NULL,'<div class="prodid-question">
  <p>
    <b>Condensation Reaction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A reaction that combines two molecules into one, with the loss of a small
						molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 828,'MCAT828',5,NULL,'<div class="prodid-question">
  <p>
    <b>Configurational Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Isomers that can only interconvert by breaking bonds; include enantiomers,
						diastereomers, and <i>cis–trans</i> isomers.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 829,'MCAT829',5,NULL,'<div class="prodid-question">
  <p>
    <b>Conformational Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stereoisomers that differ by rotation about one or more single bonds, usually
						represented using Newman projections.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 830,'MCAT830',5,NULL,'<div class="prodid-question">
  <p>
    <b>Conjugation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Alternating single and multiple bonds that create a system of parallel
						unhybridized <i>p </i>-orbitals; thus, electrons can be shared between these
						orbitals, forming electron clouds above and below the plane of the molecule
						and stabilizing it.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 831,'MCAT831',5,NULL,'<div class="prodid-question">
  <p>
    <b>Constitutional Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Molecules that have the same molecular formulas but different connectivity;
						also called structural isomers.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 832,'MCAT832',5,NULL,'<div class="prodid-question">
  <p>
    <b>Coupling</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In NMR spectroscopy, a phenomenon that occurs when there are protons in such
						close proximity to each other that their magnetic moments affect each
						other’s appearance in the NMR spectrum by subdividing their peaks
						into subpeaks; also called splitting.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 833,'MCAT833',5,NULL,'<div class="prodid-question">
  <p>
    <b>Cyanohydrin</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A functional group containing a nitrile (–C ≡ N) and a hydroxyl
						group.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 834,'MCAT834',5,NULL,'<div class="prodid-question">
  <p>
    <b>Decarboxylation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The complete loss of a carboxyl group as carbon dioxide.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 835,'MCAT835',5,NULL,'<div class="prodid-question">
  <p>
    <b>Diastereomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Stereoisomers that are not mirror images of each other. Diastereomers differ
						in their configurations at least one chiral center and share the same
						configuration at least one chiral center. They have the same chemical
						properties but different physical properties.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 836,'MCAT836',5,NULL,'<div class="prodid-question">
  <p>
    <b>Distillation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A separation technique used to separate liquids with different boiling
						points. The mixture is heated slowly, and as the liquid with the lower
						boiling point changes to its gaseous form, it passes through a condenser,
						where it cools back into its liquid form.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 837,'MCAT837',5,NULL,'<div class="prodid-question">
  <p>
    <b>Electron-Donating</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes groups that push additional electron density toward another atom;
						stabilizes positive charges and destabilizes negative charges while
						decreasing acidity.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 838,'MCAT838',5,NULL,'<div class="prodid-question">
  <p>
    <b>Electron-Withdrawing</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes groups that pull electron density away from another atom;
						stabilizes negative charges and destabilizes positive charges while
						increasing acidity.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 839,'MCAT839',5,NULL,'<div class="prodid-question">
  <p>
    <b>Electrophiles</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>“Electron-loving” atoms with a positive charge or positive
						polarization; can accept an electron pair when forming new bonds with a
						nucleophile.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 840,'MCAT840',5,NULL,'<div class="prodid-question">
  <p>
    <b>Enantiomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Nonsuperimposable stereoisomers that are mirror images of each other.
						Enantiomers differ in configuration at every chiral center but share the
						same chemical properties in a nonstereospecific environment. Their optical
						activities are exactly opposites of each other.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 841,'MCAT841',5,NULL,'<div class="prodid-question">
  <p>
    <b>Enol</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The resonance form of a carbonyl that has a carbon–carbon double bond
							(<i>ene</i>) and an alcohol (<i>–ol</i>).</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 842,'MCAT842',5,NULL,'<div class="prodid-question">
  <p>
    <b>Equatorial</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes groups on a cyclic molecule that are in the plane of the
						molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.659793','2014-07-24 12:15:09.659793',0,'skipped');
INSERT INTO flashcards VALUES(1, 843,'MCAT843',5,NULL,'<div class="prodid-question">
  <p>
    <b>Ester</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that has a –COOR group. Esters are named as alkyl or aryl
						alkanoates.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 844,'MCAT844',5,NULL,'<div class="prodid-question">
  <p>
    <b>Extraction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method used to separate a component in a mixture by exploiting its
						solubility properties. Two solvents are usually used (one aqueous and one
						organic), and the component of interest will be soluble in one phase, while
						the impurities will be soluble in the other.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 845,'MCAT845',5,NULL,'<div class="prodid-question">
  <p>
    <b>Fingerprint Region</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>In an IR spectrum, the region of 1400 to 400 cm</b>
    <sup>
      <b>–1</b>
    </sup>
    <b> where more complex vibration patterns, caused by the motion of the molecule
						as a whole, can be seen; it is characteristic of each individual
						molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 846,'MCAT846',5,NULL,'<div class="prodid-question">
  <p>
    <b>Gabriel (Malonic–Ester) Synthesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method of synthesizing amino acids that uses potassium phthalimide and
						diethyl bromomalonate followed by an alkyl halide; two substitution
						reactions are followed by hydrolysis and decarboxylation.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 847,'MCAT847',5,NULL,'<div class="prodid-question">
  <p>
    <b>Gas Chromatography (GC)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of chromatography used to separate vaporizable compounds; the
						stationary phase is a crushed metal or polymer and the mobile phase is a
						nonreactive gas.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 848,'MCAT848',5,NULL,'<div class="prodid-question">
  <p>
    <b>Hemiacetal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A functional group that contains a carbon atom bonded to one –OR
						group, one –OH group, an alkyl chain, and a hydrogen atom.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 849,'MCAT849',5,NULL,'<div class="prodid-question">
  <p>
    <b>Hemiketal</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A functional group that contains a carbon atom bonded to one –OR
						group, one –OH group, and two alkyl chains.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 850,'MCAT850',5,NULL,'<div class="prodid-question">
  <p>
    <b>High-Performance Liquid Chromatography (HPLC)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A form of chromatography in which a small sample is put into a column that
						can be manipulated with sophisticated solvent gradients to allow very
						refined separation and characterization; formerly called high-pressure
						liquid chromatography.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 851,'MCAT851',5,NULL,'<div class="prodid-question">
  <p>
    <b>Highest Occupied Molecular Orbital (HOMO)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The highest-energy molecular orbital containing electrons; in UV
						spectroscopy, electrons are excited from the HOMO to the LUMO.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 852,'MCAT852',5,NULL,'<div class="prodid-question">
  <p>
    <b>Hydroxyl Group</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An –OH group; seen in alcohols, hemiacetals and hemiketals, carboxylic
						acids, water, and other compounds.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 853,'MCAT853',5,NULL,'<div class="prodid-question">
  <p>
    <b>Imine</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A double bond between a carbon and a nitrogen.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 854,'MCAT854',5,NULL,'<div class="prodid-question">
  <p>
    <b>Immiscible</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes two solvents that will not mix with or dissolve each other.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 855,'MCAT855',5,NULL,'<div class="prodid-question">
  <p>
    <b>Induction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The pull of electron density across <i>σ</i> bonds.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 856,'MCAT856',5,NULL,'<div class="prodid-question">
  <p>
    <b>Infrared (IR) Spectroscopy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A technique that measures molecular vibrations at different frequencies, from
						which specific bonds can be determined; functional groups can be inferred
						based on this information.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 857,'MCAT857',5,NULL,'<div class="prodid-question">
  <p>
    <b>Inorganic Phosphate (P</b>
    <sub>
      <b>i</b>
    </sub>
    <b>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Derived from phosphoric acid, the molecule that forms high-energy bonds for
						energy storage in nucleotide triphosphates like ATP; also used for enzyme
						regulation.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 858,'MCAT858',5,NULL,'<div class="prodid-question">
  <p>
    <b>Ketone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A compound that has a nonterminal carbonyl group. Ketones are named by
						replacing the –<i>e </i> in the corresponding alkane with
							–<i>one</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 859,'MCAT859',5,NULL,'<div class="prodid-question">
  <p>
    <b>Lactam</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cyclic amide; named according to the number of carbon atoms other than the
						carbonyl carbon.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 860,'MCAT860',5,NULL,'<div class="prodid-question">
  <p>
    <b>Lactone</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A cyclic ester; named according to the number of carbon atoms other than the
						carbonyl carbon and for the straight-chain form of the compound.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 861,'MCAT861',5,NULL,'<div class="prodid-question">
  <p>
    <b>Leaving Group</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Atoms or groups that can dissociate from the parent chain to form a stable
						species after accepting electron pairs. Weak bases tend to be good leaving
						groups.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 862,'MCAT862',5,NULL,'<div class="prodid-question">
  <p>
    <b>Lowest Unoccupied Molecular Orbital (LUMO)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The lowest-energy molecular orbital that does not contain electrons; in UV
						spectroscopy, electrons are excited from the HOMO to the LUMO.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 863,'MCAT863',5,NULL,'<div class="prodid-question">
  <p>
    <b>Magnetic Quantum Number (<i>ml</i>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes the orbital in which an electron is found; possible values range
						from –<i>l</i> to +<i>l</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 864,'MCAT864',5,NULL,'<div class="prodid-question">
  <p>
    <b>
      <i>Meso</i> Compound</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A stereoisomer with an internal plane of symmetry. <i>Meso</i> compounds are
						optically inactive.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 865,'MCAT865',5,NULL,'<div class="prodid-question">
  <p>
    <b>Mobile Phase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A liquid (or a gas in gas chromatography) that is run through the stationary
						phase in chromatography.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 866,'MCAT866',5,NULL,'<div class="prodid-question">
  <p>
    <b>Newman Projection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method of visualizing a compound in which the line of sight is down a
						carbon–carbon bond axis.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 867,'MCAT867',5,NULL,'<div class="prodid-question">
  <p>
    <b>Nonbonded Strain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Increased energy that results when nonadjacent atoms or groups compete for
						the same space; also called steric strain.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 868,'MCAT868',5,NULL,'<div class="prodid-question">
  <p>
    <b>Nuclear Magnetic Resonance (NMR) Spectroscopy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A technique that measures the alignment of magnetic moments from certain
						molecular nuclei with an external magnetic field; can be used to determine
						the connectivity and functional groups in a molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 869,'MCAT869',5,NULL,'<div class="prodid-question">
  <p>
    <b>Nucleophile</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A species that tends to donate electrons to another atom. With the same
						attacking atom (OH</b>
    <sup>
      <b>–</b>
    </sup>
    <b>, CH</b>
    <sub>
      <b>3</b>
    </sub>
    <b>O</b>
    <sup>
      <b>–</b>
    </sup>
    <b>) in aprotic solvents, nucleophile strength correlates to basicity. In protic
						solvents and in situations where the attacking atom is different (OH</b>
    <sup>
      <b>–</b>
    </sup>
    <b>, SH</b>
    <sup>
      <b>–</b>
    </sup>
    <b>), nucleophile strength correlates to smaller size.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 870,'MCAT870',5,NULL,'<div class="prodid-question">
  <p>
    <b>Optical Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Configurational isomers that have different spatial arrangements of
						substituents, which affects the rotation of plane-polarized light.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 871,'MCAT871',5,NULL,'<div class="prodid-question">
  <p>
    <b>Oxidation State</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An indication of the hypothetical charge that an atom would have if all of
						its bonds were completely ionic.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 872,'MCAT872',5,NULL,'<div class="prodid-question">
  <p>
    <b>Phenol</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An alcohol with an aromatic ring; has slightly more acidic hydroxyl hydrogens
						than other alcohols.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 873,'MCAT873',5,NULL,'<div class="prodid-question">
  <p>
    <b>Phosphodiester Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of bond that links the sugar moieties of adjacent nucleotides in
						DNA.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 874,'MCAT874',5,NULL,'<div class="prodid-question">
  <p>
    <b>Physical Properties</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Characteristics of compounds that do not change chemical composition, such a
						melting point, boiling point, solubility, odor, color, and density.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 875,'MCAT875',5,NULL,'<div class="prodid-question">
  <p>
    <b>Pi (π) Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The bonding molecular orbital formed when two parallel <i>p</i> -orbitals
						share electrons; exists as electron clouds above and below the
							<i>σ</i> bond between the two nuclei.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 876,'MCAT876',5,NULL,'<div class="prodid-question">
  <p>
    <b>Principal Quantum Number (<i>n</i>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes the shell in which an electron is found; values range from 1 to
						∞.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 877,'MCAT877',5,NULL,'<div class="prodid-question">
  <p>
    <b>Racemic Mixture</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A mixture that contains equal amounts of (+) and (–) enantiomers.
						Racemic mixtures are not optically active.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 878,'MCAT878',5,NULL,'<div class="prodid-question">
  <p>
    <b>Relative Configuration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The spatial arrangement of groups in a chiral molecule compared to another
						chiral molecule.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 879,'MCAT879',5,NULL,'<div class="prodid-question">
  <p>
    <b>Retardation Factor (<i>R</i></b>
    <sub>
      <b>f</b>
    </sub>
    <b>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A ratio used in thin-layer chromatography to identify a compound; calculated
						as how far the compound traveled relative to how far the solvent front
						traveled.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 880,'MCAT880',5,NULL,'<div class="prodid-question">
  <p>
    <b>Ring Strain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Energy created in a cyclic molecule by angle strain, torsional strain, and
						nonbonded strain; determines whether a ring is stable enough to stay
						intact.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 881,'MCAT881',5,NULL,'<div class="prodid-question">
  <p>
    <b>Shielding</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon of atoms pushing electron density toward surrounding atoms; in
						NMR spectroscopy, pulls a group further upfield on the spectrum. Deshielding
						is the opposite—the pulling of electron density by surrounding atoms;
						in NMR, pulls a group further downfield on the spectrum.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 882,'MCAT882',5,NULL,'<div class="prodid-question">
  <p>
    <b>Sigma (<i>σ</i>) Bond</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The bonding molecular orbital formed by head-to-head or tail-to-tail overlap
						of atomic orbitals; all single bonds are <i>σ</i> bonds.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 883,'MCAT883',5,NULL,'<div class="prodid-question">
  <p>
    <b>Specific Rotation ([<i>α</i>])</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A standardized measure of a compound’s ability to rotate
						plane-polarized light.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 884,'MCAT884',5,NULL,'<div class="prodid-question">
  <p>
    <b>Spectroscopy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Laboratory technique that relies on measurement of the energy differences
						between the possible states of a molecular system by determining the
						frequencies of electromagnetic radiation (light) absorbed by the molecules
						or their response to a magnetic field.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 885,'MCAT885',5,NULL,'<div class="prodid-question">
  <p>
    <b>Spin Quantum Number (<i>m</i></b>
    <sub>
      <b>
        <i>s</i>
      </b>
    </sub>
    <b>)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Describes the intrinsic spin of the two electrons in an orbital by
						arbitrarily assigning one of the electrons a spin of <img src="/assets/contents/MCAT885img1.png" /> and the other a spin of<img src="/assets/contents/MCAT885img2.png" />.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 886,'MCAT886',5,NULL,'<div class="prodid-question">
  <p>
    <b>Stationary Phase</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phase over which the mobile phase runs during chromatography; usually a
						solid material. Also called the adsorbent.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 887,'MCAT887',5,NULL,'<div class="prodid-question">
  <p>
    <b>Stereoisomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Compounds with the same molecular formula and connectivity but different
						arrangements in space. Stereoisomers include <i>cis–trans</i>
						isomers, diastereomers, enantiomers, conformational isomers, and <i>meso</i>
						compounds.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 888,'MCAT888',5,NULL,'<div class="prodid-question">
  <p>
    <b>Steric Hindrance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The prevention of a reaction at a particular location in a molecule by
						substituent groups around the reactive site.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 889,'MCAT889',5,NULL,'<div class="prodid-question">
  <p>
    <b>Steric Protection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The prevention of the formation of alternative products using a protecting
						group such as a mesylate or tosylate.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 890,'MCAT890',5,NULL,'<div class="prodid-question">
  <p>
    <b>Strecker Synthesis</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A method of synthesizing amino acids that uses condensation between an
						aldehyde and hydrogen cyanide, followed by hydrolysis.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 891,'MCAT891',5,NULL,'<div class="prodid-question">
  <p>
    <b>Structural Isomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Molecules that have the same molecular formulas but different connectivity;
						also called constitutional isomers.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 892,'MCAT892',5,NULL,'<div class="prodid-question">
  <p>
    <b>Tautomers</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Isomers that can interconvert by changing the location of a proton.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 893,'MCAT893',5,NULL,'<div class="prodid-question">
  <p>
    <b>Thin-Layer Chromatography (TLC)</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of chromatography that uses silica gel or alumina on a card as the
						medium for the stationary phase.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 894,'MCAT894',5,NULL,'<div class="prodid-question">
  <p>
    <b>Torsional Strain</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Increased energy that results when molecules assume eclipsed or gauche
						staggered conformations.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 895,'MCAT895',5,NULL,'<div class="prodid-question">
  <p>
    <b>Transesterification</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The process that transforms one ester to another when an alcohol acts as a
						nucleophile and displaces the alkoxy group on an ester.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 896,'MCAT896',5,NULL,'<div class="prodid-question">
  <p>
    <b>Ultraviolet (UV) Spectroscopy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A technique that measures absorbance of ultraviolet light of various
						wavelengths passing through a sample.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 897,'MCAT897',5,NULL,'<div class="prodid-question">
  <p>
    <b>Wavenumber</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An analog of frequency used for infrared spectra instead of wavelength.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 898,'MCAT1000',6,NULL,'<div class="prodid-question">
  <p>
    <b>Zeroth Law of Thermodynamics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that two objects that are in thermal equilibrium with a third object
						are also in thermal equilibrium with each other. By extension, objects at
						the same temperature are in thermal equilibrium with no net transfer of
						heat.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 899,'MCAT898',6,NULL,'<div class="prodid-question">
  <p>
    <b>Acceleration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A vector quantity describing a change in velocity over the elapsed time
						during which that change occurs, expressed as <img src="/assets/contents/MCAT898img1.png" />.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 900,'MCAT899',6,NULL,'<div class="prodid-question">
  <p>
    <b>Adhesion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of attractive force that molecules of a liquid feel toward molecules
						of another substance, such as in the adhesion of water droplets to a glass
						surface.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 901,'MCAT900',6,NULL,'<div class="prodid-question">
  <p>
    <b>Alpha Decay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nuclear reaction in which an <i>α</i>-particle (<img src="/assets/contents/MCAT900img1.png" />) is emitted:<img src="/assets/contents/MCAT900img2.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 902,'MCAT901',6,NULL,'<div class="prodid-question">
  <p>
    <b>Archimedes’ Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that a body that is fully or partially immersed in a liquid will be
						buoyed upwards by a force that is equal to the weight of the liquid
						displaced by the body: <i>F</i></b>
    <sub>
      <b>buoy</b>
    </sub>
    <b> = <i>ρ</i></b>
    <sub>
      <b>fluid</b>
    </sub>
    <b>
      <i>V</i>
    </b>
    <sub>
      <b>submerged</b>
    </sub>
    <b>g.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 903,'MCAT902',6,NULL,'<div class="prodid-question">
  <p>
    <b>Bernoulli’s Equation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Equation describing the conservation of energy in fluid flow, given by <img src="/assets/contents/MCAT902img1.png" />According to the Venturi effect,
						for a given depth, linear flow speed and pressure are inversely related.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 904,'MCAT903',6,NULL,'<div class="prodid-question">
  <p>
    <b>Beta Decay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nuclear reaction in which a <i>β</i>-particle (<i>e</i>–) is
						emitted: <img src="/assets/contents/MCAT903img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 905,'MCAT904',6,NULL,'<div class="prodid-question">
  <p>
    <b>Binding Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy that holds the protons and neutrons together in the nucleus,
						defined by the equation <i>E</i> = <i>m</i>c</b>
    <sup>
      <b>2</b>
    </sup>
    <b>, where <i>m</i> is the mass defect and c is the speed of light in a
						vacuum.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 906,'MCAT905',6,NULL,'<div class="prodid-question">
  <p>
    <b>Capacitance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A measure of a capacitor’s ability to store charge at a given voltage;
						calculated by the ratio of the magnitude of charge on one plate to the
						voltage across the two plates: <img src="/assets/contents/MCAT905img1.png" />
						The SI unit for capacitance is the farad (F).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 907,'MCAT906',6,NULL,'<div class="prodid-question">
  <p>
    <b>Capacitor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An electric device used in circuits that is composed of two conducting plates
						separated by a short distance; these devices store electric charge.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 908,'MCAT907',6,NULL,'<div class="prodid-question">
  <p>
    <b>Center of Mass</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The point on some object or body where all of its mass is considered to be
						concentrated. In a uniform gravitational field, this is also the center of
						gravity.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 909,'MCAT908',6,NULL,'<div class="prodid-question">
  <p>
    <b>Centripetal Acceleration</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The acceleration of an object traveling in a circle that points toward the
						center of the circle. In uniform circular motion, it is equal in magnitude
						to the velocity squared divided by the radius of the circle traversed: <img src="/assets/contents/MCAT908img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 910,'MCAT909',6,NULL,'<div class="prodid-question">
  <p>
    <b>Cohesion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A type of attractive force felt by liquid molecules toward each other.
						Cohesion is responsible for surface tension.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 911,'MCAT910',6,NULL,'<div class="prodid-question">
  <p>
    <b>Conduction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Form of heat transfer where energy is transferred by molecular collisions or
						direct contact between two objects.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 912,'MCAT911',6,NULL,'<div class="prodid-question">
  <p>
    <b>Conductor</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A material in which electrons can move with relative ease.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 913,'MCAT912',6,NULL,'<div class="prodid-question">
  <p>
    <b>Conservation of Mechanical Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that when only conservative forces act on an object and work is done,
						energy is conserved and described by the equation: Δ<i>E</i> =
							Δ<i>U</i> + Δ<i>K</i> = 0.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 914,'MCAT913',6,NULL,'<div class="prodid-question">
  <p>
    <b>Conservative Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A force that does not cause dissipation of mechanical energy from a system.
						As such, the work performed is independent of the path taken. Examples
						include gravity and electrostatic forces. Elastic forces are nearly
						conservative.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 915,'MCAT914',6,NULL,'<div class="prodid-question">
  <p>
    <b>Continuity Equation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the mass flow rate of fluid must remain constant from one
						cross-section of a tube to another, given by <i>A</i></b>
    <sub>
      <b>1</b>
    </sub>
    <b>
      <i>v</i>
    </b>
    <sub>
      <b>1</b>
    </sub>
    <b> = <i>A</i></b>
    <sub>
      <b>2</b>
    </sub>
    <b>
      <i>v</i>
    </b>
    <sub>
      <b>2</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 916,'MCAT915',6,NULL,'<div class="prodid-question">
  <p>
    <b>Convection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Form of heat transfer where a heated fluid transfers energy by bulk flow and
						physical motion over another object, or a cooled fluid absorbs energy by the
						same means.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 917,'MCAT916',6,NULL,'<div class="prodid-question">
  <p>
    <b>Coulomb’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The law describing the electrostatic force that exists between two charges,
							<i>q</i></b>
    <sub>
      <b>1</b>
    </sub>
    <b> and <i>q</i></b>
    <sub>
      <b>2</b>
    </sub>
    <b>, given by the equation <img src="/assets/contents/MCAT916img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 918,'MCAT917',6,NULL,'<div class="prodid-question">
  <p>
    <b>Current</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A flow of charge per time. The flow of charge is motivated by a potential
						difference (voltage). Current is denoted <i>I</i> and can be calculated as
							<img src="/assets/contents/MCAT917img1.png" /> Current is conventionally
						considered the theoretical movement of positive charge.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 919,'MCAT918',6,NULL,'<div class="prodid-question">
  <p>
    <b>Density</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A scalar quantity defined as mass per unit volume, often denoted by
							<i>ρ</i>. Density of an object may be compared to water as a
						unitless quantity known as specific gravity.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 920,'MCAT919',6,NULL,'<div class="prodid-question">
  <p>
    <b>Dielectric</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An insulating material placed between the two plates of a capacitor; used to
						increase capacitance. If the circuit is plugged into a current source, more
						charge will be stored. If the circuit is not plugged in, the voltage of the
						capacitor will decrease, indirectly increasing its capacitance. The strength
						of a dielectric is measured by the dielectric constant.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 921,'MCAT920',6,NULL,'<div class="prodid-question">
  <p>
    <b>Diffraction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The spreading-out effect of light when it passes through a small slit
						opening.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 922,'MCAT921',6,NULL,'<div class="prodid-question">
  <p>
    <b>Direct Relationship</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A relationship between variables such that an increase in one variable is
						associated with an increase in the other: <img src="/assets/contents/MCAT921img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 923,'MCAT922',6,NULL,'<div class="prodid-question">
  <p>
    <b>Dispersion</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon observed when white light is incident on the face of a prism
						and emerges on the opposite side with all its wavelengths split apart,
						forming the visible spectrum. This occurs because <i>λ</i> is related
						to the index of refraction by the expression <i>n</i> = <img src="/assets/contents/MCAT922img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 924,'MCAT923',6,NULL,'<div class="prodid-question">
  <p>
    <b>Displacement</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A vector quantity describing the straight-line distance between an initial
						and a final position of some particle or object.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 925,'MCAT924',6,NULL,'<div class="prodid-question">
  <p>
    <b>Doppler Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>When a source emitting a sound and a detector receiving the sound move
						relative to each other, the perceived frequency <i>f</i>′ is less
						than or greater than the actual frequency emitted <i>f</i>, depending on
						whether the source and detector move toward or away from each other: <img src="/assets/contents/MCAT924img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 926,'MCAT925',6,NULL,'<div class="prodid-question">
  <p>
    <b>Elastic Potential Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy associated with stretching or compressing a spring, calculated by
						the equation <i>U</i> = <img src="/assets/contents/MCAT925img1.png" /><i>kx</i></b>
    <sup>
      <b>2</b>
    </sup>
    <b> and given in the SI unit of joules (J).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 927,'MCAT926',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electric Circuit</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A conducting pathway that contains one or more voltage sources that drive an
						electric current along that pathway and through connected passive circuit
						elements (such as resistors).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 928,'MCAT927',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electric Field</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The electrostatic force that a source charge would exert on a positive test
						charge <i>q</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> divided by the magnitude of that test charge:<img src="/assets/contents/MCAT927img1.png" /> Electric fields are represented
						by electric field lines—imaginary lines that show the direction in
						which a positive test charge is accelerated by the coulombic force due to
						the electric field of a source charge.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 929,'MCAT928',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electric Potential</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of electric potential energy per unit charge; the work required to
						bring a positive test charge <i>q</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> from infinity to within an electric field of another positive source charge,
							<i>Q</i>, divided by that test charge’s magnitude. Calculated by
						the equation <i>V</i> = <img src="/assets/contents/MCAT928img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 930,'MCAT929',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electric Potential Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of work required to bring a test charge <i>q</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> from infinity to a point within the electric field of some source charge
							<i>Q</i> , given by the equation <i>U</i> = <i>q</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b>
      <i>V</i> .</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 931,'MCAT930',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electric Power</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The rate at which the energy of flowing charges through a resistor or other
						device is dissipated, given by the equation <i>P</i> = <i>IV</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 932,'MCAT931',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electromagnetic Spectrum</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The full range of frequencies and wavelengths for electromagnetic waves
						broken down into the following regions (in descending order of
							<i>λ</i>): radio, infrared, visible light, ultraviolet, X-ray,
						and gamma ray.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 933,'MCAT932',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electromagnetic Waves</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>When an electric field is changing, it causes a change in a magnetic field
						and vice-versa, resulting in the propagation of a wave containing an
						electric and a magnetic field that are perpendicular to each other.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 934,'MCAT933',6,NULL,'<div class="prodid-question">
  <p>
    <b>Electromotive Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The impetus for current flow created by a voltage source, such as a battery
						or outlet.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 935,'MCAT934',6,NULL,'<div class="prodid-question">
  <p>
    <b>Equipotential Lines</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Concentric circles emanating from a source charge that cross its electric
						field lines perpendicularly. No work is required for a test charge to travel
						along the circumference of an equipotential line, because the potential at
						every point along that line is the same.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 936,'MCAT935',6,NULL,'<div class="prodid-question">
  <p>
    <b>Exponential Decay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A decrease in the amount of substance <i>N</i> at an exponential rate. Given
						by the equation: <i>N</i> = <i>N</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> x e</b>
    <sup>
      <b>–λ</b>
    </sup>
    <sup>
      <b>
        <i>t</i>
      </b>
    </sup>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 937,'MCAT936',6,NULL,'<div class="prodid-question">
  <p>
    <b>First Law of Thermodynamics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the change in internal energy of a system is equal to the heat
						transferred into the system minus the work done by the system:
							Δ<i>U</i> = <i>Q</i> – <i>W</i>. An extension of the law
						of conservation of energy.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 938,'MCAT937',6,NULL,'<div class="prodid-question">
  <p>
    <b>Focal Length</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The distance between the focal point and the mirror or lens. For spherical
						mirrors, the focal length is equal to one-half the radius of curvature.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 939,'MCAT938',6,NULL,'<div class="prodid-question">
  <p>
    <b>Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A vector quantity describing the push or pull on an object. The SI unit for
						force is the newton (N).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 940,'MCAT939',6,NULL,'<div class="prodid-question">
  <p>
    <b>Frequency</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Number of cycles per second measured in units of Hz, where 1 Hz = 1 cycle per
						second.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 941,'MCAT940',6,NULL,'<div class="prodid-question">
  <p>
    <b>Frictional Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>An antagonistic force that points parallel and opposite in direction to the
						direction of movement (or attempted movement) of an object. Related to a
						coefficient of friction and the normal force: Static friction : <img src="/assets/contents/MCAT940img1.png" /></b>
  </p>
  <p>
    <b>Kinetic friction :<img src="/assets/contents/MCAT940img2.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 942,'MCAT941',6,NULL,'<div class="prodid-question">
  <p>
    <b>Gamma Decay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nuclear reaction in which high-energy photons, also known as
							<i>γ</i>-particles, are emitted: <img src="/assets/contents/MCAT941img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 943,'MCAT942',6,NULL,'<div class="prodid-question">
  <p>
    <b>Gauge Pressure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The pressure above and beyond atmospheric pressure. When the pressure at the
						surface is atmospheric pressure, the gauge pressure is given by
							<i>ρ</i>g<i>z</i>, where <i>ρ</i> is the density of the
						fluid, g is acceleration due to gravity, and <i>z</i> is depth.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 944,'MCAT943',6,NULL,'<div class="prodid-question">
  <p>
    <b>Gravitational Potential Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy of an object due to its height above a given datum, calculated by
						the equation <i>U</i> = <i>m</i>g<i>h</i> and given in the SI unit of joules
						(J).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 945,'MCAT944',6,NULL,'<div class="prodid-question">
  <p>
    <b>Gravity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A ubiquitous attractive force existing between any two objects, with
						magnitude directly proportional to the product of the two masses observed
						and inversely proportional to the square of their distance from each other:
							<img src="/assets/contents/MCAT944img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 946,'MCAT945',6,NULL,'<div class="prodid-question">
  <p>
    <b>Half-Life</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of time it takes for one-half of a radioactive sample to decay,
						given by the equation <img src="/assets/contents/MCAT945img1.png" />, where
							<i>λ</i> is a decay constant.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 947,'MCAT946',6,NULL,'<div class="prodid-question">
  <p>
    <b>Heat of Transformation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The amount of heat required to change the phase of a substance, calculated by
						the equation <i>q</i> = <i>mL</i> , where <i>q</i> is heat, <i>m</i> is the
						mass of the substance, and <i>L</i> is the heat of transformation for that
						substance. The heat of transformation corresponding to the
						solid–liquid phase change is called the heat of fusion; that
						corresponding to liquid–gas is called the heat of vaporization.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 948,'MCAT947',6,NULL,'<div class="prodid-question">
  <p>
    <b>Heat Transfer</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The movement of thermal energy towards a state of thermodynamic equilibrium.
						Heat spontaneously transfers energy from the object with the higher
						temperature to the object with the lower temperature.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 949,'MCAT948',6,NULL,'<div class="prodid-question">
  <p>
    <b>Image</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The apparent location of an object perceived through a lens or mirror. An
						image produced at a point where light does not actually pass through or
						converge is called a virtual image. An image produced at a point where the
						light rays actually converge or pass through is called a real image.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 950,'MCAT949',6,NULL,'<div class="prodid-question">
  <p>
    <b>Insulator</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A material in which electrons cannot move freely.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 951,'MCAT950',6,NULL,'<div class="prodid-question">
  <p>
    <b>Index of Refraction</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Ratio of the speed of light in a vacuum to the speed of light though a
						medium, given by: n = <img src="/assets/contents/MCAT950img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 952,'MCAT951',6,NULL,'<div class="prodid-question">
  <p>
    <b>Interference</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>When superimposed waves are in phase, their amplitudes add (constructive
						interference). When superimposed light waves are out of phase, their
						amplitudes subtract (destructive interference).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 953,'MCAT952',6,NULL,'<div class="prodid-question">
  <p>
    <b>Inverse Relationship</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A relationship between variables such that an increase in one variable is
						associated with a decrease in the other: <i>AB</i> = constant.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 954,'MCAT953',6,NULL,'<div class="prodid-question">
  <p>
    <b>Kinetic Energy</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The energy of an object in motion, calculated by the equation <i>K</i> = <img src="/assets/contents/MCAT953img1.png" /><i>mv</i></b>
    <sup>
      <b>2</b>
    </sup>
    <b> and given in the SI unit of joules (J).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 955,'MCAT954',6,NULL,'<div class="prodid-question">
  <p>
    <b>Kirchhoff’s Junction Rule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the sum of currents directed into a node or junction point in a
						circuit equals the sum of the currents directed away from that point.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 956,'MCAT955',6,NULL,'<div class="prodid-question">
  <p>
    <b>Kirchhoff’s Loop Rule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the sum of the voltage sources in a circuit loop is equal to the
						sum of voltage drops along that loop.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 957,'MCAT956',6,NULL,'<div class="prodid-question">
  <p>
    <b>Laminar Flow</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The smoothest type of liquid flow through a tube wherein thin layers of
						liquid slide over one another. Occurs as long as the linear flow speed
						remains below a critical speed <i>v</i></b>
    <sub>
      <b>c</b>
    </sub>
    <b>. Laminar flow can be represented by roughly parallel
						streamlines—lines that trace the path of water particles as they flow
						in a tube without ever crossing each other.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 958,'MCAT957',6,NULL,'<div class="prodid-question">
  <p>
    <b>Law of Reflection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that when light waves strike a medium, the angle of incidence
							<i>θ</i></b>
    <sub>
      <b>i</b>
    </sub>
    <b> is equal to the angle of reflection <i>θ</i></b>
    <sub>
      <b>r</b>
    </sub>
    <b>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 959,'MCAT958',6,NULL,'<div class="prodid-question">
  <p>
    <b>Lens</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A transparent device with a curvature that causes light to bend (refract) as
						it passes through. May be converging or diverging. A lens with a thick
						center that converges light rays at a point where the image is formed is
						called a converging lens. A lens with a thin center that diverges light
						after refraction and always forms a virtual image is called a diverging
						lens.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 960,'MCAT959',6,NULL,'<div class="prodid-question">
  <p>
    <b>Logarithm</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A mathematical function that is the inverse of the exponentiation function.
						Logarithms with base ten are called common logarithms (log); logarithms with
						Euler’s number (<i>e</i> ≈ 2.72) are called natural logarithms
						(ln).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 961,'MCAT960',6,NULL,'<div class="prodid-question">
  <p>
    <b>Magnification</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A dimensionless value denoted by <i>m</i> given by the equation: <i>m</i> =
							<img src="/assets/contents/MCAT960img1.png" /> where <i>i</i> is image
						distance and <i>o</i> is object distance. A negative <i>m</i> denotes an
						inverted image, whereas a positive <i>m</i> denotes an upright image.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 962,'MCAT961',6,NULL,'<div class="prodid-question">
  <p>
    <b>Mass</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A scalar quantity used as a measure of an object’s inertia.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 963,'MCAT962',6,NULL,'<div class="prodid-question">
  <p>
    <b>Mass Defect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The difference between an atom’s atomic mass and the sum of the masses
						of its protons and neutrons.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 964,'MCAT963',6,NULL,'<div class="prodid-question">
  <p>
    <b>Newton’s First Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that if no net force acts on an object, its velocity is constant.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 965,'MCAT964',6,NULL,'<div class="prodid-question">
  <p>
    <b>Newton’s Second Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that an object will accelerate in proportion to the net force acting
						on it: F</b>
    <sub>
      <b>net</b>
    </sub>
    <b> = <i>m</i>a.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 966,'MCAT965',6,NULL,'<div class="prodid-question">
  <p>
    <b>Newton’s Third Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that if one object exerts a force on another, the other object exerts
						a force on the first that is equal in magnitude but opposite in direction;
						the law of “action and reaction.”</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 967,'MCAT966',6,NULL,'<div class="prodid-question">
  <p>
    <b>Nonconservative Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A force that dissipates mechanical energy from a system. As such, the energy
						dissipated depends on the path taken from initial to final position.
						Examples include friction, air resistance, and viscous drag.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 968,'MCAT967',6,NULL,'<div class="prodid-question">
  <p>
    <b>Normal Force</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Perpendicular component of the force caused when two surfaces push against
						each other, denoted by N .</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 969,'MCAT968',6,NULL,'<div class="prodid-question">
  <p>
    <b>Ohm’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Law stating that the voltage drop across a resistor is proportional to the
						current flowing through it, given by the equation <i>V</i> = <i>IR</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 970,'MCAT969',6,NULL,'<div class="prodid-question">
  <p>
    <b>Pascal’s Principle</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that when a pressure is applied to one point of an enclosed fluid,
						that pressure is transmitted in equal magnitude to all points within that
						fluid and to the walls of its container. This principle forms the basis of
						the hydraulic lift.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 971,'MCAT970',6,NULL,'<div class="prodid-question">
  <p>
    <b>Photoelectric Effect</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The phenomenon observed when light of a certain frequency causes a metal to
						emit electrons. The minimum amount of energy required to emit an electron
						from a certain metal is called the work function. This quantity, denoted by
							<i>W</i>, is used to calculate the residual kinetic energy of an
						electron emitted by a metal, given by <i>K</i> = <i>hf</i> –
						<i>W</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 972,'MCAT971',6,NULL,'<div class="prodid-question">
  <p>
    <b>Plane Mirror</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A mirror in which incident light rays remain parallel after reflection,
						always producing a virtual image that appears to be the same distance behind
						the mirror as the object is in front of the mirror.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 973,'MCAT972',6,NULL,'<div class="prodid-question">
  <p>
    <b>Plane-Polarized Light</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Light that has been passed through a polarizing filter, allowing only the
						transmission of waves containing electric field vectors parallel to the
						lines of the filter.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 974,'MCAT973',6,NULL,'<div class="prodid-question">
  <p>
    <b>Positron Decay</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A nuclear reaction in which a positron (<i>β</i></b>
    <sup>
      <b>+</b>
    </sup>
    <b> or <i>e</i></b>
    <sup>
      <b>+</b>
    </sup>
    <b>) is emitted: <img src="/assets/contents/MCAT973img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 975,'MCAT974',6,NULL,'<div class="prodid-question">
  <p>
    <b>Potential Difference</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The difference in electric potential between two points in an electric field,
						also called the voltage (Δ<i>V</i> ).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 976,'MCAT975',6,NULL,'<div class="prodid-question">
  <p>
    <b>Power</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The rate at which work is done, given by the equation <i>P</i> = <img src="/assets/contents/MCAT975img1.png" /> where <i>W</i> is work and
							<i>t</i> is time (in seconds). The SI unit for power is the watt
						(W).</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 977,'MCAT976',6,NULL,'<div class="prodid-question">
  <p>
    <b>Pressure</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The force per unit area: <img src="/assets/contents/MCAT976img1.png" /> May be
						provided as absolute (hydrostatic) pressure, which is the pressure below the
						surface of a fluid that depends on gravity and surface pressure, calculated
						by <i>P</i> = <i>P</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> + <i>ρ</i>g<i>z</i>, where <i>P</i> is the absolute pressure,
							<i>P</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> is the pressure at the surface, <i>ρ</i> is the density of the fluid,
						g is acceleration due to gravity, and <i>z</i> is depth.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 978,'MCAT977',6,NULL,'<div class="prodid-question">
  <p>
    <b>Radiation</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Form of heat transfer where the energy is carried by electromagnetic waves;
						the only form of heat transfer that can be carried out in a vacuum.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 979,'MCAT978',6,NULL,'<div class="prodid-question">
  <p>
    <b>Resistance</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The natural tendency of a conductor to block current flow to a certain extent
						resulting in loss of energy or potential. Resistance is equal to the ratio
						of the voltage applied to the resulting current.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 980,'MCAT979',6,NULL,'<div class="prodid-question">
  <p>
    <b>Resistivity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Intrinsic property of a conductor used to measure its resistance in the
						equation <i>R</i> = <img src="/assets/contents/MCAT979img1.png" /> where
							<i>R</i> is the resistance, <i>ρ</i> is the resistivity, <i>L</i>
						is the length of the conductor, and <i>A</i> is its cross-sectional
						area.</b>
  </p>
</div>','','2014-07-24 12:15:09.660793','2014-07-24 12:15:09.660793',0,'skipped');
INSERT INTO flashcards VALUES(1, 981,'MCAT980',6,NULL,'<div class="prodid-question">
  <p>
    <b>Right-Hand Rule</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A common method used to determine the direction of a vector created as the
						product of two vectors. The thumb points in the direction of the first
						vector, the fingers point in the direction of the second vector, and the
						palm points in the direction of the resultant.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 982,'MCAT981',6,NULL,'<div class="prodid-question">
  <p>
    <b>Rotational Equilibrium</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>State where the sum of the torques acting on a body is zero, giving it no net
						angular acceleration. An object may be in rotational equilibrium,
						translational equilibrium, or both simultaneously.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 983,'MCAT982',6,NULL,'<div class="prodid-question">
  <p>
    <b>Scalar</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A quantity that has magnitude but no direction.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 984,'MCAT983',6,NULL,'<div class="prodid-question">
  <p>
    <b>Second Law of Thermodynamics</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that for any process, the entropy of the universe either increases
						(for irreversible processes) or remains constant (for reversible
						processes).</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 985,'MCAT984',6,NULL,'<div class="prodid-question">
  <p>
    <b>Snell’s Law</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Equation describing the angle of refraction for a light ray passing from one
						medium to another, given by <i>n</i></b>
    <sub>
      <b>1</b>
    </sub>
    <b> sin <i>θ</i></b>
    <sub>
      <b>1</b>
    </sub>
    <b> = <i>n</i></b>
    <sub>
      <b>2</b>
    </sub>
    <b> sin θ</b>
    <sub>
      <b>2</b>
    </sub>
    <b> , where <i>n</i> represents the index of refraction in each medium.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 986,'MCAT985',6,NULL,'<div class="prodid-question">
  <p>
    <b>Sound Level</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The loudness of a sound, measured in decibels (dB) and denoted by
							<i>β</i>. Given by the equation <i>β</i> = 10 log <img src="/assets/contents/MCAT985img1.png" /> where <i>I</i> is the intensity
						of the sound and <i>I</i></b>
    <sub>
      <b>0</b>
    </sub>
    <b> is a reference intensity of 10</b>
    <sup>
      <b>–12</b>
    </sup>
    <b>
      <img src="/assets/contents/MCAT985img2.png" />
    </b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 987,'MCAT986',6,NULL,'<div class="prodid-question">
  <p>
    <b>Speed</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A scalar quantity describing the distance traveled divided by the time
						required to travel that distance.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 988,'MCAT987',6,NULL,'<div class="prodid-question">
  <p>
    <b>Speed of Light</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The speed of electromagnetic waves traveling through a vacuum, given by the
						equation c = <i>λf</i>, where c is a constant equal to 3.00 x 10</b>
    <sup>
      <b>8</b>
    </sup>
    <b>
      <img src="/assets/contents/MCAT987img1.png" />
    </b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 989,'MCAT988',6,NULL,'<div class="prodid-question">
  <p>
    <b>Spherical Mirror</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Spherical mirrors have the appearance of a curved surface that is either
						concave or convex. A converging mirror is a concave mirror with a positive
						focal length, while a diverging mirror is a convex mirror with a negative
						focal length. Diverging mirrors always produce virtual images.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 990,'MCAT989',6,NULL,'<div class="prodid-question">
  <p>
    <b>Torque</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A force creating rotation about an axis; measured as the lever arm (the
						distance between the fulcrum and the applied force) times the magnitude of
						the force times the sine of the angle between them: <i>τ</i> =
							<i>rF</i> sin <i>θ</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 991,'MCAT990',6,NULL,'<div class="prodid-question">
  <p>
    <b>Total Internal Reflection</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The condition in which the incident angle of light traveling from a medium
						with a high <i>n</i> to a medium with a low <i>n</i> is greater than the
						critical angle <i>θ</i></b>
    <sub>
      <b>c</b>
    </sub>
    <b>. This results in all of the light being reflected and none of it being
						refracted.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 992,'MCAT991',6,NULL,'<div class="prodid-question">
  <p>
    <b>Translational Equilibrium</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>State where the sum of the forces acting on an object is zero, giving it no
						net acceleration. An object may be in rotational equilibrium, translational
						equilibrium, or both simultaneously.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 993,'MCAT992',6,NULL,'<div class="prodid-question">
  <p>
    <b>Turbulent Flow</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>Type of liquid flow that occurs when the linear flow speed in a tube exceeds
						the critical speed <i>v</i></b>
    <sub>
      <b>c</b>
    </sub>
    <b>. The motion of the fluid that is not adjacent to the container walls is
						highly irregular, forming vortices and a high resistance.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 994,'MCAT993',6,NULL,'<div class="prodid-question">
  <p>
    <b>Vector</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A quantity that has both magnitude and direction.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 995,'MCAT994',6,NULL,'<div class="prodid-question">
  <p>
    <b>Velocity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A vector quantity describing an object’s displacement over the elapsed
						time, expressed as <img src="/assets/contents/MCAT994img1.png" /></b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 996,'MCAT995',6,NULL,'<div class="prodid-question">
  <p>
    <b>Viscosity</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The measure of internal friction in a fluid, often denoted by
						<i>η</i>. Viscosity is responsible for creating viscous drag, a
						nonconservative force analogous to air resistance.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 997,'MCAT996',6,NULL,'<div class="prodid-question">
  <p>
    <b>Wave Speed</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>The speed of a wave, which is related to its frequency and wavelength by the
						equation <i>v</i> = <i>fλ</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 998,'MCAT997',6,NULL,'<div class="prodid-question">
  <p>
    <b>Wavelength</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>A quantity equal to the distance between any two equivalent consecutive
						points along a wave, such as two consecutive crests (peaks) or two
						consecutive troughs (valleys); expressed as <i>λ</i>.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 999,'MCAT998',6,NULL,'<div class="prodid-question">
  <p>
    <b>Work</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>One of the two forms of energy transfer in which a force is applied to change
						the energy of a system. In mechanics, commonly calculated by the equation
							<i>W</i> = <i>Fd</i> cos <i>θ</i>, where <i>F</i> is the
						magnitude of the applied force, <i>d</i> is the magnitude of the
						displacement, and <i>θ</i> is the angle between these two
						vectors.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
INSERT INTO flashcards VALUES(1, 1000,'MCAT999',6,NULL,'<div class="prodid-question">
  <p>
    <b>Work–Energy Theorem</b>
  </p>
</div>','<div class="prodid-answer">
  <p>
    <b>States that the net work performed on an object is related to its change in
						energy. In most applications, the work–energy theorem is used to
						relate work and kinetic energy.</b>
  </p>
</div>','','2014-07-24 12:15:09.661793','2014-07-24 12:15:09.661793',0,'skipped');
