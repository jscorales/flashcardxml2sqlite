INSERT INTO categories VALUES(1,'vocabulary','Vocabulary','2014-06-18 10:15:16.704289','2014-06-18 10:15:16.711330');
INSERT INTO flashcards VALUES(0, 1,'1',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abase</b>
  </p>
  <p>verb</p>
  <p>(uh <u>bays</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to humble; to disgrace</b>
  </p>
  <p>My intention was not to <i>abase</i> the comedian.</p>
  <p />
  <p>
    <i>Synonyms: demean; humiliate</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 2,'2',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>bayt</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to reduce in amount, degree, or severity</b>
  </p>
  <p>As the hurricane’s force <i>abated</i>, the winds dropped and the sea
                    became calm.</p>
  <p />
  <p>
    <i>Synonyms: ebb; lapse; let up; moderate; relent; slacken; subside; wane</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 3,'3',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abdicate</b>
  </p>
  <p>verb</p>
  <p>(<u>aab</u> duh kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to give up a position, right, or power</b>
  </p>
  <p>With the angry mob clamoring outside the palace, the king <i>abdicated</i> his
                    throne and fled.</p>
  <p />
  <p>
    <i>Synonyms: cede; quit; relinquish; resign; yield</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 4,'4',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aberrant</b>
  </p>
  <p>adj</p>
  <p>(uh <u>ber</u> unt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deviating from what is normal or expected</b>
  </p>
  <p>Since he had been a steady, cheerful worker for many years, his fellow postal
                    workers did not expect his <i>aberrant</i> burst of rage.</p>
  <p />
  <p>
    <i>Synonyms: abnormal; anomalous; deviant; divergent; errant; irregular</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 5,'5',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abeyance</b>
  </p>
  <p>noun</p>
  <p>(uh <u>bay</u> uhns)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>temporary suppression or suspension</b>
  </p>
  <p>The baseball game was held in <i>abeyance</i> while it continued to rain.</p>
  <p />
  <p>
    <i>Synonyms: deferral; delay; dormancy; postponement; remission</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 6,'6',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abjure</b>
  </p>
  <p>verb</p>
  <p>(aab <u>jur</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to reject; to abandon formally</b>
  </p>
  <p>The spy <i>abjured</i> his allegiance to the United States when he defected to
                    Russia.</p>
  <p />
  <p>
    <i>Synonyms: forswear; recall; recant; retract; take back</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 7,'7',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abscond</b>
  </p>
  <p>verb</p>
  <p>(aab <u>skahnd</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to leave secretly</b>
  </p>
  <p>The patron <i>absconded</i> from the restaurant without paying his bill by
                    sneaking out the back door.</p>
  <p />
  <p>
    <i>Synonyms: decamp; escape; flee</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 8,'8',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abstain</b>
  </p>
  <p>verb</p>
  <p>(aab <u>stayn</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to choose not to do something</b>
  </p>
  <p>Before the medical procedure, you must <i>abstain</i> from eating.</p>
  <p />
  <p>
    <i>Synonyms: forbear; refrain; withhold</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 9,'9',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abstemious</b>
  </p>
  <p>adj</p>
  <p>(aab <u>stee</u> me uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>moderate in appetite</b>
  </p>
  <p>Because Alyce is a vegetarian, she was able to eat only an <i>abstemious</i> meal
                    at the Texas Steakhouse.</p>
  <p />
  <p>
    <i>Synonyms: abstinent; continent; self-restraining; sober; temperate</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 10,'10',1,NULL,'<div class="prodid-question">
  <p>
    <b>Abyss</b>
  </p>
  <p>noun</p>
  <p>(uh <u>bihs</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an extremely deep hole</b>
  </p>
  <p>The submarine dove into the <i>abyss</i> to chart the previously unseen
                    depths.</p>
  <p />
  <p>
    <i>Synonyms: chasm; void</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 11,'11',1,NULL,'<div class="prodid-question">
  <p>
    <b>Accretion</b>
  </p>
  <p>noun</p>
  <p>(uh <u>kree</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a gradual growth in size; an increase in amount</b>
  </p>
  <p>The committee’s strong fund-raising efforts resulted in an
                        <i>accretion</i> in scholarship money.</p>
  <p />
  <p>
    <i>Synonyms: accumulation; buildup</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 12,'12',1,NULL,'<div class="prodid-question">
  <p>
    <b>Acidulous</b>
  </p>
  <p>adj</p>
  <p>(uh <u>si</u> juh luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sour in taste or manner</b>
  </p>
  <p>The <i>acidulous</i> taste of the spoiled milk made the young boy’s lips
                    pucker.</p>
  <p />
  <p>
    <i>Synonyms: acerbic; acetous; biting; piquant; pungent; tart</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 13,'13',1,NULL,'<div class="prodid-question">
  <p>
    <b>Acme</b>
  </p>
  <p>noun</p>
  <p>(<u>aak</u> mee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the highest point; the summit; the highest level or degree attainable</b>
  </p>
  <p>Just when he reached the <i>acme</i> of his power, the dictator was
                    overthrown.</p>
  <p />
  <p>
    <i>Synonyms: apex; peak; summit</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 14,'14',1,NULL,'<div class="prodid-question">
  <p>
    <b>Adulterate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>duhl</u> tuhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make impure</b>
  </p>
  <p>The restaurateur made his ketchup last longer by <i>adulterating</i> it with
                    water.</p>
  <p />
  <p>
    <i>Synonyms: debase; doctor; load</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 15,'15',1,NULL,'<div class="prodid-question">
  <p>
    <b>Advocate</b>
  </p>
  <p>verb</p>
  <p>(<u>aad</u> vuh kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to speak in favor of</b>
  </p>
  <p>The vegetarian <i>advocated</i> a diet containing no meat.</p>
  <p />
  <p>
    <i>Synonyms: back; champion; support</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 16,'16',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aerie</b>
  </p>
  <p>noun</p>
  <p>(<u>ayr</u> ee) (<u>eer</u> ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a nest built high in the air; an elevated, often secluded, dwelling</b>
  </p>
  <p>Perched high among the trees, the eagle’s <i>aerie</i> was filled with
                    eggs.</p>
  <p />
  <p>
    <i>Synonyms: perch; stronghold</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 17,'17',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aesthetic</b>
  </p>
  <p>adj</p>
  <p>(ehs <u>theh</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>concerning the appreciation of beauty</b>
  </p>
  <p>The <i>aesthetic</i> movement regarded the pursuit of beauty as the only true
                    purpose of art.</p>
  <p />
  <p>
    <i>Synonyms: artistic; tasteful</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 18,'18',1,NULL,'<div class="prodid-question">
  <p>
    <b>Affected</b>
  </p>
  <p>adj</p>
  <p>(uh <u>fehk</u> tihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>phony; artificial</b>
  </p>
  <p>The <i>affected</i> hairdresser spouted French phrases, though she had never been
                    to France.</p>
  <p />
  <p>
    <i>Synonyms: insincere; pretentious; put-on</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 19,'19',1,NULL,'<div class="prodid-question">
  <p>
    <b>Aggrandize</b>
  </p>
  <p>verb</p>
  <p>(uh <u>graan</u> diez) (<u>aa</u> gruhn diez)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to increase in power, influence, and reputation</b>
  </p>
  <p>The supervisor sought to <i>aggrandize</i> himself by claiming that the
                    achievements of his staff were actually his own.</p>
  <p />
  <p>
    <i>Synonyms: amplify; apotheosize; augment; dignify; elevate; enlarge; ennoble;
                        exalt; glorify; magnify; swell; uplift</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 20,'20',1,NULL,'<div class="prodid-question">
  <p>
    <b>Alacrity</b>
  </p>
  <p>noun</p>
  <p>(uh <u>laak</u> crih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>speed or quickness</b>
  </p>
  <p>The restaurant won a reputation for fine service since the wait staff responded
                    to their clients’ requests with <i>alacrity</i>.</p>
  <p />
  <p>
    <i>Synonyms: celerity; dispatch; haste; swiftness</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 21,'21',1,NULL,'<div class="prodid-question">
  <p>
    <b>Alleviate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>lee</u> vee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make more bearable</b>
  </p>
  <p>Taking aspirin helps to <i>alleviate</i> a headache.</p>
  <p />
  <p>
    <i>Synonyms: allay; assuage; comfort; ease; lessen; lighten; mitigate; palliate;
                        relieve</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 22,'22',1,NULL,'<div class="prodid-question">
  <p>
    <b>Amalgamate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>maal</u> guh mayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to combine; to mix together</b>
  </p>
  <p>Giant Industries <i>amalgamated</i> with Mega Products to form Giant-Mega
                    Products Incorporated.</p>
  <p />
  <p>
    <i>Synonyms: admix; blend; coalesce; combine; commingle; commix; compound; fuse;
                        intermingle; intermix; merge; mingle; mix; unite</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 23,'23',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ambiguous</b>
  </p>
  <p>adj</p>
  <p>(aam <u>bihg</u> yoo uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>doubtful or uncertain; can be interpreted several ways</b>
  </p>
  <p>The directions he gave were so <i>ambiguous</i> that we disagreed on which way to
                    turn.</p>
  <p />
  <p>
    <i>Synonyms: cloudy; doubtful; dubious; equivocal; indeterminate; nebulous;
                        obscure; unclear; vague</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 24,'24',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ameliorate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>meel</u> yuhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make better; to improve</b>
  </p>
  <p>The doctor was able to <i>ameliorate</i> the patient’s suffering using
                    painkillers.</p>
  <p />
  <p>
    <i>Synonyms: amend; better; improve; pacify; upgrade</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 25,'25',1,NULL,'<div class="prodid-question">
  <p>
    <b>Amortize</b>
  </p>
  <p>verb</p>
  <p>(uh <u>mohr</u> tiez) (<u>aam</u> er tiez)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to diminish by installment payments</b>
  </p>
  <p>While college students are notorious for accumulating credit card debt, they are
                    not as well known for <i>amortizing</i> it.</p>
  <p />
  <p>
    <i>Synonyms: decrease; reduce</i>
  </p>
</div>','','2014-06-18 10:15:16.711330','2014-06-18 10:15:16.711330',1,'skipped');
INSERT INTO flashcards VALUES(0, 26,'26',1,NULL,'<div class="prodid-question">
  <p>
    <b>Amulet</b>
  </p>
  <p>noun</p>
  <p>(<u>aam</u> yoo liht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an ornament worn as a charm against evil spirits</b>
  </p>
  <p>Though she claimed it was not because of superstition, Vivian always wore an
                        <i>amulet</i> around her neck.</p>
  <p />
  <p>
    <i>Synonyms: fetish; talisman</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 27,'27',1,NULL,'<div class="prodid-question">
  <p>
    <b>Anachronism</b>
  </p>
  <p>noun</p>
  <p>(uh <u>naak</u> ruh nih suhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>something out of place in time</b>
  </p>
  <p>The play was set in the nineteenth century, but was ruined by
                    <i>anachronisms</i>, like the lead actor’s digital watch.</p>
  <p />
  <p>
    <i>Synonyms: archaism; incongruity</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 28,'28',1,NULL,'<div class="prodid-question">
  <p>
    <b>Analgesia</b>
  </p>
  <p>noun</p>
  <p>(aah nuhl <u>jee</u> zhuh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a lessening of pain without loss of consciousness</b>
  </p>
  <p>After having her appendix removed, Tatiana welcomed the <i>analgesia</i> that the
                    painkillers provided.</p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 29,'29',1,NULL,'<div class="prodid-question">
  <p>
    <b>Analogous</b>
  </p>
  <p>adj</p>
  <p>(uh <u>naal</u> uh guhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>similar or alike in some way; equivalent</b>
  </p>
  <p>His mother argued that not going to college was <i>analogous</i> to throwing his
                    life away.</p>
  <p />
  <p>
    <i>Synonyms: alike; comparable; corresponding; equivalent; homogeneous;
                        parallel; similar</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 30,'30',1,NULL,'<div class="prodid-question">
  <p>
    <b>Anodyne</b>
  </p>
  <p>noun</p>
  <p>(<u>aah</u> nuh dien)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>something that calms or soothes pain</b>
  </p>
  <p>The massage was an <i>anodyne</i> that helped remove the knots from the
                    lawyer’s tense shoulders.</p>
  <p />
  <p>
    <i>Synonyms: narcotic; nepenthe; opiate</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 31,'31',1,NULL,'<div class="prodid-question">
  <p>
    <b>Anomaly</b>
  </p>
  <p>noun</p>
  <p>(uh <u>nahm</u> uh lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deviation from what is normal</b>
  </p>
  <p>Albino animals may display too great an <i>anomaly</i> in their coloring to
                    attract normally colored mates.</p>
  <p />
  <p>
    <i>Synonyms: aberrancy; aberration; abnormality; deviance; deviation;
                        irregularity; preternaturalness</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 32,'32',1,NULL,'<div class="prodid-question">
  <p>
    <b>Antagonize</b>
  </p>
  <p>verb</p>
  <p>(aan <u>taa</u> guh niez)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to annoy or provoke to anger</b>
  </p>
  <p>The child discovered that he could <i>antagonize</i> the cat by pulling its
                    tail.</p>
  <p />
  <p>
    <i>Synonyms: clash; conflict; incite; irritate; oppose; pester; provoke; vex</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 33,'33',1,NULL,'<div class="prodid-question">
  <p>
    <b>Antipathy</b>
  </p>
  <p>noun</p>
  <p>(aan <u>tih</u> puh thee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extreme dislike</b>
  </p>
  <p>The <i>antipathy</i> between fans of the rival soccer teams made the game even
                    more electrifying to watch.</p>
  <p />
  <p>
    <i>Synonyms: abhorrence; animosity; animus; antagonism; aversion; dislike;
                        enmity; hatred; hostility; loathing; repellence; repugnance; repulsion;
                        revulsion</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 34,'34',1,NULL,'<div class="prodid-question">
  <p>
    <b>Apathy</b>
  </p>
  <p>noun</p>
  <p>(<u>aa</u> pah thee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lack of interest or emotion</b>
  </p>
  <p>The <i>apathy</i> of voters is so great that less than half the people who are
                    eligible to vote actually bother to do so.</p>
  <p />
  <p>
    <i>Synonyms: coolness; disinterest; disregard; impassivity; indifference;
                        insensibility; lassitude; lethargy; listlessness; phlegm; stolidity;
                        unconcern; unresponsiveness</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 35,'35',1,NULL,'<div class="prodid-question">
  <p>
    <b>Apocryphal</b>
  </p>
  <p>adj</p>
  <p>(uh <u>pahk</u> ruh fuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>of questionable authority or authenticity</b>
  </p>
  <p>There is no hard or authoritative evidence to support the <i>apocryphal</i> tales
                    that link the Roswell, New Mexico, incident to a downed UFO.</p>
  <p />
  <p>
    <i>Synonyms: disputed; doubtful; fictitious; fraudulent</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 36,'36',1,NULL,'<div class="prodid-question">
  <p>
    <b>Apostate</b>
  </p>
  <p>noun</p>
  <p>(uh <u>pahs</u> tayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>one who renounces a religious faith</b>
  </p>
  <p>So that he could divorce his wife, the king scoffed at the church doctrines and
                    declared himself an <i>apostate</i>.</p>
  <p />
  <p>
    <i>Synonyms: defector; deserter; traitor</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 37,'37',1,NULL,'<div class="prodid-question">
  <p>
    <b>Approbation</b>
  </p>
  <p>noun</p>
  <p>(aa pruh <u>bay</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>approval and praise</b>
  </p>
  <p>The <i>approbation</i> that Jerry Lewis received in France included a medal from
                    the Ministry of Culture.</p>
  <p />
  <p>
    <i>Synonyms: acclaim; adulation; applause; commendation; compliments;
                        exaltation; extolment; hail; kudos; praise</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 38,'38',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arbitrary</b>
  </p>
  <p>adj</p>
  <p>(<u>ahr</u> bih trayr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>determined by chance or impulse</b>
  </p>
  <p>When you lack the information to judge what to do next, you will be forced to
                    make an <i>arbitrary</i> decision.</p>
  <p />
  <p>
    <i>Synonyms: changeable; erratic; indiscriminate; random; wayward</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 39,'39',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arbitrate</b>
  </p>
  <p>verb</p>
  <p>(<u>ahr</u> bih trayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to judge a dispute between two opposing parties</b>
  </p>
  <p>Since the couple could not come to an agreement, a judge was forced to
                        <i>arbitrate</i> their divorce proceedings.</p>
  <p />
  <p>
    <i>Synonyms: adjudge; adjudicate; determine; intermediate; intervene; judge;
                        moderate; referee; rule</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 40,'40',1,NULL,'<div class="prodid-question">
  <p>
    <b>Archaic</b>
  </p>
  <p>adj</p>
  <p>(ahr <u>kay</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>ancient; old-fashioned</b>
  </p>
  <p>Her <i>archaic</i> Commodore computer could not run the latest software.</p>
  <p />
  <p>
    <i>Synonyms: ancient; antediluvian; antique; bygone; dated; dowdy; fusty;
                        obsolete; old-fashioned; outdated; outmoded; passé; prehistoric;
                        stale; superannuated; superseded; vintage</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 41,'41',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ardor</b>
  </p>
  <p>noun</p>
  <p>(<u>ahr</u> duhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>intense and passionate feeling</b>
  </p>
  <p>Bishop’s <i>ardor</i> for landscape was evident when he passionately
                    described the beauty of the scenic Hudson Valley.</p>
  <p />
  <p>
    <i>Synonyms: devotion; enthusiasm; fervency; fervidity; fervidness; fervor;
                        fire; passion; zeal; zealousness</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 42,'42',1,NULL,'<div class="prodid-question">
  <p>
    <b>Arrogate</b>
  </p>
  <p>verb</p>
  <p>(<u>aa</u> ruh gayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to claim without justification; to claim for oneself without right</b>
  </p>
  <p>Gretchen watched in astonishment as her boss <i>arrogated</i> the credit for her
                    brilliant work on the project.</p>
  <p />
  <p>
    <i>Synonyms: appropriate; presume; take</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 43,'43',1,NULL,'<div class="prodid-question">
  <p>
    <b>Articulate</b>
  </p>
  <p>adj</p>
  <p>(ahr <u>tih</u> kyuh luht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>able to speak clearly and expressively</b>
  </p>
  <p>She is extremely <i>articulate</i> when it comes to expressing her pro-labor
                    views; as a result, unions are among her strongest supporters.</p>
  <p />
  <p>
    <i>Synonyms: eloquent; expressive; fluent; lucid; silver-tongued;
                        smooth-spoken</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 44,'44',1,NULL,'<div class="prodid-question">
  <p>
    <b>Assail</b>
  </p>
  <p>verb</p>
  <p>(uh <u>sayl</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to attack; to assault</b>
  </p>
  <p>The foreign army will try to <i>assail</i> our bases, but they will not be
                    successful in their attack.</p>
  <p />
  <p>
    <i>Synonyms: beset; storm; strike</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 45,'45',1,NULL,'<div class="prodid-question">
  <p>
    <b>Assuage</b>
  </p>
  <p>verb</p>
  <p>(uh <u>swayj</u>) (uh <u>swayzh</u>) (uh <u>swahzh</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make something unpleasant less severe</b>
  </p>
  <p>Like many people, Philip Larkin used alcohol to <i>assuage</i> his sense of
                    meaninglessness and despair.</p>
  <p />
  <p>
    <i>Synonyms: allay; alleviate; appease; comfort; conciliate; ease; lighten;
                        mitigate; mollify; pacify; palliate; placate; propitiate; relieve; soothe;
                        sweeten</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 46,'46',1,NULL,'<div class="prodid-question">
  <p>
    <b>Attenuate</b>
  </p>
  <p>verb</p>
  <p>(uh <u>tehn</u> yoo ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to reduce in force or degree; to weaken</b>
  </p>
  <p>The Bill of Rights <i>attenuated</i> the traditional power of government to
                    change laws at will.</p>
  <p />
  <p>
    <i>Synonyms: debilitate; devitalize; dilute; enervate; enfeeble; rarefy; sap;
                        thin; undermine; undo; unnerve; weaken</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 47,'47',1,NULL,'<div class="prodid-question">
  <p>
    <b>Audacious</b>
  </p>
  <p>adj</p>
  <p>(aw <u>day</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fearless and daring</b>
  </p>
  <p>The <i>audacious</i> peasant dared to insult the king’s mother.</p>
  <p />
  <p>
    <i>Synonyms: adventuresome; aggressive; assertive; bold; brave; courageous;
                        daring; dauntless; doughty; fearless; gallant; game; heroic; intrepid;
                        mettlesome; plucky; stout; stouthearted; unafraid; undaunted; valiant;
                        valorous; venturesome; venturous</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 48,'48',1,NULL,'<div class="prodid-question">
  <p>
    <b>Augury</b>
  </p>
  <p>noun</p>
  <p>(<u>aw</u> gyuh ree) (<u>aw</u> guh ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a prophecy; a prediction of events</b>
  </p>
  <p>Troy hoped the rainbow was an <i>augury</i> of good things to come.</p>
  <p />
  <p>
    <i>Synonyms: auspice; harbinger; omen; portent; presage</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 49,'49',1,NULL,'<div class="prodid-question">
  <p>
    <b>August</b>
  </p>
  <p>adj</p>
  <p>(aw <u>guhst</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>dignified; grandiose</b>
  </p>
  <p>The <i>august </i>view of the Grand Teton summit took my breath away.</p>
  <p />
  <p>
    <i>Synonyms: admirable; awesome; grand; majestic</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(0, 50,'50',1,NULL,'<div class="prodid-question">
  <p>
    <b>Austere</b>
  </p>
  <p>adj</p>
  <p>(aw <u>steer</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>severe or stern in appearance; undecorated</b>
  </p>
  <p>The lack of decoration makes Zen temples seem <i>austere</i> to the untrained
                    eye.</p>
  <p />
  <p>
    <i>Synonyms: bleak; dour; grim; hard; harsh; severe</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',1,'skipped');
INSERT INTO flashcards VALUES(1, 51,'51',1,NULL,'<div class="prodid-question">
  <p>
    <b>Axiom</b>
  </p>
  <p>noun</p>
  <p>(<u>aak</u> see uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>premise; postulate; self-evident truth</b>
  </p>
  <p>Halle lived her life based on the <i>axioms</i> her grandmother had passed on to
                    her.</p>
  <p />
  <p>
    <i>Synonyms: adage; aphorism; apothegm; maxim; rule</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 52,'52',1,NULL,'<div class="prodid-question">
  <p>
    <b>Banal</b>
  </p>
  <p>adj</p>
  <p>(buh <u>naal</u>) (<u>bay</u> nuhl) (buh <u>nahl</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>predictable; clichéd; boring</b>
  </p>
  <p>His conversation consisted of <i>banal </i>phrases like “Have a nice
                    day” or “Another day, another dollar.”</p>
  <p />
  <p>
    <i>Synonyms: bland; bromidic; clichéd; commonplace; fatuous; hackneyed;
                        innocuous; insipid; jejune; musty; platitudinous; prosaic; quotidian;
                        shopworn; stale; stereotypic; threadbare; timeworn; tired; trite; vapid;
                        worn-out</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 53,'53',1,NULL,'<div class="prodid-question">
  <p>
    <b>Belfry</b>
  </p>
  <p>noun</p>
  <p>(<u>behl</u> free)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a bell tower; the room in which a bell is hung</b>
  </p>
  <p>The town was shocked when a bag of money was found stashed in the old
                        <i>belfry</i> of the church.</p>
  <p />
  <p>
    <i>Synonyms: spire; steeple</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 54,'54',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bevy</b>
  </p>
  <p>noun</p>
  <p>(<u>beh</u> vee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a group</b>
  </p>
  <p>As predicted, a <i>bevy</i> of teenagers surrounded the rock star’s
                    limousine.</p>
  <p />
  <p>
    <i>Synonyms: band; bunch; gang; pack; troop</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 55,'55',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bifurcate</b>
  </p>
  <p>verb</p>
  <p>(<u>bi</u> fuhr kayt) (bi <u>fuhr</u> kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to divide into two parts</b>
  </p>
  <p>The large corporation just released a press statement announcing its plans to
                        <i>bifurcate</i>.</p>
  <p>
    <i>Synonym: bisect; split</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 56,'56',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bilk</b>
  </p>
  <p>verb</p>
  <p>(bihlk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to cheat; to defraud</b>
  </p>
  <p>When the greedy salesman realized that his customer spoke poor French, he
                        <i>bilked</i> the tourist out of 20 euros.</p>
  <p />
  <p>
    <i>Synonyms: beat; defraud; overreach</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 57,'57',1,NULL,'<div class="prodid-question">
  <p>
    <b>Blight</b>
  </p>
  <p>verb</p>
  <p>(bliet)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to afflict; to destroy</b>
  </p>
  <p>The farmers feared that the night’s frost would <i>blight</i> the potato
                    crops entirely.</p>
  <p />
  <p>
    <i>Synonyms: damage; plague</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 58,'58',1,NULL,'<div class="prodid-question">
  <p>
    <b>Blithe</b>
  </p>
  <p>adj</p>
  <p>(blieth)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>joyful, cheerful, or without appropriate thought</b>
  </p>
  <p>Summer finally came, and the <i>blithe</i> students spent their days at the
                    beach.</p>
  <p />
  <p>
    <i>Synonyms: carefree; lighthearted; merry</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 59,'59',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bolster</b>
  </p>
  <p>verb</p>
  <p>(<u>bohl</u> stuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to support; to prop up</b>
  </p>
  <p>The presence of giant footprints <i>bolstered</i> the argument that Bigfoot was
                    in the area.</p>
  <p />
  <p>
    <i>Synonyms: brace; buttress; crutch; prop; stay; support; sustain; underpin;
                        uphold</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 60,'60',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bombastic</b>
  </p>
  <p>adj</p>
  <p>(bahm <u>baast</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>pompous in speech and manner</b>
  </p>
  <p>Mussolini’s speeches were mostly <i>bombastic</i>; his boasting and
                    outrageous claims had no basis in fact.</p>
  <p />
  <p>
    <i>Synonyms: bloated; declamatory; fustian; grandiloquent; grandiose;
                        high-flown; magniloquent; orotund; pretentious; rhetorical;
                        self-important</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 61,'61',1,NULL,'<div class="prodid-question">
  <p>
    <b>Bonhomie</b>
  </p>
  <p>noun</p>
  <p>(bah nuh <u>mee</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>good-natured geniality; an atmosphere of good cheer</b>
  </p>
  <p>The aspects of her job that Dana loved the most were the flexible hours and the
                    pleasant <i>bonhomie</i> in the office.</p>
  <p />
  <p>
    <i>Synonyms: affability; amiability; cordiality; geniality</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 62,'62',1,NULL,'<div class="prodid-question">
  <p>
    <b>Boor</b>
  </p>
  <p>noun</p>
  <p>(bohr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a crude person; one lacking manners or taste</b>
  </p>
  <p>“That utter <i>boor</i> ruined my recital with his constant
                    guffawing!” wailed the pianist.</p>
  <p />
  <p>
    <i>Synonyms: clod; lout; oaf; vulgarian; yahoo</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 63,'63',1,NULL,'<div class="prodid-question">
  <p>
    <b>Burgeon</b>
  </p>
  <p>verb</p>
  <p>(<u>buhr</u> juhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to grow and flourish</b>
  </p>
  <p>Faulkner neither confirmed nor denied stories about himself, allowing rumor to
                        <i>burgeon</i> where it would.</p>
  <p />
  <p>
    <i>Synonyms: bloom; flourish; prosper; thrive</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 64,'64',1,NULL,'<div class="prodid-question">
  <p>
    <b>Burnish</b>
  </p>
  <p>verb</p>
  <p>(<u>buhr</u> nihsh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to polish</b>
  </p>
  <p>He <i>burnished</i> the silver coffee pot until it shone brightly.</p>
  <p />
  <p>
    <i>Synonyms: buff; luster; polish; scour</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 65,'65',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cabal</b>
  </p>
  <p>noun</p>
  <p>(kuh <u>bahl</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a secret group seeking to overturn something</b>
  </p>
  <p>The boys on the street formed a <i>cabal</i> to keep girls out of their tree
                    house.</p>
  <p />
  <p>
    <i>Synonyms: camp; circle; clan; clique; coterie; in-group; mafia; mob; ring</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 66,'66',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cacophony</b>
  </p>
  <p>noun</p>
  <p>(kuh <u>kah</u> fuh nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a harsh, jarring noise</b>
  </p>
  <p>The junior high orchestra created an almost unbearable <i>cacophony</i> as they
                    tried to tune their instruments.</p>
  <p />
  <p>
    <i>Synonyms: chaos; clamor; din; discord; disharmony; noise</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 67,'67',1,NULL,'<div class="prodid-question">
  <p>
    <b>Calumny</b>
  </p>
  <p>noun</p>
  <p>(<u>kaa</u> luhm nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a false and malicious accusation; misrepresentation</b>
  </p>
  <p>The unscrupulous politician used <i>calumny</i> to bring down his opponent in the
                    senatorial race.</p>
  <p />
  <p>
    <i>Synonyms: defamation; libel; slander</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 68,'68',1,NULL,'<div class="prodid-question">
  <p>
    <b>Canard</b>
  </p>
  <p>noun</p>
  <p>(kuh <u>nard</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a lie</b>
  </p>
  <p>That tabloid’s feature story about a goat giving birth to a human child
                    was clearly a <i>canard</i>.</p>
  <p />
  <p>
    <i>Synonyms: falsehood; falsity; fib; misrepresentation; prevarication; tale;
                        untruth</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 69,'69',1,NULL,'<div class="prodid-question">
  <p>
    <b>Candid</b>
  </p>
  <p>adj</p>
  <p>(<u>kaan</u> did)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impartial and honest in speech</b>
  </p>
  <p>The observations of a child can be charming since they are <i>candid</i> and
                    unpretentious.</p>
  <p />
  <p>
    <i>Synonyms: direct; forthright; frank; honest; open; sincere; straight;
                        straightforward; undisguised</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 70,'70',1,NULL,'<div class="prodid-question">
  <p>
    <b>Capricious</b>
  </p>
  <p>adj</p>
  <p>(kuh <u>pree</u> shuhs) (kuh <u>prih</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>changing one’s mind quickly and often</b>
  </p>
  <p>Queen Elizabeth I was quite <i>capricious</i>; her courtiers could never be sure
                    which one would catch her fancy.</p>
  <p />
  <p>
    <i>Synonyms: arbitrary; chance; changeable; erratic; fickle; inconstant;
                        mercurial; random; whimsical; willful</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 71,'71',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cartography</b>
  </p>
  <p>noun</p>
  <p>(kahr <u>tahg</u> ruh fee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the science or art of making maps</b>
  </p>
  <p>Gail’s interest in <i>cartography</i> may stem from the extensive
                    traveling she did as a child.</p>
  <p />
  <p>
    <i>Synonyms: charting; surveying; topography</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 72,'72',1,NULL,'<div class="prodid-question">
  <p>
    <b>Castigate</b>
  </p>
  <p>verb</p>
  <p>(<u>kaa</u> stih gayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to punish or criticize harshly</b>
  </p>
  <p>Martina <i>castigated</i> her boyfriend for not remembering her birthday.</p>
  <p />
  <p>
    <i>Synonyms: admonish; chastise; chide; rebuke; reprimand; reproach; reprove;
                        scold; tax; upbraid</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 73,'73',1,NULL,'<div class="prodid-question">
  <p>
    <b>Catalyst</b>
  </p>
  <p>noun</p>
  <p>(<u>kaa</u> tuh lihst)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>something that brings about a change in something else</b>
  </p>
  <p>The imposition of harsh taxes was the <i>catalyst</i> that finally brought on the
                    revolution.</p>
  <p />
  <p>
    <i>Synonyms: accelerator; goad; impetus; impulse; incentive; motivation; spur;
                        stimulant</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 74,'74',1,NULL,'<div class="prodid-question">
  <p>
    <b>Catholic</b>
  </p>
  <p>adj</p>
  <p>(<u>kaa</u> thuh lihk) (<u>kaa</u> thlihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>universal; broad and comprehensive</b>
  </p>
  <p>His <i>catholic</i> musical tastes included everything from opera to rap.</p>
  <p />
  <p>
    <i>Synonyms: extensive; general</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 75,'75',1,NULL,'<div class="prodid-question">
  <p>
    <b>Caustic</b>
  </p>
  <p>adj</p>
  <p>(<u>kaw</u> stihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>biting in wit</b>
  </p>
  <p>Dorothy Parker gained her <i>caustic</i> reputation from her cutting, yet witty,
                    insults.</p>
  <p />
  <p>
    <i>Synonyms: acerbic; biting; mordant; trenchant</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 76,'76',1,NULL,'<div class="prodid-question">
  <p>
    <b>Chaos</b>
  </p>
  <p>noun</p>
  <p>(<u>kay</u> ahs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>great disorder; a confused situation</b>
  </p>
  <p>In most religious traditions, God created an ordered universe from
                    <i>chaos</i>.</p>
  <p />
  <p>
    <i>Synonyms: clutter; confusion; disarrangement; disarray; disorder;
                        disorderliness; disorganization; jumble; mess; muddle; scramble; snarl; topsy-turviness; turmoil</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 77,'77',1,NULL,'<div class="prodid-question">
  <p>
    <b>Chauvinist</b>
  </p>
  <p>noun</p>
  <p>(<u>shoh</u> vuh nist)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>someone prejudiced in favor of a group to which he or she belongs</b>
  </p>
  <p>The attitude that men must be obeyed since they are inherently superior to women
                    is common among male <i>chauvinists</i>.</p>
  <p />
  <p>
    <i>Synonyms: bigot; jingoist; partisan</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 78,'78',1,NULL,'<div class="prodid-question">
  <p>
    <b>Chicanery</b>
  </p>
  <p>noun</p>
  <p>(shih <u>kayn</u> ree) (shi <u>kay</u> nuh ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deception by means of craft or guile</b>
  </p>
  <p>Dishonest used-car salesmen often use <i>chicanery</i> to sell their beat-up old
                    cars.</p>
  <p />
  <p>
    <i>Synonyms: artifice; conniving; craftiness; deception; deviousness;
                        misrepresentation; pettifoggery; shadiness; sneakiness; sophistry;
                        subterfuge; underhandedness</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 79,'79',1,NULL,'<div class="prodid-question">
  <p>
    <b>Circumspect</b>
  </p>
  <p>adj</p>
  <p>(<u>suhr</u> kuhm spehkt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>cautious; aware of potential consequences</b>
  </p>
  <p>She was very <i>circumspect</i> in her language and behavior when first
                    introduced to her fiancé’s parents.</p>
  <p />
  <p>
    <i>Synonyms: alert; cautious; heedful; mindful; prudent; solicitous; vigilant;
                        wary</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 80,'80',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cloying</b>
  </p>
  <p>adj</p>
  <p>(<u>kloy</u> ing)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sickly sweet; excessive</b>
  </p>
  <p>When Enid and Jay first started dating, their <i>cloying</i> affection toward one
                    another often made their friends ill.</p>
  <p />
  <p>
    <i>Synonyms: excessive; fulsome</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 81,'81',1,NULL,'<div class="prodid-question">
  <p>
    <b>Coalesce</b>
  </p>
  <p>verb</p>
  <p>(koh uh <u>lehs</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to grow together to form a single whole</b>
  </p>
  <p>The sun and planets eventually <i>coalesced</i> out of a vast cloud of gas and dust.</p>
  <p />
  <p>
    <i>Synonyms: amalgamate; blend; condense; consolidate; fuse; unite</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 82,'82',1,NULL,'<div class="prodid-question">
  <p>
    <b>Coffer</b>
  </p>
  <p>noun</p>
  <p>(<u>kah</u> fuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a strongbox; a large chest for money</b>
  </p>
  <p>The bulletproof glass of the <i>coffer</i> is what keeps the crown jewels
                    secure.</p>
  <p />
  <p>
    <i>Synonyms: chest; exchequer; treasury; war chest</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 83,'83',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cogent</b>
  </p>
  <p>adj</p>
  <p>(<u>koh</u> juhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>convincing and well-reasoned</b>
  </p>
  <p>Swayed by the <i>cogent</i> argument of the defense, the jury had no choice but to acquit the defendant.</p>
  <p>
    <i>Synonyms: convincing; persuasive; solid; sound; telling; valid</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 84,'84',1,NULL,'<div class="prodid-question">
  <p>
    <b>Collusion</b>
  </p>
  <p>noun</p>
  <p>(kuh <u>loo</u> zhuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a collaboration; complicity; conspiracy</b>
  </p>
  <p>It came to light that the police chief and the mafia had a <i>collusion</i> in
                    running the numbers racket.</p>
  <p />
  <p>
    <i>Synonyms: connivance; intrigue; machination</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 85,'85',1,NULL,'<div class="prodid-question">
  <p>
    <b>Condone</b>
  </p>
  <p>verb</p>
  <p>(kuhn <u>dohn</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to overlook, pardon, or disregard</b>
  </p>
  <p>Some theorists believe that failing to prosecute minor crimes is the same as
                        <i>condoning</i> an air of lawlessness.</p>
  <p />
  <p>
    <i>Synonyms: exculpate; excuse; pardon; remit</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 86,'86',1,NULL,'<div class="prodid-question">
  <p>
    <b>Connoisseur</b>
  </p>
  <p>noun</p>
  <p>(kah nuh <u>suhr</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a person with expert knowledge or discriminating tastes</b>
  </p>
  <p>Dr. Crane was a <i>connoisseur</i> of fine food and wine, drinking and eating
                    only the best.</p>
  <p />
  <p>
    <i>Synonyms: authority; epicure; expert; gastronome; gourmet</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 87,'87',1,NULL,'<div class="prodid-question">
  <p>
    <b>Contrite</b>
  </p>
  <p>adj</p>
  <p>(kuhn <u>triet</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deeply sorrowful and repentant for a wrong</b>
  </p>
  <p>After three residents were mugged in the lobby while the watchman was away from
                    his post, he felt very <i>contrite</i>.</p>
  <p />
  <p>
    <i>Synonyms: apologetic; regretful; remorseful</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 88,'88',1,NULL,'<div class="prodid-question">
  <p>
    <b>Contumacious</b>
  </p>
  <p>adj</p>
  <p>(kahn tuh <u>may</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>rebellious</b>
  </p>
  <p>The <i>contumacious</i> teenager ran away from home when her parents told her she
                    was grounded.</p>
  <p />
  <p>
    <i>Synonyms: factious; insubordinate; insurgent; mutinous; rebellious;
                        seditious</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 89,'89',1,NULL,'<div class="prodid-question">
  <p>
    <b>Convoluted</b>
  </p>
  <p>adj</p>
  <p>(kahn vuh <u>loo</u> tehd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>intricate and complicated</b>
  </p>
  <p>Although many people bought the professor’s book, few people could follow
                    its <i>convoluted</i> ideas and theories.</p>
  <p />
  <p>
    <i>Synonyms: Byzantine; complex; elaborate; intricate; knotty; labyrinthine;
                        perplexing; tangled</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 90,'90',1,NULL,'<div class="prodid-question">
  <p>
    <b>Corroborate</b>
  </p>
  <p>verb</p>
  <p>(kuh <u>rahb</u> uhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to support with evidence</b>
  </p>
  <p>All the DA needed were fingerprints to <i>corroborate</i> the witness’s
                    testimony that he saw the defendant in the victim’s apartment.</p>
  <p />
  <p>
    <i>Synonyms: authenticate; back; buttress; confirm; substantiate; validate;
                        verify</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 91,'91',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cosset</b>
  </p>
  <p>verb</p>
  <p>(<u>kah</u> suht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to pamper; to treat with great care</b>
  </p>
  <p>Marta just loves to <i>cosset</i> her first and only grandchild.</p>
  <p />
  <p>
    <i>Synonyms: cater to; cuddle; dandle; fondle; love; pamper; pet; spoil</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 92,'92',1,NULL,'<div class="prodid-question">
  <p>
    <b>Coterie</b>
  </p>
  <p>noun</p>
  <p>(<u>koh</u> tuh ree) (koh tuh <u>ree</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an intimate group of persons with a similar purpose</b>
  </p>
  <p>Angel invited a <i>coterie</i> of fellow stamp enthusiasts to a stamp-trading
                    party.</p>
  <p />
  <p>
    <i>Synonyms: clique; set</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 93,'93',1,NULL,'<div class="prodid-question">
  <p>
    <b>Craven</b>
  </p>
  <p>adj</p>
  <p>(<u>kray</u> vuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking courage</b>
  </p>
  <p>The <i>craven</i> lion cringed in the corner of his cage, terrified of the
                    mouse.</p>
  <p />
  <p>
    <i>Synonyms: fainthearted; spineless; timid</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 94,'94',1,NULL,'<div class="prodid-question">
  <p>
    <b>Credulous</b>
  </p>
  <p>adj</p>
  <p>(<u>kreh</u> juh luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>too trusting; gullible</b>
  </p>
  <p>Although some four-year-olds believe in the Tooth Fairy, only the most <i>credulous</i> nine-year-olds also believe in her.</p>
  <p />
  <p>
    <i>Synonyms: naïve; susceptible; trusting</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 95,'95',1,NULL,'<div class="prodid-question">
  <p>
    <b>Crescendo</b>
  </p>
  <p>noun</p>
  <p>(kruh <u>shehn</u> doh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>steady increase in volume or force</b>
  </p>
  <p>The <i>crescendo</i> of tension became unbearable as Evel Knievel prepared to
                    jump his motorcycle over the school buses.</p>
  <p />
  <p>
    <i>Synonyms: acme; capstone; climax; crest; culmination; meridian; peak</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 96,'96',1,NULL,'<div class="prodid-question">
  <p>
    <b>Cupidity</b>
  </p>
  <p>noun</p>
  <p>(kyoo <u>pih</u> dih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>greed; strong desire</b>
  </p>
  <p>The thief stared at the shining jewels with <i>cupidity</i> in his gleaming
                    eyes.</p>
  <p />
  <p>
    <i>Synonyms: avarice; covetousness; rapacity</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 97,'97',1,NULL,'<div class="prodid-question">
  <p>
    <b>Curmudgeon</b>
  </p>
  <p>noun</p>
  <p>(kuhr <u>muh</u> juhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a cranky person, usually an old one</b>
  </p>
  <p>Ernesto was a notorious <i>curmudgeon</i> who snapped at anyone who disturbed him
                    for any reason.</p>
  <p />
  <p>
    <i>Synonyms: coot; crab; grouch</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 98,'98',1,NULL,'<div class="prodid-question">
  <p>
    <b>Debutante</b>
  </p>
  <p>noun</p>
  <p>(<u>dehb</u> yoo tahnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a young woman making debut in high society</b>
  </p>
  <p>The <i>debutante</i> spent hours dressing for her very first ball, hoping to
                    catch the eye of an eligible bachelor.</p>
  <p />
  <p>
    <i>Synonyms: lady; maiden</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 99,'99',1,NULL,'<div class="prodid-question">
  <p>
    <b>Declivity</b>
  </p>
  <p>noun</p>
  <p>(dih <u>klih</u> vih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a downward slope</b>
  </p>
  <p>Because the village was situated on the <i>declivity</i> of a hill, it never
                    flooded.</p>
  <p />
  <p>
    <i>Synonyms: decline; descent; grade; slant; tilt</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 100,'100',1,NULL,'<div class="prodid-question">
  <p>
    <b>Decorous</b>
  </p>
  <p>adj</p>
  <p>(<u>deh</u> kuhr uhs) (deh <u>kohr</u> uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>proper; tasteful; socially correct</b>
  </p>
  <p>The countess trained her daughters in the finer points of <i>decorous</i>
                    behavior, hoping they would make a good impression when she presented them at
                    Court.</p>
  <p />
  <p>
    <i>Synonyms: appropriate; comme il faut; courteous; polite</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 101,'101',1,NULL,'<div class="prodid-question">
  <p>
    <b>Decorum</b>
  </p>
  <p>noun</p>
  <p>(deh <u>kohr</u> uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>appropriateness of behavior or conduct; propriety</b>
  </p>
  <p>The countess complained that the vulgar peasants lacked the <i>decorum</i>
                    appropriate for a visit to the palace.</p>
  <p />
  <p>
    <i>Synonyms: correctness; decency; etiquette; manners; mores; propriety;
                        seemliness</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 102,'102',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deface</b>
  </p>
  <p>verb</p>
  <p>(dih <u>fays</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to mar the appearance of; to vandalize</b>
  </p>
  <p>After the wall was torn down, the students began to <i>deface</i> the statues of
                    Communist leaders of the former Eastern Bloc.</p>
  <p />
  <p>
    <i>Synonyms: disfigure; impair; spoil</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 103,'103',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deference</b>
  </p>
  <p>noun</p>
  <p>(<u>deh</u> fuh ruhn(t)s) (<u>def</u> ruhn(t)s)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>respect; courtesy</b>
  </p>
  <p>The respectful young law clerk treated the Supreme Court justice with the utmost
                        <i>deference</i>.</p>
  <p />
  <p>
    <i>Synonyms: courtesy; homage; honor; obeisance; respect; reverence;
                        veneration</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 104,'104',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deleterious</b>
  </p>
  <p>adj</p>
  <p>(dehl ih <u>teer</u> ee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>subtly or unexpectedly harmful</b>
  </p>
  <p>If only we had known the clocks were defective before putting them on the market,
                    it wouldn’t have been quite so <i>deleterious</i> to our reputation.</p>
  <p />
  <p>
    <i>Synonyms: adverse; hurtful; inimical; injurious</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 105,'105',1,NULL,'<div class="prodid-question">
  <p>
    <b>Demagogue</b>
  </p>
  <p>noun</p>
  <p>(<u>deh</u> muh gahg) (<u>deh</u> muh gawg)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a leader or rabble-rouser, usually one appealing to emotion or prejudice</b>
  </p>
  <p>He began his career as a <i>demagogue</i>, giving fiery speeches at political
                    rallies.</p>
  <p />
  <p>
    <i>Synonyms: agitator; inciter; instigator</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 106,'106',1,NULL,'<div class="prodid-question">
  <p>
    <b>Demur</b>
  </p>
  <p>verb</p>
  <p>(dih <u>muhr</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to express doubts or objections</b>
  </p>
  <p>When scientific authorities claimed that all the planets revolved around the
                    Earth, Galileo, with his superior understanding of the situation, was forced to
                        <i>demur</i>.</p>
  <p />
  <p>
    <i>Synonyms: dissent; expostulate; protest; remonstrate</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 107,'107',1,NULL,'<div class="prodid-question">
  <p>
    <b>Deride</b>
  </p>
  <p>verb</p>
  <p>(dih <u>ried</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to speak of or treat with contempt; to mock</b>
  </p>
  <p>The awkward child was often <i>derided</i> by his “cooler”
                    peers.</p>
  <p />
  <p>
    <i>Synonyms: gibe; jeer; mock; ridicule; scoff; sneer; taunt</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 108,'108',1,NULL,'<div class="prodid-question">
  <p>
    <b>Desiccate</b>
  </p>
  <p>verb</p>
  <p>(<u>deh</u> sih kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to dry out thoroughly</b>
  </p>
  <p>After a few weeks lying on the desert’s baking sands, the cow’s
                    carcass became completely <i>desiccated</i>.</p>
  <p />
  <p>
    <i>Synonyms: dehydrate; dry; parch</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 109,'109',1,NULL,'<div class="prodid-question">
  <p>
    <b>Desultory</b>
  </p>
  <p>adj</p>
  <p>(dehs <u>uhl</u> tohr ee) (<u>dehz</u> uhl tohr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>jumping from one thing to another; disconnected</b>
  </p>
  <p>Athena had a <i>desultory</i> academic record; she had changed majors 12 times in
                    3 years.</p>
  <p />
  <p>
    <i>Synonyms: aimless; disconnected; erratic; haphazard; indiscriminate;
                        objectless; purposeless; random; stray; unconsidered; unplanned</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 110,'110',1,NULL,'<div class="prodid-question">
  <p>
    <b>Diaphanous</b>
  </p>
  <p>adj</p>
  <p>(die <u>aaf</u> uh nuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>allowing light to show through; delicate</b>
  </p>
  <p>These <i>diaphanous</i> curtains do nothing to block out the sunlight.</p>
  <p />
  <p>
    <i>Synonyms: gauzy; sheer; tenuous; translucent; transparent</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 111,'111',1,NULL,'<div class="prodid-question">
  <p>
    <b>Diatribe</b>
  </p>
  <p>noun</p>
  <p>(<u>die</u> uh trieb)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an abusive, condemnatory speech</b>
  </p>
  <p>The trucker bellowed a <i>diatribe</i> at the driver who had cut him off.</p>
  <p />
  <p>
    <i>Synonyms: fulmination; harangue; invective; jeremiad; malediction; obloquy;
                        tirade</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 112,'112',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dictum</b>
  </p>
  <p>noun</p>
  <p>(<u>dihk</u> tuhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an authoritative statement</b>
  </p>
  <p>“You have time to lean, you have time to clean,” was the
                        <i>dictum</i> our boss made us live by.</p>
  <p />
  <p>
    <i>Synonyms: adage; aphorism; apothegm; decree; edict</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 113,'113',1,NULL,'<div class="prodid-question">
  <p>
    <b>Diffident</b>
  </p>
  <p>adj</p>
  <p>(<u>dih</u> fih dint)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking self-confidence</b>
  </p>
  <p>Steve was <i>diffident</i> during the job interview because of his nervous nature
                    and lack of experience in the field.</p>
  <p />
  <p>
    <i>Synonyms: backward; bashful; coy; demure; modest; retiring; self-effacing;
                        shy; timid</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 114,'114',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dilate</b>
  </p>
  <p>verb</p>
  <p>(<u>die</u> layt) (die <u>layt</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make larger; to expand</b>
  </p>
  <p>When you enter a darkened room, the pupils of your eyes <i>dilate</i> so as to
                    let in more light.</p>
  <p />
  <p>
    <i>Synonyms: amplify; develop; elaborate; enlarge; expand; expatiate</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 115,'115',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dilatory</b>
  </p>
  <p>adj</p>
  <p>(<u>dihl</u> uh tohr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>intended to delay</b>
  </p>
  <p>The congressman used <i>dilatory</i> measures to delay the passage of the
                    bill.</p>
  <p />
  <p>
    <i>Synonyms: dragging; flagging; laggard; lagging; slow; slow-footed;
                        slow-going; slow-paced; tardy</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 116,'116',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dilettante</b>
  </p>
  <p>noun</p>
  <p>(<u>dih</u> luh tahnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>someone with an amateurish and superficial interest in a topic</b>
  </p>
  <p>Jerry’s friends were such <i>dilettantes</i> they seemed to have new jobs
                    and hobbies every week.</p>
  <p />
  <p>
    <i>Synonyms: amateur; dabbler; superficial; tyro</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 117,'117',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dirge</b>
  </p>
  <p>noun</p>
  <p>(duhrj)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a funeral hymn or mournful speech</b>
  </p>
  <p>Melville wrote a <i>dirge</i> for the funeral of James McPherson, a Union general who
                    was killed in 1864.</p>
  <p />
  <p>
    <i>Synonyms: elegy; lament</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 118,'118',1,NULL,'<div class="prodid-question">
  <p>
    <b>Disabuse</b>
  </p>
  <p>verb</p>
  <p>(dih suh <u>byuze</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to set right; to free from error</b>
  </p>
  <p>Galileo’s observations <i>disabused</i> scholars of the notion that the
                    sun revolved around the Earth.</p>
  <p />
  <p>
    <i>Synonyms: correct; undeceive</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 119,'119',1,NULL,'<div class="prodid-question">
  <p>
    <b>Discern</b>
  </p>
  <p>verb</p>
  <p>(dihs <u>uhrn</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to perceive or recognize</b>
  </p>
  <p>It is easy to <i>discern</i> the difference between butter and butter-flavored
                    topping.</p>
  <p />
  <p>
    <i>Synonyms: catch; descry; detect; differentiate; discriminate; distinguish;
                        espy; glimpse; know; separate; spot; spy; tell</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 120,'120',1,NULL,'<div class="prodid-question">
  <p>
    <b>Disparate</b>
  </p>
  <p>adj</p>
  <p>(<u>dih</u> spuh ruht) (di <u>spar</u> uht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fundamentally different; entirely unlike</b>
  </p>
  <p>Although the twins are physically identical, their personalities are
                        <i>disparate</i>.</p>
  <p />
  <p>
    <i>Synonyms: different; dissimilar; divergent; diverse; variant; various</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 121,'121',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dissemble</b>
  </p>
  <p>verb</p>
  <p>(dihs <u>sehm</u> buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to present a false appearance; to disguise one’s real intentions or
                        character</b>
  </p>
  <p>The villain could <i>dissemble</i> to the police no longer—he admitted the
                    deed and tore up the floor to reveal the stash of stolen money.</p>
  <p />
  <p>
    <i>Synonyms: act; affect; assume; camouflage; cloak; counterfeit; cover up;
                        disguise; dissimulate; fake; feign; mask; masquerade; pose; pretend; put on;
                        sham</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 122,'122',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dissonance</b>
  </p>
  <p>noun</p>
  <p>(<u>dihs</u> uh nuhns)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a harsh and disagreeable combination, especially of sounds</b>
  </p>
  <p>Cognitive <i>dissonance</i> is the inner conflict produced when long-standing
                    beliefs are contradicted by new evidence.</p>
  <p />
  <p>
    <i>Synonyms: clash; contention; discord; dissension; dissent; dissidence;
                        friction; strife; variance</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 123,'123',1,NULL,'<div class="prodid-question">
  <p>
    <b>Distaff</b>
  </p>
  <p>noun</p>
  <p>(<u>dis</u> taf)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the female branch of a family</b>
  </p>
  <p>The lazy husband refused to cook dinner for his wife, joking that the duty
                    belongs to the <i>distaff’s</i> side.</p>
  <p />
  <p>
    <i>Synonyms: maternal</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 124,'124',1,NULL,'<div class="prodid-question">
  <p>
    <b>Distend</b>
  </p>
  <p>verb</p>
  <p>(dih <u>stehnd</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to swell, inflate, or bloat</b>
  </p>
  <p>Her stomach was <i>distended</i> after she gorged on the six-course meal.</p>
  <p>
    <i>Synonyms: broaden; bulge</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 125,'125',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dither</b>
  </p>
  <p>verb</p>
  <p>(<u>dihth</u> uhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to act confusedly or without clear purpose</b>
  </p>
  <p>Ellen <i>dithered</i> around her apartment, uncertain how to tackle the family
                    crisis.</p>
  <p />
  <p>
    <i>Synonyms: falter; hesitate; vacillate; waffle; waver</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 126,'126',1,NULL,'<div class="prodid-question">
  <p>
    <b>Diurnal</b>
  </p>
  <p>adj</p>
  <p>(die <u>uhr</u> nuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>active or occurring during the day</b>
  </p>
  <p>
    <i>Diurnal</i> creatures tend to become inactive during the night.</p>
  <p />
  <p>
    <i>Synonyms: daylight; daytime</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 127,'127',1,NULL,'<div class="prodid-question">
  <p>
    <b>Divine</b>
  </p>
  <p>verb</p>
  <p>(dih <u>vien</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to foretell or know by inspiration</b>
  </p>
  <p>The fortune-teller <i>divined</i> from the pattern of the tea leaves that her
                    customer would marry five times.</p>
  <p />
  <p>
    <i>Synonyms: auger; foresee; intuit; predict; presage</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 128,'128',1,NULL,'<div class="prodid-question">
  <p>
    <b>Doctrinaire</b>
  </p>
  <p>adj</p>
  <p>(dahk truh <u>nayr</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>rigidly devoted to theories without regard for practicality; dogmatic</b>
  </p>
  <p>The professor’s manner of teaching was considered <i>doctrinaire</i> for
                    such a liberal school.</p>
  <p />
  <p>
    <i>Synonyms: dictatorial; inflexible</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 129,'129',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dogma</b>
  </p>
  <p>noun</p>
  <p>(<u>dahg</u> muh) (<u>dawg</u> muh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a firmly held opinion, especially a religious belief</b>
  </p>
  <p>Linus’s central <i>dogma</i> was that children who believed in the Great
                    Pumpkin would be rewarded.</p>
  <p />
  <p>
    <i>Synonyms: creed; doctrine; teaching; tenet</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 130,'130',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dogmatic</b>
  </p>
  <p>adj</p>
  <p>(dahg <u>maat</u> ihk) (dawg <u>maat</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>dictatorial in one’s opinions</b>
  </p>
  <p>The dictator was <i>dogmatic</i>, claiming he, and only he, was right.</p>
  <p />
  <p>
    <i>Synonyms: authoritarian; bossy; dictatorial; doctrinaire; domineering;
                        imperious; magisterial; masterful; overbearing; peremptory</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 131,'131',1,NULL,'<div class="prodid-question">
  <p>
    <b>Droll</b>
  </p>
  <p>adj</p>
  <p>(drohl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>amusing in a wry, subtle way</b>
  </p>
  <p>Although the play couldn’t be described as hilarious, it was certainly
                        <i>droll</i>.</p>
  <p />
  <p>
    <i>Synonyms: comic; entertaining; funny; risible; witty</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 132,'132',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dupe</b>
  </p>
  <p>verb</p>
  <p>(doop)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to deceive</b>
  </p>
  <p>Bugs Bunny was able to <i>dupe</i> Elmer Fudd by dressing up as a lady
                    rabbit.</p>
  <p />
  <p>
    <i>Synonyms: beguile; betray; bluff; cozen; deceive; delude; fool; hoodwink;
                        humbug; mislead; take in; trick</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 133,'133',1,NULL,'<div class="prodid-question">
  <p>
    <b>Dyspeptic</b>
  </p>
  <p>adj</p>
  <p>(dihs <u>pehp</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>suffering from indigestion; gloomy and irritable</b>
  </p>
  <p>The <i>dyspeptic</i> young man cast a gloom over the party the minute he walked
                    in.</p>
  <p />
  <p>
    <i>Synonyms: acerbic; melancholy; morose; solemn; sour</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 134,'134',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ebullient</b>
  </p>
  <p>adj</p>
  <p>(ih <u>byool</u> yuhnt) (ih <u>buhl</u> yuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>exhilarated; full of enthusiasm and high spirits</b>
  </p>
  <p>The <i>ebullient</i> child exhausted the baby-sitter, who lacked the energy to
                    keep up with her.</p>
  <p />
  <p>
    <i>Synonyms: ardent; avid; bubbly; zestful</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 135,'135',1,NULL,'<div class="prodid-question">
  <p>
    <b>Eclectic</b>
  </p>
  <p>adj</p>
  <p>(ih <u>klehk</u> tihk) (eh <u>klehk</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>selecting from or made up from a variety of sources</b>
  </p>
  <p>Budapest’s architecture is an <i>eclectic</i> mix of eastern and western
                    styles.</p>
  <p />
  <p>
    <i>Synonyms: broad; catholic</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 136,'136',1,NULL,'<div class="prodid-question">
  <p>
    <b>Edify</b>
  </p>
  <p>verb</p>
  <p>(<u>eh</u> duh fie)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to instruct morally and spiritually</b>
  </p>
  <p>The guru was paid to <i>edify</i> the actress in the ways of Buddhism.</p>
  <p />
  <p>
    <i>Synonyms: educate; enlighten; guide; teach</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 137,'137',1,NULL,'<div class="prodid-question">
  <p>
    <b>Efficacy</b>
  </p>
  <p>noun</p>
  <p>(<u>eh</u> fih kuh see)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>effectiveness</b>
  </p>
  <p>The <i>efficacy</i> of penicillin was unsurpassed when it was first introduced;
                    the drug completely eliminated almost all bacterial infections.</p>
  <p />
  <p>
    <i>Synonyms: dynamism; effectiveness; efficiency; force; power; productiveness;
                        proficiency; strength; vigor</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 138,'138',1,NULL,'<div class="prodid-question">
  <p>
    <b>Effigy</b>
  </p>
  <p>noun</p>
  <p>(<u>eh</u> fuh jee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a stuffed doll; a likeness of a person</b>
  </p>
  <p>Protesters burned <i>effigies</i> of the unpopular leader.</p>
  <p />
  <p>
    <i>Synonyms: dummy; figure; image</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 139,'139',1,NULL,'<div class="prodid-question">
  <p>
    <b>Effrontery</b>
  </p>
  <p>noun</p>
  <p>(ih <u>fruhnt</u> uhr ee) (eh <u>fruhnt</u> uhr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impudent boldness; audacity</b>
  </p>
  <p>The receptionist had the <i>effrontery</i> to laugh out loud when the CEO tripped
                    over a computer wire and fell flat on his face.</p>
  <p />
  <p>
    <i>Synonyms: brashness; gall; nerve; presumption; temerity</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 140,'140',1,NULL,'<div class="prodid-question">
  <p>
    <b>Elegy</b>
  </p>
  <p>noun</p>
  <p>(<u>eh</u> luh jee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a sorrowful poem or speech</b>
  </p>
  <p>Though the beautiful <i>elegy</i> is about death and loss, it urges its readers
                    to endure this life, and to trust in spirituality.</p>
  <p />
  <p>
    <i>Synonyms: dirge; lament</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 141,'141',1,NULL,'<div class="prodid-question">
  <p>
    <b>Eloquent</b>
  </p>
  <p>adj</p>
  <p>(<u>ehl</u> uh kwunt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>persuasive and moving, especially in speech</b>
  </p>
  <p>The Gettysburg Address is moving not only because of its lofty sentiments but
                    because of its <i>eloquent</i> words.</p>
  <p />
  <p>
    <i>Synonyms: articulate; expressive; fluent; meaningful; significant;
                        smooth-spoken</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 142,'142',1,NULL,'<div class="prodid-question">
  <p>
    <b>Embellish</b>
  </p>
  <p>verb</p>
  <p>(ehm <u>behl</u> ihsh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to add ornamental or fictitious details</b>
  </p>
  <p>Britt <i>embellished</i> her résumé, hoping to make the lowly
                    positions she had held seem more important.</p>
  <p />
  <p>
    <i>Synonyms: adorn; bedeck; elaborate; embroider; enhance; exaggerate</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 143,'143',1,NULL,'<div class="prodid-question">
  <p>
    <b>Emulate</b>
  </p>
  <p>verb</p>
  <p>(<u>ehm</u> yuh layt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to copy; to try to equal or excel</b>
  </p>
  <p>The graduate student sought to <i>emulate</i> his professor in every way, copying
                    not only how she taught but also how she conducted herself outside of class.</p>
  <p />
  <p>
    <i>Synonyms: ape; imitate; simulate</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 144,'144',1,NULL,'<div class="prodid-question">
  <p>
    <b>Encomium</b>
  </p>
  <p>noun</p>
  <p>(ehn <u>koh</u> me uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>warm praise</b>
  </p>
  <p>She wrote an <i>encomium</i> in praise of the outgoing president.</p>
  <p />
  <p>
    <i>Synonyms: citation; eulogy; panegyric; salutation; tribute</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 145,'145',1,NULL,'<div class="prodid-question">
  <p>
    <b>Endemic</b>
  </p>
  <p>adj</p>
  <p>(ehn <u>deh</u> mihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>belonging to a particular area; inherent</b>
  </p>
  <p>The health department determined that the outbreak was <i>endemic</i> to the
                    small village, so they quarantined the inhabitants before the virus could
                    spread.</p>
  <p />
  <p>
    <i>Synonyms: indigenous; local; native</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.712336',0,'skipped');
INSERT INTO flashcards VALUES(1, 146,'146',1,NULL,'<div class="prodid-question">
  <p>
    <b>Enervate</b>
  </p>
  <p>verb</p>
  <p>(ehn <u>uhr</u> vayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to reduce in strength</b>
  </p>
  <p>The guerrillas hoped that a series of surprise attacks would <i>enervate</i> the
                    regular army.</p>
  <p />
  <p>
    <i>Synonyms: debilitate; enfeeble; sap; weaken</i>
  </p>
</div>','','2014-06-18 10:15:16.712336','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 147,'147',1,NULL,'<div class="prodid-question">
  <p>
    <b>Engender</b>
  </p>
  <p>verb</p>
  <p>(ehn <u>gehn</u> duhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to produce, cause, or bring about</b>
  </p>
  <p>His fear and hatred of clowns was <i>engendered</i> when he witnessed a bank
                    robbery carried out by five men wearing clown suits and make-up.</p>
  <p />
  <p>
    <i>Synonyms: beget; generate; spawn</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 148,'148',1,NULL,'<div class="prodid-question">
  <p>
    <b>Enigma</b>
  </p>
  <p>noun</p>
  <p>(ih <u>nig</u> muh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a puzzle; a mystery</b>
  </p>
  <p>Speaking in riddles and dressed in old robes, the artist gained a reputation as
                    something of an <i>enigma</i>.</p>
  <p />
  <p>
    <i>Synonyms: conundrum; perplexity</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 149,'149',1,NULL,'<div class="prodid-question">
  <p>
    <b>Enumerate</b>
  </p>
  <p>verb</p>
  <p>(ih <u>noo</u> muhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to count, list, or itemize</b>
  </p>
  <p>Before making his decision, Jacob asked the waiter to <i>enumerate</i> the
                    different varieties of ice cream that the restaurant carried.</p>
  <p />
  <p>
    <i>Synonyms: catalog; index; tabulate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 150,'150',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ephemeral</b>
  </p>
  <p>adj</p>
  <p>(ih <u>fehm</u> uhr uhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lasting a short time</b>
  </p>
  <p>The lives of mayflies seem <i>ephemeral </i>to us, since the flies’
                    average life span is a matter of hours.</p>
  <p />
  <p>
    <i>Synonyms: evanescent; fleeting; momentary; transient</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 151,'151',1,NULL,'<div class="prodid-question">
  <p>
    <b>Epicure</b>
  </p>
  <p>noun</p>
  <p>(eh pih <u>kyoor</u>) (<u>eh</u> pih kyuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a person with refined taste in food and wine</b>
  </p>
  <p>Niren is an <i>epicure</i> who always throws the most splendid dinner
                    parties.</p>
  <p />
  <p>
    <i>Synonyms: bon vivant; connoisseur; gastronome; gastronomist; gourmand;
                        gourmet</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 152,'152',1,NULL,'<div class="prodid-question">
  <p>
    <b>Equivocate</b>
  </p>
  <p>verb</p>
  <p>(ih <u>kwihv</u> uh kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to use expressions of double meaning in order to mislead</b>
  </p>
  <p>When faced with criticism of his policies, the politician <i>equivocated</i> and
                    left all parties thinking he agreed with them.</p>
  <p />
  <p>
    <i>Synonyms: ambiguous; evasive; waffling</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 153,'153',1,NULL,'<div class="prodid-question">
  <p>
    <b>Erratic</b>
  </p>
  <p>adj</p>
  <p>(ih <u>raat</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>wandering and unpredictable</b>
  </p>
  <p>The plot seemed predictable until it suddenly took a series of <i>erratic</i>
                    turns that surprised the audience.</p>
  <p />
  <p>
    <i>Synonyms: capricious; inconstant; irresolute; whimsical</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 154,'154',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ersatz</b>
  </p>
  <p>adj</p>
  <p>(<u>uhr</u> sats) (uhr <u>sats</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fake</b>
  </p>
  <p>Edda, a fashion maven, knew instantly that her friend’s new Kate Spade bag
                    was really an <i>ersatz</i> version purchased on the street.</p>
  <p />
  <p>
    <i>Synonyms: artificial; dummy; false; imitation; mock; sham; simulated;
                        spurious; substitute</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 155,'155',1,NULL,'<div class="prodid-question">
  <p>
    <b>Erudite</b>
  </p>
  <p>adj</p>
  <p>(<u>ehr</u> yuh dite) (<u>ehr</u> uh dite)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>learned; scholarly; bookish</b>
  </p>
  <p>The annual meeting of philosophy professors was a gathering of the most
                        <i>erudite</i>, well-published individuals in the field.</p>
  <p />
  <p>
    <i>Synonyms: learned; scholastic; wise</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 156,'156',1,NULL,'<div class="prodid-question">
  <p>
    <b>Eschew</b>
  </p>
  <p>verb</p>
  <p>(ehs <u>choo</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to shun; to avoid (as something wrong or distasteful)</b>
  </p>
  <p>The filmmaker <i>eschewed</i> artificial light for her actors, resulting in a
                    stark movie style.</p>
  <p />
  <p>
    <i>Synonyms: avoid; elude; escape; evade; shun; shy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 157,'157',1,NULL,'<div class="prodid-question">
  <p>
    <b>Esoteric</b>
  </p>
  <p>adj</p>
  <p>(eh suh <u>tehr</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>known or understood only by a few</b>
  </p>
  <p>Only a handful of experts are knowledgeable about the <i>esoteric</i> world of
                    particle physics.</p>
  <p />
  <p>
    <i>Synonyms: abstruse; arcane; obscure</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 158,'158',1,NULL,'<div class="prodid-question">
  <p>
    <b>Estimable</b>
  </p>
  <p>adj</p>
  <p>(<u>eh</u> stuh muh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>admirable</b>
  </p>
  <p>Most people consider it <i>estimable</i> that Mother Teresa spent her life
                    helping the poor of India.</p>
  <p />
  <p>
    <i>Synonyms: admirable; commendable; creditable; honorable; laudable;
                        meritorious; praiseworthy; respectable; venerable; worthy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 159,'159',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ethos</b>
  </p>
  <p>noun</p>
  <p>(<u>ee</u> thohs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the beliefs or character of a group</b>
  </p>
  <p>It is the Boy Scouts’ <i>ethos</i> that one should always be prepared.</p>
  <p />
  <p>
    <i>Synonyms: culture; ethic; philosophy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 160,'160',1,NULL,'<div class="prodid-question">
  <p>
    <b>Eulogy</b>
  </p>
  <p>noun</p>
  <p>(<u>yoo</u> luh jee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a speech in praise of someone</b>
  </p>
  <p>His best friend gave the <i>eulogy</i>, outlining his many achievements and
                    talents.</p>
  <p />
  <p>
    <i>Synonyms: commend; extol; laud</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 161,'161',1,NULL,'<div class="prodid-question">
  <p>
    <b>Euphemism</b>
  </p>
  <p>noun</p>
  <p>(<u>yoo</u> fum ih zuhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>use of an inoffensive word or phrase in place of a more distasteful one</b>
  </p>
  <p>The funeral director preferred to use the <i>euphemism</i>
                    “sleeping” instead of the word “dead.”</p>
  <p>
    <i>Synonym: circumlocution; delicacy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 162,'162',1,NULL,'<div class="prodid-question">
  <p>
    <b>Euphony</b>
  </p>
  <p>noun</p>
  <p>(<u>yoo</u> fuh nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>pleasant, harmonious sound</b>
  </p>
  <p>To their loving parents, the children’s orchestra performance sounded like
                        <i>euphony</i>, although an outside observer probably would have called it a
                    cacophony of hideous sounds.</p>
  <p />
  <p>
    <i>Synonyms: harmony; melody; music; sweetness</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 163,'163',1,NULL,'<div class="prodid-question">
  <p>
    <b>Exacerbate</b>
  </p>
  <p>verb</p>
  <p>(ihg <u>zaas</u> uhr bayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make worse</b>
  </p>
  <p>It is unwise to take aspirin to try to relieve heartburn since, instead of
                    providing relief, it will only <i>exacerbate</i> the problem.</p>
  <p />
  <p>
    <i>Synonyms: aggravate; annoy; intensify; irritate; provoke</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 164,'164',1,NULL,'<div class="prodid-question">
  <p>
    <b>Exculpate</b>
  </p>
  <p>verb</p>
  <p>(<u>ehk</u> skuhl payt) (<u>ihk</u> skuhl payt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to clear from blame; to prove innocent</b>
  </p>
  <p>The legal system is intended to convict those who are guilty and <i>exculpate</i>
                    those who are innocent.</p>
  <p />
  <p>
    <i>Synonyms: absolve; acquit; clear; exonerate; vindicate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 165,'165',1,NULL,'<div class="prodid-question">
  <p>
    <b>Exigent</b>
  </p>
  <p>adj</p>
  <p>(<u>ehk</u> suh juhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>urgent; requiring immediate action</b>
  </p>
  <p>The patient was losing blood so rapidly that it was <i>exigent</i> to stop the
                    bleeding.</p>
  <p />
  <p>
    <i>Synonyms: critical; imperative; needed; urgent</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 166,'166',1,NULL,'<div class="prodid-question">
  <p>
    <b>Exonerate</b>
  </p>
  <p>verb</p>
  <p>(ihg <u>zahn</u> uh rayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to clear of blame</b>
  </p>
  <p>The fugitive was <i>exonerated</i> when another criminal confessed to committing
                    the crime.</p>
  <p />
  <p>
    <i>Synonyms: absolve; acquit; clear; exculpate; vindicate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 167,'167',1,NULL,'<div class="prodid-question">
  <p>
    <b>Explicit</b>
  </p>
  <p>adj</p>
  <p>(ehk <u>splih</u> siht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>clearly stated or shown; forthright in expression</b>
  </p>
  <p>The journalist wrote an <i>explicit</i> description of the gruesome murder.</p>
  <p />
  <p>
    <i>Synonyms: candid; clear-cut; definite; definitive; express; frank; specific;
                        straightforward; unambiguous; unequivocal</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 168,'168',1,NULL,'<div class="prodid-question">
  <p>
    <b>Exponent</b>
  </p>
  <p>noun</p>
  <p>(ihk <u>spoh</u> nuhnt) (<u>ehk</u> spoh nuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>one who champions or advocates</b>
  </p>
  <p>The vice president was an enthusiastic <i>exponent</i> of computer
                    technology.</p>
  <p />
  <p>
    <i>Synonyms: representative; supporter</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 169,'169',1,NULL,'<div class="prodid-question">
  <p>
    <b>Expurgate</b>
  </p>
  <p>verb</p>
  <p>(<u>ehk</u> spuhr gayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to censor</b>
  </p>
  <p>Government propagandists <i>expurgated</i> all negative references to the
                    dictator from the film.</p>
  <p />
  <p>
    <i>Synonyms: bowdlerize; cut; sanitize</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 170,'170',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fallow</b>
  </p>
  <p>noun</p>
  <p>(<u>faa</u> loh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>dormant; unused</b>
  </p>
  <p>This field should lie <i>fallow</i> for a year so the soil does not become
                    completely depleted.</p>
  <p />
  <p>
    <i>Synonyms: idle; inactive; unseeded</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 171,'171',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fanatical</b>
  </p>
  <p>adj</p>
  <p>(fuh <u>nah</u> tih kuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>acting excessively enthusiastic; filled with extreme, unquestioned devotion</b>
  </p>
  <p>The stormtroopers were <i>fanatical </i>in their devotion to the emperor, readily sacrificing their lives for him.</p>
  <p />
  <p>
    <i>Synonyms: extremist; fiery; frenzied; zealous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 172,'172',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fatuous</b>
  </p>
  <p>adj</p>
  <p>(<u>fah</u> choo uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>stupid; foolishly self-satisfied</b>
  </p>
  <p>Ted’s <i>fatuous</i> comments always embarrassed his keen-witted wife at
                    parties.</p>
  <p />
  <p>
    <i>Synonyms: absurd; ludicrous; preposterous; ridiculous; silly</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 173,'173',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fawn</b>
  </p>
  <p>verb</p>
  <p>(fahn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to grovel</b>
  </p>
  <p>The understudy <i>fawned</i> over the director in hopes of being cast in the part
                    on a permanent basis.</p>
  <p />
  <p>
    <i>Synonyms: bootlick; grovel; pander; toady</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 174,'174',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fecund</b>
  </p>
  <p>adj</p>
  <p>(<u>fee</u> kuhnd) (<u>feh</u> kuhnd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fertile; fruitful; productive</b>
  </p>
  <p>The <i>fecund</i> couple yielded a total of 20 children.</p>
  <p />
  <p>
    <i>Synonyms: flourishing; prolific</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 175,'175',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fervid</b>
  </p>
  <p>adj</p>
  <p>(<u>fuhr</u> vihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>intensely emotional; feverish</b>
  </p>
  <p>The fans of Maria Callas were particularly <i>fervid</i>, doing anything to catch
                    a glimpse of the great opera singer.</p>
  <p />
  <p>
    <i>Synonyms: burning; impassioned; passionate; vehement; zealous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 176,'176',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fetid</b>
  </p>
  <p>adj</p>
  <p>(<u>feh</u> tihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>foul-smelling; putrid</b>
  </p>
  <p>The <i>fetid</i> stench from the outhouse caused Francesca to wrinkle her nose in
                    disgust.</p>
  <p />
  <p>
    <i>Synonyms: funky; malodorous; noisome; rank; stinky</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 177,'177',1,NULL,'<div class="prodid-question">
  <p>
    <b>Flag</b>
  </p>
  <p>verb</p>
  <p>(flaag)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to decline in vigor, strength, or interest</b>
  </p>
  <p>The marathon runner slowed down as his strength <i>flagged</i>.</p>
  <p />
  <p>
    <i>Synonyms: dwindle; ebb; slacken; subside; wane</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 178,'178',1,NULL,'<div class="prodid-question">
  <p>
    <b>Florid</b>
  </p>
  <p>adj</p>
  <p>(<u>flohr</u> ihd) (<u>flahr</u> ihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>excessively decorated or embellished</b>
  </p>
  <p>The palace had been decorated in an excessively <i>florid</i> style; every
                    surface had been carved and gilded.</p>
  <p />
  <p>
    <i>Synonyms: baroque; elaborate; flamboyant; ornate; ostentatious; rococo</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 179,'179',1,NULL,'<div class="prodid-question">
  <p>
    <b>Foment</b>
  </p>
  <p>verb</p>
  <p>(foh <u>mehnt</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to arouse or incite</b>
  </p>
  <p>The rebels tried to <i>foment</i> revolution through their attacks on the
                    government.</p>
  <p />
  <p>
    <i>Synonyms: agitate; impassion; inflame; instigate; kindle</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 180,'180',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ford</b>
  </p>
  <p>verb</p>
  <p>(fohrd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to cross a body of water by wading</b>
  </p>
  <p>Because of the recent torrential rains, the cowboys were unable to <i>ford</i>
                    the swollen river.</p>
  <p />
  <p>
    <i>Synonyms: traverse; wade</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 181,'181',1,NULL,'<div class="prodid-question">
  <p>
    <b>Forestall</b>
  </p>
  <p>verb</p>
  <p>(fohr <u>stahl</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to prevent or delay; to anticipate</b>
  </p>
  <p>The landlord <i>forestalled</i> T.J.’s attempt to avoid paying the rent by
                    waiting for him outside his door.</p>
  <p />
  <p>
    <i>Synonyms: avert; deter; hinder; obviate; preclude</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 182,'182',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fortuitous</b>
  </p>
  <p>adj</p>
  <p>(fohr <u>too</u> ih tuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>happening by chance; fortunate</b>
  </p>
  <p>It was <i>fortuitous</i> that he won the lotto just before he had to pay back his
                    loans.</p>
  <p />
  <p>
    <i>Synonyms: chance; fortunate; haphazard; lucky; propitious; prosperous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 183,'183',1,NULL,'<div class="prodid-question">
  <p>
    <b>Fractious</b>
  </p>
  <p>adj</p>
  <p>(<u>fraak</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>unruly; rebellious</b>
  </p>
  <p>The general had a hard time maintaining discipline among his <i>fractious</i>
                    troops.</p>
  <p />
  <p>
    <i>Synonyms: contentious; cranky; peevish; quarrelsome</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 184,'184',1,NULL,'<div class="prodid-question">
  <p>
    <b>Frenetic</b>
  </p>
  <p>adj</p>
  <p>(fruh <u>neht</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>frantic; frenzied</b>
  </p>
  <p>The employee’s <i>frenetic</i> schedule left her little time to
                    socialize.</p>
  <p />
  <p>
    <i>Synonyms: corybantic; delirious; feverish; mad; rabid; wild</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 185,'185',1,NULL,'<div class="prodid-question">
  <p>
    <b>Frugality</b>
  </p>
  <p>noun</p>
  <p>(fru <u>gaa</u> luh tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>tending to be thrifty or cheap</b>
  </p>
  <p>Scrooge McDuck’s <i>frugality</i> was so great that he accumulated enough
                    wealth to fill a giant storehouse with money.</p>
  <p />
  <p>
    <i>Synonyms: economy; parsimony; prudence; sparingness</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 186,'186',1,NULL,'<div class="prodid-question">
  <p>
    <b>Furtive</b>
  </p>
  <p>adj</p>
  <p>(<u>fuhr</u> tihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>secret; stealthy</b>
  </p>
  <p>Glenn was <i>furtive</i> when he peered out of the corner of his eye at the
                    stunningly beautiful model.</p>
  <p />
  <p>
    <i>Synonyms: clandestine; covert; shifty; surreptitious; underhand</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 187,'187',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gambol</b>
  </p>
  <p>verb</p>
  <p>(<u>gaam</u> buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to dance or skip around playfully</b>
  </p>
  <p>From her office, Amy enviously watched the playful puppies <i>gambol</i> around
                    Central Park.</p>
  <p />
  <p>
    <i>Synonyms: caper; cavort; frisk; frolic; rollick; romp</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 188,'188',1,NULL,'<div class="prodid-question">
  <p>
    <b>Garner</b>
  </p>
  <p>verb</p>
  <p>(<u>gahr</u> nuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to gather and store</b>
  </p>
  <p>The director managed to <i>garner</i> financial backing from several different
                    sources for his next project.</p>
  <p />
  <p>
    <i>Synonyms: acquire; amass; glean; harvest; reap</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 189,'189',1,NULL,'<div class="prodid-question">
  <p>
    <b>Garrulous</b>
  </p>
  <p>adj</p>
  <p>(<u>gaar</u> uh luhs) (<u>gaar</u> yuh luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>tending to talk a lot</b>
  </p>
  <p>The <i>garrulous</i> parakeet distracted its owner with its continuous
                    talking.</p>
  <p />
  <p>
    <i>Synonyms: effusive; loquacious</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 190,'190',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gestation</b>
  </p>
  <p>noun</p>
  <p>(jeh <u>stay</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the growth process from conception to birth</b>
  </p>
  <p>The longer the <i>gestation</i> period of an organism, the more developed the
                    baby is at birth.</p>
  <p />
  <p>
    <i>Synonyms: development; gravidity; pregnancy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 191,'191',1,NULL,'<div class="prodid-question">
  <p>
    <b>Glib</b>
  </p>
  <p>adj</p>
  <p>(glihb)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fluent in an insincere manner; offhand; casual</b>
  </p>
  <p>The slimy politician managed to continue gaining supporters because he was a
                        <i>glib</i> speaker.</p>
  <p />
  <p>
    <i>Synonyms: easy; superficial</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 192,'192',1,NULL,'<div class="prodid-question">
  <p>
    <b>Glower</b>
  </p>
  <p>verb</p>
  <p>(<u>glow</u> uhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to glare or stare angrily and intensely</b>
  </p>
  <p>The cranky waiter <i>glowered</i> at the indecisive customer.</p>
  <p />
  <p>
    <i>Synonyms: frown; lower; scowl</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 193,'193',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gradation</b>
  </p>
  <p>noun</p>
  <p>(gray <u>day</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a degree or stage in a process; a variation in color</b>
  </p>
  <p>The paint store offers so many different <i>gradations</i> of red that
                    it’s impossible to choose among them.</p>
  <p />
  <p>
    <i>Synonyms: nuance; shade; step; subtlety</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 194,'194',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gregarious</b>
  </p>
  <p>adj</p>
  <p>(greh <u>gayr</u> ee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>outgoing; sociable</b>
  </p>
  <p>She was so <i>gregarious</i> that when she found herself alone she felt quite
                    sad.</p>
  <p />
  <p>
    <i>Synonyms: affable; communicative; congenial; sociable</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 195,'195',1,NULL,'<div class="prodid-question">
  <p>
    <b>Grievous</b>
  </p>
  <p>adj</p>
  <p>(<u>gree</u> vuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>causing grief or sorrow; serious and distressing</b>
  </p>
  <p>Maude and Bertha sobbed loudly throughout the <i>grievous</i> event.</p>
  <p />
  <p>
    <i>Synonyms: dire; dolorous; grave; mournful</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 196,'196',1,NULL,'<div class="prodid-question">
  <p>
    <b>Grovel</b>
  </p>
  <p>verb</p>
  <p>(<u>grah</u> vuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to humble oneself in a demeaning way</b>
  </p>
  <p>Thor <i>groveled</i> to his ex-girlfriend, hoping she would take him back.</p>
  <p />
  <p>
    <i>Synonyms: bootlick; cringe; fawn; kowtow; toady</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 197,'197',1,NULL,'<div class="prodid-question">
  <p>
    <b>Guile</b>
  </p>
  <p>noun</p>
  <p>(<u>gie</u> uhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deceit; trickery</b>
  </p>
  <p>Since he was not fast enough to catch the roadrunner on foot, the coyote resorted
                    to <i>guile</i> in an effort to trap his enemy.</p>
  <p>
    <i>Synonyms: artifice; chicanery; connivery; duplicity</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 198,'198',1,NULL,'<div class="prodid-question">
  <p>
    <b>Gullible</b>
  </p>
  <p>adj</p>
  <p>(<u>guh</u> luh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>easily deceived</b>
  </p>
  <p>The con man pretended to be a bank officer so as to fool <i>gullible</i> bank
                    customers into giving him their account information.</p>
  <p />
  <p>
    <i>Synonyms: credulous; exploitable; naïve</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 199,'199',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hapless</b>
  </p>
  <p>adj</p>
  <p>(<u>haap</u> luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>unfortunate; having bad luck</b>
  </p>
  <p>I wish someone would give that poor, <i>hapless</i> soul some food and
                    shelter.</p>
  <p />
  <p>
    <i>Synonyms: ill-fated; ill-starred; jinxed; luckless; unlucky</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 200,'200',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hegemony</b>
  </p>
  <p>noun</p>
  <p>(hih <u>jeh</u> muh nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the domination of one state or group over its allies</b>
  </p>
  <p>When Germany claimed <i>hegemony</i> over Russia, Stalin was outraged.</p>
  <p />
  <p>
    <i>Synonyms: authority; power</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 201,'201',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hermetic</b>
  </p>
  <p>adj</p>
  <p>(huhr <u>meh</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>tightly sealed</b>
  </p>
  <p>The <i>hermetic</i> seal of the jar proved impossible to break.</p>
  <p />
  <p>
    <i>Synonyms: airtight; impervious; watertight</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 202,'202',1,NULL,'<div class="prodid-question">
  <p>
    <b>Heterogeneous</b>
  </p>
  <p>adj</p>
  <p>(heh tuh ruh <u>jee</u> nee uhs)<br />(he truh <u>jee</u> nyuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>composed of unlike parts; different; diverse</b>
  </p>
  <p>The United Nations is by nature a <i>heterogeneous</i> body.</p>
  <p />
  <p>
    <i>Synonyms: assorted; miscellaneous; mixed; motley; varied</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 203,'203',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hoary</b>
  </p>
  <p>adj</p>
  <p>(<u>hohr</u> ee) (<u>haw</u> ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>very old; whitish or gray from age</b>
  </p>
  <p>The old man’s <i>hoary</i> beard contrasted starkly to the new stubble of
                    his teenage grandson.</p>
  <p />
  <p>
    <i>Synonyms: ancient; antediluvian; antique; venerable; vintage</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 204,'204',1,NULL,'<div class="prodid-question">
  <p>
    <b>Homogeneous</b>
  </p>
  <p>adj</p>
  <p>(hoh muh <u>jee</u> nee uhs)<br />(huh <u>mah</u> juhn uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>of a similar kind</b>
  </p>
  <p>The class was fairly <i>homogeneous</i> since almost all of the students were
                    journalism majors.</p>
  <p />
  <p>
    <i>Synonyms: consistent; standardized; uniform; unvarying</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 205,'205',1,NULL,'<div class="prodid-question">
  <p>
    <b>Husband</b>
  </p>
  <p>verb</p>
  <p>(<u>huhz</u> buhnd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to manage economically; to use sparingly</b>
  </p>
  <p>The cyclist paced herself at the start of the race, knowing that if she
                        <i>husbanded</i> her resources she’d have the strength to break out
                    of the pack later on.</p>
  <p />
  <p>
    <i>Synonyms: conserve; ration</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 206,'206',1,NULL,'<div class="prodid-question">
  <p>
    <b>Hyperbole</b>
  </p>
  <p>noun</p>
  <p>(hie <u>puhr</u> boh lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>purposeful exaggeration for effect</b>
  </p>
  <p>When the mayor claimed his town was one of the seven wonders of the world,
                    outsiders classified his statement as a <i>hyperbole</i>.</p>
  <p />
  <p>
    <i>Synonyms: embellishment; inflation; magnification</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 207,'207',1,NULL,'<div class="prodid-question">
  <p>
    <b>Iconoclast</b>
  </p>
  <p>noun</p>
  <p>(ie <u>kahn</u> uh klaast)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>one who opposes established beliefs, customs, and institutions</b>
  </p>
  <p>His lack of regard for traditional beliefs soon established him as an
                        <i>iconoclast</i>.</p>
  <p />
  <p>
    <i>Synonyms: maverick; nonconformist; rebel; revolutionary</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 208,'208',1,NULL,'<div class="prodid-question">
  <p>
    <b>Idiosyncrasy</b>
  </p>
  <p>noun</p>
  <p>(ih dee uh <u>sihn</u> kruh see)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>peculiarity of temperament; eccentricity</b>
  </p>
  <p>His numerous <i>idiosyncrasies</i> included a fondness for wearing bright green
                    shoes with mauve socks.</p>
  <p />
  <p>
    <i>Synonyms: humor; oddity; quirk</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 209,'209',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ignoble</b>
  </p>
  <p>adj</p>
  <p>(ihg <u>noh</u> buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>having low moral standards; not noble in character; mean</b>
  </p>
  <p>The photographer was paid a princely sum for the picture of the self-proclaimed
                    ethicist in the <i>ignoble</i> act of pick-pocketing.</p>
  <p />
  <p>
    <i>Synonyms: lowly; vulgar</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 210,'210',1,NULL,'<div class="prodid-question">
  <p>
    <b>Imbue</b>
  </p>
  <p>verb</p>
  <p>(ihm <u>byoo</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to infuse, dye, wet, or moisten</b>
  </p>
  <p>Marcia struggled to <i>imbue</i> her children with decent values, a difficult
                    task in this day and age.</p>
  <p />
  <p>
    <i>Synonyms: charge; freight; impregnate; permeate; pervade</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 211,'211',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impasse</b>
  </p>
  <p>noun</p>
  <p>(ihm <u>paas</u>) (<u>ihm</u> paas)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a blocked path; a dilemma with no solution</b>
  </p>
  <p>The rock slide produced an <i>impasse</i>, so no one could proceed further on the
                    road.</p>
  <p />
  <p>
    <i>Synonyms: cul-de-sac; deadlock; stalemate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 212,'212',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impecunious</b>
  </p>
  <p>adj</p>
  <p>(ihm pih <u>kyoo</u> nyuhs)<br />(ihm pih <u>kyoo</u> nee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>poor; having no money</b>
  </p>
  <p>After the stock market crashed, many former millionaires found themselves
                        <i>impecunious</i>.</p>
  <p />
  <p>
    <i>Synonyms: destitute; impoverished; indigent; needy; penniless</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 213,'213',1,NULL,'<div class="prodid-question">
  <p>
    <b>Imperturbable</b>
  </p>
  <p>adj</p>
  <p>(im puhr <u>tuhr</u> buh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>not capable of being disturbed</b>
  </p>
  <p>The counselor had so much experience dealing with distraught children that she was <i>imperturbable</i>, even when faced with
                    the wildest tantrums.</p>
  <p />
  <p>
    <i>Synonyms: composed; dispassionate; impassive; serene; stoical</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 214,'214',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impervious</b>
  </p>
  <p>adj</p>
  <p>(ihm <u>puhr</u> vee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impossible to penetrate; incapable of being affected</b>
  </p>
  <p>A good raincoat will be <i>impervious</i> to moisture.</p>
  <p>
    <i>Synonyms: impregnable; resistant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 215,'215',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impetuous</b>
  </p>
  <p>adj</p>
  <p>(ihm <u>peh</u> choo uhs)<br />(ihm <u>pehch</u> wuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>quick to act without thinking</b>
  </p>
  <p>It is not good for an investment broker to be <i>impetuous</i> since much thought
                    should be given to all the possible options.</p>
  <p />
  <p>
    <i>Synonyms: impulsive; precipitate; rash; reckless; spontaneous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 216,'216',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impious</b>
  </p>
  <p>adj</p>
  <p>(<u>ihm</u> pee uhs) (ihm <u>pie</u> uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>not devout in religion</b>
  </p>
  <p>The nun cut herself off from her <i>impious</i> family after she entered the
                    convent.</p>
  <p />
  <p>
    <i>Synonyms: immoral; irreverent; profane</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 217,'217',1,NULL,'<div class="prodid-question">
  <p>
    <b>Implacable</b>
  </p>
  <p>adj</p>
  <p>(ihm <u>play</u> kuh buhl)<br />(ihm <u>plaa</u> kuh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>unable to be calmed down or made peaceful</b>
  </p>
  <p>His rage at the betrayal was so great that he remained <i>implacable</i> for
                    weeks.</p>
  <p />
  <p>
    <i>Synonyms: inexorable; intransigent; irreconcilable; relentless; remorseless;
                        unforgiving; unrelenting</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 218,'218',1,NULL,'<div class="prodid-question">
  <p>
    <b>Imprecation</b>
  </p>
  <p>noun</p>
  <p>(ihm prih <u>kay</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a curse</b>
  </p>
  <p>Spouting violent <i>imprecations</i>, Hank searched for the person who had
                    vandalized his truck.</p>
  <p>
    <i>Synonym: damnation</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 219,'219',1,NULL,'<div class="prodid-question">
  <p>
    <b>Impugn</b>
  </p>
  <p>verb</p>
  <p>(ihm <u>pyoon</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to call into question; to attack verbally</b>
  </p>
  <p>“How dare you <i>impugn</i> my motives?” protested the lawyer, on
                    being accused of ambulance chasing.</p>
  <p />
  <p>
    <i>Synonyms: challenge; dispute</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 220,'220',1,NULL,'<div class="prodid-question">
  <p>
    <b>Incarnadine</b>
  </p>
  <p>adj</p>
  <p>(in <u>car</u> nuh deen)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>blood-red in color</b>
  </p>
  <p>At his mother’s mention of his baby pictures, the shy boy’s cheeks
                    turned <i>incarnadine</i> with embarrassment.</p>
  <p />
  <p>
    <i>Synonyms: reddened; ruby; ruddy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 221,'221',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inchoate</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>koh</u> uht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>not fully formed; disorganized</b>
  </p>
  <p>The ideas expressed in Nietzsche’s mature work also appear in an
                        <i>inchoate</i> form in his earliest writing.</p>
  <p />
  <p>
    <i>Synonyms: amorphous; incoherent; incomplete; unorganized</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 222,'222',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inculcate</b>
  </p>
  <p>verb</p>
  <p>(ihn <u>kuhl</u> kayt) (<u>ihn</u> kuhl kayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to teach; to impress in the mind</b>
  </p>
  <p>Most parents <i>inculcate</i> their children with their beliefs and ideas instead
                    of allowing their children to develop their own values.</p>
  <p />
  <p>
    <i>Synonyms: implant; indoctrinate; instill; preach</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 223,'223',1,NULL,'<div class="prodid-question">
  <p>
    <b>Indolent</b>
  </p>
  <p>adj</p>
  <p>(<u>ihn</u> duh luhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>habitually lazy or idle</b>
  </p>
  <p>Her <i>indolent</i> ways got her fired from many jobs.</p>
  <p />
  <p>
    <i>Synonyms: fainéant; languid; lethargic; slothful; sluggish</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 224,'224',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inexorable</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>ehk</u> suhr uh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>inflexible; unyielding</b>
  </p>
  <p>The <i>inexorable</i> force of the twister swept away their house.</p>
  <p />
  <p>
    <i>Synonyms: adamant; obdurate; relentless</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 225,'225',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ingenuous</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>jehn</u> yoo uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>showing innocence or childlike simplicity</b>
  </p>
  <p>She was so <i>ingenuous</i> that her friends feared that her innocence and
                    trustfulness would be exploited when she visited the big city.</p>
  <p />
  <p>
    <i>Synonyms: artless; guileless; innocent; naïve; simple; unaffected</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 226,'226',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ingrate</b>
  </p>
  <p>noun</p>
  <p>(<u>ihn</u> grayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an ungrateful person</b>
  </p>
  <p>When none of her relatives thanked her for the fruitcakes she had sent them,
                    Audrey condemned them all as <i>ingrates</i>.</p>
  <p />
  <p>
    <i>Synonyms: cad; churl</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 227,'227',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ingratiate</b>
  </p>
  <p>verb</p>
  <p>(ihn <u>gray</u> shee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to gain favor with another by deliberate effort; to seek to please somebody
                        so as to gain an advantage</b>
  </p>
  <p>The new intern tried to <i>ingratiate</i> herself with the managers so that they
                    might consider her for a future job.</p>
  <p />
  <p>
    <i>Synonyms: curry favor; flatter</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 228,'228',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inimical</b>
  </p>
  <p>adj</p>
  <p>(ih <u>nihm</u> ih kuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>hostile; unfriendly</b>
  </p>
  <p>Even though a cease-fire had been in place for months, the two sides were still
                        <i>inimical</i> to each other.</p>
  <p />
  <p>
    <i>Synonyms: adverse; antagonistic; dissident; recalcitrant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 229,'229',1,NULL,'<div class="prodid-question">
  <p>
    <b>Iniquity</b>
  </p>
  <p>noun</p>
  <p>(ih <u>nihk</u> wih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a sin; an evil act</b>
  </p>
  <p>“I promise to close every den of <i>iniquity</i> in this town!”
                    thundered the conservative new mayor.</p>
  <p />
  <p>
    <i>Synonyms: enormity; immorality; injustice; vice; wickedness</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 230,'230',1,NULL,'<div class="prodid-question">
  <p>
    <b>Innocuous</b>
  </p>
  <p>adj</p>
  <p>(ih <u>nahk</u> yoo uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>harmless</b>
  </p>
  <p>Some snakes are poisonous, but most species are <i>innocuous</i> and pose no
                    danger to humans.</p>
  <p />
  <p>
    <i>Synonyms: benign; harmless; inoffensive; insipid</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 231,'231',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inquest</b>
  </p>
  <p>noun</p>
  <p>(<u>ihn</u> kwehst)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an investigation; an inquiry</b>
  </p>
  <p>The police chief ordered an <i>inquest</i> to determine what went wrong.</p>
  <p />
  <p>
    <i>Synonyms: probe; quest; research</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 232,'232',1,NULL,'<div class="prodid-question">
  <p>
    <b>Insipid</b>
  </p>
  <p>adj</p>
  <p>(in <u>sih</u> pid)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking interest or flavor</b>
  </p>
  <p>The critic claimed that the painting was <i>insipid</i>, containing no
                    interesting qualities at all.</p>
  <p />
  <p>
    <i>Synonyms: banal; bland; dull; stale; vapid</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 233,'233',1,NULL,'<div class="prodid-question">
  <p>
    <b>Insurrection</b>
  </p>
  <p>noun</p>
  <p>(ihn suh <u>rehk</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>rebellion</b>
  </p>
  <p>After the emperor’s troops crushed the <i>insurrection</i>, its leaders fled
                        the country.</p>
  <p>
    <i>Synonyms: mutiny; revolt; revolution; uprising</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 234,'234',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inter</b>
  </p>
  <p>verb</p>
  <p>(ihn <u>tuhr</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to bury</b>
  </p>
  <p>After giving the masses one last chance to pay their respects, the
                    leader’s body was <i>interred</i>.</p>
  <p />
  <p>
    <i>Synonyms: entomb; inhume; sepulcher; sepulture; tomb</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 235,'235',1,NULL,'<div class="prodid-question">
  <p>
    <b>Interregnum</b>
  </p>
  <p>noun</p>
  <p>(in tuh <u>reg</u> nuhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a period between reigns</b>
  </p>
  <p>When John F. Kennedy was shot, there was a brief <i>interregnum</i> before Lyndon
                    B. Johnson became president.</p>
  <p />
  <p>
    <i>Synonyms: interval</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 236,'236',1,NULL,'<div class="prodid-question">
  <p>
    <b>Intractable</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>traak</u> tuh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>not easily managed or manipulated</b>
  </p>
  <p>
    <i>Intractable</i> for hours, the wild horse eventually allowed the rider to
                    mount.</p>
  <p />
  <p>
    <i>Synonyms: stubborn; unruly</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 237,'237',1,NULL,'<div class="prodid-question">
  <p>
    <b>Intransigent</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>traan</u> suh juhnt)<br />(ihn <u>traan</u> zuh juhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>uncompromising; refusing to be reconciled</b>
  </p>
  <p>The professor was <i>intransigent</i> on the deadline, insisting that everyone
                    turn the assignment in at the same time.</p>
  <p />
  <p>
    <i>Synonyms: implacable; inexorable; irreconcilable; obdurate; obstinate;
                        remorseless; rigid; unbending; unrelenting; unyielding</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 238,'238',1,NULL,'<div class="prodid-question">
  <p>
    <b>Intrepid</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>treh</u> pihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fearless; resolutely courageous</b>
  </p>
  <p>Despite freezing winds, the <i>intrepid</i> hiker completed his ascent.</p>
  <p>
    <i>Synonym: brave</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 239,'239',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inundate</b>
  </p>
  <p>verb</p>
  <p>(<u>ih</u> nuhn dayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to overwhelm; to cover with water</b>
  </p>
  <p>The tidal wave <i>inundated</i> Atlantis, which was lost beneath the water.</p>
  <p />
  <p>
    <i>Synonyms: deluge; drown; engulf; flood; submerge</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 240,'240',1,NULL,'<div class="prodid-question">
  <p>
    <b>Inure</b>
  </p>
  <p>verb</p>
  <p>(ih <u>nyoor</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to harden; accustom</b>
  </p>
  <p>Eventually, Hassad became <i>inured</i> to the sirens that went off every night
                    and could sleep through them.</p>
  <p />
  <p>
    <i>Synonyms: condition; familiarize; habituate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 241,'241',1,NULL,'<div class="prodid-question">
  <p>
    <b>Invective</b>
  </p>
  <p>noun</p>
  <p>(ihn <u>vek</u> tihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>abusive language</b>
  </p>
  <p>A stream of <i>invective</i> poured from Mrs. Pratt’s mouth as she watched
                    the vandals smash her ceramic frog.</p>
  <p />
  <p>
    <i>Synonyms: denunciation; revilement; vituperation</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 242,'242',1,NULL,'<div class="prodid-question">
  <p>
    <b>Investiture</b>
  </p>
  <p>noun</p>
  <p>(in <u>ves</u> tuh chur)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>ceremony conferring authority</b>
  </p>
  <p>At Napoleon’s <i>investiture</i>, he grabbed the crown from the
                    Pope’s hands and placed it on his head himself.</p>
  <p />
  <p>
    <i>Synonyms: inaugural; inauguration; induction; initiation; installation</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 243,'243',1,NULL,'<div class="prodid-question">
  <p>
    <b>Invidious</b>
  </p>
  <p>adj</p>
  <p>(ihn <u>vihd</u> ee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>envious, obnoxious, or offensive; likely to promote ill-will</b>
  </p>
  <p>It is cruel and <i>invidious</i> for parents to play favorites with their
                    children.</p>
  <p>
    <i>Synonyms: discriminatory; insulting; jaundiced; resentful</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 244,'244',1,NULL,'<div class="prodid-question">
  <p>
    <b>Irascible</b>
  </p>
  <p>adj</p>
  <p>(ih <u>rah</u> suh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>easily made angry</b>
  </p>
  <p>Attila the Hun’s <i>irascible</i> and violent nature made all who dealt
                    with him fear for their lives.</p>
  <p />
  <p>
    <i>Synonyms: cantankerous; irritable; ornery; testy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 245,'245',1,NULL,'<div class="prodid-question">
  <p>
    <b>Itinerant</b>
  </p>
  <p>adj</p>
  <p>(ie <u>tihn</u> uhr uhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>wandering from place to place; unsettled</b>
  </p>
  <p>The <i>itinerant</i> tomcat came back to the Johansson homestead every two
                    months.</p>
  <p />
  <p>
    <i>Synonyms: nomadic; vagrant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 246,'246',1,NULL,'<div class="prodid-question">
  <p>
    <b>Jargon</b>
  </p>
  <p>noun</p>
  <p>(<u>jahr</u> guhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>nonsensical talk; specialized language</b>
  </p>
  <p>You need to master technical <i>jargon</i> in order to communicate successfully
                    with engineers.</p>
  <p />
  <p>
    <i>Synonyms: argot; cant; dialect; idiom; slang</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 247,'247',1,NULL,'<div class="prodid-question">
  <p>
    <b>Jettison</b>
  </p>
  <p>verb</p>
  <p>(<u>jeht</u> ih zuhn) (<u>jeht</u> ih suhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to discard; to get rid of as unnecessary or encumbering</b>
  </p>
  <p>The sinking ship <i>jettisoned</i> its cargo in a desperate attempt to reduce its
                    weight.</p>
  <p />
  <p>
    <i>Synonyms: dump; eject</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 248,'248',1,NULL,'<div class="prodid-question">
  <p>
    <b>Jingoism</b>
  </p>
  <p>noun</p>
  <p>(<u>jing</u> goh ihz uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>belligerent support of one’s country</b>
  </p>
  <p>The professor’s <i>jingoism</i> made it difficult for the students to
                    participate in an open political discussion.</p>
  <p />
  <p>
    <i>Synonyms: chauvinism; nationalism</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 249,'249',1,NULL,'<div class="prodid-question">
  <p>
    <b>Jocular</b>
  </p>
  <p>adj</p>
  <p>(<u>jahk</u> yuh luhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>playful; humorous</b>
  </p>
  <p>The <i>jocular</i> old man entertained his grandchildren for hours.</p>
  <p />
  <p>
    <i>Synonyms: amusing; comical</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 250,'250',1,NULL,'<div class="prodid-question">
  <p>
    <b>Judicious</b>
  </p>
  <p>adj</p>
  <p>(joo <u>dih</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sensible; showing good judgment</b>
  </p>
  <p>The wise and distinguished judge was well known for having a <i>judicious</i>
                    temperament.</p>
  <p />
  <p>
    <i>Synonyms: circumspect; prudent; sagacious; sapient</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 251,'251',1,NULL,'<div class="prodid-question">
  <p>
    <b>Juncture</b>
  </p>
  <p>noun</p>
  <p>(<u>juhnk</u> chuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a point of time, especially one where two things are joined</b>
  </p>
  <p>At this <i>juncture</i>, I think it would be a good idea for us to take a coffee
                    break.</p>
  <p />
  <p>
    <i>Synonyms: confluence; convergence; crisis; crossroads; moment</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 252,'252',1,NULL,'<div class="prodid-question">
  <p>
    <b>Keen</b>
  </p>
  <p>adj</p>
  <p>(keen)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>having a sharp edge; intellectually sharp; perceptive</b>
  </p>
  <p>With her <i>keen</i> intelligence, she figured out the puzzle in ten seconds
                    flat.</p>
  <p />
  <p>
    <i>Synonyms: acute; canny; quick</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 253,'253',1,NULL,'<div class="prodid-question">
  <p>
    <b>Kindle</b>
  </p>
  <p>verb</p>
  <p>(<u>kihn</u> duhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to set fire to or ignite; to excite or inspire</b>
  </p>
  <p>With only damp wood to work with, Tilda had great difficulty trying to
                        <i>kindle</i> the campfire.</p>
  <p />
  <p>
    <i>Synonyms: arouse; awaken; light; spark</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 254,'254',1,NULL,'<div class="prodid-question">
  <p>
    <b>Kinetic</b>
  </p>
  <p>adj</p>
  <p>(kih <u>neh</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>relating to motion; characterized by movement</b>
  </p>
  <p>The <i>kinetic</i> sculpture moved back and forth, startling the museum
                    visitors.</p>
  <p />
  <p>
    <i>Synonyms: active; dynamic; mobile</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 255,'255',1,NULL,'<div class="prodid-question">
  <p>
    <b>Knell</b>
  </p>
  <p>noun</p>
  <p>(nehl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sound of a funeral bell; omen of death or failure</b>
  </p>
  <p>When the townspeople heard the <i>knell</i> from the church belfry, they knew
                    that their mayor had died.</p>
  <p />
  <p>
    <i>Synonyms: chime; peal; toll</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 256,'256',1,NULL,'<div class="prodid-question">
  <p>
    <b>Kudos</b>
  </p>
  <p>noun</p>
  <p>(<u>koo</u> dohs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fame, glory, or honor</b>
  </p>
  <p>The actress happily accepted <i>kudos</i> from the press for her stunning
                    performance in the film.</p>
  <p />
  <p>
    <i>Synonyms: acclaim; accolade; encomium; homage; praise</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 257,'257',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lachrymose</b>
  </p>
  <p>adj</p>
  <p>(<u>laak</u> ruh mohs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>tearful</b>
  </p>
  <p>Marcella always became <i>lachrymose</i> when it was time to bid her daughter
                    good-bye.</p>
  <p />
  <p>
    <i>Synonyms: teary; weeping</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 258,'258',1,NULL,'<div class="prodid-question">
  <p>
    <b>Laconic</b>
  </p>
  <p>adj</p>
  <p>(luh <u>kah</u> nihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>using few words</b>
  </p>
  <p>He was the classic <i>laconic</i> native of Maine; he talked as if he were being
                    charged for each word.</p>
  <p />
  <p>
    <i>Synonyms: concise; curt; pithy; taciturn; terse</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 259,'259',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lament</b>
  </p>
  <p>verb</p>
  <p>(luh <u>mehnt</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to express sorrow; to grieve</b>
  </p>
  <p>The children continued to <i>lament</i> the death of the goldfish weeks after its
                    demise.</p>
  <p />
  <p>
    <i>Synonyms: bewail; deplore; grieve; mourn</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 260,'260',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lampoon</b>
  </p>
  <p>verb</p>
  <p>(laam <u>poon</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to ridicule with satire</b>
  </p>
  <p>The mayor hated being <i>lampooned</i> by the press for his efforts to improve
                    people’s politeness.</p>
  <p>
    <i>Synonym: tease</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 261,'261',1,NULL,'<div class="prodid-question">
  <p>
    <b>Languid</b>
  </p>
  <p>adj</p>
  <p>(<u>laang</u> gwihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking energy; indifferent; slow</b>
  </p>
  <p>The <i>languid</i> cat cleaned its fur, ignoring the vicious, snarling dog
                    chained a few feet away from it.</p>
  <p />
  <p>
    <i>Synonyms: lackadaisical; listless; sluggish; weak</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 262,'262',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lapidary</b>
  </p>
  <p>adj</p>
  <p>(<u>laa</u> puh der ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>relating to precious stones or the art of cutting them</b>
  </p>
  <p>Most <i>lapidary</i> work today is done with the use of motorized equipment.</p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 263,'263',1,NULL,'<div class="prodid-question">
  <p>
    <b>Larceny</b>
  </p>
  <p>noun</p>
  <p>(<u>laar</u> suh nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>theft of property</b>
  </p>
  <p>The crime of stealing a wallet can be categorized as petty <i>larceny</i>.</p>
  <p />
  <p>
    <i>Synonyms: burglary; robbery; stealing</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 264,'264',1,NULL,'<div class="prodid-question">
  <p>
    <b>Largess</b>
  </p>
  <p>noun</p>
  <p>(laar <u>jehs</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>generous giving (as of money) to others who may seem inferior</b>
  </p>
  <p>She’d always relied on her parents’ <i>largess</i>, but after
                    graduation she had to get a job.</p>
  <p />
  <p>
    <i>Synonyms: benevolence; boon; compliment; favor; present</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 265,'265',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lassitude</b>
  </p>
  <p>noun</p>
  <p>(<u>laas</u> ih tood)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a state of diminished energy</b>
  </p>
  <p>The lack of energy that characterizes patients with anemia makes <i>lassitude</i>
                    one of the primary symptoms of the disease.</p>
  <p />
  <p>
    <i>Synonyms: debilitation; enervation; fatigue; languor; listlessness;
                        tiredness; weariness</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 266,'266',1,NULL,'<div class="prodid-question">
  <p>
    <b>Latent</b>
  </p>
  <p>adj</p>
  <p>(<u>lay</u> tehnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>potentially available, but not readily apparent</b>
  </p>
  <p>
    <i>Latent</i> trait testing seeks to identify skills that the test takers may
                    have but are not aware of.</p>
  <p />
  <p>
    <i>Synonyms: concealed; dormant; inert; potential; quiescent</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 267,'267',1,NULL,'<div class="prodid-question">
  <p>
    <b>Laud</b>
  </p>
  <p>verb</p>
  <p>(lawd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to give praise; to glorify</b>
  </p>
  <p>Parades and fireworks were staged to <i>laud</i> the success of the rebels.</p>
  <p />
  <p>
    <i>Synonyms: acclaim; applaud; commend; compliment; exalt; extol; hail;
                        praise</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 268,'268',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lavish</b>
  </p>
  <p>adj</p>
  <p>(<u>laa</u> vish)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extremely generous or extravagant; giving unsparingly</b>
  </p>
  <p>She was so <i>lavish</i> with her puppy that it soon became overweight and
                    spoiled.</p>
  <p />
  <p>
    <i>Synonyms: extravagant; exuberant; luxuriant; opulent; prodigal; profuse;
                        superabundant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 269,'269',1,NULL,'<div class="prodid-question">
  <p>
    <b>Leery</b>
  </p>
  <p>adj</p>
  <p>(<u>lihr</u> ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>suspicious</b>
  </p>
  <p>After being swindled once, Ruth became <i>leery</i> of strangers trying to sell
                    things to her.</p>
  <p />
  <p>
    <i>Synonyms: distrustful; guarded; wary</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 270,'270',1,NULL,'<div class="prodid-question">
  <p>
    <b>Legerdemain</b>
  </p>
  <p>noun</p>
  <p>(lehj uhr duh <u>mayn</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>trickery</b>
  </p>
  <p>The little boy thought his <i>legerdemain</i> was working on his mother, but she
                    in fact knew about every hidden toy and stolen cookie.</p>
  <p />
  <p>
    <i>Synonyms: chicanery; conjuring</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 271,'271',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lethargic</b>
  </p>
  <p>adj</p>
  <p>(luh <u>thar</u> jik)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>acting in an indifferent or slow, sluggish manner</b>
  </p>
  <p>The clerk was so <i>lethargic</i> that, even when business was slow, he always
                    had a long line in front of him.</p>
  <p />
  <p>
    <i>Synonyms: apathetic; lackadaisical; languid; listless; torpid</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 272,'272',1,NULL,'<div class="prodid-question">
  <p>
    <b>Levity</b>
  </p>
  <p>noun</p>
  <p>(<u>leh</u> vih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an inappropriate lack of seriousness; an overly casual atmosphere</b>
  </p>
  <p>The joke added a jarring note of <i>levity</i> to the otherwise serious
                    meeting.</p>
  <p />
  <p>
    <i>Synonyms: amusement; humor</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 273,'273',1,NULL,'<div class="prodid-question">
  <p>
    <b>Liberal</b>
  </p>
  <p>adj</p>
  <p>(<u>lihb</u> uh ruhl) (<u>lihb</u> ruhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>tolerant or broad-minded; generous or lavish</b>
  </p>
  <p>Cali’s <i>liberal</i> parents trusted her and allowed her to manage her
                    own affairs to a large extent.</p>
  <p />
  <p>
    <i>Synonyms: bounteous; latitudinarian; munificent; permissive; progressive</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 274,'274',1,NULL,'<div class="prodid-question">
  <p>
    <b>Libertine</b>
  </p>
  <p>noun</p>
  <p>(<u>lihb</u> uhr teen)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a free thinker (usually used disparagingly); one without moral restraint</b>
  </p>
  <p>The <i>libertine</i> took pleasure in gambling away his family’s
                    money.</p>
  <p>
    <i>Synonym: hedonist</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 275,'275',1,NULL,'<div class="prodid-question">
  <p>
    <b>Licentious</b>
  </p>
  <p>adj</p>
  <p>(lie <u>sehn</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>immoral; unrestrained by society</b>
  </p>
  <p>Religious citizens were outraged by the <i>licentious</i> exploits of the
                    free-spirited artists living in town.</p>
  <p />
  <p>
    <i>Synonyms: lewd; wanton</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 276,'276',1,NULL,'<div class="prodid-question">
  <p>
    <b>Limpid</b>
  </p>
  <p>adj</p>
  <p>(<u>lim</u> pihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>clear; transparent</b>
  </p>
  <p>Fernando could see all the way to the bottom through the pond’s
                        <i>limpid</i> water.</p>
  <p />
  <p>
    <i>Synonyms: lucid; pellucid; serene</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 277,'277',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lionize</b>
  </p>
  <p>verb</p>
  <p>(<u>lie</u> uhn iez)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to treat as a celebrity</b>
  </p>
  <p>After the success of his novel, the author was <i>lionized</i> by the press.</p>
  <p />
  <p>
    <i>Synonyms: feast; honor; regale</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 278,'278',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lissome</b>
  </p>
  <p>adj</p>
  <p>(<u>lihs</u> uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>easily flexed; limber; agile</b>
  </p>
  <p>The <i>lissome</i> yoga instructor twisted herself into shapes that her students
                    could only dream of.</p>
  <p />
  <p>
    <i>Synonyms: graceful; lithe; supple</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 279,'279',1,NULL,'<div class="prodid-question">
  <p>
    <b>Listless</b>
  </p>
  <p>adj</p>
  <p>(<u>lihst</u> lihs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking energy and enthusiasm</b>
  </p>
  <p>
    <i>Listless</i> and depressed after breaking up with his girlfriend, Raj spent
                    his days moping on the couch.</p>
  <p />
  <p>
    <i>Synonyms: fainéant; indolent; languid; lethargic; sluggish</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 280,'280',1,NULL,'<div class="prodid-question">
  <p>
    <b>Livid</b>
  </p>
  <p>adj</p>
  <p>(<u>lih</u> vihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>discolored from a bruise; pale; reddened with anger</b>
  </p>
  <p>André was <i>livid</i> when he discovered that someone had spilled grape
                    juice all over his cashmere coat.</p>
  <p />
  <p>
    <i>Synonyms: ashen; black-and-blue; furious; pallid</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 281,'281',1,NULL,'<div class="prodid-question">
  <p>
    <b>Loquacious</b>
  </p>
  <p>adj</p>
  <p>(loh <u>kway</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>talkative</b>
  </p>
  <p>She is naturally <i>loquacious</i>, which is a problem in situations where
                    listening is more important than talking.</p>
  <p />
  <p>
    <i>Synonyms: effusive; garrulous; verbose</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 282,'282',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lucid</b>
  </p>
  <p>adj</p>
  <p>(<u>loo</u> sihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>clear and easily understood</b>
  </p>
  <p>The explanations were written in a simple and <i>lucid</i> manner so that
                    students were immediately able to apply what they learned.</p>
  <p />
  <p>
    <i>Synonyms: clear; coherent; explicit; intelligible; limpid</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 283,'283',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lugubrious</b>
  </p>
  <p>adj</p>
  <p>(loo <u>goo</u> bree uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sorrowful; mournful; dismal</b>
  </p>
  <p>Irish wakes are a rousing departure from the <i>lugubrious</i> funeral services
                    to which most people are accustomed.</p>
  <p />
  <p>
    <i>Synonyms: funereal; gloomy; melancholy; somber; woeful</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 284,'284',1,NULL,'<div class="prodid-question">
  <p>
    <b>Lumber</b>
  </p>
  <p>verb</p>
  <p>(<u>luhm</u> buhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to move slowly and awkwardly</b>
  </p>
  <p>The bear <i>lumbered</i> toward the garbage, drooling at the prospect of the
                    leftovers he smelled.</p>
  <p />
  <p>
    <i>Synonyms: galumph; hulk; lurch; stumble</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 285,'285',1,NULL,'<div class="prodid-question">
  <p>
    <b>Luminous</b>
  </p>
  <p>adj</p>
  <p>(<u>loo</u> muhn uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>bright; brilliant; glowing</b>
  </p>
  <p>The park was bathed in <i>luminous</i> sunshine that warmed the bodies and the
                    souls of the visitors.</p>
  <p />
  <p>
    <i>Synonyms: incandescent; lucent; lustrous; radiant; resplendent</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 286,'286',1,NULL,'<div class="prodid-question">
  <p>
    <b>Machination</b>
  </p>
  <p>noun</p>
  <p>(mahk uh <u>nay</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>plot or scheme</b>
  </p>
  <p>Tired of his enemies’ endless <i>machinations</i> to remove him from the
                    throne, the king had them executed.</p>
  <p />
  <p>
    <i>Synonyms: cabal; conspiracy; design; intrigue</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 287,'287',1,NULL,'<div class="prodid-question">
  <p>
    <b>Maelstrom</b>
  </p>
  <p>noun</p>
  <p>(<u>mayl</u> struhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>whirlpool; turmoil; agitated state of mind</b>
  </p>
  <p>The transportation system of the city had collapsed in the <i>maelstrom</i> of
                    war.</p>
  <p />
  <p>
    <i>Synonyms: eddy; turbulence</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 288,'288',1,NULL,'<div class="prodid-question">
  <p>
    <b>Magnate</b>
  </p>
  <p>noun</p>
  <p>(<u>maag</u> nayt) (<u>maag</u> niht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>powerful or influential person</b>
  </p>
  <p>The entertainment <i>magnate</i> bought two cable TV stations to add to his
                    collection of magazines and publishing houses.</p>
  <p />
  <p>
    <i>Synonyms: dignitary; luminary; nabob; potentate; tycoon</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 289,'289',1,NULL,'<div class="prodid-question">
  <p>
    <b>Malediction</b>
  </p>
  <p>noun</p>
  <p>(maal ih <u>dihk</u> shun)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a curse; a wish of evil upon another</b>
  </p>
  <p>The frog prince looked for a princess to kiss him and put an end to the
                    witch’s <i>malediction</i>.</p>
  <p />
  <p>
    <i>Synonyms: anathema; imprecation</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 290,'290',1,NULL,'<div class="prodid-question">
  <p>
    <b>Malinger</b>
  </p>
  <p>verb</p>
  <p>(muh <u>ling</u> guhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to evade responsibility by pretending</b>
    <b>to be ill</b>
  </p>
  <p>A common way to avoid the draft was by <i>malingering</i>—pretending to be
                    mentally or physically ill so as to avoid being taken by the army.</p>
  <p />
  <p>
    <i>Synonyms: shirk; slack</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 291,'291',1,NULL,'<div class="prodid-question">
  <p>
    <b>Malleable</b>
  </p>
  <p>adj</p>
  <p>(<u>mah</u> lee uh buhl) (<u>mal</u> yuh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>capable of being shaped</b>
  </p>
  <p>Gold is the most <i>malleable</i> of precious metals; it can easily be formed
                    into almost any shape.</p>
  <p />
  <p>
    <i>Synonyms: adaptable; ductile; plastic; pliable; pliant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 292,'292',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mannered</b>
  </p>
  <p>adj</p>
  <p>(<u>maan</u> uhrd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>artificial or stilted in character</b>
  </p>
  <p>The portrait is an example of the <i>mannered</i> style that was favored in that
                    era.</p>
  <p />
  <p>
    <i>Synonyms: affected; unnatural</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 293,'293',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mar</b>
  </p>
  <p>verb</p>
  <p>(mahr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to damage or deface; to spoil</b>
  </p>
  <p>Telephone poles <i>mar</i> the natural beauty of the countryside.</p>
  <p />
  <p>
    <i>Synonyms: blemish; disfigure; impair; injure; scar</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 294,'294',1,NULL,'<div class="prodid-question">
  <p>
    <b>Martinet</b>
  </p>
  <p>noun</p>
  <p>(mahr tihn <u>eht</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>strict disciplinarian; one who rigidly follows rules</b>
  </p>
  <p>A complete <i>martinet</i>, the official insisted that Pete fill out all the
                    forms again even though he was already familiar with his case.</p>
  <p />
  <p>
    <i>Synonyms: dictator; stickler; tyrant</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 295,'295',1,NULL,'<div class="prodid-question">
  <p>
    <b>Maudlin</b>
  </p>
  <p>adj</p>
  <p>(<u>mawd</u> lihn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>overly sentimental</b>
  </p>
  <p>The movie’s treatment of the mother’s death was so <i>maudlin</i>
                    that, instead of making the audience cry, it made them cringe.</p>
  <p />
  <p>
    <i>Synonyms: bathetic; mawkish; saccharine; weepy</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 296,'296',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mendacious</b>
  </p>
  <p>adj</p>
  <p>(mehn <u>day</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>dishonest</b>
  </p>
  <p>So many of her stories were <i>mendacious</i> that I decided she must be a
                    pathological liar.</p>
  <p />
  <p>
    <i>Synonyms: deceitful; false; lying; untruthful</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 297,'297',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mendicant</b>
  </p>
  <p>noun</p>
  <p>(<u>mehn</u> dih kuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>beggar</b>
  </p>
  <p>“Please, sir, can you spare a dime?” begged the <i>mendicant</i> as
                    the businessman walked past.</p>
  <p />
  <p>
    <i>Synonyms: panhandler; pauper</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 298,'298',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mercurial</b>
  </p>
  <p>adj</p>
  <p>(muhr <u>kyoor</u> ee uhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>quick, shrewd, and unpredictable</b>
  </p>
  <p>Her <i>mercurial</i> personality made it difficult to guess how she would react
                    to the bad news.</p>
  <p />
  <p>
    <i>Synonyms: clever; crafty; volatile; whimsical</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 299,'299',1,NULL,'<div class="prodid-question">
  <p>
    <b>Meretricious</b>
  </p>
  <p>adj</p>
  <p>(mehr ih <u>trihsh</u> uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>gaudy; falsely attractive</b>
  </p>
  <p>The casino’s <i>meretricious</i> decor horrified the cultivated interior
                    designer.</p>
  <p />
  <p>
    <i>Synonyms: flashy; insincere; loud; specious; tawdry</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 300,'300',1,NULL,'<div class="prodid-question">
  <p>
    <b>Metaphor</b>
  </p>
  <p>noun</p>
  <p>(<u>meht</u> uh fohr) (<u>meht</u> uh fuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>figure of speech comparing two different things</b>
  </p>
  <p>The <i>metaphor</i> “a sea of troubles” suggests a lot of troubles
                    by comparing their number to the vastness of the sea.</p>
  <p />
  <p>
    <i>Synonyms: allegory; analogy; simile; symbol</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 301,'301',1,NULL,'<div class="prodid-question">
  <p>
    <b>Meticulous</b>
  </p>
  <p>adj</p>
  <p>(mih <u>tihk</u> yuh luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extremely careful; fastidious; painstaking</b>
  </p>
  <p>To find all the clues at the crime scene, the <i>meticulous</i> investigators
                    examined every inch of the area.</p>
  <p />
  <p>
    <i>Synonyms: finicky; fussy; picky; precise; punctilious; scrupulous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 302,'302',1,NULL,'<div class="prodid-question">
  <p>
    <b>Militate</b>
  </p>
  <p>verb</p>
  <p>(<u>mihl</u> ih tayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to operate against; work against</b>
  </p>
  <p>Lenin <i>militated</i> against the tsar for years before he overthrew him and
                    established the Soviet Union.</p>
  <p />
  <p>
    <i>Synonyms: affect; change; influence</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 303,'303',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mirth</b>
  </p>
  <p>noun</p>
  <p>(muhrth)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>frivolity; gaiety; laughter</b>
  </p>
  <p>Vera’s hilarious jokes contributed to the general <i>mirth</i> at the
                    dinner party.</p>
  <p />
  <p>
    <i>Synonyms: glee; hilarity; jollity; merriment</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 304,'304',1,NULL,'<div class="prodid-question">
  <p>
    <b>Misanthrope</b>
  </p>
  <p>noun</p>
  <p>(<u>mihs</u> ahn throhp)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a person who dislikes others</b>
  </p>
  <p>The Grinch was such a <i>misanthrope</i> that even the sight of children singing
                    made him angry.</p>
  <p>
    <i>Synonym: curmudgeon</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 305,'305',1,NULL,'<div class="prodid-question">
  <p>
    <b>Missive</b>
  </p>
  <p>noun</p>
  <p>(<u>mihs</u> ihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a written note or letter</b>
  </p>
  <p>Priscilla spent hours composing a romantic <i>missive</i> for Elvis.</p>
  <p>
    <i>Synonym: message</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 306,'306',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mitigate</b>
  </p>
  <p>verb</p>
  <p>(<u>miht</u> ih gayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to soften; to lessen</b>
  </p>
  <p>A judge may <i>mitigate</i> a sentence if she decides that a person committed a
                    crime out of need.</p>
  <p />
  <p>
    <i>Synonyms: allay; alleviate; assuage; ease; lighten; moderate; mollify;
                        palliate; temper</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 307,'307',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mollify</b>
  </p>
  <p>verb</p>
  <p>(<u>mahl</u> uh fie)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to calm or make less severe</b>
  </p>
  <p>Their argument was so intense that is was difficult to believe any compromise
                    would <i>mollify</i> them.</p>
  <p />
  <p>
    <i>Synonyms: appease; assuage; conciliate; pacify</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 308,'308',1,NULL,'<div class="prodid-question">
  <p>
    <b>Molt</b>
  </p>
  <p>verb</p>
  <p>(muhlt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to shed hair, skin, or an outer layer periodically</b>
  </p>
  <p>The snake <i>molted</i> its skin and left it behind in a crumpled mass.</p>
  <p />
  <p>
    <i>Synonyms: cast; defoliate; desquamate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 309,'309',1,NULL,'<div class="prodid-question">
  <p>
    <b>Monastic</b>
  </p>
  <p>adj</p>
  <p>(muh <u>naas</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extremely plain or secluded, as in a monastery</b>
  </p>
  <p>The philosopher retired to his <i>monastic</i> lodgings to contemplate life free
                    from any worldly distraction.</p>
  <p />
  <p>
    <i>Synonyms: austere; contemplative; disciplined; regimented;
                        self-abnegating</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 310,'310',1,NULL,'<div class="prodid-question">
  <p>
    <b>Monotony</b>
  </p>
  <p>noun</p>
  <p>(muh <u>naht</u> nee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>no variation; tediously the same</b>
  </p>
  <p>The <i>monotony</i> of the sound of the dripping faucet almost drove the research
                    assistant crazy.</p>
  <p />
  <p>
    <i>Synonyms: drone; tedium</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 311,'311',1,NULL,'<div class="prodid-question">
  <p>
    <b>Mores</b>
  </p>
  <p>noun</p>
  <p>(<u>mawr</u> ayz)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fixed customs or manners; moral attitudes</b>
  </p>
  <p>In keeping with the <i>mores</i> of ancient Roman society, Nero held a
                    celebration every weekend.</p>
  <p />
  <p>
    <i>Synonyms: conventions; practices</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 312,'312',1,NULL,'<div class="prodid-question">
  <p>
    <b>Multifarious</b>
  </p>
  <p>adj</p>
  <p>(muhl tuh <u>faar</u> ee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>diverse</b>
  </p>
  <p>Ken opened the hotel room window, letting in the <i>multifarious</i> noises of
                    the great city.</p>
  <p />
  <p>
    <i>Synonyms: assorted; heterogeneous; indiscriminate; legion; motley; multifold;
                        multiform; multiplex; populous; varied</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 313,'313',1,NULL,'<div class="prodid-question">
  <p>
    <b>Myopic</b>
  </p>
  <p>adj</p>
  <p>(mie <u>ahp</u> ihk) (mie <u>oh</u> pihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking foresight; having a narrow view or short-range perspective</b>
  </p>
  <p>Not wanting to spend a lot of money up front, the <i>myopic</i> business owner
                    would likely suffer the consequences later.</p>
  <p />
  <p>
    <i>Synonyms: short-sighted; unthinking</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 314,'314',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nadir</b>
  </p>
  <p>noun</p>
  <p>(<u>nay</u> dihr) (<u>nay</u> duhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lowest point</b>
  </p>
  <p>As Joey waited in line to audition for the diaper commercial, he realized he had
                    reached the <i>nadir</i> of his acting career.</p>
  <p />
  <p>
    <i>Synonyms: bottom; depth; pit</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 315,'315',1,NULL,'<div class="prodid-question">
  <p>
    <b>Naïve</b>
  </p>
  <p>adj</p>
  <p>(nah <u>eev</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking sophistication or experience</b>
  </p>
  <p>Inexperienced writers often are <i>naïve</i> and assume that big words
                    make them sound smarter.</p>
  <p />
  <p>
    <i>Synonyms: artless; credulous; guileless; ingenuous; simple; unaffected;
                        unsophisticated</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 316,'316',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nascent</b>
  </p>
  <p>adj</p>
  <p>(<u>nay</u> sehnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>starting to develop; coming into existence</b>
  </p>
  <p>The advertising campaign was still in a <i>nascent</i> stage, and nothing had
                    been finalized yet.</p>
  <p />
  <p>
    <i>Synonyms: embryonic; emerging; inchoate; incipient</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 317,'317',1,NULL,'<div class="prodid-question">
  <p>
    <b>Neologism</b>
  </p>
  <p>noun</p>
  <p>(nee <u>ah</u> luh ji zuhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>new word or expression</b>
  </p>
  <p>Aunt Mabel simply does not understand today’s youth; she is perplexed by
                    their clothing, music, and <i>neologisms</i>.</p>
  <p />
  <p>
    <i>Synonyms: slang; coinage</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 318,'318',1,NULL,'<div class="prodid-question">
  <p>
    <b>Neophyte</b>
  </p>
  <p>noun</p>
  <p>(<u>nee</u> oh fiet)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>novice; beginner</b>
  </p>
  <p>A relative <i>neophyte</i> at bowling, Rodolfo rolled all of his balls into the
                    gutter.</p>
  <p />
  <p>
    <i>Synonyms: apprentice; greenhorn; tyro</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 319,'319',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nettle</b>
  </p>
  <p>verb</p>
  <p>(<u>neh</u> tuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to irritate</b>
  </p>
  <p>I don’t particularly like having blue hair—I just do it to
                        <i>nettle</i> my parents.</p>
  <p />
  <p>
    <i>Synonyms: annoy; vex</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 320,'320',1,NULL,'<div class="prodid-question">
  <p>
    <b>Noisome</b>
  </p>
  <p>adj</p>
  <p>(<u>noy</u> suhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>stinking; putrid</b>
  </p>
  <p>A dead mouse trapped in your walls produces a <i>noisome</i> odor.</p>
  <p />
  <p>
    <i>Synonyms: disgusting; foul; malodorous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 321,'321',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nominal</b>
  </p>
  <p>adj</p>
  <p>(<u>nah</u> mihn uhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>existing in name only; negligible</b>
  </p>
  <p>A <i>nominal</i> but far from devoted member of the high school yearbook
                    committee, she rarely attends meetings.</p>
  <p />
  <p>
    <i>Synonyms: minimal; titular</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 322,'322',1,NULL,'<div class="prodid-question">
  <p>
    <b>Nuance</b>
  </p>
  <p>noun</p>
  <p>(<u>noo</u> ahns)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a subtle expression of meaning or quality</b>
  </p>
  <p>The scholars argued for hours over tiny <i>nuances</i> in the interpretation of
                    the last line of the poem.</p>
  <p />
  <p>
    <i>Synonyms: gradation; subtlety; tone</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 323,'323',1,NULL,'<div class="prodid-question">
  <p>
    <b>Numismatics</b>
  </p>
  <p>noun</p>
  <p>(nu miz <u>maa</u> tiks)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>coin collecting</b>
  </p>
  <p>Tomas’s passion for <i>numismatics</i> has resulted in an impressive
                    collection of coins from all over the world.</p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 324,'324',1,NULL,'<div class="prodid-question">
  <p>
    <b>Obdurate</b>
  </p>
  <p>adj</p>
  <p>(<u>ahb</u> duhr uht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>hardened in feeling; resistant to persuasion</b>
  </p>
  <p>The president was completely <i>obdurate</i> on the issue, and no amount of
                    persuasion would change his mind.</p>
  <p />
  <p>
    <i>Synonyms: inflexible; intransigent; recalcitrant; tenacious; unyielding</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 325,'325',1,NULL,'<div class="prodid-question">
  <p>
    <b>Oblique</b>
  </p>
  <p>adj</p>
  <p>(oh <u>bleek</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>indirect or evasive; misleading or devious</b>
  </p>
  <p>Usually open and friendly, Reynaldo has been behaving in a curiously
                        <i>oblique</i> manner lately.</p>
  <p />
  <p>
    <i>Synonyms: glancing; slanted; tangential</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 326,'326',1,NULL,'<div class="prodid-question">
  <p>
    <b>Obsequious</b>
  </p>
  <p>adj</p>
  <p>(uhb <u>see</u> kwee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>overly submissive and eager to please</b>
  </p>
  <p>The <i>obsequious</i> new associate made sure to compliment her
                    supervisor’s tie and agree with him on every issue.</p>
  <p />
  <p>
    <i>Synonyms: compliant; deferential; servile; subservient</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 327,'327',1,NULL,'<div class="prodid-question">
  <p>
    <b>Obstinate</b>
  </p>
  <p>adj</p>
  <p>(<u>ahb</u> stih nuht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>stubborn; unyielding</b>
  </p>
  <p>The <i>obstinate</i> child could not be made to eat any food that he perceived to
                    be “yucky.”</p>
  <p />
  <p>
    <i>Synonyms: intransigent; mulish; persistent; pertinacious; stubborn;
                        tenacious</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 328,'328',1,NULL,'<div class="prodid-question">
  <p>
    <b>Obviate</b>
  </p>
  <p>verb</p>
  <p>(<u>ahb</u> vee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to prevent; to make unnecessary</b>
  </p>
  <p>The river was shallow enough to wade across at many points, which <i>obviated</i>
                    the need for a bridge.</p>
  <p />
  <p>
    <i>Synonyms: forestall; preclude; prohibit</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 329,'329',1,NULL,'<div class="prodid-question">
  <p>
    <b>Occlude</b>
  </p>
  <p>verb</p>
  <p>(uh <u>klood</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to stop up; to prevent the passage of</b>
  </p>
  <p>A shadow is thrown across the Earth’s surface during a solar eclipse, when
                    the light from the sun is <i>occluded</i> by the moon.</p>
  <p />
  <p>
    <i>Synonyms: barricade; block; close; obstruct</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 330,'330',1,NULL,'<div class="prodid-question">
  <p>
    <b>Officious</b>
  </p>
  <p>adj</p>
  <p>(uh <u>fihsh</u> uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>too helpful; meddlesome</b>
  </p>
  <p>While planning her wedding, Maya discovered just how <i>officious</i> her future
                    mother-in-law could be.</p>
  <p />
  <p>
    <i>Synonyms: eager; intrusive; unwanted</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 331,'331',1,NULL,'<div class="prodid-question">
  <p>
    <b>Onerous</b>
  </p>
  <p>adj</p>
  <p>(<u>oh</u> neh ruhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>troublesome and oppressive; burdensome</b>
  </p>
  <p>The assignment was so extensive and difficult to manage that it proved
                        <i>onerous</i> to the team in charge of it.</p>
  <p />
  <p>
    <i>Synonyms: arduous; backbreaking; burdensome; cumbersome; difficult; exacting;
                        formidable; hard; laborious; oppressive; rigorous; taxing; trying</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 332,'332',1,NULL,'<div class="prodid-question">
  <p>
    <b>Opaque</b>
  </p>
  <p>adj</p>
  <p>(oh <u>payk</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impossible to see through; preventing the passage of light</b>
  </p>
  <p>The heavy build-up of dirt and grime on the windows made them almost
                        <i>opaque</i>.</p>
  <p />
  <p>
    <i>Synonyms: blurred; cloudy; nontransparent</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 333,'333',1,NULL,'<div class="prodid-question">
  <p>
    <b>Opine</b>
  </p>
  <p>verb</p>
  <p>(oh <u>pien</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to express an opinion</b>
  </p>
  <p>At the “Let’s Chat Talk Show,” the audience member
                        <i>opined</i> that the guest was in the wrong.</p>
  <p />
  <p>
    <i>Synonyms: point out; voice</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 334,'334',1,NULL,'<div class="prodid-question">
  <p>
    <b>Opprobrium</b>
  </p>
  <p>noun</p>
  <p>(uh <u>pro</u> bree uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>public disgrace</b>
  </p>
  <p>After the scheme to defraud the elderly was made public, the treasurer resigned
                    in utter <i>opprobrium</i>.</p>
  <p />
  <p>
    <i>Synonyms: discredit; disgrace; dishonor; disrepute; ignominy; infamy;
                        obloquy; shame</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 335,'335',1,NULL,'<div class="prodid-question">
  <p>
    <b>Orotund</b>
  </p>
  <p>adj</p>
  <p>(<u>or</u> uh tuhnd) (<u>ah</u> ruh tuhnd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>pompous</b>
  </p>
  <p>Roberto soon grew tired of his date’s <i>orotund</i> babble about her new
                    job, and decided their first date would probably be their last.</p>
  <p />
  <p>
    <i>Synonyms: aureate; bombastic; declamatory; euphuistic; flowery;
                        grandiloquent; magniloquent; oratorical; overblown; sonorous</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 336,'336',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ossify</b>
  </p>
  <p>verb</p>
  <p>(<u>ah</u> sih fie)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to change into bone; to become hardened or set in a rigidly conventional pattern</b>
  </p>
  <p>The forensics expert ascertained the body’s age based on the degree to
                    which the facial structure had <i>ossified</i>.</p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 337,'337',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ostensible</b>
  </p>
  <p>adj</p>
  <p>(ah <u>stehn</u> sih buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>apparent</b>
  </p>
  <p>The <i>ostensible</i> reason for his visit was to borrow a book, but he secretly
                    wanted to chat with the lovely Wanda.</p>
  <p />
  <p>
    <i>Synonyms: represented; supposed; surface</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 338,'338',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ostentation</b>
  </p>
  <p>noun</p>
  <p>(ah stehn <u>tay</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>excessive showiness</b>
  </p>
  <p>The <i>ostentation</i> of the Sun King’s court is evident in the lavish
                    decoration and luxuriousness of his palace at Versailles.</p>
  <p />
  <p>
    <i>Synonyms: conspicuousness; flashiness; pretentiousness; showiness</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 339,'339',1,NULL,'<div class="prodid-question">
  <p>
    <b>Overwrought</b>
  </p>
  <p>adj</p>
  <p>(oh vuhr <u>rawt</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>agitated; overdone</b>
  </p>
  <p>The lawyer’s <i>overwrought</i> voice on the phone made her clients worry
                    about the outcome of their case.</p>
  <p />
  <p>
    <i>Synonyms: elaborate; excited; nervous; ornate</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 340,'340',1,NULL,'<div class="prodid-question">
  <p>
    <b>Palatial</b>
  </p>
  <p>adj</p>
  <p>(puh <u>lay</u> shuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>relating to a palace; magnificent</b>
  </p>
  <p>After living in a cramped studio apartment for years, Siobhan thought the modest
                    one bedroom looked downright <i>palatial</i>.</p>
  <p />
  <p>
    <i>Synonyms: grand; stately</i>
  </p>
</div>','','2014-06-18 10:15:16.713341','2014-06-18 10:15:16.713341',0,'skipped');
INSERT INTO flashcards VALUES(1, 341,'341',1,NULL,'<div class="prodid-question">
  <p>
    <b>Palliate</b>
  </p>
  <p>verb</p>
  <p>(<u>paa</u> lee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to make less serious; to ease</b>
  </p>
  <p>The alleged crime was so vicious that the defense lawyer could not
                        <i>palliate</i> it for the jury.</p>
  <p />
  <p>
    <i>Synonyms: alleviate; assuage; extenuate; mitigate</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 342,'342',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pallid</b>
  </p>
  <p>adj</p>
  <p>(<u>paa</u> lihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lacking color or liveliness</b>
  </p>
  <p>The old drugstore’s <i>pallid</i> window could not compete with the new
                    megastore’s extravagant display next door.</p>
  <p />
  <p>
    <i>Synonyms: ashen; blanched; ghostly; pale; wan</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 343,'343',1,NULL,'<div class="prodid-question">
  <p>
    <b>Panache</b>
  </p>
  <p>noun</p>
  <p>(puh <u>nahsh</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>flamboyance or dash in style and action; verve</b>
  </p>
  <p>Leah has such <i>panache</i> when planning parties, even when they’re
                    last-minute affairs.</p>
  <p>
    <i>Synonym: flair</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 344,'344',1,NULL,'<div class="prodid-question">
  <p>
    <b>Panegyric</b>
  </p>
  <p>noun</p>
  <p>(paan uh <u>jeer</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>elaborate praise; formal hymn of praise</b>
  </p>
  <p>The director’s <i>panegyric</i> for the donor who kept his charity going
                    was heart-warming.</p>
  <p />
  <p>
    <i>Synonyms: compliment; homage</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 345,'345',1,NULL,'<div class="prodid-question">
  <p>
    <b>Panoply</b>
  </p>
  <p>noun</p>
  <p>(<u>paa</u> nuh plee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impressive array</b>
  </p>
  <p>Her résumé indicates a <i>panoply</i> of skills and
                    accomplishments.</p>
  <p />
  <p>
    <i>Synonyms: array; display; fanfare; parade; pomp; shine; show</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 346,'346',1,NULL,'<div class="prodid-question">
  <p>
    <b>Paradox</b>
  </p>
  <p>noun</p>
  <p>(<u>par</u> uh doks)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a contradiction or dilemma</b>
  </p>
  <p>It is a <i>paradox</i> that those most in need of medical attention are often
                    those least able to obtain it.</p>
  <p />
  <p>
    <i>Synonyms: ambiguity; incongruity</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 347,'347',1,NULL,'<div class="prodid-question">
  <p>
    <b>Paragon</b>
  </p>
  <p>noun</p>
  <p>(<u>par</u> uh gohn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>model of excellence or perfection</b>
  </p>
  <p>He is the <i>paragon</i> of what a judge should be: honest, intelligent,
                    hardworking, and just.</p>
  <p />
  <p>
    <i>Synonyms: apotheosis; ideal; quintessence; standard</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 348,'348',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pare</b>
  </p>
  <p>verb</p>
  <p>(payr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to trim off excess; to reduce</b>
  </p>
  <p>The cook’s hands were sore after she <i>pared</i> hundreds of potatoes for
                    the banquet.</p>
  <p />
  <p>
    <i>Synonyms: clip; peel</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 349,'349',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pariah</b>
  </p>
  <p>noun</p>
  <p>(puh <u>rie</u> uh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an outcast</b>
  </p>
  <p>Once he betrayed those in his community, he was banished and lived the life of a
                        <i>pariah</i>.</p>
  <p />
  <p>
    <i>Synonyms: castaway; derelict; leper; offscouring; untouchable</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 350,'350',1,NULL,'<div class="prodid-question">
  <p>
    <b>Parley</b>
  </p>
  <p>noun</p>
  <p>(<u>par</u> lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>discussion, usually between enemies</b>
  </p>
  <p>The <i>parley</i> between the rival cheerleading teams resulted in neither side
                    admitting that they copied the other’s dance moves.</p>
  <p />
  <p>
    <i>Synonyms: debate; dialogue; negotiations; talks</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 351,'351',1,NULL,'<div class="prodid-question">
  <p>
    <b>Parry</b>
  </p>
  <p>verb</p>
  <p>(<u>paar</u> ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to ward off or deflect, especially by a quick-witted answer</b>
  </p>
  <p>Kari <i>parried</i> every question the army officers fired at her, much to their
                    frustration.</p>
  <p>
    <i>Synonyms: avoid; evade; repel</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 352,'352',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pastiche</b>
  </p>
  <p>noun</p>
  <p>(pah <u>steesh</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a piece of literature or music imitating other works</b>
  </p>
  <p>The playwright’s clever <i>pastiche</i> of the well-known
                    children’s story had the audience rolling in the aisles.</p>
  <p />
  <p>
    <i>Synonyms: medley; spoof</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 353,'353',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pathogenic</b>
  </p>
  <p>adj</p>
  <p>(paa thoh <u>jehn</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>causing disease</b>
  </p>
  <p>Bina’s research on the origins of <i>pathogenic</i> microorganisms should
                    help stop the spread of disease.</p>
  <p />
  <p>
    <i>Synonyms: infecting; noxious</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 354,'354',1,NULL,'<div class="prodid-question">
  <p>
    <b>Peccadillo</b>
  </p>
  <p>noun</p>
  <p>(pehk uh <u>dih</u> loh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a minor sin or offense</b>
  </p>
  <p>Gabriel tends to harp on his brother’s <i>peccadilloes</i> and never lets
                    him live them down.</p>
  <p />
  <p>
    <i>Synonyms: failing; fault; lapse; misstep</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 355,'355',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pedant</b>
  </p>
  <p>noun</p>
  <p>(<u>peh</u> daant)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>someone who shows off learning</b>
  </p>
  <p>The graduate instructor’s tedious and excessive commentary on the subject
                    soon gained her a reputation as a <i>pedant</i>.</p>
  <p />
  <p>
    <i>Synonyms: doctrinaire; nit-picker; pedagogue; scholar; schoolmaster;
                        sophist</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 356,'356',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pejorative</b>
  </p>
  <p>adj</p>
  <p>(peh <u>jaw</u> ruh tihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>having bad connotations; disparaging</b>
  </p>
  <p>The teacher scolded Mark for his unduly <i>pejorative</i> comments about his
                    classmate’s presentation.</p>
  <p />
  <p>
    <i>Synonyms: belittling; dismissive; insulting</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 357,'357',1,NULL,'<div class="prodid-question">
  <p>
    <b>Penury</b>
  </p>
  <p>noun</p>
  <p>(<u>pehn</u> yuh ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>an oppressive lack of resources (as money); severe poverty</b>
  </p>
  <p>Once a famous actor, he eventually died in <i>penury</i> and anonymity.</p>
  <p />
  <p>
    <i>Synonyms: destitution; impoverishment</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 358,'358',1,NULL,'<div class="prodid-question">
  <p>
    <b>Peregrinate</b>
  </p>
  <p>verb</p>
  <p>(<u>peh</u> ruh gruh nayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to wander from place to place; to travel, especially on foot</b>
  </p>
  <p>Shivani enjoyed <i>peregrinating</i> the expansive grounds of Central Park.</p>
  <p>
    <i>Synonyms: journey; traverse; trek</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 359,'359',1,NULL,'<div class="prodid-question">
  <p>
    <b>Perfidious</b>
  </p>
  <p>adj</p>
  <p>(puhr <u>fih</u> dee uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>willing to betray someone’s trust</b>
  </p>
  <p>The actress’s <i>perfidious</i> companion revealed all of her intimate
                    secrets to the gossip columnist.</p>
  <p />
  <p>
    <i>Synonyms: disloyal; faithless; traitorous; treacherous</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 360,'360',1,NULL,'<div class="prodid-question">
  <p>
    <b>Perfunctory</b>
  </p>
  <p>adj</p>
  <p>(pur <u>fuhnk</u> tuhr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>done in a routine way; indifferent</b>
  </p>
  <p>The machine-like teller processed the transaction and gave the waiting customer a
                        <i>perfunctory</i> smile.</p>
  <p />
  <p>
    <i>Synonyms: apathetic; automatic; mechanical</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 361,'361',1,NULL,'<div class="prodid-question">
  <p>
    <b>Peripatetic</b>
  </p>
  <p>adj</p>
  <p>(peh ruh puh <u>teh</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>wandering from place to place, especially on foot</b>
  </p>
  <p>Eleana’s <i>peripatetic</i> meanderings took her all over the
                        countryside<i></i>in the summer months.</p>
  <p />
  <p>
    <i>Synonyms: itinerant; nomadic; wayfaring</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 362,'362',1,NULL,'<div class="prodid-question">
  <p>
    <b>Permeate</b>
  </p>
  <p>verb</p>
  <p>(<u>puhr</u> mee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to penetrate</b>
  </p>
  <p>This miraculous new cleaning fluid is able to <i>permeate</i> stains and dissolve
                    them in minutes!</p>
  <p />
  <p>
    <i>Synonyms: imbue; infuse; pervade; suffuse</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 363,'363',1,NULL,'<div class="prodid-question">
  <p>
    <b>Perspicacious</b>
  </p>
  <p>adj</p>
  <p>(puhr spuh <u>kay</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>shrewd, astute, or keen-witted</b>
  </p>
  <p>Inspector Poirot used his <i>perspicacious</i> mind to solve mysteries.</p>
  <p />
  <p>
    <i>Synonyms: insightful; intelligent; sagacious</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 364,'364',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pervade</b>
  </p>
  <p>verb</p>
  <p>(puhr <u>vayd</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to be present throughout; to permeate</b>
  </p>
  <p>Four spices—cumin, turmeric, coriander, and cayenne—<i>pervade</i> almost every Indian dish, and give the cuisine
                    its distinctive flavor.</p>
  <p />
  <p>
    <i>Synonyms: imbue; infuse; penetrate; permeate; suffuse</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 365,'365',1,NULL,'<div class="prodid-question">
  <p>
    <b>Phalanx</b>
  </p>
  <p>noun</p>
  <p>(<u>fay</u> laanks)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a compact or close-knit body of people, animals, or things</b>
  </p>
  <p>A <i>phalanx</i> of guards stood outside the prime minister’s home day and
                    night.</p>
  <p />
  <p>
    <i>Synonyms: legion; mass</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 366,'366',1,NULL,'<div class="prodid-question">
  <p>
    <b>Philanthropy</b>
  </p>
  <p>noun</p>
  <p>(fihl <u>aan</u> throh pee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>charity; a desire or effort to promote goodness</b>
  </p>
  <p>The Metropolitan Museum of Art owes much of its collection to the
                        <i>philanthropy</i> of private collectors who willed their estates to the
                    museum.</p>
  <p />
  <p>
    <i>Synonyms: altruism; humanitarianism</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 367,'367',1,NULL,'<div class="prodid-question">
  <p>
    <b>Philistine</b>
  </p>
  <p>noun</p>
  <p>(<u>fihl</u> uh steen)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a person who is guided by materialism and is disdainful of intellectual or
                        artistic values</b>
  </p>
  <p>The <i>philistine</i> never even glanced at the rare violin in his collection but
                    instead kept an eye on its value and sold it at a profit.</p>
  <p />
  <p>
    <i>Synonyms: boor; bourgeois; capitalist; materialist; vulgarian</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 368,'368',1,NULL,'<div class="prodid-question">
  <p>
    <b>Phlegmatic</b>
  </p>
  <p>adj</p>
  <p>(flehg <u>maa</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>calm and unemotional in temperament</b>
  </p>
  <p>Although the bomb could go off at any moment, the <i>phlegmatic</i> demolition
                    expert remained calm and unafraid.</p>
  <p />
  <p>
    <i>Synonyms: apathetic; calm; emotionless; impassive; indifferent; passionless;
                        unemotional</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 369,'369',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pithy</b>
  </p>
  <p>adj</p>
  <p>(<u>pih</u> thee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>profound or substantial yet concise, succinct, and to the point</b>
  </p>
  <p>Martha’s <i>pithy</i> comments during the interview must have been
                    impressive because she got the job.</p>
  <p />
  <p>
    <i>Synonyms: brief; compact; laconic; terse</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 370,'370',1,NULL,'<div class="prodid-question">
  <p>
    <b>Placate</b>
  </p>
  <p>verb</p>
  <p>(<u>play</u> cayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to soothe or pacify</b>
  </p>
  <p>The burglar tried to <i>placate</i> the snarling dog by referring to it as a
                    “nice doggy” and offering it a treat.</p>
  <p />
  <p>
    <i>Synonyms: appease; conciliate; mollify</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 371,'371',1,NULL,'<div class="prodid-question">
  <p>
    <b>Plastic</b>
  </p>
  <p>adj</p>
  <p>(<u>plaa</u> stihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>able to be molded, altered, or bent</b>
  </p>
  <p>The new material was very <i>plastic</i> and could be formed into products of
                    vastly different shape.</p>
  <p />
  <p>
    <i>Synonyms: adaptable; ductile; malleable; pliant</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 372,'372',1,NULL,'<div class="prodid-question">
  <p>
    <b>Plebeian</b>
  </p>
  <p>adj</p>
  <p>(plee <u>bee</u> uhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>crude or coarse; characteristic of commoners</b>
  </p>
  <p>After five weeks of rigorous studying, the graduate settled in for a weekend of
                        <i>plebeian</i> socializing and television watching.</p>
  <p />
  <p>
    <i>Synonyms: conventional; unrefined</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 373,'373',1,NULL,'<div class="prodid-question">
  <p>
    <b>Plethora</b>
  </p>
  <p>noun</p>
  <p>(<u>pleh</u> thor uh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>excess</b>
  </p>
  <p>Assuming that more was better, the defendant offered the judge a <i>plethora</i>
                    of excuses.</p>
  <p />
  <p>
    <i>Synonyms: glut; overabundance; superfluity; surfeit</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 374,'374',1,NULL,'<div class="prodid-question">
  <p>
    <b>Plucky</b>
  </p>
  <p>adj</p>
  <p>(<u>pluh</u> kee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>courageous; spunky</b>
  </p>
  <p>The <i>plucky</i> young nurse dove into the foxhole, determined to help the
                    wounded soldier.</p>
  <p />
  <p>
    <i>Synonyms: brave; bold; gutsy</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 375,'375',1,NULL,'<div class="prodid-question">
  <p>
    <b>Polemic</b>
  </p>
  <p>noun</p>
  <p>(puh <u>leh</u> mihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>controversy; argument; verbal attack</b>
  </p>
  <p>The candidate’s <i>polemic</i> against his opponent was vicious and
                    small-minded rather than convincing and well-reasoned.</p>
  <p />
  <p>
    <i>Synonyms: denunciation; refutation</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 376,'376',1,NULL,'<div class="prodid-question">
  <p>
    <b>Politic</b>
  </p>
  <p>adj</p>
  <p>(<u>pah</u> lih tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>shrewd and practical in managing or dealing with things; diplomatic</b>
  </p>
  <p>She was wise to curb her tongue and was able to explain her problem to the judge
                    in a respectful and <i>politic </i>manner.</p>
  <p>
    <i>Synonym: tactful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 377,'377',1,NULL,'<div class="prodid-question">
  <p>
    <b>Polyglot</b>
  </p>
  <p>noun</p>
  <p>(<u>pah</u> lee glaht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a speaker of many languages</b>
  </p>
  <p>Ling’s extensive travels have helped her to become a true
                    <i>polyglot</i>.</p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 378,'378',1,NULL,'<div class="prodid-question">
  <p>
    <b>Posit</b>
  </p>
  <p>verb</p>
  <p>(<u>pah</u> siht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to assume as real or conceded; to propose as an explanation</b>
  </p>
  <p>Before proving the math formula, we needed to <i>posit</i> that <i>x</i> and
                        <i>y</i> were real numbers.</p>
  <p>
    <i>Synonym: suggest</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 379,'379',1,NULL,'<div class="prodid-question">
  <p>
    <b>Potentate</b>
  </p>
  <p>noun</p>
  <p>(<u>poh</u> tehn tayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a monarch or ruler with great power</b>
  </p>
  <p>Alex was much kinder before he assumed the role of <i>potentate</i>.</p>
  <p />
  <p>
    <i>Synonyms: dominator; leader</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 380,'380',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pragmatic</b>
  </p>
  <p>adj</p>
  <p>(praag <u>maa</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>practical, as opposed to idealistic</b>
  </p>
  <p>While idealistic gamblers think they can get rich by frequenting casinos,
                        <i>pragmatic</i> gamblers realize that the odds are heavily stacked against
                    them.</p>
  <p />
  <p>
    <i>Synonyms: rational; realistic</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 381,'381',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prattle</b>
  </p>
  <p>noun</p>
  <p>(<u>praa</u> tuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>meaningless, foolish talk</b>
  </p>
  <p>Her husband’s mindless <i>prattle</i> drove Heidi insane; sometimes she
                    wished he would just shut up.</p>
  <p />
  <p>
    <i>Synonyms: babble; blather; chatter; drivel; gibberish</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 382,'382',1,NULL,'<div class="prodid-question">
  <p>
    <b>Precipitate</b>
  </p>
  <p>adj</p>
  <p>(preh <u>sih</u> puh tayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>thrown violently or brought about abruptly; lacking deliberation</b>
  </p>
  <p>Theirs was a <i>precipitate</i> marriage—they had only known each other
                    for two weeks before they wed.</p>
  <p />
  <p>
    <i>Synonyms: abrupt; hasty; headlong; hurried; ill-considered; impetuous;
                        impulsive; prompt; rash; reckless; sudden</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 383,'383',1,NULL,'<div class="prodid-question">
  <p>
    <b>Précis</b>
  </p>
  <p>noun</p>
  <p>(<u>pray</u> see) (pray <u>see</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>short summary of facts</b>
  </p>
  <p>Farah wrote a <i>précis</i> of her thesis on the epic poem to share with
                    the class.</p>
  <p>
    <i>Synonym: summary</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 384,'384',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prescient</b>
  </p>
  <p>adj</p>
  <p>(<u>preh</u> shuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>having foresight</b>
  </p>
  <p>Jonah’s decision to sell the apartment seemed to be a <i>prescient</i>
                    one, as its value soon dropped by half.</p>
  <p />
  <p>
    <i>Synonyms: augural; divinatory; mantic; oracular; premonitory</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 385,'385',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prevaricate</b>
  </p>
  <p>verb</p>
  <p>(prih <u>vaar</u> uh cayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to lie or deviate from the truth</b>
  </p>
  <p>Rather than admit that he had overslept again, the employee <i>prevaricated</i>
                    and claimed that heavy traffic had prevented him from arriving at work on
                    time.</p>
  <p />
  <p>
    <i>Synonyms: equivocate; lie; perjure</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 386,'386',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pristine</b>
  </p>
  <p>adj</p>
  <p>(prih <u>steen</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>fresh and clean; uncorrupted</b>
  </p>
  <p>Since concerted measures had been taken to prevent looting, the archeological
                    site was still <i>pristine</i> when researchers arrived.</p>
  <p />
  <p>
    <i>Synonyms: innocent; undamaged</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 387,'387',1,NULL,'<div class="prodid-question">
  <p>
    <b>Probity</b>
  </p>
  <p>noun</p>
  <p>(<u>proh</u> bih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>complete honesty and integrity</b>
  </p>
  <p>George Washington’s reputation for <i>probity</i> is illustrated in the
                    legend about his inability to lie after he chopped down the cherry tree.</p>
  <p />
  <p>
    <i>Synonyms: integrity; morality; rectitude; uprightness; virtue</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 388,'388',1,NULL,'<div class="prodid-question">
  <p>
    <b>Proclivity</b>
  </p>
  <p>noun</p>
  <p>(proh <u>clih</u> vuh tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a natural inclination or predisposition</b>
  </p>
  <p>Her childhood love of acting, singing, and adoration indicated a
                        <i>proclivity</i> for the theater in later life.</p>
  <p />
  <p>
    <i>Synonyms: bias; leaning; partiality; penchant; predilection; predisposition;
                        prejudice; propensity</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 389,'389',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prodigal</b>
  </p>
  <p>adj</p>
  <p>(<u>prah</u> dih guhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lavish; wasteful</b>
  </p>
  <p>The <i>prodigal</i> son quickly wasted all of his inheritance on a lavish
                    lifestyle devoted to pleasure.</p>
  <p />
  <p>
    <i>Synonyms: extravagant; lavish; profligate; spendthrift; wasteful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 390,'390',1,NULL,'<div class="prodid-question">
  <p>
    <b>Profligate</b>
  </p>
  <p>adj</p>
  <p>(<u>prah</u> flih guht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>corrupt; degenerate</b>
  </p>
  <p>Some historians claim that it was the Romans’ decadent, <i>profligate</i>
                    behavior that led to the decline of the Roman Empire.</p>
  <p />
  <p>
    <i>Synonyms: dissolute; extravagant; improvident; prodigal; wasteful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 391,'391',1,NULL,'<div class="prodid-question">
  <p>
    <b>Proliferate</b>
  </p>
  <p>verb</p>
  <p>(proh <u>lih</u> fuhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to increase in number quickly</b>
  </p>
  <p>Although he only kept two guinea pigs initially, they <i>proliferated</i> to such
                    an extent that he soon had dozens.</p>
  <p />
  <p>
    <i>Synonyms: breed; multiply; procreate; propagate; reproduce; spawn</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 392,'392',1,NULL,'<div class="prodid-question">
  <p>
    <b>Propitiate</b>
  </p>
  <p>verb</p>
  <p>(proh <u>pih</u> shee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to conciliate; to appease</b>
  </p>
  <p>Because their gods were angry and vengeful, the Vikings <i>propitiated</i> them with many sacrifices.</p>
  <p>
    <i>Synonyms: appease; conciliate; mollify; pacify; placate</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 393,'393',1,NULL,'<div class="prodid-question">
  <p>
    <b>Propriety</b>
  </p>
  <p>noun</p>
  <p>(pruh <u>prie</u> uh tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the quality of behaving in a proper manner; obeying rules and customs</b>
  </p>
  <p>The aristocracy maintained a high level of <i>propriety</i>, adhering to even the
                    most minor social rules.</p>
  <p />
  <p>
    <i>Synonyms: appropriateness; decency; decorum; modesty</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 394,'394',1,NULL,'<div class="prodid-question">
  <p>
    <b>Prudence</b>
  </p>
  <p>noun</p>
  <p>(<u>proo</u> dehns)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>wisdom, caution, or restraint</b>
  </p>
  <p>The college student exhibited <i>prudence </i>by obtaining practical experience
                    along with her studies, which greatly strengthened her résumé.</p>
  <p />
  <p>
    <i>Synonyms: astuteness; circumspection; discretion; frugality; judiciousness;
                        providence; thrift</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 395,'395',1,NULL,'<div class="prodid-question">
  <p>
    <b>Puerile</b>
  </p>
  <p>adj</p>
  <p>(<u>pyoo</u> ruhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>childish, immature, or silly</b>
  </p>
  <p>Olivia’s boyfriend’s <i>puerile</i> antics are really annoying;
                    sometimes he acts like a five-year-old!</p>
  <p />
  <p>
    <i>Synonyms: infantile; jejune; juvenile</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 396,'396',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pugilism</b>
  </p>
  <p>noun</p>
  <p>(<u>pyoo</u> juhl ih suhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>boxing</b>
  </p>
  <p>
    <i>Pugilism</i> has been defended as a positive outlet for aggressive
                    impulses.</p>
  <p />
  <p>
    <i>Synonyms: fighting; sparring</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 397,'397',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pulchritude</b>
  </p>
  <p>noun</p>
  <p>(<u>puhl</u> kruh tood)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>beauty</b>
  </p>
  <p>The mortals gazed in admiration at Venus, stunned by her incredible
                        <i>pulchritude</i>.</p>
  <p />
  <p>
    <i>Synonyms: comeliness; gorgeousness; handsomeness; loveliness; prettiness</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 398,'398',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pungent</b>
  </p>
  <p>adj</p>
  <p>(<u>puhn</u> juhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sharp and irritating to the senses</b>
  </p>
  <p>The smoke from the burning tires was extremely <i>pungent</i>.</p>
  <p />
  <p>
    <i>Synonyms: acrid; caustic; piquant; poignant; stinging</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 399,'399',1,NULL,'<div class="prodid-question">
  <p>
    <b>Pusillanimous</b>
  </p>
  <p>adj</p>
  <p>(pyoo suh <u>laa</u> nih muhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>cowardly; without courage</b>
  </p>
  <p>The <i>pusillanimous</i> man would not enter the yard where the miniature poodle
                    was barking.</p>
  <p />
  <p>
    <i>Synonyms: cowardly; timid</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 400,'400',1,NULL,'<div class="prodid-question">
  <p>
    <b>Querulous</b>
  </p>
  <p>adj</p>
  <p>(<u>kwehr</u> yoo luhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>inclined to complain; irritable</b>
  </p>
  <p>Curtis’s complaint letter received prompt attention after the company
                    labeled him a <i>querulous</i> potential troublemaker.</p>
  <p />
  <p>
    <i>Synonyms: peevish; puling; sniveling; whiny</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 401,'401',1,NULL,'<div class="prodid-question">
  <p>
    <b>Quiescent</b>
  </p>
  <p>adj</p>
  <p>(kwie <u>eh</u> sihnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>motionless</b>
  </p>
  <p>Many animals are <i>quiescent</i> over the winter months, minimizing activity in
                    order to conserve energy.</p>
  <p />
  <p>
    <i>Synonyms: dormant; latent</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 402,'402',1,NULL,'<div class="prodid-question">
  <p>
    <b>Quixotic</b>
  </p>
  <p>adj</p>
  <p>(kwihk <u>sah</u> tihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>overly idealistic; impractical</b>
  </p>
  <p>The practical Danuta was skeptical of her roommate’s <i>quixotic</i> plans
                    to build a roller coaster in their yard.</p>
  <p />
  <p>
    <i>Synonyms: capricious; impulsive; romantic; unrealistic</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 403,'403',1,NULL,'<div class="prodid-question">
  <p>
    <b>Quotidian</b>
  </p>
  <p>adj</p>
  <p>(kwo <u>tih</u> dee uhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>occurring daily; commonplace</b>
  </p>
  <p>The sight of people singing on the street is so <i>quotidian</i> in New York that
                    passersby rarely react to it.</p>
  <p />
  <p>
    <i>Synonyms: everyday; normal; usual</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 404,'404',1,NULL,'<div class="prodid-question">
  <p>
    <b>Raconteur</b>
  </p>
  <p>noun</p>
  <p>(raa cahn <u>tuhr</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a witty, skillful storyteller</b>
  </p>
  <p>The <i>raconteur</i> kept all the passengers entertained with his stories during
                    the six-hour flight.</p>
  <p />
  <p>
    <i>Synonyms: anecdotalist; monologist</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 405,'405',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rarefy</b>
  </p>
  <p>verb</p>
  <p>(<u>rayr</u> uh fie)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to become thinner or sparser</b>
  </p>
  <p>Since the atmosphere <i>rarefies</i> as altitudes increase, the air at the top of
                    very tall mountains is too thin to breathe.</p>
  <p />
  <p>
    <i>Synonyms: attenuate; thin</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 406,'406',1,NULL,'<div class="prodid-question">
  <p>
    <b>Redress</b>
  </p>
  <p>noun</p>
  <p>(<u>rih</u> drehs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>relief from wrong or injury</b>
  </p>
  <p>Seeking <i>redress </i>for the injuries she had received in the accident, Doreen
                    sued the driver of the truck that had hit her.</p>
  <p />
  <p>
    <i>Synonyms: amends; indemnity; quittance; reparation; restitution</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 407,'407',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rejoinder</b>
  </p>
  <p>noun</p>
  <p>(rih <u>joyn</u> duhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>response</b>
  </p>
  <p>Patrick tried desperately to think of a clever <i>rejoinder</i> to
                    Marianna’s joke, but he couldn’t.</p>
  <p />
  <p>
    <i>Synonyms: retort; riposte</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 408,'408',1,NULL,'<div class="prodid-question">
  <p>
    <b>Repast</b>
  </p>
  <p>noun</p>
  <p>(<u>rih</u> paast)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>meal or mealtime</b>
  </p>
  <p>Ravi prepared a delicious <i>repast</i> of chicken tikka and naan.</p>
  <p />
  <p>
    <i>Synonyms: banquet; feast</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 409,'409',1,NULL,'<div class="prodid-question">
  <p>
    <b>Replete</b>
  </p>
  <p>adj</p>
  <p>(rih <u>pleet</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>abundantly supplied; complete</b>
  </p>
  <p>The gigantic supermarket was <i>replete</i> with consumer products of every
                    kind.</p>
  <p />
  <p>
    <i>Synonyms: abounding; full</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 410,'410',1,NULL,'<div class="prodid-question">
  <p>
    <b>Repose</b>
  </p>
  <p>noun</p>
  <p>(rih <u>pohz</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>relaxation; leisure</b>
  </p>
  <p>After working hard every day in the busy city, Mike finds his <i>repose</i> on
                    weekends playing golf with friends.</p>
  <p />
  <p>
    <i>Synonyms: calmness; tranquility</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 411,'411',1,NULL,'<div class="prodid-question">
  <p>
    <b>Repudiate</b>
  </p>
  <p>verb</p>
  <p>(ree <u>pyoo</u> dee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to reject the validity of</b>
  </p>
  <p>The old woman’s claim that she was Russian royalty was <i>repudiated</i>
                    by other known relatives.</p>
  <p />
  <p>
    <i>Synonyms: deny; disavow; disclaim; disown; renounce</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 412,'412',1,NULL,'<div class="prodid-question">
  <p>
    <b>Requite</b>
  </p>
  <p>verb</p>
  <p>(rih <u>kwiet</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to return or repay</b>
  </p>
  <p>Thanks for offering to lend me $1,000, but I know I’ll never be able to
                        <i>requite</i> your generosity.</p>
  <p />
  <p>
    <i>Synonyms: compensate; reciprocate</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 413,'413',1,NULL,'<div class="prodid-question">
  <p>
    <b>Restive</b>
  </p>
  <p>adj</p>
  <p>(<u>reh</u> stihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>impatient, uneasy, or restless</b>
  </p>
  <p>The passengers became <i>restive</i> after having to wait in line for hours and
                    began to shout complaints at the airline staff.</p>
  <p />
  <p>
    <i>Synonyms: agitated; anxious; fretful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 414,'414',1,NULL,'<div class="prodid-question">
  <p>
    <b>Reticent</b>
  </p>
  <p>adj</p>
  <p>(<u>reh</u> tih suhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>silent; reserved</b>
  </p>
  <p>Physically small and verbally <i>reticent</i>, Joan Didion often went unnoticed by those she was reporting upon.</p>
  <p />
  <p>
    <i>Synonyms: cool; introverted; laconic; standoffish; taciturn;
                        undemonstrative</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 415,'415',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rhetoric</b>
  </p>
  <p>noun</p>
  <p>(<u>reh</u> tuhr ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>effective writing or speaking</b>
  </p>
  <p>Lincoln’s talent for <i>rhetoric</i> was evident in his beautifully
                    expressed Gettysburg Address.</p>
  <p />
  <p>
    <i>Synonyms: eloquence; oratory</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 416,'416',1,NULL,'<div class="prodid-question">
  <p>
    <b>Ribald</b>
  </p>
  <p>adj</p>
  <p>(<u>rih</u> buhld)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>humorous in a vulgar way</b>
  </p>
  <p>The court jester’s <i>ribald </i>brand of humor delighted the rather
                    uncouth king.</p>
  <p />
  <p>
    <i>Synonyms: coarse; gross; indelicate; lewd; obscene</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 417,'417',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rococo</b>
  </p>
  <p>adj</p>
  <p>(ruh koh <u>koh</u>) (roh <u>kuh</u> koh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>very highly ornamented; relating to an 18th century artistic style of
                        elaborate ornamentation</b>
  </p>
  <p>The ornate furniture in the house reminded Tatiana of the <i>rococo</i>
                    style.</p>
  <p />
  <p>
    <i>Synonyms: intricate; ornate</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 418,'418',1,NULL,'<div class="prodid-question">
  <p>
    <b>Rustic</b>
  </p>
  <p>adj</p>
  <p>(<u>ruh</u> stihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>rural</b>
  </p>
  <p>The <i>rustic</i> cabin was an ideal setting for a vacation in the country.</p>
  <p />
  <p>
    <i>Synonyms: bucolic; pastoral</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 419,'419',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sacrosanct</b>
  </p>
  <p>adj</p>
  <p>(<u>saa</u> kroh saankt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extremely sacred; beyond criticism</b>
  </p>
  <p>Many people considered Mother Teresa to be <i>sacrosanct</i> and would not
                    tolerate any criticism of her.</p>
  <p />
  <p>
    <i>Synonyms: holy; inviolable; off-limits</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 420,'420',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sagacious</b>
  </p>
  <p>adj</p>
  <p>(suh <u>gay</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>shrewd; wise</b>
  </p>
  <p>Owls have a reputation for being <i>sagacious</i>, perhaps because of their big
                    eyes, which resemble glasses.</p>
  <p />
  <p>
    <i>Synonyms: astute; judicious; perspicacious; sage; wise</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 421,'421',1,NULL,'<div class="prodid-question">
  <p>
    <b>Salient</b>
  </p>
  <p>adj</p>
  <p>(<u>say</u> lee uhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>prominent; of notable significance</b>
  </p>
  <p>His most <i>salient</i> characteristic is his tendency to dominate every
                    conversation.</p>
  <p />
  <p>
    <i>Synonyms: marked; noticeable; outstanding</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 422,'422',1,NULL,'<div class="prodid-question">
  <p>
    <b>Salubrious</b>
  </p>
  <p>adj</p>
  <p>(suh <u>loo</u> bree uhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>healthful</b>
  </p>
  <p>Run-down and sickly, Rita hoped that the fresh mountain air would have a
                        <i>salubrious</i> effect on her health.</p>
  <p />
  <p>
    <i>Synonyms: bracing; curative; medicinal; therapeutic; tonic</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 423,'423',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sanguine</b>
  </p>
  <p>adj</p>
  <p>(<u>saan</u> gwuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>ruddy; cheerfully optimistic</b>
  </p>
  <p>A <i>sanguine</i> person thinks the glass is half full, whereas a depressed
                    person thinks it’s half empty.</p>
  <p />
  <p>
    <i>Synonyms: confident; hopeful; positive; rosy; rubicund</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 424,'424',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sardonic</b>
  </p>
  <p>adj</p>
  <p>(sahr <u>dah</u> nihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>cynical; scornfully mocking</b>
  </p>
  <p>Isabella was offended by the <i>sardonic</i> way in which her date made fun of
                    her ideas and opinions.</p>
  <p />
  <p>
    <i>Synonyms: acerbic; caustic; sarcastic; satirical; snide</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 425,'425',1,NULL,'<div class="prodid-question">
  <p>
    <b>Satiate</b>
  </p>
  <p>verb</p>
  <p>(<u>say</u> shee ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to satisfy fully or overindulge</b>
  </p>
  <p>His desire for power was so great that nothing less than complete control of the
                    country could <i>satiate</i> it.</p>
  <p />
  <p>
    <i>Synonyms: cloy; glut; gorge; surfeit</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 426,'426',1,NULL,'<div class="prodid-question">
  <p>
    <b>Scintilla</b>
  </p>
  <p>noun</p>
  <p>(sihn <u>tihl</u> uh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>trace amount</b>
  </p>
  <p>This poison is so powerful that no more than a <i>scintilla </i>of it is needed
                    to kill a horse.</p>
  <p />
  <p>
    <i>Synonyms: atom; iota; mote; spark; speck</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 427,'427',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sedition</b>
  </p>
  <p>noun</p>
  <p>(sih <u>dih</u> shuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>behavior that promotes rebellion or civil disorder against the state</b>
  </p>
  <p>Li was arrested for <i>sedition</i> after he gave a fiery speech in the main
                    square.</p>
  <p />
  <p>
    <i>Synonyms: conspiracy; insurrection</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 428,'428',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sentient</b>
  </p>
  <p>adj</p>
  <p>(<u>sehn</u> shuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>aware; conscious; able to perceive</b>
  </p>
  <p>The anesthetic didn’t work, and I was still <i>sentient</i> when the
                    dentist started drilling!</p>
  <p />
  <p>
    <i>Synonyms: feeling; intelligent; thinking</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 429,'429',1,NULL,'<div class="prodid-question">
  <p>
    <b>Seraphic</b>
  </p>
  <p>adj</p>
  <p>(seh <u>rah</u> fihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>angelic; sweet</b>
  </p>
  <p>Selena’s <i>seraphic</i> appearance belied her nasty, bitter
                    personality.</p>
  <p />
  <p>
    <i>Synonyms: cherubic; heavenly</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 430,'430',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sinecure</b>
  </p>
  <p>noun</p>
  <p>(<u>sien</u> ih kyoor)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a well-paying job or office that requires little or no work</b>
  </p>
  <p>The corrupt mayor made sure to set up all his relatives in <i>sinecures</i>
                    within the administration.</p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 431,'431',1,NULL,'<div class="prodid-question">
  <p>
    <b>Slake</b>
  </p>
  <p>verb</p>
  <p>(slayk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to calm down or moderate</b>
  </p>
  <p>In order to <i>slake</i> his curiosity, Bryan finally took a tour backstage at the theater.</p>
  <p>
    <i>Synonyms: moderate; quench; satisfy</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 432,'432',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sobriquet</b>
  </p>
  <p>noun</p>
  <p>(<u>soh</u> brih keht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>nickname</b>
  </p>
  <p>One of Ronald Reagan’s <i>sobriquets</i> was “The
                    Gipper.”</p>
  <p />
  <p>
    <i>Synonyms: alias; pseudonym</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 433,'433',1,NULL,'<div class="prodid-question">
  <p>
    <b>Solecism</b>
  </p>
  <p>noun</p>
  <p>(<u>sah</u> lih sihz uhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>grammatical mistake</b>
  </p>
  <p>“I ain’t going with you,” she said, obviously unaware of her
                        <i>solecism</i>.</p>
  <p />
  <p>
    <i>Synonyms: blooper; faux pas; vulgarism</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 434,'434',1,NULL,'<div class="prodid-question">
  <p>
    <b>Soporific</b>
  </p>
  <p>adj</p>
  <p>(sahp uhr <u>ihf</u> ihk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>causing sleep or lethargy</b>
  </p>
  <p>The movie proved to be so <i>soporific</i> that soon loud snores were heard throughout the cinema.</p>
  <p>
    <i>Synonyms: hypnotic; narcotic; slumberous; somnolent</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 435,'435',1,NULL,'<div class="prodid-question">
  <p>
    <b>Spartan</b>
  </p>
  <p>adj</p>
  <p>(<u>spahr</u> tihn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>highly self-disciplined; frugal; austere</b>
  </p>
  <p>When he was in training, the athlete preferred to live in a <i>spartan</i> room,
                    so he could shut out all distractions.</p>
  <p />
  <p>
    <i>Synonyms: restrained; simple</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 436,'436',1,NULL,'<div class="prodid-question">
  <p>
    <b>Specious</b>
  </p>
  <p>adj</p>
  <p>(<u>spee</u> shuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>deceptively attractive; seemingly plausible but fallacious</b>
  </p>
  <p>The student’s <i>specious</i> excuse for being late sounded legitimate,
                    but was proved otherwise when his teacher called his home.</p>
  <p />
  <p>
    <i>Synonyms: illusory; ostensible; plausible; sophistic; spurious</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 437,'437',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sportive</b>
  </p>
  <p>adj</p>
  <p>(<u>spohr</u> tihv)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>frolicsome; playful</b>
  </p>
  <p>The lakeside vacation meant more <i>sportive</i> opportunities for the kids than
                    the wine tour through France.</p>
  <p />
  <p>
    <i>Synonyms: frisky; merry</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 438,'438',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stasis</b>
  </p>
  <p>noun</p>
  <p>(<u>stay</u> sihs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a state of static balance or equilibrium; stagnation</b>
  </p>
  <p>The rusty, ivy-covered World War II tank had obviously been in <i>stasis</i> for
                    years.</p>
  <p />
  <p>
    <i>Synonyms: inertia; standstill</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 439,'439',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stentorian</b>
  </p>
  <p>adj</p>
  <p>(stehn <u>tohr</u> ee yehn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extremely loud</b>
  </p>
  <p>Cullen couldn’t hear her speaking over the <i>stentorian</i> din of the
                    game on TV.</p>
  <p />
  <p>
    <i>Synonyms: clamorous; noisy</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 440,'440',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stigma</b>
  </p>
  <p>noun</p>
  <p>(<u>stihg</u> mah)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a mark of shame or discredit</b>
  </p>
  <p>In <i>The Scarlet Letter,</i> Hester Prynne was required to wear the letter A on
                    her clothes as a public <i>stigma</i> for her adultery.</p>
  <p />
  <p>
    <i>Synonyms: blemish; blot; opprobrium; stain; taint</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 441,'441',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stolid</b>
  </p>
  <p>adj</p>
  <p>(<u>stah</u> lihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>unemotional; lacking sensitivity</b>
  </p>
  <p>The prisoner appeared <i>stolid</i> and unaffected by the judge’s harsh
                    sentence.</p>
  <p />
  <p>
    <i>Synonyms: apathetic; impassive; indifferent; phlegmatic; stoic;
                        unconcerned</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 442,'442',1,NULL,'<div class="prodid-question">
  <p>
    <b>Stratagem</b>
  </p>
  <p>noun</p>
  <p>(<u>straa</u> tuh juhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>trick designed to deceive an enemy</b>
  </p>
  <p>The Trojan Horse must be one of the most successful military <i>stratagems</i>
                    used in all of history.</p>
  <p />
  <p>
    <i>Synonyms: artifice; feint; maneuver; ruse; wile</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 443,'443',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sublime</b>
  </p>
  <p>adj</p>
  <p>(suh <u>bliem</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lofty or grand</b>
  </p>
  <p>The music was so <i>sublime</i> that it transformed the rude surroundings into a
                    special place.</p>
  <p />
  <p>
    <i>Synonyms: august; exalted; glorious; grand; magnificent; majestic; noble;
                        resplendent; superb</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 444,'444',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sully</b>
  </p>
  <p>verb</p>
  <p>(<u>suh</u> lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to tarnish; to taint</b>
  </p>
  <p>With the help of a public relations firm, he was able to restore his <i>sullied</i> reputation.</p>
  <p>
    <i>Synonyms: besmirch; defile</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 445,'445',1,NULL,'<div class="prodid-question">
  <p>
    <b>Supplant</b>
  </p>
  <p>verb</p>
  <p>(suh <u>plaant</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to replace (another) by force; to take the place of</b>
  </p>
  <p>The overthrow of the government meant a new leader to <i>supplant</i> the
                    tyrannical former one.</p>
  <p />
  <p>
    <i>Synonyms: displace; supersede</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 446,'446',1,NULL,'<div class="prodid-question">
  <p>
    <b>Surfeit</b>
  </p>
  <p>noun</p>
  <p>(<u>suhr</u> fiht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>excessive amount</b>
  </p>
  <p>Because of the <i>surfeit</i> of pigs, pork prices have never been lower.</p>
  <p />
  <p>
    <i>Synonyms: glut; plethora; repletion; superfluity; surplus</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 447,'447',1,NULL,'<div class="prodid-question">
  <p>
    <b>Surly</b>
  </p>
  <p>adj</p>
  <p>(<u>suhr</u> lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>rude and bad-tempered</b>
  </p>
  <p>When asked to clean the windshield, the <i>surly</i> gas station attendant tossed
                    a dirty rag at the customer and walked away.</p>
  <p />
  <p>
    <i>Synonyms: gruff; grumpy; testy</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 448,'448',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sybarite</b>
  </p>
  <p>noun</p>
  <p>(<u>sih</u> buh riet)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a person devoted to pleasure and luxury</b>
  </p>
  <p>A confirmed <i>sybarite</i>, the nobleman fainted at the thought of having to
                    leave his palace and live in a small cottage.</p>
  <p />
  <p>
    <i>Synonyms: hedonist; sensualist</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 449,'449',1,NULL,'<div class="prodid-question">
  <p>
    <b>Sycophant</b>
  </p>
  <p>noun</p>
  <p>(<u>sih</u> kuh fuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a self-serving flatterer; a yes-man</b>
  </p>
  <p>Dreading criticism, the actor surrounded himself with admirers and
                        <i>sycophants</i>.</p>
  <p />
  <p>
    <i>Synonyms: bootlicker; fawner; lickspittle; toady</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 450,'450',1,NULL,'<div class="prodid-question">
  <p>
    <b>Symbiosis</b>
  </p>
  <p>noun</p>
  <p>(sihm bee <u>oh</u> sihs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>cooperation; mutual helpfulness</b>
  </p>
  <p>The rhino and the tick-eating bird live in <i>symbiosis</i>; the rhino gives the
                    bird food in the form of ticks, and the bird rids the rhino of parasites.</p>
  <p />
  <p>
    <i>Synonyms: association; interdependence</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 451,'451',1,NULL,'<div class="prodid-question">
  <p>
    <b>Syncopation</b>
  </p>
  <p>noun</p>
  <p>(sihn cuh <u>pay</u> shun)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>temporary irregularity in musical rhythm</b>
  </p>
  <p>A jazz enthusiast will appreciate the use of <i>syncopation</i> in that musical
                    genre.</p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 452,'452',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tacit</b>
  </p>
  <p>adj</p>
  <p>(<u>taa</u> siht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>done without using words</b>
  </p>
  <p>Although not a word was said, everyone in the room knew that a <i>tacit</i>
                    agreement had been made about what course of action to take.</p>
  <p />
  <p>
    <i>Synonyms: implicit; implied; undeclared; unsaid; unuttered</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 453,'453',1,NULL,'<div class="prodid-question">
  <p>
    <b>Taciturn</b>
  </p>
  <p>adj</p>
  <p>(<u>taa</u> sih tuhrn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>silent; not talkative</b>
  </p>
  <p>The clerk’s <i>taciturn</i> nature earned him the nickname Silent Bob.</p>
  <p />
  <p>
    <i>Synonyms: laconic; reticent</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 454,'454',1,NULL,'<div class="prodid-question">
  <p>
    <b>Talon</b>
  </p>
  <p>noun</p>
  <p>(<u>taa</u> luhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>claw of an animal, especially a bird of prey</b>
  </p>
  <p>A vulture holds its prey in its <i>talons</i> while it dismembers it with its
                    beak.</p>
  <p />
  <p>
    <i>Synonyms: claw; nail</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 455,'455',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tangential</b>
  </p>
  <p>adj</p>
  <p>(taan <u>jehn</u> shuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>digressing; diverting</b>
  </p>
  <p>Your argument is interesting, but it’s <i>tangential</i> to the matter at
                    hand, so I suggest we get back to the point.</p>
  <p />
  <p>
    <i>Synonyms: digressive; extraneous; inconsequential; irrelevant; peripheral</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 456,'456',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tawdry</b>
  </p>
  <p>adj</p>
  <p>(<u>taw</u> dree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>gaudy, cheap, or showy</b>
  </p>
  <p>The performer changed into her <i>tawdry</i>, spangled costume and stepped out
                    onto the stage to do her show.</p>
  <p />
  <p>
    <i>Synonyms: flashy; loud; meretricious</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 457,'457',1,NULL,'<div class="prodid-question">
  <p>
    <b>Terrestrial</b>
  </p>
  <p>adj</p>
  <p>(tuh <u>reh</u> stree uhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>earthly; down-to-earth; commonplace</b>
  </p>
  <p>Many “extraterrestrial” objects turn out to be <i>terrestrial</i>
                    in origin, as when flying saucers turn out to be normal airplanes.</p>
  <p />
  <p>
    <i>Synonyms: earthbound; mundane; sublunary; tellurian; terrene</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 458,'458',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tirade</b>
  </p>
  <p>noun</p>
  <p>(<u>tie</u> rayd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a long, harsh speech or verbal attack</b>
  </p>
  <p>Observers were shocked at the manager’s <i>tirade</i> over such a minor
                    mistake.</p>
  <p />
  <p>
    <i>Synonyms: diatribe; fulmination; harangue; obloquy; revilement;
                        vilification</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 459,'459',1,NULL,'<div class="prodid-question">
  <p>
    <b>Toady</b>
  </p>
  <p>noun</p>
  <p>(<u>toh</u> dee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>one who flatters in the hope of gaining favors</b>
  </p>
  <p>The king was surrounded by <i>toadies</i> who rushed to agree with whatever
                    outrageous thing he said.</p>
  <p />
  <p>
    <i>Synonyms: parasite; sycophant</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 460,'460',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tome</b>
  </p>
  <p>noun</p>
  <p>(tohm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a book, usually a large and academic one</b>
  </p>
  <p>The teacher was forced to refer to various <i>tomes</i> to find the answer to the
                    advanced student’s question.</p>
  <p />
  <p>
    <i>Synonyms: codex; volume</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 461,'461',1,NULL,'<div class="prodid-question">
  <p>
    <b>Torpor</b>
  </p>
  <p>noun</p>
  <p>(<u>tohr</u> puhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>extreme mental and physical sluggishness</b>
  </p>
  <p>After surgery, the patient’s <i>torpor</i> lasted several hours until the
                    anesthesia wore off.</p>
  <p />
  <p>
    <i>Synonyms: apathy; languor</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 462,'462',1,NULL,'<div class="prodid-question">
  <p>
    <b>Transitory</b>
  </p>
  <p>adj</p>
  <p>(<u>trahn</u> sih tohr ee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>temporary; lasting a brief time</b>
  </p>
  <p>The reporter’s homes were <i>transitory</i>; she stayed in one place only long enough to cover the current story.</p>
  <p>
    <i>Synonyms: ephemeral; evanescent; fleeting; impermanent; momentary</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 463,'463',1,NULL,'<div class="prodid-question">
  <p>
    <b>Trenchant</b>
  </p>
  <p>adj</p>
  <p>(<u>trehn</u> chuhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>acute, sharp, or incisive; forceful; effective</b>
  </p>
  <p>Tyrone’s <i>trenchant</i> observations in class made him the
                    professor’s favorite student.</p>
  <p />
  <p>
    <i>Synonyms: biting; caustic; cutting; keen</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 464,'464',1,NULL,'<div class="prodid-question">
  <p>
    <b>Turgid</b>
  </p>
  <p>adj</p>
  <p>(<u>tuhr</u> jihd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>swollen as from a fluid; bloated</b>
  </p>
  <p>In the process of osmosis, water passes through the walls of <i>turgid</i> cells,
                    ensuring that they never contain too much water.</p>
  <p>
    <i>Synonym: distended</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 465,'465',1,NULL,'<div class="prodid-question">
  <p>
    <b>Tyro</b>
  </p>
  <p>noun</p>
  <p>(<u>tie</u> roh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>beginner; novice</b>
  </p>
  <p>An obvious <i>tyro</i> at salsa, Millicent received no invitations to dance.</p>
  <p />
  <p>
    <i>Synonyms: apprentice; fledgling; greenhorn; neophyte; tenderfoot</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 466,'466',1,NULL,'<div class="prodid-question">
  <p>
    <b>Umbrage</b>
  </p>
  <p>noun</p>
  <p>(<u>uhm</u> brihj)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>offense; resentment</b>
  </p>
  <p>The businessman took <i>umbrage </i>at the security guard’s accusation
                    that he had shoplifted a packet of gum.</p>
  <p />
  <p>
    <i>Synonyms: asperity; dudgeon; ire; pique; rancor</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 467,'467',1,NULL,'<div class="prodid-question">
  <p>
    <b>Unconscionable</b>
  </p>
  <p>adj</p>
  <p>(uhn <u>kahn</u> shuhn uh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>unscrupulous; shockingly unfair or unjust</b>
  </p>
  <p>After she promised me the project, the fact that she gave it to someone else is
                        <i>unconscionable</i>.</p>
  <p />
  <p>
    <i>Synonyms: dishonorable; indefensible</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 468,'468',1,NULL,'<div class="prodid-question">
  <p>
    <b>Unequivocal</b>
  </p>
  <p>adj</p>
  <p>(uhn ee <u>kwih</u> vih kuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>absolute; certain</b>
  </p>
  <p>The jury’s verdict was <i>unequivocal</i>: the organized crime boss would
                    be locked up for life.</p>
  <p />
  <p>
    <i>Synonyms: categorical; clear; explicit; express; unambiguous</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 469,'469',1,NULL,'<div class="prodid-question">
  <p>
    <b>Upbraid</b>
  </p>
  <p>verb</p>
  <p>(uhp <u>brayd</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to scold sharply</b>
  </p>
  <p>The teacher <i>upbraided</i> the student for scrawling graffiti all over the
                    walls of the school.</p>
  <p />
  <p>
    <i>Synonyms: berate; chide; rebuke; reproach; tax</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 470,'470',1,NULL,'<div class="prodid-question">
  <p>
    <b>Usury</b>
  </p>
  <p>noun</p>
  <p>(<u>yoo</u> zhuh ree)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the practice of lending money at exorbitant rates</b>
  </p>
  <p>The moneylender was convicted of <i>usury</i> when it was discovered that he
                    charged 50 percent interest on all his loans.</p>
  <p>
    <i>Synonym: loan-sharking</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 471,'471',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vacillate</b>
  </p>
  <p>verb</p>
  <p>(<u>vaa</u> sihl ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to physically sway; to be indecisive</b>
  </p>
  <p>The customer held up the line as he <i>vacillated</i> between ordering
                    chocolate-chip or rocky-road ice cream.</p>
  <p />
  <p>
    <i>Synonyms: dither; falter; fluctuate; oscillate; waver</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 472,'472',1,NULL,'<div class="prodid-question">
  <p>
    <b>Variegated</b>
  </p>
  <p>adj</p>
  <p>(<u>vaar</u> ee uh <u>gayt</u> ehd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>varied; marked with different colors</b>
  </p>
  <p>The <i>variegated</i> foliage of the jungle allows it to support thousands of
                    different animal species.</p>
  <p>
    <i>Synonym: diversified</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 473,'473',1,NULL,'<div class="prodid-question">
  <p>
    <b>Venerable</b>
  </p>
  <p>adj</p>
  <p>(<u>veh</u> nehr uh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>respected because of age</b>
  </p>
  <p>All of the villagers sought the <i>venerable</i> old woman’s advice
                    whenever they had a problem.</p>
  <p />
  <p>
    <i>Synonyms: distinguished; elderly; respectable</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 474,'474',1,NULL,'<div class="prodid-question">
  <p>
    <b>Venerate</b>
  </p>
  <p>verb</p>
  <p>(<u>vehn</u> uhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to respect deeply</b>
  </p>
  <p>In a traditional Confucian society, the young <i>venerate</i> their elders,
                    deferring to the elders’ wisdom and experience.</p>
  <p />
  <p>
    <i>Synonyms: adore; honor; idolize; revere</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 475,'475',1,NULL,'<div class="prodid-question">
  <p>
    <b>Veracity</b>
  </p>
  <p>noun</p>
  <p>(vuhr <u>aa</u> sih tee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>filled with truth and accuracy</b>
  </p>
  <p>She had a reputation for <i>veracity</i>, so everyone trusted her description of
                    events.</p>
  <p />
  <p>
    <i>Synonyms: candor; exactitude; fidelity; probity</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 476,'476',1,NULL,'<div class="prodid-question">
  <p>
    <b>Verbose</b>
  </p>
  <p>adj</p>
  <p>(vuhr <u>bohs</u>)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>wordy</b>
  </p>
  <p>The professor’s answer was so <i>verbose</i> that his student forgot what
                    the original question had been.</p>
  <p />
  <p>
    <i>Synonyms: long-winded; loquacious; prolix; superfluous</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 477,'477',1,NULL,'<div class="prodid-question">
  <p>
    <b>Verdant</b>
  </p>
  <p>adj</p>
  <p>(<u>vuhr</u> duhnt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>green with vegetation; inexperienced</b>
  </p>
  <p>He wandered deep into the <i>verdant</i> woods in search of mushrooms and other
                    edible flora.</p>
  <p />
  <p>
    <i>Synonyms: grassy; leafy; wooded</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 478,'478',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vernal</b>
  </p>
  <p>adj</p>
  <p>(<u>vuhr</u> nuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>related to spring; fresh</b>
  </p>
  <p>Bea basked in the balmy <i>vernal</i> breezes, happy that winter was coming to an
                    end.</p>
  <p />
  <p>
    <i>Synonyms: springlike; youthful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 479,'479',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vestige</b>
  </p>
  <p>noun</p>
  <p>(<u>veh</u> stihj)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a trace; a remnant</b>
  </p>
  <p>
    <i>Vestiges </i>of the former tenant still remained in the apartment, though he
                    hadn’t lived there for years.</p>
  <p />
  <p>
    <i>Synonyms: relic; remains; sign</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 480,'480',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vex</b>
  </p>
  <p>verb</p>
  <p>(vehks)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to annoy, irritate, puzzle, or confuse</b>
  </p>
  <p>The old man who loved his peace and quiet was <i>vexed</i> by his
                    neighbor’s loud music.</p>
  <p />
  <p>
    <i>Synonyms: annoy; bother; chafe; exasperate; irk; nettle; peeve; provoke</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 481,'481',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vicissitude</b>
  </p>
  <p>noun</p>
  <p>(vih <u>sih</u> sih tood)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a change or variation</b>
  </p>
  <p>Investors must be prepared for <i>vicissitudes </i>of the stock market.</p>
  <p />
  <p>
    <i>Synonyms: inconstancy; mutability</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 482,'482',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vim</b>
  </p>
  <p>noun</p>
  <p>(vihm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>vitality and energy</b>
  </p>
  <p>The <i>vim</i> with which she worked so early in the day explained why she was so
                    productive.</p>
  <p />
  <p>
    <i>Synonyms: force; power</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 483,'483',1,NULL,'<div class="prodid-question">
  <p>
    <b>Viscous</b>
  </p>
  <p>adj</p>
  <p>(<u>vih</u> skuhs)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>thick and adhesive, like a slow-flowing fluid</b>
  </p>
  <p>Most <i>viscous</i> liquids, like oil or honey, become even thicker as they are
                    cooled down.</p>
  <p />
  <p>
    <i>Synonyms: gelatinous; glutinous; thick</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 484,'484',1,NULL,'<div class="prodid-question">
  <p>
    <b>Vituperate</b>
  </p>
  <p>verb</p>
  <p>(vih <u>too</u> puhr ayt)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to abuse verbally; to berate</b>
  </p>
  <p>
    <i>Vituperating </i>someone is never a constructive way to effect change.</p>
  <p />
  <p>
    <i>Synonyms: castigate; reproach; scold</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 485,'485',1,NULL,'<div class="prodid-question">
  <p>
    <b>Volatile</b>
  </p>
  <p>adj</p>
  <p>(<u>vah</u> luh tuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>easily aroused or changeable; lively or explosive</b>
  </p>
  <p>His <i>volatile</i> personality made it difficult to predict his reaction to
                    anything.</p>
  <p />
  <p>
    <i>Synonyms: capricious; erratic; fickle; inconsistent; inconstant; mercurial;
                        temperamental</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 486,'486',1,NULL,'<div class="prodid-question">
  <p>
    <b>Voluble</b>
  </p>
  <p>adj</p>
  <p>(<u>vahl</u> yuh buhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>talkative; speaking easily; glib</b>
  </p>
  <p>The <i>voluble</i> man and his reserved wife proved the old saying that opposites
                    attract.</p>
  <p />
  <p>
    <i>Synonyms: loquacious; verbose</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 487,'487',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wan</b>
  </p>
  <p>adj</p>
  <p>(wahn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>sickly pale</b>
  </p>
  <p>The sick child had a <i>wan</i> face, in contrast to her rosy-cheeked sister.</p>
  <p />
  <p>
    <i>Synonyms: ashen; sickly</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 488,'488',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wanton</b>
  </p>
  <p>adj</p>
  <p>(<u>wahn</u> tuhn)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>undisciplined; unrestrained; reckless</b>
  </p>
  <p>The townspeople were outraged by the <i>wanton</i> display of disrespect when
                    they discovered the statue of the town founder covered in graffiti.</p>
  <p />
  <p>
    <i>Synonyms: capricious; lewd; licentious</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 489,'489',1,NULL,'<div class="prodid-question">
  <p>
    <b>Waver</b>
  </p>
  <p>verb</p>
  <p>(<u>way</u> vuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to fluctuate between choices</b>
  </p>
  <p>If you <i>waver</i> too long before making a decision about which testing site to
                    register for, you may not get your first choice.</p>
  <p />
  <p>
    <i>Synonyms: dither; falter; fluctuate; oscillate; vacillate</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 490,'490',1,NULL,'<div class="prodid-question">
  <p>
    <b>Whimsical</b>
  </p>
  <p>adj</p>
  <p>(<u>wihm</u> sih cuhl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>lightly acting in a fanciful or capricious manner; unpredictable</b>
  </p>
  <p>The ballet was <i>whimsical</i>, delighting the children with its imaginative
                    characters and unpredictable sets.</p>
  <p />
  <p>
    <i>Synonyms: capricious; erratic; flippant; frivolous</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 491,'491',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wily</b>
  </p>
  <p>adj</p>
  <p>(<u>wie</u> lee)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>clever; deceptive</b>
  </p>
  <p>Yet again, the <i>wily</i> coyote managed to elude the ranchers who wanted to
                    capture it.</p>
  <p />
  <p>
    <i>Synonyms: crafty; cunning; tricky</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 492,'492',1,NULL,'<div class="prodid-question">
  <p>
    <b>Winsome</b>
  </p>
  <p>adj</p>
  <p>(<u>wihn</u> suhm)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>charming; happily engaging</b>
  </p>
  <p>Lenore gave the doorman a <i>winsome</i> smile, and he let her pass to the front of the line.</p>
  <p />
  <p>
    <i>Synonyms: attractive; delightful</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 493,'493',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wizened</b>
  </p>
  <p>adj</p>
  <p>(<u>wih</u> zuhnd)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>shriveled; withered; wrinkled</b>
  </p>
  <p>The <i>wizened </i>old man was told that the plastic surgery necessary to make
                    him look young again would cost more money than he could imagine.</p>
  <p />
  <p>
    <i>Synonyms: atrophied; desiccated; gnarled; wasted</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 494,'494',1,NULL,'<div class="prodid-question">
  <p>
    <b>Wraith</b>
  </p>
  <p>noun</p>
  <p>(rayth)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a ghost or specter; a ghost of a living person seen just before his or her
                        death</b>
  </p>
  <p>Gideon thought he saw a <i>wraith</i> late one night as he sat vigil outside his
                    great uncle’s bedroom door.</p>
  <p />
  <p>
    <i>Synonyms: apparition; bogeyman; phantasm; shade; spirit</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 495,'495',1,NULL,'<div class="prodid-question">
  <p>
    <b>Xenophobia</b>
  </p>
  <p>noun</p>
  <p>(zee nuh <u>foh</u> bee uh)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a fear or hatred of foreigners or strangers</b>
  </p>
  <p>Countries in which <i>xenophobia</i> is prevalent often have more restrictive
                    immigration policies than countries that are more open to foreign
                    influences.</p>
  <p />
  <p>
    <i>Synonyms: bigotry; chauvinism; prejudice</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 496,'496',1,NULL,'<div class="prodid-question">
  <p>
    <b>Yoke</b>
  </p>
  <p>verb</p>
  <p>(yohk)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>to join together</b>
  </p>
  <p>As soon as the farmer had <i>yoked</i> his oxen together, he began to plow the fields.</p>
  <p>
    <i>Synonyms: bind; harness; pair</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 497,'497',1,NULL,'<div class="prodid-question">
  <p>
    <b>Zeal</b>
  </p>
  <p>noun</p>
  <p>(zeehl)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>passion; excitement</b>
  </p>
  <p>She brought her typical <i>zeal</i> to the project, sparking enthusiasm in the
                    other team members.</p>
  <p />
  <p>
    <i>Synonyms: ardency; fervor; fire; passion</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 498,'498',1,NULL,'<div class="prodid-question">
  <p>
    <b>Zealot</b>
  </p>
  <p>noun</p>
  <p>(<u>zeh</u> luht)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>someone passionately devoted to a cause</b>
  </p>
  <p>The religious <i>zealot </i>had no time for those who failed to share his strong
                    beliefs.</p>
  <p />
  <p>
    <i>Synonyms: enthusiast; fanatic; militant; radical</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 499,'499',1,NULL,'<div class="prodid-question">
  <p>
    <b>Zenith</b>
  </p>
  <p>noun</p>
  <p>(<u>zee</u> nihth)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>the point of culmination; peak</b>
  </p>
  <p>The diva considered her appearance at the Metropolitan Opera to be the
                        <i>zenith</i> of her career.</p>
  <p />
  <p>
    <i>Synonyms: acme; pinnacle</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
INSERT INTO flashcards VALUES(1, 500,'500',1,NULL,'<div class="prodid-question">
  <p>
    <b>Zephyr</b>
  </p>
  <p>noun</p>
  <p>(<u>zeh</u> fuhr)</p>
</div>','<div class="prodid-answer">
  <p>
    <b>a gentle breeze; something airy or unsubstantial</b>
  </p>
  <p>The <i>zephyr</i> from the ocean made the intense heat on the beach bearable for
                    the sunbathers.</p>
  <p />
  <p>
    <i>Synonyms: breath; draft</i>
  </p>
</div>','','2014-06-18 10:15:16.714347','2014-06-18 10:15:16.714347',0,'skipped');
