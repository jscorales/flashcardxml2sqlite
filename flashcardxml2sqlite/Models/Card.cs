﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flashcardxml2sqlite.Models
{
    public class Card
    {
        public int Id
        {
            get;
            set;
        }

        public string Uid
        {
            get;
            set;
        }

        public Category Category
        {
            get;
            set;
        }

        public string SubTopicTitle
        {
            get;
            set;
        }

        public string Question
        {
            get;
            set;
        }

        public string Answer
        {
            get;
            set;
        }

        public int Additional
        {
            get;
            set;
        }

        public int Available
        {
            get;
            set;
        }

        public string Status
        {
            get
            {
                return "skipped";
            }
        }
    }

    public static class CardManager
    {
        private static int counter = 1;
        private static readonly Dictionary<string, Card> cards = new Dictionary<string, Card>();

        public static Card Create(string uid, Category category, string subtopicTitle, string question, string answer)
        {
            var card = new Card
            {
                Uid = uid,
                Category = category,
                SubTopicTitle = subtopicTitle,
                Question = question,
                Answer = answer,
                Available = cards.Count >= 50 ? 0 : 1,
                Additional = cards.Count >= 50 ? 1 : 0
            };

            if (!cards.ContainsKey(uid))
            {
                card.Id = counter;
                cards.Add(uid, card);
                counter++;
            }

            return card;
        }

        public static List<Card> Cards
        {
            get
            {
                var list = new List<Card>(cards.Values);

                return list;
            }
        }
    }
}
