﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flashcardxml2sqlite.Models
{
    public class Category
    {
        public int Id
        {
            get;
            set;
        }

        public string Uid
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }
    }

    public static class CategoryManager
    {
        private static int counter = 1;
        private static readonly Dictionary<string, Category> categories = new Dictionary<string, Category>();

        public static Category Create(string uid, string title)
        {
            var category = new Category
            {
                Uid = uid,
                Title = title
            };

            if (!categories.ContainsKey(uid))
            {
                category.Id = counter;
                categories.Add(uid, category);
                counter++;
            }
            else
            {
                category.Id = categories[uid].Id;
            }
            
            return category;
        }

        public static List<Category> Categories
        {
            get
            {
                var list = new List<Category>(categories.Values);

                return list;
            }
        }
    }
}
